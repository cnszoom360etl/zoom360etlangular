export const environment = {
  production: true,
  // assetUrl: 'http://gettimely.jangoodev.com/assets',
  // apiUrl: 'http://gettimely.jangoodev.com/node-timely',
  apiUrl: 'http://192.168.223.111:8081',
  assetUrl: 'http://192.168.223.111:95/assets',
  // apiUrl: 'http://192.168.223.111:8082',
  // assetUrl: 'http://192.168.223.111:96/assets',
  staffRole: 2,
  adminRole: 1,

  storage: {
    // userData: 'live_zoom_user_data'
    userData: 'zoom_user_data',
    ModeId: 'ModeId',
    appMode:'appMode',
    presentMode:'presentMode',
    userId:'userId',
    clientId:'clientid',
    workspaceId:'workspaceId',
    subMenuId:'subMenuId',
    treeNodeid:'treeNodeId',
    accountId:"accountId",
    
  }
  // eventsColors: [
  //   {
  //     textColor: '#fff',
  //     color:'#f00',
  //   },
  //   {
  //     textColor: '#fff',
  //     color:'#2000ff',
  //   },
  //   {
  //     textColor: '#ffff',
  //     color:'#f220e5',
  //   },
  //   {
  //     textColor: '#fff',
  //     color:'#295718',
  //   },
  //   {
  //     textColor: '#fff',
  //     color:'#295718',
  //   }
  // ],

  // appointmentStatus:{
  //   arrived:1,
  //   completed:3
  // }
};
