// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  assetUrl: 'http://localhost:4200/assets',
  apiUrl: 'http://localhost:51070',
  // latest ports
  //  apiUrl: 'http://192.168.223.111:8082',
  //  assetUrl: 'http://192.168.223.111:80/assets',

    // end 
    // assetUrl: 'http://192.168.223.111:9096/assets',
     // assetUrl: 'http://192.168.223.111:9095/assets',
  // apiUrl: 'http://192.168.223.111:81', 
  staffRole: 2,
  adminRole: 1,

  storage: {
    userData: 'zoom_user_data',
    ModeId: 'ModeId',
    appMode:'appMode',
    presentMode:'presentMode',
    mainMenu:'mainMenu',
    myPage:'myPage',
    userId:'userId',
    clientId:'clientid',
    workspaceId:'workspaceId',
    subMenuId:'subMenuId',
    treeNodeid:'treeNodeId',
    accountId:"accountId",
    /// import File Localstorage Area Start
    sheetname:"sheetname",
    loadoption:"loadoption",
    tablename:"tablename",
    tabletype:"tabletype",
    remove_row_above_header:"remove_row_above_header",
    first_row_have_header:"first_row_have_header",
    SkipRows:"SkipRows",
    ExistAndCustomtableToggle:"ExistAndCustomtableToggle",
    ConnectorHeadingForNewAndUpdate:"ConnectorHeadingForNewAndUpdate",
    Source_Account_Id:"Source_Account_Id",
    userProfileImage:"userProfileImage",
    defaultpage:"defaultPage",
    governUserId:"governUserId",
    /// import File Localstorage Area End
  }
  // eventsColors: [
  //   {
  //     textColor: '#fff',
  //     color:'#f00',
  //   },
  //   {
  //     textColor: '#fff',
  //     color:'#2000ff',
  //   },
  //   {
  //     textColor: '#ffff',
  //     color:'#f220e5',
  //   },
  //   {
  //     textColor: '#fff',
  //     color:'#295718',
  //   },
  //   {
  //     textColor: '#fff',
  //     color:'#295718',
  //   }
  // ],

  // appointmentStatus:{
  //   arrived:1,
  //   completed:3
  // }

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
