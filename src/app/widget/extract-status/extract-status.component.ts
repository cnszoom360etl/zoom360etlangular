import { Variable } from '@angular/compiler/src/render3/r3_ast';
import { Component, Injector, OnInit } from '@angular/core';
import { RecentConnectionsService } from 'src/app/services/common/recent-connections.service';
import { environment } from 'src/environments/environment';
import { AppComponentBase } from 'src/app/services/AppComponentBase';
import { inject } from '@angular/core/testing';
import { filtersModel } from 'src/app/models/extract/filter.model';
@Component({
  selector: 'app-extract-status',
  templateUrl: './extract-status.component.html',
  styleUrls: ['./extract-status.component.css']
})
export class ExtractStatusComponent extends AppComponentBase implements OnInit {

  buttonAccorStatus:boolean=false;
  widgetAccorButtonText:string='Show all';
  ResentModules:any[]=[];
  _recordLength:number=0;
  constructor(private recentConnectionsService:RecentConnectionsService,injector : Injector) 
  { 
    super(injector)
  }
  
  ngOnInit(): void {
  //this.GetRecentModule();
  }
  GetRecentModule(filtersValue:filtersModel){
    debugger
  //  var UserId=this.storageService.getItem(environment.storage.userId);
  //  var Workspaceid=this.storageService.getItem(environment.storage.workspaceId);
  //  var Clientid=this.storageService.getItem(environment.storage.clientId);
  // var GetConnectorIds="182";
    this.recentConnectionsService.GetRecentModule(filtersValue).subscribe((data:any)=>{
      if(data.length >0){
        debugger
        this.ResentModules=data;
        this._recordLength = data.length;
      }
      });
  }
  buttonText(){
    this.buttonAccorStatus=!this.buttonAccorStatus;
    if(this.buttonAccorStatus){
      this.widgetAccorButtonText ='Hide All';
    }else{
      
      this.widgetAccorButtonText ='Show All';
    }
    
  }
}

