import { Component, OnInit } from '@angular/core';
import { DashboardService } from 'src/app/services/extract/dashboard/Dashboard.service';


@Component({
  selector: 'app-kpi-graph',
  templateUrl: './kpi-graph.component.html',
  styleUrls: ['./kpi-graph.component.css']
})
export class KpiGraphComponent implements OnInit {
  ngOnInit(): void {
  }
  data = {
    chart: {
      // caption: "Average Fastball Velocity",
      // yaxisname: "Velocity (in mph)",
      // subcaption: "[2005-2016]",
      bgColor: "#fbf9fc",
      numbersuffix: "k",
      rotatelabels: "1",
      setadaptiveymin: "1",
      drawAnchors: "0",
      showLabels: "0",
      showYAxisvalue:"0", 
      theme: "fusion"
    },
    data: [ 
    ]
  };
  data2 = {
    chart: {
      // caption: "Average Fastball Velocity",
      // yaxisname: "Velocity (in mph)",
      // subcaption: "[2005-2016]",
      bgColor: "#fbf9fc",
      numbersuffix: "k",
      rotatelabels: "1",
      setadaptiveymin: "1",
      drawAnchors: "0",
      showLabels: "0",
      showYAxisvalue:"0",
      theme: "fusion"
    },
    data: [ 
    ]
  };
  data3 = {
    chart: {
      // caption: "Average Fastball Velocity",
      // yaxisname: "Velocity (in mph)",
      // subcaption: "[2005-2016]",
      bgColor: "#fbf9fc",
      numbersuffix: "k",
      rotatelabels: "1",
      setadaptiveymin: "1",
      drawAnchors: "0",
      showLabels: "0",
      showYAxisvalue:"0",
      theme: "fusion"
    },
    data: [ 
    ]
  };
  data4 = {
    chart: {
      // caption: "Average Fastball Velocity",
      // yaxisname: "Velocity (in mph)",
      // subcaption: "[2005-2016]",
      bgColor: "#fbf9fc",
      numbersuffix: "k",
      rotatelabels: "1",
      setadaptiveymin: "1",
      showLabels: "0",
      drawAnchors: "0",
      showValues: "0",
      showYAxisvalue:"0",
      theme: "fusion"
    },
    data: [ 
    ]
  }; 
  constructor(  private dashboardService: DashboardService ) {
    dashboardService.get().subscribe(res => {
      res.forEach(element => {
        if (element.platform == "Facebook") {
        this.data.data.push({label:element.period_Display_Date,value: element.imperssions}); 
        this.data4.data.push({label:element.period_Display_Date,value: element.ctr});       
        } else if (element.platform == "Spreadsheet") {
          this.data2.data.push({label:element.period_Display_Date,value: element.imperssions});          
        } else if (element.platform == "MS SQL Server") {
          this.data3.data.push({label:element.period_Display_Date,value: element.imperssions});

        }
        
});
     
    });
   }
  width = 450;
  height = 200;
  type = "line";
  dataFormat = "json";
  dataSource = this.data;
  dataSource2 = this.data2;
  dataSource3 = this.data3;
  dataSource4 = this.data4;

}
