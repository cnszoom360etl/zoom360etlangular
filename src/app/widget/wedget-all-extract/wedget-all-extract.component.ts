import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { filtersModel } from 'src/app/models/extract/filter.model';
 
import { GetAllExtractsService } from 'src/app/Services/extract/get-all-extracts.service';


@Component({
  selector: 'app-wedget-all-extract',
  templateUrl: './wedget-all-extract.component.html',
  styleUrls: ['./wedget-all-extract.component.css']
})
export class WedgetAllExtractComponent implements OnInit {
  workspaceName=null;
  connectionName=null;
  sourceName=null;
  accessGranted=null;
  createdBy=null;
  isActive=null;
  lastAccessed=null;
  destinationEnabled=null;
  widgetAccorButtonText='Show All';
  buttonAccorStatus:boolean=false;
  constructor(public AllExtractService:GetAllExtractsService,private router: Router) { }

  ngOnInit(): void {
  
  }
  getallExtractsSummery(filtersValue:filtersModel){
this.AllExtractService.getAllextract(filtersValue);
  }
  buttonText(){
    this.buttonAccorStatus=!this.buttonAccorStatus;
    if(this.buttonAccorStatus){
      this.widgetAccorButtonText ='Hide All';
    }else{
      
      this.widgetAccorButtonText ='Show All';
    }
    
    
  }
  getfiledata(filename){
      debugger
      JSON.stringify(localStorage.setItem("extractfile",filename));
       this.router.navigate(['/extract/extraction/Extractlogfileviewer']);
  }
}
