import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { filtersModel } from 'src/app/models/extract/filter.model';
import { GetAllExtractsService } from 'src/app/Services/extract/get-all-extracts.service';


@Component({
  selector: 'app-all-extract-log-widget',
  templateUrl: './all-extract-log-widget.component.html',
  styleUrls: ['./all-extract-log-widget.component.css']
})
export class AllExtractLogWidgetComponent implements OnInit {
  workspaceName:string=null;
  connectionName:string=null;
  sourceName:string=null;
  accessGranted:string=null;
  createdBy:string=null;
  isActive:string=null;
  lastAccessed:string=null;
  destinationEnabled:string=null;
  constructor(public AllExtractService:GetAllExtractsService,private router: Router) { }

  ngOnInit(): void {
    }
    getExtractsSummery(filtersValue:filtersModel){
      this.AllExtractService.getAllextract(filtersValue);
    }
    getfiledata()
    {
          debugger
          this.router.navigate(['/extract/extraction/view-detail']);
   }

}
