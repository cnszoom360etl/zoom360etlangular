import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WidgetRoutingModule } from './widget-routing.module';
import { AllExtractLogWidgetComponent } from './all-extract-log-widget/all-extract-log-widget.component';
import { AllIssuesLogWidgetComponent } from './all-issues-log-widget/all-issues-log-widget.component';
import { ExtractStatusComponent } from './extract-status/extract-status.component';
import { WedgetSourceLogComponent } from './wedget-source-log/wedget-source-log.component';
import { WedgetAllissuesComponent } from './wedget-allissues/wedget-allissues.component';
import { ExploreEditorComponent } from './explore-editor/explore-editor.component';
import { WedgetAllExtractComponent } from './wedget-all-extract/wedget-all-extract.component';
import { WedgetEnrichLogComponent } from './wedget-enrich-log/wedget-enrich-log.component';
import { WedgetLoadLogComponent } from './wedget-load-log/wedget-load-log.component';
import { WedgetObjectLogComponent } from './wedget-object-log/wedget-object-log.component';
import * as FusionCharts from "fusioncharts";
import * as charts from "fusioncharts/fusioncharts.charts";
import * as FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";
import { FusionChartsModule } from 'angular-fusioncharts';
import { AgGridModule } from 'ag-grid-angular';
import 'ag-grid-enterprise';
import { WedgetActivityMonitorComponent } from './wedget-activity-monitor/wedget-activity-monitor.component';

FusionChartsModule.fcRoot(FusionCharts, charts, FusionTheme);
@NgModule({
  declarations: [
    AllExtractLogWidgetComponent,
    AllIssuesLogWidgetComponent,
    ExtractStatusComponent,
    WedgetSourceLogComponent,
    WedgetAllExtractComponent,
    WedgetAllissuesComponent,
    ExploreEditorComponent,
  
    WedgetEnrichLogComponent,
    WedgetLoadLogComponent,
    WedgetObjectLogComponent,
    ExtractStatusComponent,
    WedgetActivityMonitorComponent
  ],
  imports: [
    CommonModule,
    WidgetRoutingModule,
    FusionChartsModule,
    AgGridModule.withComponents([])
    
  ],
  exports:[
    AllExtractLogWidgetComponent,
    AllIssuesLogWidgetComponent,
    ExtractStatusComponent,
    WedgetSourceLogComponent,
    WedgetAllExtractComponent,
    WedgetAllissuesComponent,
    ExploreEditorComponent,
    WedgetEnrichLogComponent,
    WedgetLoadLogComponent,
    WedgetObjectLogComponent,
    ExtractStatusComponent,
    WedgetActivityMonitorComponent
  ]
})
export class WidgetModule { }
