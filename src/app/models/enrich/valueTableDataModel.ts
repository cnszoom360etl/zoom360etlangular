export class valueTableDataModel{

    
      sourceValue:string; 
      targetValue:string;  
      userNameInsert:string;  
      userNameUpdate:string;  
      serverInsertDate:string;  
      serverInsertTime:string;  
      serverInsertTimeZone:string;  
      serverUpdateDate:string;  
      serverUpdateTime:string;  
      serverUpdateTimeZone:string;  
      clientInsertDate:string;  
      clientInsertTime:string;  
      clientInsertTimeZone:string;  
      clientUpdateDate:string;  
      clientUpdateTime:string;  
      clientUpdateTimeZone:string;  
      bStatus:boolean=false;  
      bDelete:string;  
      bMap:string;  
      Remark1:string;  
      Remark2:string;  
      Remark3:string;  
      Remark4:string;  
      Flex1:string;  
      Flex2:string;  
      Flex3:string;  
      Flex4:string;  
      Flex5:string;  
      Flex6:string;  
      Flex7:string;  
      Flex8:string;  
      Flex9:string;  
      Flex10:string;  
      Flex11:string;  
      Flex12:string;  
      Flex13:string;  
      Flex14:string;  
      Flex15:string;  
      Flex16:string;  
 

 
}
export class saveValueTableData{

  
   userId :string;
   workspaceId :string;
   clientId :string;
   valueTableName :string;
   sourceValue :string;
   targetValue :string;
   enabled :string;
   ClientDate :string;
   ClientTime :string;
   ClientTimeZone :string;
   remark1:string;  
   remark2:string;  
   remark3:string;  
   remark4:string;  
   Flex1:string;  
   Flex2:string;  
   Flex3:string;  
   Flex4:string;  
   Flex5:string;  
   Flex6:string;  
   Flex7:string;  
   Flex8:string;  
   Flex9:string;  
   Flex10:string;  
   Flex11:string;  
   Flex12:string;  
   Flex13:string;  
   Flex14:string;  
   Flex15:string;  
   Flex16:string;  
}