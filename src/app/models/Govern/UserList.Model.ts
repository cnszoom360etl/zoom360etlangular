export class UserListModel
{
    subUserID:number;
    emailAddress:string;
    workspaces :string;
    lastActivity:string;
    lastStatus:string;
    userActive:boolean;
    state:boolean;
}
