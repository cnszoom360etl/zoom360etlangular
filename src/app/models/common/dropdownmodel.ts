export class dropdownModel{
    dropdownValue:string;
    dropdownText:string; 
}
export class dropdownModelforboolean{
    dropdownValue:boolean;
    dropdownText:boolean; 
}
export class UAMDropdownModel{
    dropdownValue:string;
    dropdownText:string;
     
}

export class dropdownWithCategoryModel{
    dropdownValue:string;
    dropdownText:string;
    dropdownCategory:string;
}

// Column Mapping

export class GetTargetColumnNameList{
    Index:number;
    source_Column:string;
    target_value:string;
    field_type:fieldNameList;
    // keycoilumn:boolean=false;
     visibility:boolean=true;

    
     
}
export class fieldNameList{
    dropdownvalue:string;
    dropdownText:string;
    
     
}


// Content Mapping

export class GetlookuptableAndColumnNameList{
    Index:number;
    source_column_name:string;
    lookup_table_name:string;
    lookup_field_name:string;
  }
 
//contentmappinglookuptable backup 
export class lookuptablestate{
    Index:string;
    lookup_table_name:string;
   
}