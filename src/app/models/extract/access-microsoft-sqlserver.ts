import { StringFilterUI } from "@syncfusion/ej2-grids";

export class AccessConfigServer {
HostName:string="192.168.50.101";
DataBase:string;
UserName:string;
Password:string;

}
export class EmailAuthorization {
    Email:string;
    AccountAuthurization:string;
}
export class SourceAccount {
    AccountName:string=null;
    EnableConnectoins:boolean=true;
    workspace:string="Default";
    Email:string=null;
    AccountAuthurization:string="Eachtimeforconnectionestablishing";
    visibilitymode:boolean=true;
    commentsection:string="null";
    specialcomments:string="null";
    bstatus:boolean=false;
    connectivitystatus:boolean;
}
export class SourcObject {
    SourceDisplayName:string=null;
    WorkspaceName:string="Conference Room";
    Authorizationgranted:boolean;
     
}
export class TupleMsg {
    item1:string;
    item2:string;
    item3:string;
}
export class updatesourceaccountStep2{
     accountId:string;
     accountdisplayname:string;  
     enableconnection:boolean; 
     emailid :string; 
     authorizationgrant:string;  
     statusnotifyGrant :string; 
     filedname :string; 
     fieldvalue:string;  
     bStatusforvarification:string;  
     connectorname:string;
     transformed:string;
     connectivitystatus:boolean;
     objectname:string;
     visibileconnection:boolean;
     comments:string;
     spcomments:string;
     workspacename:string;
     hostname:string;  
     database :string; 
     username :string;  
     password :string;  
     port:string; 
 
      
}
export class DestinationList{
    accountdisplayname:string;  
    enableconnection :boolean; 
    emailid :string; 
    authorizationgrant:string;  
    statusnotifyGrant :string; 
    filedname :string; 
    fieldvalue:string;  
    bStatusforvarification:string;  
    connectorname:string;
    objectname:string;
    dataoption:string;
    
   
     
}
export class ExtractDataSave{
     
    accountdisplayname:string;  
    enableconnection :boolean; 
    emailid :string; 
    authorizationgrant:string;  
    statusnotifygrant :string; 
    filedname :string; 
    ConnectorType:string;
    fieldvalue:CreaditionalData;  
    bstatusforvarification:string; 
    Filename:string;
    Filepath:string; 
    tablename:string[];
    transformed:string;
}
export class CreaditionalData{
    hostName:string;
    databaseName:string;
    port:string=null;
    username:string;
    password:string;
}


export class LoadDataSave{
    Desaccountdisplayname:string;  
    Desfiledname :string; 
    DesConnectorType:string;
    Desfieldvalue:LoadCreaditionalData;  
    DesFilename:string;
    DesFilepath:string; 
    Destablename:string[];
    transformation:functionDetails[];

}
export class LoadDataSaveupdated{
    Desaccountdisplayname:string;  
    Desfiledname :string; 
    DesConnectorType:string;
    Desfieldvalue:LoadCreaditionalData;  
    DesFilename:string;
    DesFilepath:string; 
    Destablename:string;
 

}
export class LoadCreaditionalData{
    DeshostName:string;
    DesdatabaseName:string;
    Desport:string=null;
    Desusername:string;
    Despassword:string;
}

export class functionDetails{
            
            groupId :string
            functionGroup :string
            functionId :string
            functionName :string
            functionPerameter :peramscounter;
            PerameterValues :peramsvaluecounter;
}
export class FilterfunctionDetails{
            
    groupId :string
    functionGroup :string
    functionId :string
    functionName :string
    functionPerameter :peramscounter;
    PerameterValues :peramsvaluecounter;
}
export class functionperamslist{
    cols:string[]=[];
    create_col:string;
    save_previous_changes:string; 
    report:string;
    ref_string:string;
    separator:string;
    delimiter:string;
    source:string;
    target:string; 
    mode:string;
    sign:string;
    replacement_dict:string;
    substring:string;
    start_date:string;
     end_date:string;


}
export class peramscounter{
     first1:string;
     second2:string;
     third3:string;
     fourth4:string;
}
export class peramsvaluecounter{
    first1:string;
    second2:string;
    third3:string;
    fourth4:string;


}

export class ExtractAutoETL{
    accountid:string;
    accountdisplayname:string;  
    enableconnection :boolean; 
    emailid :string; 
    authorizationgrant:string;  
    statusnotifygrant :string; 
    filedname :string; 
    ConnectorType:string;
    fieldvalue:DBInfoForAutoETL;  
    bstatusforvarification:string; 
    Filename:string;
    Filepath:string; 
    tablename:string[];
    transformed:string;
}
export class DBInfoForAutoETL{
    hostName:string;
    databaseName:string;
    port:string=null;
    username:string;
    password:string;
}

export class DB_Credentials_Info_For_Load_Json{
                    Database:string;
                    serviceName:string;
                    host:string;
                    dbname:string;
                    user:string;
                    password:string;
                    insert:string;
                    tablename:string;
                    port:string=null;
}