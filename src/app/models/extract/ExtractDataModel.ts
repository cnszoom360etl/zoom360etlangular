import { AstMemoryEfficientTransformer } from "@angular/compiler";

export class ExtractSqlTableData
{
     accountid:string;
     connectorname:string;
     connectorId:string;
     displayName:string;
     piplineid:string;
     accessgranted:string;
      
}

export class AutoETLExtaction
{
     accountid:string;
     connectorname:string;
     connectorId:string;
     displayName:string;
     piplineid:string;
     accessgranted:string;
      
}

export class ScriptDetails
{
     
     scriptId:string;
      
}


export class DBConnectorIdsAndAccountIdsForExtract
{
     AccountId:string;
     ConnectorId:string;
      
}


export class TransformationJsonPart
{
     AccountId:string;
     Transformation:any;
     contentmapping:any;
     columnmapping:any;
     load:any;
      
}
export class etlstatus
{
     _id:string;
     Extract:number;
     Load:number;
     Transform:number;
     
      
}
export class buttonGroupmodel
{
       inputType  :string;
       inputId  :string;
       inputName  :string;
       value:string;
       ngModel  :string;
       labelId  :string;
       ngClass  :string;
       htmlAttribute  :string;
       click  :string;
       buttonDisplayName  :string;
}
 