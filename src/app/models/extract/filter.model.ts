import { UserDetail } from "../user-detail.model";

export class filtersModel extends UserDetail{
    accountId:string;
    workspaceName:string;
    connectionName:string;
    sourceName:string;
    accessGranted:string;
    createdBy:string;
    isActive:string;
    destinationEnabled:string;
    lastAccessed:string;
    TimeFilter:string;
}