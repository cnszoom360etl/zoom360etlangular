export class SyncGridColumnModel{
    columnName:string;
}
export class SyncMappingGridModel{
    SourceColumn:string;
    keycolumn:boolean=true;
    visibilty:boolean=true;
    mappingBit:string;
     
    // TargetColumn:string;
}
export class MappedLookupTableModel{
    Index:number;
    mappingTablename:string;
    mappingFieldname:string;
    }
   export class ArrayForMappedFilter{
    Index:number;
        mappingTablename:string;
        mappingFieldname:string;
        SourceColumn:string;
        keycolumn:boolean=true;
        visibilty:boolean=true;
        mappingBit:string;
        }
export class ColumnMappingJsonModel{
    source_name:string;
    target_name:string;
    field_type:string;
    visibility:boolean=true;
    // TargetColumn:string;
}

export class ColumnMappingEditMode{
     Index:number;
    SourceColumn:string;
    keycolumn:boolean=true;
    visibilty:boolean=true;
    mappingBit:string;
    target_name:string;
    field_type:string;
     
     
     
    // TargetColumn:string;
}