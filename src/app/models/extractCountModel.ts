export class extractionCountModel{
    extractionCompleteCount:number;
    extractionTotalCount:number;
    enrichmentCompleteCount:number;
    enrichmentTotalCount:number;
    loadCompleteCount:number;
    loadTotalCount:number;
}