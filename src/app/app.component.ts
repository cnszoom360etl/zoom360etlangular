import { Component, Injector } from '@angular/core';
import { MianMenuModel } from './Models/mainmenu.model';
import { AppComponentBase } from './services/AppComponentBase';
import { AppMenuService } from './Services/common/app-menu.service';
import { FiltersService } from './Services/common/filters.service';
import { AddNewConnectoinWizardStyle } from './services/extract/AddNewConnectionWizardStyle';
 
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent extends AppComponentBase  {
  _mainMenu : MianMenuModel[] = [];
  applicationMode:boolean=true;
  presentationMode:boolean=false;
  mode_id: number=1;
  mainmenuID:number=2;
  userId:string='';
  workSpaceId:string='';
  client_id:string='';
  path:String;
  title = 'DataAnalysisRaptore';
  
  constructor(private MenuService: AppMenuService,private _Filters:FiltersService, inject:Injector,public _AddNewConnectoinWizardStyleservice:AddNewConnectoinWizardStyle){
    super(inject);
   // this.loginDataService.getLoginDetail();
  //  this.getMenu(this.mode_id);
  }
  ngOnInit(){
    let path = window.location.href;
    let subMenuName = path.substring(path.lastIndexOf("/") + 1);
  }
   
  getMenu(mode_id){
    debugger
    this.userId = this.clientDetailService.getuserID();
    this.workSpaceId=this.clientDetailService.getWorkspaceID();
    this.client_id = this.clientDetailService.getClientID();
      this.MenuService
        .getMenuItems(mode_id,this.userId,this.workSpaceId,this.client_id)
        .subscribe((data: any[]) => {
          debugger
          if(data.length  > 0){
            this._mainMenu = data;
          }
      });
    }
 
  changeMode(value){
     if(value==1){
   this.applicationMode=true;
   this.presentationMode=false;
   this.mode_id=1;
   this.getMenu(this.mode_id)

  }
  else if(value==2){
    this.applicationMode=false;
    this.presentationMode=true;
    this.mode_id=2;
    this.getMenu(this.mode_id)
  }
//test usman 
}
}
