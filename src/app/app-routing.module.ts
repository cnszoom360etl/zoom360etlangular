import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApplicationStartupComponent } from './application-startup/application-startup.component';
import { AuthGuard } from './guards/auth.guard';
import { AuthurizationResponseComponent } from './authurization-response/authurization-response.component';

const routes: Routes = [
    {path:'home', component:ApplicationStartupComponent},
    {path:'Success/:id', component:AuthurizationResponseComponent},
    {path:'', redirectTo:'', pathMatch:'full'},
  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)
  },
  {

    path: '',
    loadChildren: () => import('./main/main.module').then(m => m.MainModule), canActivate: [AuthGuard]
  },

{ path: '**', redirectTo: 'auth/login' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

