import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ColDefUtil } from 'ag-grid-community';
import { googleoauth } from '../services/Enrich/googleOauthToken';
import { AddNewConnectoinWizardStyle } from '../services/extract/AddNewConnectionWizardStyle';

@Component({
  selector: 'app-authurization-response',
  templateUrl: './authurization-response.component.html',
  styleUrls: ['./authurization-response.component.css']
})
export class AuthurizationResponseComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute,private fbtoken:googleoauth,private _AddNewConnectoinWizardStyleservice:AddNewConnectoinWizardStyle) { }

  ngOnInit() {
    debugger
    let accountid = this.activatedRoute.snapshot.params.id;
     
    let path = window.location.href;
    var rest = path.substring(path.lastIndexOf("code") + 5);
     this._AddNewConnectoinWizardStyleservice.hide_the_main_menu_when_user_authorized=false;
    this.savetokenindb(accountid,rest)
     
    // alert(rest);
  }
  savetokenindb(accountid:string,code:string)
  {

   
this.fbtoken.fbUrl(accountid,code);
  }

}
