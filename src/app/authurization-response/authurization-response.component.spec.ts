import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthurizationResponseComponent } from './authurization-response.component';

describe('AuthurizationResponseComponent', () => {
  let component: AuthurizationResponseComponent;
  let fixture: ComponentFixture<AuthurizationResponseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthurizationResponseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthurizationResponseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
