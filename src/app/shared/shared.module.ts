import { NgModule, Injector } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ServiceInjector } from '../libraries/serviceInjector';
import { UserService } from '../services/user.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MyHelper } from '../utils/myHelper';
import { TokenInterceptor } from '../interceptors/token-interceptor';
import {PopoverModule} from "ngx-popover";
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SidebarModule } from 'ng-sidebar';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { ArraySortPipe } from './pipes/array-pipes';
import { ListBoxComponent } from '@syncfusion/ej2-angular-dropdowns';
import { EnrichModule } from '../main/enrich/enrich.module';
import { LoadModule } from '../main/load/Load.module';
import { DialogModule } from '@syncfusion/ej2-angular-popups';
import { ToastMessage } from '../models/MessageTypes/toast-message';
import { ToastModule } from '@syncfusion/ej2-angular-notifications';
 

@NgModule({
  entryComponents: [
  ],
  declarations: [
    ArraySortPipe,
    
      
  ],
  imports: [
    CommonModule,
    TabsModule.forRoot(),
    BsDropdownModule.forRoot(),
    AngularFontAwesomeModule,
    ModalModule.forRoot(),
    HttpClientModule,
    PopoverModule,
    AutocompleteLibModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    ToastModule,
    SidebarModule.forRoot(),
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot(),
    EnrichModule,
    LoadModule,
    DialogModule
    
  ],
  exports: [
    TabsModule,
    BsDropdownModule,
    ModalModule,
    AngularFontAwesomeModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    PopoverModule,
    ToastModule,
    AutocompleteLibModule,
    NgbModule,
    SidebarModule,
    BsDatepickerModule,
    TimepickerModule,
    ArraySortPipe,
     
    
  ],
  providers: [
    UserService,
    MyHelper,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    ArraySortPipe
  ]
})
export class SharedModule {
  constructor(private injector: Injector) {
    ServiceInjector.injector = this.injector;
  }
}
