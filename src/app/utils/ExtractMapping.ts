import { ConnectorName } from "../main/extract/AddNewDataStream/EnumForConnectorList";

 
 
export class stringmappingDictionary{
     fingerCounts: Map<string, string> = new Map();
     constructor( ) {
       
        //DB Component mapping
       this.fingerCounts.set(ConnectorName.Sqlserver,"DBExtraction")
        this.fingerCounts.set(ConnectorName.MySQL,"DBExtraction")
        this.fingerCounts.set(ConnectorName.OracleDBMS,"DBExtraction")
        this.fingerCounts.set(ConnectorName.MongoDB,"DBExtraction")
        this.fingerCounts.set(ConnectorName.MicrosoftAccess,"DBExtraction")
        this.fingerCounts.set(ConnectorName.PostgreSQL,"DBExtraction")
        this.fingerCounts.set(ConnectorName.ElasticDB,"DBExtraction")
        this.fingerCounts.set(ConnectorName.CassandraDB,"DBExtraction")
        this.fingerCounts.set(ConnectorName.CosmosDB,"DBExtraction")
        this.fingerCounts.set(ConnectorName.Redis,"DBExtraction")
        this.fingerCounts.set(ConnectorName.AmazonDynamoDB,"DBExtraction")
        this.fingerCounts.set(ConnectorName.AmazonRedshift,"DBExtraction")
        this.fingerCounts.set(ConnectorName.MicrosoftAzure,"DBExtraction")
        this.fingerCounts.set(ConnectorName.GoogleBigQuery,"DBExtraction")
        this.fingerCounts.set(ConnectorName.Snowflake,"DBExtraction")
        this.fingerCounts.set(ConnectorName.MicroFocusVertica,"DBExtraction")
        this.fingerCounts.set(ConnectorName.AmazonRDS,"DBExtraction")
        this.fingerCounts.set(ConnectorName.MariaDB,"DBExtraction")
        this.fingerCounts.set(ConnectorName.IBMDb2,"DBExtraction")
        this.fingerCounts.set(ConnectorName.OracleAutonomous,"DBExtraction")
        this.fingerCounts.set(ConnectorName.MarkLogic,"DBExtraction")
       //file component  mapping 
        this.fingerCounts.set(ConnectorName.CSVFile,"FileExtraction")
        this.fingerCounts.set(ConnectorName.XMLFile,"FileExtraction")
        this.fingerCounts.set(ConnectorName.JSONFile,"FileExtraction")
        this.fingerCounts.set(ConnectorName.PDFFile,"FileExtraction")
        this.fingerCounts.set(ConnectorName.ZipFil,"FileExtraction")
        this.fingerCounts.set(ConnectorName.MicrosoftExcel,"FileExtraction")
        this.fingerCounts.set(ConnectorName.GoogleSheet,"FileExtraction")
        this.fingerCounts.set(ConnectorName.OpenOffice,"FileExtraction")
        this.fingerCounts.set(ConnectorName.ODSFile,"FileExtraction")
        this.fingerCounts.set(ConnectorName.FTPServer,"FileExtraction")
        this.fingerCounts.set(ConnectorName.SFTPServer,"FileExtraction")
        this.fingerCounts.set(ConnectorName.Dropbox,"FileExtraction")
        this.fingerCounts.set(ConnectorName.GoogleDrive,"FileExtraction")
        this.fingerCounts.set(ConnectorName.MicrosoftOneDrive,"FileExtraction")
        this.fingerCounts.set(ConnectorName.OneHub,"FileExtraction")
        this.fingerCounts.set(ConnectorName.Box,"FileExtraction")
        this.fingerCounts.set(ConnectorName.EgnyteUSA,"FileExtraction")
        this.fingerCounts.set(ConnectorName.MASV,"FileExtraction")
        this.fingerCounts.set(ConnectorName.eFileCabinet,"FileExtraction")
        this.fingerCounts.set(ConnectorName.iDrive,"FileExtraction")
        this.fingerCounts.set(ConnectorName.Wire,"FileExtraction")
        this.fingerCounts.set(ConnectorName.MEGA,"FileExtraction")
        this.fingerCounts.set(ConnectorName.ApacheHDFS,"FileExtraction")
        this.fingerCounts.set(ConnectorName.Azurefile,"FileExtraction")
        this.fingerCounts.set(ConnectorName.AmazonS3,"FileExtraction")
        this.fingerCounts.set(ConnectorName.GoogleCloud,"FileExtraction")
        this.fingerCounts.set(ConnectorName.WeTransfe,"FileExtraction")
        this.fingerCounts.set(ConnectorName.SendAnywheree,"FileExtraction")
        this.fingerCounts.set(ConnectorName.EmailAttachment,"FileExtraction")
        this.fingerCounts.set(ConnectorName.Hightail,"FileExtraction")
        this.fingerCounts.set(ConnectorName.Excel,"FileExtraction")
        this.fingerCounts.set(ConnectorName.Slack,"FileExtraction")
        this.fingerCounts.set(ConnectorName.Csv,"FileExtraction")
        this.fingerCounts.set(ConnectorName.GoogleDriveDownload,"FileExtraction")
        //SocialMedia 
        this.fingerCounts.set(ConnectorName.FacebookPublicPage,"SocialMediaExtraction")
        this.fingerCounts.set(ConnectorName.FacebookPageMentions,"SocialMediaExtraction")
        this.fingerCounts.set(ConnectorName.FacebookAuthorization,"SocialMediaExtraction")
 
     }

 getStringKey(name:string):any{
  return  this.fingerCounts.get(name);
 }
 
}

 