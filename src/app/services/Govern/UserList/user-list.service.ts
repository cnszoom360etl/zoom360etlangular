import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserListService {
  userId:string='admin';
  workSpaceId:string=''+1;
  client_id:string=''+1002;
  Status_message:string;
  SubUserId:string='Humna';
  _getUserListUrl= environment.apiUrl+'/api/UserAccessManagement/GetUserList';
  constructor(private _http:HttpClient) { }
  getUserList(){
    let params = new HttpParams()
    .set("UserId", this.userId)
    .set("WorkSpaceId", ''+this.workSpaceId)
    .set("Client_Id", this.client_id)
    return this._http.get(`${this._getUserListUrl}`,{params:params});
  }
}
