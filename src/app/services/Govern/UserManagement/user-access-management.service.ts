import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SaveUserAccessManagementModel } from 'src/app/models/Govern/UserAccessManagment.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserAccessManagementService {
  _getDropDownUrl= environment.apiUrl+'/api/CommonDropdownList/GetUAMDropdown';
  _SaveUAMUrl= environment.apiUrl+'/api/UserAccessManagement/SaveUserAccesManagmentSetup';
  
  userId:string='admin';
  workSpaceId:string=''+11;
  client_id:string=''+1003;
  subUser:string;
  Status_message:string;
  SubUserId:string='Humna';
  constructor(private _http:HttpClient) {
    this.subUser=localStorage.getItem("userId");
   }
  // getDropDown(dropdownName){
  //   let params = new HttpParams().set("userId", this.userId).set("dropdownName", dropdownName);
  //   return this._http.get(`${this._getDropDownUrl}`,{params:params});
  // }
getDropDown(dropdownName){
    let params = new HttpParams().set("userId", this.userId).set("dropdownName", dropdownName)
    .set("subUserID", this.subUser);
    debugger
    return this._http.get(`${this._getDropDownUrl}`,{params:params});
  }

   saveUserAccessManagementSetup(UserData:SaveUserAccessManagementModel){
    UserData.UserId = this.userId;
    UserData.SubUserId = this.SubUserId;
    UserData.WorkspaceId = this.workSpaceId;
    UserData.ClientId = this.client_id;
    this._http.post(`${this._SaveUAMUrl}`,UserData,{responseType:'text'}).subscribe((data:string) => {
      debugger
      this.Status_message = data;
      console.log(this.Status_message);
      });
    }
  getUserProfile(){
    let params = new HttpParams().set("userId", this.userId).set("dropdownName", 'test')
    .set("subUserID", this.subUser);
    debugger
    return this._http.get(`${this._getDropDownUrl}`,{params:params});
  }
  getUserPermissions(){
    let params = new HttpParams().set("userId", this.userId).set("dropdownName", 'test')
    .set("subUserID", this.subUser);
    debugger
    return this._http.get(`${this._getDropDownUrl}`,{params:params});
  }
  getUserRestriction(){
    let params = new HttpParams().set("userId", this.userId).set("dropdownName", 'test')
    .set("subUserID", this.subUser);
    debugger
    return this._http.get(`${this._getDropDownUrl}`,{params:params});
  }
}
