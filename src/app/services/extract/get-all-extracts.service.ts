import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AllExtractListModel } from 'src/app/models/connect.model';
import { filtersModel } from 'src/app/models/extract/filter.model';
import { ArraySortPipe } from 'src/app/shared/pipes/array-pipes';
import { environment } from 'src/environments/environment';
import { BaseService } from '../base.service';
import { FiltersService } from '../common/filters.service';

@Injectable({
  providedIn: 'root'
})
export class GetAllExtractsService {
  //_AllExtractsUrl ='https://localhost:44357/api/AllExtracts/GetAllExtractsRowsLimitBysp';
  //_AllExtractsUrl ='https://localhost:44357/api/AllExtracts/GetAllExtractsRowsLimitBysp';
  
  _allExtractListUrl= environment.apiUrl + '/api/AllExtract/getAllExtractList';
  userId:string='admin';
  workSpaceId:number=1;
  client_id:string=''+1002;
  extractData:any[]=[];
  _ExtractSearch: AllExtractListModel[]=[];
  _recordLength:number=0;
  _ExtactRecordLenght:number=0;
  _sortToggle : boolean=false;
  _tablecounter:number=0;
  accountId=null;
 
  
  constructor(private _http:HttpClient,private sort: ArraySortPipe,private filterService:FiltersService) { 
    
  }

  getAllextract(filtersValue:filtersModel){
    debugger
    //this.accountId= localStorage.getItem("accountId");
    this.accountId=localStorage.getItem("sourceaccount_Id");
    this.extractData=[];
    this._recordLength=0;
    let params = new HttpParams()
    .set("userId", filtersValue.userId)
    .set("workSpaceId", filtersValue.workspaceId)
    .set("clientId", filtersValue.clientId)
    .set("accountId",filtersValue.accountId)
    .set("workspaceName", filtersValue.workspaceName)
    .set("connectionName", filtersValue.connectionName)
    .set("sourceName", filtersValue.sourceName)
    .set("accessGranted", filtersValue.accessGranted)
    .set("createdBy", filtersValue.createdBy)
    .set("isActive", filtersValue.isActive)
    .set("lastAccessed", filtersValue.lastAccessed)
    .set("destinationEnabled", filtersValue.destinationEnabled)
    .set("TimeFilter", filtersValue.TimeFilter);
  
  this._http.get(`${this._allExtractListUrl}`,{params:params}).subscribe((data: any) => { 
    debugger
    if(data.length  > 0){
        debugger
        this.extractData= data;
        this._ExtractSearch = data;
        this._recordLength = data.length;
        this._ExtactRecordLenght=data.length;
        this.filterService._recordLength=data.length;
      }
  });
     }
//   geAllExtract(limit){
//     let params = new HttpParams().set("limit", limit)
//     .set("userId", this.userId);
//   return this._http.get(`${this._allExtractListUrl}`,{params:params}).subscribe((data:any) => {
//       if(data.length  > 0){
//         this.extractData= data;
//         this._ExtractSearch = data;
//         this._recordLength = data.length;
//         console.log(this.extractData);
//       }
//   });
// }
searchAllExtract(text){
  this.extractData = this._ExtractSearch;
    this.extractData = this._ExtractSearch.filter((data) =>data.connectionName.toLowerCase().includes(text.toLowerCase()));
    this._recordLength = this.extractData.length;
    this.filterService._recordLength=this.extractData.length;
    //t => t.title.toLowerCase().includes(this.query.toLowerCase())
  }
sortAllExtract(field){
 if(this._sortToggle){
    this.extractData = this.sort.transform(this.extractData,field);
  }

  else if(!this._sortToggle){
    this.extractData = this.sort.transform2(this.extractData,field);
  }

  }
}
