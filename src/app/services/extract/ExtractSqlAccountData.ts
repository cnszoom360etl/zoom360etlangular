import { Injectable, Injector } from '@angular/core';
import {HttpClient,HttpHeaders, HttpParams} from '@angular/common/http';
import { ServiceService } from './service.service';

 
import { AppComponentBase } from 'src/app/services/AppComponentBase';
import { TouchSequence } from 'selenium-webdriver';
import { stringmappingDictionary } from 'src/app/utils/ExtractMapping';
import { ExtractSqlTableData } from 'src/app/models/extract/ExtractDataModel';
import { DestinationList, ExtractDataSave, FilterfunctionDetails, functionDetails, LoadDataSave, updatesourceaccountStep2 } from 'src/app/models/extract/access-microsoft-sqlserver';
import { environment } from 'src/environments/environment';
import * as FileSaver from 'file-saver';
import { DestinationExtractModel } from 'src/app/models/Load/destinationExtractModel';
 
 
@Injectable({
  providedIn: 'root'
})
export class ExtractData extends AppComponentBase {
    url='https://localhost:44397';
    checkUserAccessGrant=environment.apiUrl+'/api/Extraction/checkuserRights';
    GetSqlConnectorList=environment.apiUrl+'/api/SqlConnector/GetSqlConnectorList';
    GetDesConnectorList=environment.apiUrl+'/api/Destination/GetDestinationConnectorList';
    GetObjectListFromStep4=environment.apiUrl+'/api/SqlConnector/GetSourceObjectListForStep4Grid';
    gettransformationfunctiondetails=environment.apiUrl+'/api/SqlConnector/getFunctionList';
     data:any[]=[];
     stringmappingDictionary=new stringmappingDictionary();
     SourceAccountLst:ExtractDataSave[]=[];
     accessgrantedarray:any[]=[];
     TES: ExtractSqlTableData[] = []
     key:string;
     Bstatus:string;
     Pipline:any[]=[];
    counter:number= 1;
    transformationcounter:number=1;
    LoadDataList:DestinationExtractModel[]=[];
    LoadObject:LoadDataSave[]=[];
    transformationArrayList:functionDetails[]=[];
    perametrsvalue:string[]=[];
    perametername:string[]=[];
    FilterstheRowfromtransformation:FilterfunctionDetails[]=[];
    functionIdsarraylist:any[] = [1001,1002,1003];

      
     
     
     

     SourceAccountObjectLst:any[]=[];
      resultkeyname; 
    resultkeyvalue:any[]=[]; 
    constructor(private _http: HttpClient ,public serviceService:ServiceService,injector : Injector) {
      super(injector)
   }
    

   Runforextractdata(){
    debugger
    var UserId=this.storageService.getItem(environment.storage.userId);
    var Workspaceid="1";
    var Clientid=this.storageService.getItem(environment.storage.clientId);
    var ConnectorId="214";
    var Databasename="zmdb";
    var mappedtable="1";
    if(this.LoadDataList.length>0){
      var totallenght=this.serviceService.IdsCollectionArray.length;
      var totallenghtofload=this.LoadDataList.length;
      for(var i=0;i<this.serviceService.IdsCollectionArray.length; i++)
      {
      //   if(this.serviceService.IdsCollectionArray[i].connectorname=="Facebook Authorization"){
      //     this.SourceAccountLst.push({
      //       accountdisplayname:"",
      //       enableconnection:false,
      //       emailid:"",   
      //       authorizationgrant:"",   
      //       statusnotifygrant:"",   
      //       filedname:"Hostname",
      //       ConnectorType:"Facebook Authorization", 
      //       Filename:"",
      //       Filepath:JSON.parse(localStorage.getItem("FB")), 
      //       fieldvalue:{
      //       hostName:"",
      //       databaseName:"",
      //       port:"",
      //       username:"",
      //       password:"",
      //       },   
      //         bstatusforvarification:this.Bstatus, 
      //         tablename:[],
      //         transformed:""            
      //  });
      //   }
      // else if(this.serviceService.IdsCollectionArray[i].connectorname!="Facebook Authorization"){
        var ConnectorTypeId=this.serviceService.IdsCollectionArray[i].connectorId;
        var connectorIdForGettheTable="214";
        var AccountID=this.serviceService.IdsCollectionArray[i].accountid;
        var getStatus= this.stringmappingDictionary.getStringKey(this.serviceService.IdsCollectionArray[i].connectorname);
        this.GetSourceAccountList(this.serviceService.IdsCollectionArray[i].accountid,UserId,Workspaceid,Clientid,ConnectorTypeId,this.serviceService.IdsCollectionArray[i].connectorname).subscribe((data:updatesourceaccountStep2[])=>{
        debugger
          if(data.length > 0){
             if(data[0].connectorname=="Excel" || data[0].connectorname=="Csv")
             {
              if(data[0].bStatusforvarification=='1')
              {
               this.Bstatus="true"; 
              }
              else {
                this.Bstatus="false"; 
              }
              this.SourceAccountLst.push({
                accountdisplayname:data[0].accountdisplayname,
                enableconnection:data[0].enableconnection,
                emailid:data[0].emailid,   
                authorizationgrant:data[0].authorizationgrant,   
                statusnotifygrant:data[0].statusnotifyGrant,   
                filedname:"Hostname",
                ConnectorType:data[0].connectorname, 
                Filename:data[0].filedname,
                Filepath:data[0].fieldvalue, 
                fieldvalue:{
                hostName:"",
                databaseName:"",
                port:"",
                username:"",
                password:"",
                },   
                  bstatusforvarification:this.Bstatus, 
                  tablename:[],
                  transformed:data[0].transformed             
           });
             
             }
             else if(data[0].connectorname=="Facebook Authorization"){
              if(data[0].bStatusforvarification=='1')
              {
               this.Bstatus="true"; 
              }
              else {
                this.Bstatus="false"; 
              }
              this.SourceAccountLst.push({
                accountdisplayname:data[0].accountdisplayname,
                enableconnection:data[0].enableconnection,
                emailid:data[0].emailid,   
                authorizationgrant:data[0].authorizationgrant,   
                statusnotifygrant:data[0].statusnotifyGrant,   
                filedname:"Hostname",
                ConnectorType:data[0].connectorname, 
                Filename:data[0].filedname,
                Filepath:data[0].fieldvalue, 
                fieldvalue:{
                hostName:"",
                databaseName:"",
                port:"",
                username:"",
                password:"",
                },   
                  bstatusforvarification:this.Bstatus, 
                  tablename:[],
                  transformed:""            
           });
             }
             else if(data[0].connectorname=="Google Drive Download"){
              if(data[0].bStatusforvarification=='1')
              {
               this.Bstatus="true"; 
              }
              else {
                this.Bstatus="false"; 
              }
              this.SourceAccountLst.push({
                accountdisplayname:data[0].accountdisplayname,
                enableconnection:data[0].enableconnection,
                emailid:data[0].emailid,   
                authorizationgrant:data[0].authorizationgrant,   
                statusnotifygrant:data[0].statusnotifyGrant,   
                filedname:"Hostname",
                ConnectorType:data[0].connectorname, 
                Filename:data[0].filedname,
                Filepath:data[0].fieldvalue, 
                fieldvalue:{
                hostName:"",
                databaseName:"",
                port:"",
                username:"",
                password:"",
                },   
                  bstatusforvarification:this.Bstatus, 
                  tablename:[],
                  transformed:data[0].transformed     
                         
           });
             
  
             }
             else {
  
  
  
              if(data[0].bStatusforvarification=='1')
              {
               this.Bstatus="true"; 
              }
              else {
                this.Bstatus="false"; 
              }
              this.SourceAccountLst.push({
                accountdisplayname:data[0].accountdisplayname,
                enableconnection:data[0].enableconnection,
                emailid:data[0].emailid,   
                authorizationgrant:data[0].authorizationgrant,   
                statusnotifygrant:data[0].statusnotifyGrant,   
                filedname:"Hostname",
                ConnectorType:data[0].connectorname,  
                Filename:"",
                Filepath:"",
                fieldvalue:{
                hostName:data[1].fieldvalue,
                databaseName:data[0].fieldvalue,
                port:data[3].fieldvalue,
                username:data[4].fieldvalue,
                password:data[2].fieldvalue,
                },   
                  bstatusforvarification:this.Bstatus, 
                  tablename:[],
                  transformed:data[0].transformed        
               });
  
              }
           
            }
            debugger
        var getresponselength=this.SourceAccountLst.length;
         if(getresponselength==totallenght)
         { 
              for(var L=0;L<this.LoadDataList.length;L++)   
              {
                var connectorid='214';
                this.GetLoadSourceList(this.LoadDataList[L].AccountId,UserId,Workspaceid,Clientid,this.LoadDataList[L].CoonectorId,this.LoadDataList[L].ConnectorName).subscribe((data:DestinationList[])=>{
                   debugger
                  if(data.length>0){
                    debugger
                    this.LoadObject.push({
                      
                      Desaccountdisplayname:data[0].accountdisplayname,
                      Desfiledname:"Hostname",
                      DesConnectorType:data[0].connectorname, 
                      DesFilename:"",
                      DesFilepath:"",
                      Desfieldvalue:{
                        DeshostName:data[1].fieldvalue,
                        DesdatabaseName:data[0].fieldvalue,
                        Desport:data[3].fieldvalue,
                        Desusername:data[4].fieldvalue,
                        Despassword:data[2].fieldvalue,
                      },   
                         
                      Destablename:[data[0].objectname],
                      transformation:[]
                    })



                  }
                 
                   
                })
              }   
              // var totalload=this.LoadObject.length;
              //     if(totallenghtofload==totalload)
              //     {
              //       var myaccountid=this.serviceService.IdsCollectionArray[0].accountid;
              //       this.GetSourceObjectList(myaccountid,mappedtable,UserId,Clientid,Workspaceid,connectorIdForGettheTable,Databasename);
              //       console.log(this.SourceAccountLst);
              //     }   
              debugger
              //  var totalload=this.LoadObject.length;
              //  if(totallenghtofload==totalload)
              //  {
                for(var i=0;i<this.functionIdsarraylist.length;i++){
                  var functiongroupID='1';
                  var functionID='1002';
                  this.getFunctionDetailsForTransformation(UserId,Workspaceid,Clientid,functiongroupID,this.functionIdsarraylist[i]).subscribe((x:any)=>{
                    debugger
                    for(var z=0;z<x.length;z++)
                    {
                      this.perametername.push(x[z].functionPerameter);
                      this.perametrsvalue.push(x[z].perameterValues)
                       
    
                      this.transformationArrayList.push({
                        groupId:x[0].groupId,
                        functionId:x[0].functionId,
                        functionGroup:x[0].functionGroup,
                        functionName:x[0].functionName,
                        
                        functionPerameter:{
                              first1:this.perametername[0],
                              second2:this.perametername[1],
                              third3:this.perametername[2],
                              fourth4:this.perametername[3],
                        },
                        PerameterValues:{
                          first1:this.perametrsvalue[0],
                          second2:this.perametrsvalue[1],
                          third3:this.perametrsvalue[2],
                          fourth4:this.perametrsvalue[3],
                        },
                      });
       
    
    
                    
                      
                       
                    }
                    
                    this.FilterstheRowfromtransformation.push({
                      groupId:this.transformationArrayList[this.transformationArrayList.length-1].groupId,
                      functionGroup:this.transformationArrayList[this.transformationArrayList.length-1].functionGroup,
                      functionId:this.transformationArrayList[this.transformationArrayList.length-1].functionId,
                      functionName:this.transformationArrayList[this.transformationArrayList.length-1].functionName,
                      functionPerameter:this.transformationArrayList[this.transformationArrayList.length-1].functionPerameter,
                      PerameterValues:this.transformationArrayList[this.transformationArrayList.length-1].PerameterValues,
    
                    })
                    
                   if(this.FilterstheRowfromtransformation.length==this.functionIdsarraylist.length)
                   {
                    var myaccountid=this.serviceService.IdsCollectionArray[0].accountid;
                    this.GetSourceObjectList(myaccountid,mappedtable,UserId,Clientid,Workspaceid,connectorIdForGettheTable,Databasename);
                    console.log(this.SourceAccountLst);
                   }
                    
    
                  })
                }
              

               
               //}
             
          
         }
         })
        // }
       }
    }
    else{


    }
   
    }
 settablevalue(){
    
  for(var u=0;u<this.SourceAccountLst.length;u++){
    debugger
   this.SourceAccountLst[u].tablename.push(this.Pipline[0]);

  }
   console.log(this.SourceAccountLst);
 }
 
  
   GetSourceAccountList(Account_Id:string,UserId:string,Workspaceid:string,Clientid:string,ConnectorId:string,connectorname:string){
   
     let params = new HttpParams()
     .set("Account_Id",Account_Id)
     .set("UserId",UserId)
     .set("Workspaceid",Workspaceid)
     .set("Clientid",Clientid)
     .set("ConnectorId",ConnectorId)
     .set("connectorname",connectorname);
     return  this._http.get(`${this.GetSqlConnectorList}`,{params:params});
  
  }
  GetLoadSourceList(Account_Id:string,UserId:string,Workspaceid:string,Clientid:string,ConnectorId:string,connectorname:string){
    let params = new HttpParams()
    .set("Account_Id",Account_Id)
    .set("UserId",UserId)
    .set("Workspaceid",Workspaceid)
    .set("Clientid",Clientid)
    .set("ConnectorId",ConnectorId)
    .set("connectorname",connectorname);
    return  this._http.get(`${this.GetDesConnectorList}`,{params:params});
  }
      GetSourceObjectList(Account_Id:string,Mappedtable:string,UserId:string,Clientid:string,Workspaceid:string,ConnectorId:string,Databasename:string){
         
       let params = new HttpParams()
       .set("Account_Id", Account_Id)
       .set("Mappedtable", Mappedtable)
       .set("UserId", UserId)
       .set("Workspaceid",Workspaceid)
       .set("Clientid", Clientid)
       .set("ConnectorId",ConnectorId)
       .set("Databasename", Databasename);
   
        this._http.get(`${this.GetObjectListFromStep4}`,{params:params}).subscribe((data:any)=>{
             debugger
             if(data.length > 0){
                   for(let i=0;i<this.SourceAccountLst.length;i++){
                   
                   this.SourceAccountLst[i].tablename.push(data[0].objecT_NAME).toString();
                  //  this.Pipline.push(data[0].objecT_NAME);
                  //     this.SourceAccountLst.forEach((item)=>{
                  //         item.tablename.push(data[i].objecT_NAME).toString();
                  //           var tbl="FB_RAW_DATA";
                  //            item.tablename.push(tbl).toString();
                  //   }); 
                 
                   }
                  
      // var objtemp = {"pipeline":[],
      // // "connection":[]
      // };
      var transformtion=[];
      let transform;
      this.FilterstheRowfromtransformation.forEach((itemT)=>{
        var transforms=
        {
          [this.transformationcounter]: {
                        [itemT.functionName]: {
                            [itemT.functionPerameter.first1]: [
                              itemT.PerameterValues.first1
                            ],
                            [itemT.functionPerameter.second2]: itemT.PerameterValues.second2,
                            [itemT.functionPerameter.third3]: itemT.PerameterValues.third3,
                            [itemT.functionPerameter.fourth4]: itemT.PerameterValues.fourth4,
                        } 
                         
                    }
                    
                    
                    
          
            
        } 
        
         this.transformationcounter+=1;
         transformtion.push(transforms);
         transform=Object.assign({},...transformtion);
      })
      

      let objtemp=[];
         this.SourceAccountLst.forEach((item)=>{
           debugger
           
           if(item.ConnectorType=="Excel")
            { 
              
               
              this.LoadObject.forEach((Items)=>{
                // this.FilterstheRowfromtransformation.forEach((itemT)=>{
                  
                  var ExcelFile={
                    
                    [this.counter]:{
                      [item.ConnectorType]:{
                        "extract":{
                          "Database":item.ConnectorType,
                          "fileName":item.Filename,
                          "tablename":[item.Filename],
                          "ifstop":0 // 0 == STOP, 1 or any other number == NOT STOP
                  
                        },
                        "load":{
                          "Database":Items.DesConnectorType,
                          "serviceName":"orcl.CNSE.COM.PK",
                          "host":Items.Desfieldvalue.DeshostName,
                          "dbname":Items.Desfieldvalue.DesdatabaseName,
                          "user":Items.Desfieldvalue.Desusername,
                          "password":Items.Desfieldvalue.Despassword,
                          "tablename":Items.Destablename,
                          "port":Items.Desfieldvalue.Desport
                  
                        },
                        // "transformation":{
                        //   "destination":1,
                        //       "transform":item.transformed 
                        //   }
                        "transformation": {
                          "destination": 0,
                          transform
                          // "transform": {
                          //   transform
                              // [this.transformationcounter]: {
                              //     [itemT.functionName]: {
                              //         [itemT.functionPerameter.first1]: [
                              //           itemT.PerameterValues.first1
                              //         ],
                              //         [itemT.functionPerameter.second2]: itemT.PerameterValues.second2,
                              //         [itemT.functionPerameter.third3]: itemT.PerameterValues.third3,
                              //         [itemT.functionPerameter.fourth4]: itemT.PerameterValues.fourth4,
                              //     } 
                              // }
                         // }
                      },
              "userInfo":{
  
                "SOURCE_STG_ID":"",
                "SOURCE_STG_NAME":"",
                "SOURCE_NAME":item.accountdisplayname,
                "WORKSPACE_NAME":""
                }
                    }
                  }
                 }
                 objtemp.push(ExcelFile);
                 this.counter+=1;
                  
                })
                // })
              
             
              // objtemp.pipeline.push(ExcelFile);
              
  
              
                
            }
            else if(item.ConnectorType=="Google Drive Download"){
              this.LoadObject.forEach((Items)=>{
                
                this.FilterstheRowfromtransformation.forEach((itemT)=>{


                
              var googledrive={
                [this.counter]:{
                  [item.ConnectorType]:{
                    "extract":{
                      "Database":item.ConnectorType,
                      "fileName":item.Filename,
                      "tablename":[item.Filename],
                      "ifstop":0 // 0 == STOP, 1 or any other number == NOT STOP
              
                    },
                    "load":{
                      "Database":Items.DesConnectorType,
                      "serviceName":"orcl.CNSE.COM.PK",
                      "host":Items.Desfieldvalue.DeshostName,
                      "dbname":Items.Desfieldvalue.DesdatabaseName,
                      "user":Items.Desfieldvalue.Desusername,
                      "password":Items.Desfieldvalue.Despassword,
                      "tablename":Items.Destablename,
                      "port":Items.Desfieldvalue.Desport
              
                    },
                    // "transformation":{
                    //   "destination":1,
                    //       "transform":item.transformed 
                    //   }
                    "transformation": {
                      "destination": 0,
                      "transform": {
                        [this.transformationcounter]: {
                          [itemT.functionName]: {
                              [itemT.functionPerameter.first1]: [
                                itemT.PerameterValues.first1
                              ],
                              [itemT.functionPerameter.second2]: itemT.PerameterValues.second2,
                              [itemT.functionPerameter.third3]: itemT.PerameterValues.third3,
                              [itemT.functionPerameter.fourth4]: itemT.PerameterValues.fourth4,
                          } 
                      }
                      }
                  },
                  "userInfo":{
      
                    "SOURCE_STG_ID":"",
                    "SOURCE_STG_NAME":"",
                    "SOURCE_NAME":item.accountdisplayname,
                    "WORKSPACE_NAME":""
                    }
                }
              }
             }
             objtemp.push(googledrive);
  
             this.counter+=1;
            })
            })
              // objtemp.pipeline.push(ExcelFile);
              
            }
            else if(item.ConnectorType=="Facebook Authorization"){
              



             
              this.LoadObject.forEach((Items)=>{
                this.FilterstheRowfromtransformation.forEach((itemT)=>{
              var Fbtoken={
                [this.counter]:{
                  [item.ConnectorType]:{
                    "extract":{
                      "Database":item.ConnectorType,
                      // "token":item.Filepath,
                      "token":"EAAF2J67mtZAkBAHT5ftMTDG0y3ZBfy6LA6x28hZBIRlcHhshhZAdRxLZAEhQFfcEyBd8sIZCFZB6VNhQLVPQbXQ81PpCBGtxxTfeAlyfy2UMdAeLaIncPXlNA63vCHp0w9pVzhLPuJn3WXOwECLZBYAdaF6SWHufVZAWJElVPPsV3bN4eTZAiuxk19",
                      "tablename": 
                        item.tablename,
                     
                      "ifstop":0 // 0 == STOP, 1 or any other number == NOT STOP
              
                    },
                    "load":{
                      "Database":Items.DesConnectorType,
                      "serviceName":"orcl.CNSE.COM.PK",
                      "host":Items.Desfieldvalue.DeshostName,
                      "dbname":Items.Desfieldvalue.DesdatabaseName,
                      "user":Items.Desfieldvalue.Desusername,
                      "password":Items.Desfieldvalue.Despassword,
                      "tablename":Items.Destablename,
                      "port":Items.Desfieldvalue.Desport
              
                    },
                    // "transformation":{
                    //   "destination":1,
                    //       "transform":{
                    //           "e1": {
                    //               "fill_null_values": {
                    //                   "replacement_dict":{"account_id":6000},
                    //                   "create_col": false,
                    //                   "save_previous_changes": false
                    //               },
                    //               "convert_to_upper_case":{
                    //                   "cols":["email_address", "user_login_id"],
                    //                   "create_col":false,
                    //                   "save_previous_changes":false
                    //               },
                    //               "remove_multiline": {
                    //                   "cols": ["*"],
                    //                   "create_col": false,
                    //                   "save_previous_changes": false
                    //               },
                    //               "remove_unicode_characters": {
                    //                   "cols": ["*"],
                    //                   "create_col": false,
                    //                   "save_previous_changes": false
                    //               }
                    //           }
                    //       }
                    //   }
                    "transformation": {
                      "destination": 0,
                      "transform": {
                        [this.transformationcounter]: {
                          [itemT.functionName]: {
                              [itemT.functionPerameter.first1]: [
                                itemT.PerameterValues.first1
                              ],
                              [itemT.functionPerameter.second2]: itemT.PerameterValues.second2,
                              [itemT.functionPerameter.third3]: itemT.PerameterValues.third3,
                              [itemT.functionPerameter.fourth4]: itemT.PerameterValues.fourth4,
                          } 
                      }
                      }
                  },
                  "userInfo":{
      
                    "SOURCE_STG_ID":"",
                    "SOURCE_STG_NAME":"",
                    "SOURCE_NAME":"Facebook Public Page",
                    "WORKSPACE_NAME":""
                    }
                }
              }
             }
             objtemp.push(Fbtoken);
  
             this.counter+=1;
            })
            })
              // objtemp.pipeline.push(ExcelFile);
              
            }
          //  else if(item.ConnectorType=="My SQL")
          //  {
            //           var mySql={
            //             [this.counter]:{
            //               [item.ConnectorType]:{
            //                 "extract":{
            //                   "Database":"mysql",
            //                   "host":item.fieldvalue.hostName,
            //                   "dbname":item.fieldvalue.databaseName,
            //                   "user":item.fieldvalue.username,
            //                   "password":item.fieldvalue.password,
            //                   "tablename":item.tablename,
            //                   "port":item.fieldvalue.port
            //                 },
            //                 "load":{
            //                   "Database":"mssql",
            //                   "host":"192.168.222.111",
            //                   "dbname":"zmdb",
            //                   "user":"sa",
            //                   "password":"CNSE@12345",
            //                   "tablename":"FCTEXTALL",
            //                   "port":"" 
                      
            //                 }
            //             }
            //           }
            // }
           // objtemp.pipeline.push(mySql);
          //  objtemp.push(mySql);

          //   this.counter+=1;
          // }
          // else if(item.ConnectorType=="Mongo DB")
          // {
          //   var MongoDB={
          //     [this.counter]:{
          //       [item.ConnectorType]:{
          //         "extract":{
          //           "Database":"",
          //           "host":item.fieldvalue.hostName,
          //           "dbname":item.fieldvalue.databaseName,
          //           "user":item.fieldvalue.username,
          //           "password":item.fieldvalue.password,
          //           "tablename":item.tablename,
          //           "port":item.fieldvalue.port
            
          //         },
          //         "load":{
          //           "Database":"mssql",
          //           "host":"192.168.222.111",
          //           "dbname":"zmdb",
          //           "user":"sa",
          //           "password":"CNSE@12345",
          //           "tablename":"FCTEXTALL",
          //           "port":""
            
          //         }
          //     }
          //   }
          //  }
            //objtemp.pipeline.push(MongoDB);
            // objtemp.push(MongoDB);
            // this.counter+=1;

          // }
         else if(item.ConnectorType=="My SQL"){
          this.LoadObject.forEach((Items)=>{

            this.FilterstheRowfromtransformation.forEach((itemT)=>{


          
          var obj={
            [this.counter]:{
              
               [item.ConnectorType]:{
                    "extract":{
                    "Database":item.ConnectorType,
                    "host":item.fieldvalue.hostName,
                    "dbname":item.fieldvalue.databaseName,
                    "user":item.fieldvalue.username,
                    "password":item.fieldvalue.password,
                    "tablename":['ACCOUNT_USER_SRC'],
                    "port":item.fieldvalue.port,
                    "ifstop":0 // 0 == STOP, 1 or any other number == NOT STOP
                    },
           "load":{
            "Database":Items.DesConnectorType,
            "serviceName":"orcl.CNSE.COM.PK",
            "host":Items.Desfieldvalue.DeshostName,
            "dbname":Items.Desfieldvalue.DesdatabaseName,
            "user":Items.Desfieldvalue.Desusername,
            "password":Items.Desfieldvalue.Despassword,
            "tablename":Items.Destablename,
            "port":Items.Desfieldvalue.Desport
     
           },
          //  "transformation":{
          //   "destination":1,
          //       "transform":item.transformed 
          //   }
          "transformation": {
            "destination": 0,
            "transform": {
              [this.transformationcounter]: {
                [itemT.functionName]: {
                    [itemT.functionPerameter.first1]: [
                      itemT.PerameterValues.first1
                    ],
                    [itemT.functionPerameter.second2]: itemT.PerameterValues.second2,
                    [itemT.functionPerameter.third3]: itemT.PerameterValues.third3,
                    [itemT.functionPerameter.fourth4]: itemT.PerameterValues.fourth4,
                } 
            }
            }
        },
        "userInfo":{

          "SOURCE_STG_ID":"",
          "SOURCE_STG_NAME":"",
          "SOURCE_NAME":item.accountdisplayname,
          "WORKSPACE_NAME":""
          }
         
         }
       }
      }
      objtemp.push(obj);
      this.counter+=1;
    })
    })
        // objtemp.pipeline.push(obj);
       

          


          }
          else{
            this.LoadObject.forEach((Items)=>{
              this.FilterstheRowfromtransformation.forEach((itemT)=>{
            var objmongo={
              [this.counter]:{
                
                 [item.ConnectorType]:{
                      "extract":{
                      "Database":item.ConnectorType,
                      "host":item.fieldvalue.hostName,
                      "dbname":item.fieldvalue.databaseName,
                      "user":item.fieldvalue.username,
                      "password":item.fieldvalue.password,
                      "tablename":['ACCOUNT_USER_SRC'],
                      "port":item.fieldvalue.port,
                      "ifstop":0 // 0 == STOP, 1 or any other number == NOT STOP
                      },
                 "load":{
                   "Database":Items.DesConnectorType,
                   "serviceName":"orcl.CNSE.COM.PK",
                   "host":Items.Desfieldvalue.DeshostName,
                   "dbname":Items.Desfieldvalue.DesdatabaseName,
                   "user":Items.Desfieldvalue.Desusername,
                   "password":Items.Desfieldvalue.Despassword,
                   "tablename":Items.Destablename,
                   "port":Items.Desfieldvalue.Desport
       
             },
            //  "transformation":{
            //   "destination":1,
            //       "transform":item.transformed
            //   }
            "transformation": {
              "destination": 0,
              "transform": {
                [this.transformationcounter]: {
                  [itemT.functionName]: {
                      [itemT.functionPerameter.first1]: [
                        itemT.PerameterValues.first1
                      ],
                      [itemT.functionPerameter.second2]: itemT.PerameterValues.second2,
                      [itemT.functionPerameter.third3]: itemT.PerameterValues.third3,
                      [itemT.functionPerameter.fourth4]: itemT.PerameterValues.fourth4,
                  } 
              }
              }
          },
          "userInfo":{

            "SOURCE_STG_ID":"",
            "SOURCE_STG_NAME":"",
            "SOURCE_NAME":item.accountdisplayname,
            "WORKSPACE_NAME":""
            }
           
           }
         }
        }
        objtemp.push(objmongo);
        this.counter+=1;
      })
      })
          // objtemp.pipeline.push(obj);
         
  
             
  
  
            }
         
         })


         debugger
         var  object=Object.assign({},...objtemp);
         console.log(object);
          
         var mainobj={"pipeline":object
            ,
            "userInfo":{

              "SOURCE_STG_ID":"",
              "SOURCE_STG_NAME":"",
              "SOURCE_NAME":"",
              "WORKSPACE_NAME":""
              }
        };
         console.log("pipeline",mainobj);
         const blob = new Blob([JSON.stringify(mainobj)], {type : 'application/json'});
           FileSaver.saveAs(blob, 'Pipeline.json');
        // console.log(objtemp);
            //const blob = new Blob([JSON.stringify( mainobj)], {type : 'application/json'});

           //const blob = new Blob([JSON.stringify( objtemp)], {type : 'application/json'});
 

        //  const blob = new Blob(([mainobj]), {type : 'application/json'});
       // FileSaver.saveAs(blob, 'DBPipeline.json');
      // const blob = new Blob([JSON.stringify( obj)], {type : 'application/json'});
 
       // FileSaver.saveAs(blob, 'DBPipeline.json');
        let headers = new Headers();
        
        
      headers.append('Access-Control-Allow-Headers', 'Content-Type');
      headers.append('Access-Control-Allow-Methods', 'GET');
      headers.append('Access-Control-Allow-Origin', '*'); 
      this._http.post('http://192.168.223.102:4444/extractTransformLoad',mainobj,{responseType: 'text'}).subscribe((data:any)=>{
       debugger
        var data=JSON.parse(data);
         alert(data.message);
         location.reload();
        
      
     });
      // this._http.post('http://localhost:4444/extractTransformLoad',mainobj,{responseType: 'text'}).subscribe((data:any)=>{
      //   debugger
      //    var data=JSON.parse(data);
      //     alert(data.message);
         
       
      //  });
       
           }
       
        

       })
     
     }

  AccessGrantValidation(connectorIds: ExtractSqlTableData[],Account_Id:string,UserId1:string,Workspaceid1:string,Clientid1:string){
     
    let params = new HttpParams()
   .set("Account_Id", Account_Id)
   .set("UserId", UserId1)
   .set("Workspaceid",Workspaceid1)
   .set("Clientid", Clientid1);
    
   return this._http.post(this.checkUserAccessGrant,connectorIds,{params:params});
     
   
  
  }


  getFunctionDetailsForTransformation(userId:string,workspaceId:string,clientID:string,functionGroupId:string,functionId:string){
     
    let params = new HttpParams()
   .set("userId",userId)
   .set("workspaceId",workspaceId)
   .set("clientID",clientID)
   .set("functionGroupId",functionGroupId)
   .set("functionId",functionId);
    return this._http.get(this.gettransformationfunctiondetails,{params:params});
     
   
  
  }


   
}