import { Injectable } from '@angular/core';
import { HttpParams, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

_graphData= environment.apiUrl+'/api/GridAndGraph/Graphdata';

  constructor(private _http:HttpClient) {
   }
  get(): Observable<any>{
    
  return this._http.get<any>(`${this._graphData}`)
  }
}
