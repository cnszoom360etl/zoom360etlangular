import { Injectable, Injector } from '@angular/core';
import { HttpParams, HttpClient } from '@angular/common/http';
import { ConnectionListModel } from '../../../../src/app/models/connect.model';
import { ArraySortPipe } from '../../../../src/app/shared/pipes/array-pipes';
import { FiltersService } from '../common/filters.service';
import{environment} from '../../../../src/environments/environment';
import { listconnection } from 'src/app/models/extract/Step6ObjectFieldListModelClass';
import { Router } from '@angular/router';
import { AppComponentBase } from 'src/app/services/AppComponentBase';
import { inject } from '@angular/core/testing';
import { filtersModel } from 'src/app/models/extract/filter.model';
@Injectable({
  providedIn: 'root'
})
export class GetAllConnectionsService extends AppComponentBase{
//_urlAllConnections='https://localhost:44357/api/ConnectionList/GetConnectionlist';
_urlAllConnections=environment.apiUrl+'/api/AllExtract/GetConnectionList';
userId:string=null;
workSpaceId:string=null;
client_id:string=null;

_connectionsearch : ConnectionListModel[]=[];
_connectionEdit : ConnectionListModel[]=[];
_recordLength:number=0;
_sortToggle:boolean=false;
_tablecounter:number=0;
accountId:string='all';
connectionLogo:string;
connectionName:string;
arraylist:listconnection[]=[];
_connectionData: any[]=[];
  constructor(private _http:HttpClient,private sort: ArraySortPipe, private filterService:FiltersService,
    private rout:Router,injector : Injector) {
      super(injector)
      this.userId=this.storageService.getItem(environment.storage.userId);
      this.workSpaceId=this.storageService.getItem(environment.storage.workspaceId);
      this.client_id=this.storageService.getItem(environment.storage.clientId);
    // this.accountId= JSON.parse(localStorage.getItem("SourceId"));
   }
  getAllConnectionswithLimit(filtersValue:filtersModel){
      debugger
    this._connectionData.splice(0,this._connectionData.length); 
   // this.accountId= localStorage.getItem("accountId");
    this._recordLength=0;
   // var clientId=this.storageService.getItem(environment.storage.clientId);
    let params = new HttpParams()
    .set("userId", filtersValue.userId)
    .set("workSpaceId",filtersValue.workspaceId)
    .set("clientId",filtersValue.clientId)
    .set("accountId", filtersValue.accountId)
    .set("workspaceName", filtersValue.workspaceName)
    .set("connectionName", filtersValue.connectionName)
    .set("sourceName", filtersValue.sourceName)
    .set("accessGranted", filtersValue.accessGranted)
    .set("createdBy", filtersValue.createdBy)
    .set("isActive", filtersValue.isActive)
    .set("lastAccessed",filtersValue.lastAccessed)
    .set("destinationEnabled", filtersValue.destinationEnabled)
    .set("TimeFilter", filtersValue.TimeFilter);
  this._http.get(`${this._urlAllConnections}`,{params:params}).subscribe((data: ConnectionListModel[]) => { 
    if(data.length  > 0){
        debugger
        this._connectionData= data;
        for(let i=0;i<this._connectionData.length;i++)
        {
          if(this._connectionData[i].accessgranted==null)
          {
            this._connectionData[i].accessgranted="no";
          }
        }
        
        this._connectionsearch = data;
        this.connectionName=data[0].connectorName;
        this.connectionLogo=data[0].appearanceLogo;
        this.filterService._recordLength = data.length;
        this._recordLength=data.length;
        
       // this.rout.navigate(['extract/source/overview']);
        console.log(this._connectionEdit);
      }
  });
     }
searchAllConnections(text){
      this._connectionData = this._connectionsearch;
      this._connectionData = this._connectionsearch.filter((data) =>data.connectorName.toLowerCase().includes(text.toLowerCase()));
      this._recordLength = this._connectionData.length;
      this.filterService._recordLength=this._connectionData.length;
      //t => t.title.toLowerCase().includes(this.query.toLowerCase())
    }
    sortAllissues(field){
      if(this._sortToggle){
         this._connectionData = this.sort.transform(this._connectionData,field);
       }
     
       else if(!this._sortToggle){
         this._connectionData = this.sort.transform2(this._connectionData,field);
       }
     
       }
    getAllConnectionswithfilter(workspaceName,connectionName,sourceName,accessGranted
        ,createdBy,isActive,lastAccessed,destinationEnabled,limit){
          debugger
        this._connectionData.splice(0,this._connectionData.length); 
        this.accountId= localStorage.getItem("accountId");
        this._recordLength=0;
        let params = new HttpParams()
        .set("userId", this.userId)
        .set("workSpaceId", ''+this.workSpaceId)
        .set("CLIENT_ID", this.client_id)
        .set("accountId", this.accountId)
        .set("workspaceName", workspaceName)
        .set("connectionName", connectionName)
        .set("sourceName", sourceName)
        .set("accessGranted", accessGranted)
        .set("createdBy", createdBy)
        .set("isActive", isActive)
        .set("lastAccessed", lastAccessed)
        .set("destinationEnabled", destinationEnabled);
        
      return this._http.get(`${this._urlAllConnections}`,{params:params})
         }



         getSourceswithLimit(workspaceName,connectionName,sourceName,accessGranted
          ,createdBy,isActive,lastAccessed,destinationEnabled,limit){
            debugger
          this._connectionData.splice(0,this._connectionData.length); 
          this.accountId= localStorage.getItem("accountId");
          this._recordLength=0;
          var clientId=this.storageService.getItem(environment.storage.clientId);
          let params = new HttpParams()
          .set("userId", this.userId)
          .set("workSpaceId", ''+this.workSpaceId)
          .set("CLIENT_ID", clientId)
          .set("accountId", this.accountId)
          .set("workspaceName", workspaceName)
          .set("connectionName", connectionName)
          .set("sourceName", sourceName)
          .set("accessGranted", accessGranted)
          .set("createdBy", createdBy)
          .set("isActive", isActive)
          .set("lastAccessed",lastAccessed)
          .set("destinationEnabled", destinationEnabled);
          
        this._http.get(`${this._urlAllConnections}`,{params:params}).subscribe((data: ConnectionListModel[]) => { 
          if(data.length  > 0){
              debugger
              this._connectionData= data;
              this._connectionsearch = data;
              this.connectionName=data[0].connectorName;
              this.connectionLogo=data[0].appearanceLogo;
              this.filterService._recordLength = data.length;
              this._recordLength=data.length;
              
              this.rout.navigate(['extract/source/overview']);
              console.log(this._connectionEdit);
            }
        });
            }
}
