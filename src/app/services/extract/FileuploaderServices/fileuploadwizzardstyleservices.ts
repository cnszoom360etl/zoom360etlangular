import { Injectable } from '@angular/core';
  
@Injectable({
  providedIn: 'root'
})
export class fileuploaderwizrad {
  
    ConnectionTypeWizardClass="css-fileupload"
    SourceAccountDisable=true;
    SourceAccountClass="contentmapping"
    TempleteAccountWizard=true;
    TempleteWizardClass="ColumnMapping";
    ApplyTransformationDisabled=true;
    ApplyTransformationClass="applytransformation";
    FilePreviewDisabled=true;
    FilePreviewClass="FilePreview";
    // FilterWizzard=true;
    // FilterWizzardClass="css-filterwizard";
    // FilterSelectionWizard=true;
    // FilterSelectionClass="css-FilterSelectionwizard";
    // ExecitionPlanWizard=true;
    // ExecitionPlanWizardClass="css-ExecutionPlanWizard";
    // ExtractWizard=true;
    // ExtractWizardClass="css-ExractWizard";

    constructor() {
   
   
    }


}