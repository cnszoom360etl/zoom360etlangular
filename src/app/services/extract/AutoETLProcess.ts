import { Injectable, Injector } from '@angular/core';
import {HttpClient,HttpHeaders, HttpParams} from '@angular/common/http';
import { ServiceService } from './service.service';

import { AutoETLExtaction } from 'src/app/models/extract/ExtractDataModel';
import { TouchSequence } from 'selenium-webdriver';
import { stringmappingDictionary } from 'src/app/utils/ExtractMapping';
import { DBConnectorIdsAndAccountIdsForExtract, etlstatus } from 'src/app/models/extract/ExtractDataModel';
import { DestinationList, ExtractAutoETL, ExtractDataSave, FilterfunctionDetails, functionDetails, LoadDataSave, updatesourceaccountStep2 } from 'src/app/models/extract/access-microsoft-sqlserver';
import { environment } from 'src/environments/environment';
import * as FileSaver from 'file-saver';
import { DestinationExtractModel } from 'src/app/models/Load/destinationExtractModel';
import { GetAllConnectionsService } from './get-all-connections.service';
import { AppComponentBase } from 'src/app/services/AppComponentBase';
import { inject } from '@angular/core/testing';
 
@Injectable({
  providedIn: 'root'
})
export class AutoEtlServics extends AppComponentBase {
  allcheckbox:boolean=false;
   
    url='https://localhost:44397';
    checkUserAccessGrant=environment.apiUrl+'/api/Extraction/checkuserRights';
    GetSqlConnectorList=environment.apiUrl+'/api/SqlConnector/GetSqlConnectorList';
    Etlconnectordetail=environment.apiUrl+'/api/SqlConnector/ETLConnectorList';
    GetDesConnectorList=environment.apiUrl+'/api/Destination/GetDestinationConnectorList';
    GetObjectListFromStep4=environment.apiUrl+'/api/SqlConnector/GetSourceObjectListForStep4Grid';
    gettransformationfunctiondetails=environment.apiUrl+'/api/SqlConnector/getFunctionList';
    getstatus=environment.apiUrl+'/api/DynamicEnrichScript/ETLStatus';
     data:any[]=[];
     status:etlstatus;
     ExtractStatus:number=0;
     TransformStatus:number=0;
     LoadStatus:number=0;
     interval:NodeJS.Timer;
     DBConnectorIdsForTable:DBConnectorIdsAndAccountIdsForExtract[]=[];
     NotAccreessGrantedArray:any[]=[];
     stringmappingDictionary=new stringmappingDictionary();
     SourceAccountLst:ExtractAutoETL[]=[];
     accessgrantedarray:any[]=[];
     Message:string;
     MessageStatusForSHowHide=false;
     MessagefornotAccessGranted:string;
     ExpireTokenMessageBox=false;
     ExpireTokenMessage:string
     MessageStatusForNotAccessGranted=false;
     token_expire_connector:number[]=[];
    //  TES: ExtractSqlTableData[] = []
     key:string;
     Bstatus:string;
     GroupName:string;
     Pipline:any[]=[];
    counter:number= 1;
    transformationcounter:number=1;
    LoadDataList:DestinationExtractModel[]=[];
    LoadObject:LoadDataSave[]=[];
    transformationArrayList:functionDetails[]=[];
    perametrsvalue:string[]=[];
    perametername:string[]=[];
    FilterstheRowfromtransformation:FilterfunctionDetails[]=[];
    functionIdsarraylist:any[] = [1001,1002,1003];

      
     
     
     

     SourceAccountObjectLst:any[]=[];
      resultkeyname; 
    resultkeyvalue:any[]=[]; 
    constructor(private _http: HttpClient ,public serviceService:ServiceService,public allConService:GetAllConnectionsService,injector : Injector) {
      super(injector)
   }
    

   AutoETLextractdata(){
    debugger
    var UserId=this.storageService.getItem(environment.storage.userId);
    var Workspaceid=this.storageService.getItem(environment.storage.workspaceId);
    var Clientid=this.storageService.getItem(environment.storage.clientId);
    var ConnectorId="214";
    var Databasename="zmdb";
    var mappedtable="1";
    this.token_expire_connector=[];
    this.SourceAccountLst=[];
      // for(var i=0;i<this.serviceService.AutoETLConnectionArrayForExtact.length; i++)
      // {
          
          // var totallenght=this.serviceService.AutoETLConnectionArrayForExtact.length;
          // var ConnectorTypeId=this.serviceService.AutoETLConnectionArrayForExtact[i].connectorId;
          // var connectorIdForGettheTable="214";
          // var AccountID=this.serviceService.AutoETLConnectionArrayForExtact[i].accountid;
          // var getStatus= this.stringmappingDictionary.getStringKey(this.serviceService.AutoETLConnectionArrayForExtact[i].connectorname);
          this.GetSourceAccountList(this.serviceService.AutoETLConnectionArrayForExtact,UserId,Workspaceid,Clientid).subscribe((data:updatesourceaccountStep2[])=>{
          debugger
            if(data.length > 0){
              for(var i=0;i<data.length;i++)
              {
                this.GroupName=this.stringmappingDictionary.getStringKey(data[i].connectorname);
                if(this.GroupName=="SocialMediaExtraction" || this.GroupName=="FileExtraction")
                {
                  debugger
                 if(data[i].bStatusforvarification=='1')
                    {
                     this.Bstatus="true"; 
                    }
                    else {
                      this.Bstatus="false"; 
                    }
                    this.SourceAccountLst.push({
                      accountid:data[i].accountId,
                      accountdisplayname:data[i].accountdisplayname,
                      enableconnection:data[i].enableconnection,
                      emailid:data[i].emailid,   
                      authorizationgrant:data[i].authorizationgrant,   
                      statusnotifygrant:data[i].statusnotifyGrant,   
                      filedname:"Hostname",
                      ConnectorType:data[i].connectorname, 
                      Filename:data[i].filedname,
                      Filepath:data[i].fieldvalue, 
                      fieldvalue:{
                      hostName:"",
                      databaseName:"",
                      port:"",
                      username:"",
                      password:"",
                      },   
                        bstatusforvarification:this.Bstatus, 
                        tablename:[],
                        transformed:data[i].transformed             
                 });
                  
               
            }
              else if (this.GroupName=="DBExtraction") {
                     // totallenght += data.length-1;
                     // for(var u=0;u<data.length;u++)
                     // {
                       if(data[i].bStatusforvarification=='1')
                       {
                        this.Bstatus="true"; 
                       }
                       else {
                         this.Bstatus="false"; 
                       }
                       this.SourceAccountLst.push({
                         accountid:data[i].accountId,
                         accountdisplayname:data[i].accountdisplayname,
                         enableconnection:data[i].enableconnection,
                         emailid:data[i].emailid,   
                         authorizationgrant:data[i].authorizationgrant,   
                         statusnotifygrant:data[i].statusnotifyGrant,   
                         filedname:"Hostname",
                         ConnectorType:data[i].connectorname,  
                         Filename:"",
                         Filepath:"",
                         fieldvalue:{
                         hostName:data[i].hostname,
                         databaseName:data[i].database,
                         port:data[i].port,
                         username:data[i].username,
                         password:data[i].password,
                         },   
                           bstatusforvarification:this.Bstatus, 
                           tablename:['FCTEXTALL'],
                           transformed:data[i].transformed        
                        });
                     // }
                 
     
                 }
              }
             
              this.GetSourceObjectList();
              }
              
              
                  
                 
                
              // console.log(this.token_expire_connector.length);
              // if(this.token_expire_connector.length==0)
              // {
                
                
              // }
              // else{
              //   var sourcelistlength=totallenght-this.token_expire_connector.length;
              //   if(this.SourceAccountLst.length==sourcelistlength)
              //   {
              //     debugger
                  
              //     this.GetSourceObjectList();
              //    } 
              // }
             
              
         
           })
           
       
      
      //  }
     
      
   
    }
 
 GetSourceAccountList(listData:AutoETLExtaction[],UserId:string,Workspaceid:string,Clientid:string){
   
     let params = new HttpParams()
    
     .set("UserId",UserId)
     .set("Workspaceid",Workspaceid)
     .set("Clientid",Clientid);
  
     return  this._http.post(`${this.Etlconnectordetail}`,listData,{params:params});
  
  }
 GetSourceObjectList(){
         
     
                  
    let objtemp=[];
         this.SourceAccountLst.forEach((item)=>{
           debugger
           this.GroupName=this.stringmappingDictionary.getStringKey(item.ConnectorType);
             if(this.GroupName=="FileExtraction")
            { 
               var ExcelFile={
                   [this.counter]:{
                      [item.ConnectorType]:{
                        "extract":{
                          "account_id":item.accountid, 
                          "Database":item.ConnectorType,
                          "filename":item.Filename,
                          "tablename":item.tablename,
                          "ifstop":0 // 0 == STOP, 1 or any other number == NOT STOP
                  
                        },
                        "userInfo":{

                          "SOURCE_STG_ID":"",
                          "SOURCE_STG_NAME":"",
                          "SOURCE_NAME":item.accountdisplayname,
                          "WORKSPACE_NAME":""
                          }
                    }
                    
                  }
                 }
                 objtemp.push(ExcelFile);
                 this.counter+=1;
             }
            
            else if(this.GroupName=="SocialMediaExtraction"){
              
             var Fbtoken={
                [this.counter]:{
                  [item.ConnectorType]:{
                    "extract":{
                      "account_id":item.accountid, 
                      "Database":item.ConnectorType,
                      "token":item.Filepath,
                      "tablename": 
                                   item.tablename,
                      "ifstop":0 // 0 == STOP, 1 or any other number == NOT STOP
              
                    },
                    "userInfo":{
                      "SOURCE_STG_ID":"",
                      "SOURCE_STG_NAME":"",
                      "SOURCE_NAME":item.accountdisplayname,
                      "WORKSPACE_NAME":""
                      }
                }
                
              }
             }
             objtemp.push(Fbtoken);
  
             this.counter+=1;
        }
         
         else if(this.GroupName=="DBExtraction"){
           if(item.ConnectorType=="Oracle DBMS")
           {
            var objoracle={
              [this.counter]:{
                
                 [item.ConnectorType]:{
                      "extract":{
                      "serviceName":"orcl.CNSE.COM.PK",
                      "account_id":item.accountid, 
                      "Database":item.ConnectorType,
                      "host":item.fieldvalue.hostName,
                      "dbname":item.fieldvalue.databaseName,
                      "user":item.fieldvalue.username,
                      "password":item.fieldvalue.password,
                      "tablename":item.tablename,
                      "port":item.fieldvalue.port,
                      "ifstop":0 // 0 == STOP, 1 or any other number == NOT STOP
                      },
                      "userInfo":{
                        "SOURCE_STG_ID":"",
                        "SOURCE_STG_NAME":"",
                        "SOURCE_NAME":item.accountdisplayname,
                        "WORKSPACE_NAME":""
                        }
                 }
                 
                
         }
        }
        objtemp.push(objoracle);
        this.counter+=1;
           }
           else{
            var obj={
              [this.counter]:{
                
                 [item.ConnectorType]:{
                      "extract":{
                      "account_id":item.accountid, 
                      "Database":item.ConnectorType,
                      "host":item.fieldvalue.hostName,
                      "dbname":item.fieldvalue.databaseName,
                      "user":item.fieldvalue.username,
                      "password":item.fieldvalue.password,
                      "tablename":item.tablename,
                      "port":item.fieldvalue.port,
                      "ifstop":0 // 0 == STOP, 1 or any other number == NOT STOP
                      },
                      "userInfo":{
                        "SOURCE_STG_ID":"",
                        "SOURCE_STG_NAME":"",
                        "SOURCE_NAME":item.accountdisplayname,
                        "WORKSPACE_NAME":""
                        }
                 }
                 
                
         }
        }
        objtemp.push(obj);
        this.counter+=1;
           }
          
     
    }
 })


         debugger
         var  object=Object.assign({},...objtemp);
         console.log(object);
          
         var mainobj={"pipeline":object
            ,
            "userInfo":{

              "SOURCE_STG_ID":"",
              "SOURCE_STG_NAME":"",
              "SOURCE_NAME":"",
              "WORKSPACE_NAME":""
              }
        };
         console.log("pipeline",mainobj);
         const blob = new Blob([JSON.stringify(mainobj)], {type : 'application/json'});
           FileSaver.saveAs(blob, 'Pipeline.json');
        
        let headers = new Headers();
        
        
      headers.append('Access-Control-Allow-Headers', 'Content-Type');
      headers.append('Access-Control-Allow-Methods', 'GET');
      headers.append('Access-Control-Allow-Origin', '*'); 
      this._http.post('http://192.168.223.102:4444/autoetl',mainobj,{responseType: 'text'}).subscribe((data:any)=>{
       debugger
        var data=JSON.parse(data);
         if(data.message=="Error")
         {
          this.Message=data.message;
         }
         else{
          this.Message="Extraction Complete Successfully";
          // clearInterval(this.interval);
          this.serviceService.AutoETLConnectionArrayForExtact=[];
          this.allcheckbox=false;
          
         }
        
      
     });
  //      this.interval = setInterval(() => {
  //     this._http.get(`${this.getstatus}`).subscribe((data:any)=>{
  //       debugger
  //        var data=JSON.parse(data);
  //          this.ExtractStatus=data[0].Extract;
  //          this.TransformStatus=data[0].Transform;
  //          this.LoadStatus=data[0].Load;
  //       });
       
     
  //  }, 2000);
    
      //   }
      // })
     
     }

  // AccessGrantValidation(connectorIds: ExtractSqlTableData[],Account_Id:string,UserId1:string,Workspaceid1:string,Clientid1:string){
     
  //   let params = new HttpParams()
  //  .set("Account_Id", Account_Id)
  //  .set("UserId", UserId1)
  //  .set("Workspaceid",Workspaceid1)
  //  .set("Clientid", Clientid1);
    
  //  return this._http.post(this.checkUserAccessGrant,connectorIds,{params:params});
    
  // }


   


   
}