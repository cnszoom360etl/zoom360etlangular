import { Injectable, Injector } from '@angular/core';
import {HttpClient,HttpHeaders, HttpParams} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AppComponentBase } from 'src/app/services/AppComponentBase';
@Injectable({
  providedIn: 'root'
})

export class ConnectionVarification extends AppComponentBase{
   
  getRecentConnection=environment.apiUrl+"/api/ConnectionLog/RecentConnectionLog";
  GetDBCOnnectionStatus=environment.apiUrl+"/api/MongoDBConnectionStatus/getstatus";
  constructor(private _http: HttpClient,injector : Injector) { super(injector)}


  ConnectionStatus(obj){
     debugger 
     var connectortitle=localStorage.getItem("Connectortitle");
     var connectorId=localStorage.getItem("ConnectorId");
     var Account_Id=localStorage.getItem("Account_Id");
     var userId=this.storageService.getItem(environment.storage.userId);
     var ClientId=this.storageService.getItem(environment.storage.clientId);
     var workspaceId=this.storageService.getItem(environment.storage.workspaceId);
     let params = new HttpParams()
     .set("connectortitle", connectortitle)
     .set("connectorId", connectorId)
     .set("Account_Id", Account_Id)
     .set("userId", userId)
     .set("ClientId", ClientId)
     .set("workspaceId", workspaceId);
     return  this._http.post(`${this.GetDBCOnnectionStatus}`,obj,{params:params});
}
    //  return  this._http.get(`${this.getRecentConnection}`,obj);
}