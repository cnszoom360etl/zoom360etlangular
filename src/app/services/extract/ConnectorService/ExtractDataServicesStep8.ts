import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders, HttpParams} from '@angular/common/http';
 
import {observable, Observable} from 'rxjs';
import { extractModel } from 'src/app/models/extract/ExtractPageModelForStep8';
import { environment } from 'src/environments/environment';
import { param } from 'jquery';


@Injectable({
  providedIn: 'root'
})

export class ExtractServices {
  url=environment.apiUrl+'/api/SqlConnector/ExtractPageDataSaveForStep8';
  buttongroupurl=environment.apiUrl+'/api/AllExtract/GetButtonGroupList';
  constructor(private _http: HttpClient) { }


  SaveDataForExtract(inputs:extractModel,UserId:string,Clientid:string,Workspaceid:string,AccountId:string,ConnectorId:string){
    debugger
    inputs.UserId=UserId;
    inputs.ACCOUNT_Id=AccountId;
    inputs.CONNECTORID=ConnectorId;
    inputs.Workspaceid=Workspaceid;
    inputs.Clientid=Clientid;
    inputs.FromandTo=inputs.date_LINK+"To"+inputs.date_LINK1;
   
   return this._http.post(this.url,inputs); 
}

GetButtongroup(userId:string,clientID:string,workspacID)
{
  let params = new HttpParams()
  .set("userId",userId)
  .set("clientid",clientID)
  .set("workspaceId",workspacID)
  .set("inputButton",null);
return this._http.get(`${this.buttongroupurl}`,{params:params});
}

}