import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ConnectorStatusService {
  _Rightsstatusurl=environment.apiUrl+'/api/SqlConnector/GetConnectorStatus';
  userId:string='admin';
  workSpaceId:number=1;
  client_id:string=''+1002;
  ConnectorId:string;
  
  constructor(private http:HttpClient) { }
getConnectorStatus(id:string){
 var obj={
  "userId":this.userId,
  "workSpaceId":''+this.workSpaceId,
  "CLIENT_ID":this.client_id,
  "ConnectorId":id,
 }
  return this.http.post(`${this._Rightsstatusurl}`,obj,{responseType:'text'});
  
}
}