import { Injectable,Injector } from '@angular/core';
import {HttpClient,HttpHeaders, HttpParams} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { pipelineConnectorSave } from 'src/app/models/extract/connectorPipeline';
import { DatePipe } from '@angular/common'
import { data } from 'jquery';
import { AppComponentBase } from 'src/app/services/AppComponentBase';
 
@Injectable({
    providedIn: 'root'
     
  })
  export class ConectorPipline extends AppComponentBase{
   pipline =new pipelineConnectorSave();
    SaveConnectorsList=environment.apiUrl+"/api/ConnectorPipeline/SaveConnectorType";
    GetConnectorsPipelineList=environment.apiUrl+"/api/ConnectorPipeline/GetConnectorList";
   
    constructor(private _http: HttpClient, public datepipe: DatePipe ,
      injector : Injector) {super(injector) }
  
  
    SaveConnectorList(ConnectorIds:string,ConnectionIds:string,DisplayName:string,id:string,connectorsnameList:string){
       debugger
       this.pipline.USER_ID=this.storageService.getItem(environment.storage.userId);
       this.pipline.CLIENT_ID=this.storageService.getItem(environment.storage.clientId);
       this.pipline.WORKSPACE_ID=this.storageService.getItem(environment.storage.workspaceId);
       this.pipline.ACCOUNT_ID=ConnectionIds;
       this.pipline.CONNECTOR_ID=ConnectorIds;
       this.pipline.CONNECTORS_NAME=connectorsnameList;
       this.pipline.CONNECTOR_DISPLAY_NAME=DisplayName;
       this.pipline.PIPELINE_ID=id;
       var date=new Date();
       let latest_date =this.datepipe.transform(date, 'yyyy-MM-dd');
       this.pipline.CLIENT_DATE=latest_date;
       this.pipline.CLIENT_TIME=this.datepipe.transform(date,'HH:mm');
       this.pipline.CLIENT_TIME_ZONE=null;


       return  this._http.post(`${this.SaveConnectorsList}`,this.pipline);
  }


  GetConnectorList(pipelineId:string,UserId:string,WorkspaceId:string,ClientId:string)
  {
    debugger
    let params = new HttpParams()
    .set("pipelineId", pipelineId)
    .set("UserId", UserId)
    .set("WorkspaceId", WorkspaceId)
    .set("ClientId", ClientId);
     return  this._http.get(`${this.GetConnectorsPipelineList}`,{params:params});
  }
      //  return  this._http.get(`${this.getRecentConnection}`,obj);
  }