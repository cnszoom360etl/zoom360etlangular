import { Injectable, Injector } from '@angular/core';
import { environment } from 'src/environments/environment';
import { AppComponentBase } from '../AppComponentBase';
  
@Injectable({
  providedIn: 'root'
})
export class AddNewConnectoinWizardStyle extends AppComponentBase {
     
    ConnectionTypeWizardClass="css-Connectorwizard"
    SourceAccountDisable=true;
    SourceAccountClass="AddNewConnectionSourcewizard"
    TempleteAccountWizard=true;
    TempleteWizardClass="AddNewConnectionTemplatewizard";
    ConfiguureWizard=true;
    ConfiguureWizardClass="AddNewConnectionConfigure";

    hide_the_main_menu_when_user_authorized=true;
  
    // FilterWizzard=true;
    // FilterWizzardClass="css-filterwizard";
    // FilterSelectionWizard=true;
    // FilterSelectionClass="css-FilterSelectionwizard";
    // ExecitionPlanWizard=true;
    // ExecitionPlanWizardClass="css-ExecutionPlanWizard";
    // ExtractWizard=true;
    // ExtractWizardClass="css-ExractWizard";

    constructor(injector : Injector) {
     
      super(injector)
      
      
      }


}