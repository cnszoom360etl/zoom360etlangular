import { Injectable, Injector } from '@angular/core';
import {HttpClient,HttpHeaders, HttpParams} from '@angular/common/http';
import { ServiceService } from './service.service';


import { TouchSequence } from 'selenium-webdriver';
import { stringmappingDictionary } from 'src/app/utils/ExtractMapping';
import { DBConnectorIdsAndAccountIdsForExtract, ExtractSqlTableData } from 'src/app/models/extract/ExtractDataModel';
import { DestinationList, ExtractAutoETL, ExtractDataSave, FilterfunctionDetails, functionDetails, LoadDataSave, updatesourceaccountStep2 } from 'src/app/models/extract/access-microsoft-sqlserver';
import { environment } from 'src/environments/environment';
import * as FileSaver from 'file-saver';
import { DestinationExtractModel } from 'src/app/models/Load/destinationExtractModel';
import { AppComponentBase } from 'src/app/services/AppComponentBase';
 
 
 
 
@Injectable({
  providedIn: 'root'
})
export class Transformation_Mapping_Loading_Using_Auto_ETL  extends AppComponentBase{
    
     
    constructor(private _http: HttpClient,injector : Injector) {
      super(injector)
   }
    
 JSON_For_Column_Mapping(Columnmaping:any,modelName:string){
     debugger
    const _MTL=environment.apiUrl+'/api/DynamicEnrichScript/savescriptFor_Transformation_Mapping_Loading_Data'
    var acocuntid=localStorage.getItem("sourceaccount_Id");
    let params = new HttpParams()
    .set("accountId", acocuntid)
    var collection={
          "userId":this.storageService.getItem(environment.storage.userId),
          "workspaceId":this.storageService.getItem(environment.storage.workspaceId),  
          "clientId":this.storageService.getItem(environment.storage.clientId),
          "accountId":acocuntid,
          "column_mapping":Columnmaping ,
          "modelName":modelName,
              }
        return this._http.post(_MTL,collection,{params:params});
    
  }
  JSON_For_Content_Mapping(Contentmaping:any){
    debugger
   const _MTL=environment.apiUrl+'/api/DynamicEnrichScript/savescriptFor_Transformation_Mapping_Loading_Data'
   var acocuntid=localStorage.getItem("sourceaccount_Id");
   let params = new HttpParams()
   .set("accountId", acocuntid)
   var collection={
         "userId":this.storageService.getItem(environment.storage.userId),
         "workspaceId":this.storageService.getItem(environment.storage.workspaceId),  
         "clientId":this.storageService.getItem(environment.storage.clientId),
         "accountId":acocuntid,
         "content_mapping":Contentmaping 
             }
       return this._http.post(_MTL,collection,{params:params});
   
 }
 JSON_For_Transformation(transformation:any){
      debugger
     const _MTL=environment.apiUrl+'/api/DynamicEnrichScript/savescriptFor_Transformation_Mapping_Loading_Data'
     var acocuntid=localStorage.getItem("sourceaccount_Id");
     let params = new HttpParams()
     .set("accountId", acocuntid)
     var collection={
           "userId":this.storageService.getItem(environment.storage.userId),
           "workspaceId":this.storageService.getItem(environment.storage.workspaceId),  
           "clientId":this.storageService.getItem(environment.storage.clientId),
           "accountId":acocuntid,
           "transformation":transformation 
               }
         return this._http.post(_MTL,collection,{params:params});
     
   }
   JSON_For_Loading(Load:any){
      debugger
     const _L=environment.apiUrl+'/api/DynamicEnrichScript/DestinationList'
     var acocuntid=localStorage.getItem("sourceaccount_Id");
     let params = new HttpParams()
     .set("userId", this.storageService.getItem(environment.storage.userId))
     .set("workspaceId", this.storageService.getItem(environment.storage.workspaceId))
     .set("clientId", this.storageService.getItem(environment.storage.clientId))
     .set("accountId", acocuntid)
       return this._http.post(_L,Load,{params:params});
     
   }
   DB_JSON_Loading(Load:any){
      debugger
      const _MTL=environment.apiUrl+'/api/DynamicEnrichScript/savescriptFor_Transformation_Mapping_Loading_Data'
     var acocuntid=localStorage.getItem("sourceaccount_Id");
     let params = new HttpParams()
     .set("accountId", acocuntid)
     var collection={
           "userId":this.storageService.getItem(environment.storage.userId),
           "workspaceId":this.storageService.getItem(environment.storage.workspaceId),  
           "clientId":this.storageService.getItem(environment.storage.clientId),
           "accountId":acocuntid,
           "load":Load 
               }
      return this._http.post(_MTL,collection,{params:params});
     
   }


   /// Import File Step 4 Apply Transformation
ApplyTransformationOnFile(object:any){
      let headers = new HttpHeaders();
        
    
      headers.append('Access-Control-Allow-Headers', 'Content-Type');
      headers.append('Access-Control-Allow-Methods', 'POST');
      headers.append('Access-Control-Allow-Origin', '*'); 
      const url="http://192.168.223.102:4444/transform";
   return this._http.post(url,object); 
   }


   
}