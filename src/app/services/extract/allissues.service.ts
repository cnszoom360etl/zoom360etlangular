import { Injectable } from '@angular/core';
import { HttpParams, HttpClient } from '@angular/common/http';
import { AllIssuesModel } from 'src/app/models/connect.model';
import { ArraySortPipe } from 'src/app/shared/pipes/array-pipes';
import { FiltersService } from '../common/filters.service';
import { environment } from 'src/environments/environment';
import { filtersModel } from 'src/app/models/extract/filter.model';
 

@Injectable({
  providedIn: 'root'
})
export class AllissuesService {
//_urlAllissues='https://localhost:44357/api/Allissues/GetAllIssuesRowsLimitBysp';
//_urlAllissues='https://localhost:44357/api/Allissues/GetAllIssuesRowsLimitBysp';
//_urlAllissues='https://localhost:44357/api/AllissuesList/getIssuelist';
_urlAllissues = environment.apiUrl+'/api/AllExtract/GetAllIssueList';
userId:string='admin';
workSpaceId:number=1;
client_id:string=''+1002;
_issueData:any[]=[];
_issueSearch:AllIssuesModel[]=[];
_recordLength:number;
_Issuerecordlength:number=0;
_tablecounter:number=0;
_sortToggle : boolean=false;
accountId:string=null;
  constructor(private _http:HttpClient,private sort: ArraySortPipe,private filterService:FiltersService) {
   }
  getAllissues(filtersValue:filtersModel){
      //this.accountId= localStorage.getItem("accountId");
    //this.accountId=localStorage.getItem("sourceaccount_Id");
    this._issueData.splice(0,this._issueData.length); 
    this._recordLength=0;
    let params = new HttpParams()
    .set("userId", filtersValue.userId)
    .set("workSpaceId", filtersValue.workspaceId)
    .set("clientId", filtersValue.clientId)
    .set("accountId", filtersValue.accountId)
    .set("workspaceName",filtersValue.workspaceName)
    .set("connectionName",filtersValue.connectionName)
    .set("sourceName", filtersValue.sourceName)
    .set("accessGranted", filtersValue.accessGranted)
    .set("createdBy", filtersValue.createdBy)
    .set("isActive", filtersValue.isActive)
    .set("lastAccessed", filtersValue.lastAccessed)
    .set("destinationEnabled", filtersValue.destinationEnabled)
    .set("TimeFilter", filtersValue.TimeFilter);
  this._http.get(`${this._urlAllissues}`,{params:params}).subscribe((data: AllIssuesModel[]) => { 
    if(data.length  > 0){
        this._issueData= data;
        this._issueSearch = data;
        this._recordLength = data.length;
        this._Issuerecordlength=data.length;
        this.filterService._recordLength= data.length;
      }
  });
     }
  // getAllissuewithLimit(limit){
  //   let params = new HttpParams().set("limit", limit).set("userId", this.userId);
  // this._http.get(`${this._urlAllissues}`,{params:params}).subscribe((data: tblAllissues[]) => {
  //     if(data.length  > 0){
  //       this._issueData= data;
  //       this._issueSearch = data;
  //       this._recordLength = data.length;
  //     }
  // });
  //    }
searchAllissue(text){
  
      this._issueData = this._issueSearch;
      this._issueData = this._issueSearch.filter((data) =>data.connectionName.toLowerCase().includes(text.toLowerCase()));
      this._recordLength = this._issueData.length;
      this.filterService._recordLength=this._issueData.length;
      //t => t.title.toLowerCase().includes(this.query.toLowerCase())
    }
sortAllIssues(field){
   if(this._sortToggle){
      this._issueData = this.sort.transform(this._issueData,field);
    }
  
    else if(!this._sortToggle){
      this._issueData = this.sort.transform2(this._issueData,field);
    }
  
    }
}
