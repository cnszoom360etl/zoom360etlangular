import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { Descriptionmodel } from 'src/app/models/extract/description-model';
import { environment } from 'src/environments/environment';
import { SourceAccountSettup } from '../AddNewConnectionServices/SourceAccountSettup';
import { ServiceService } from '../service.service';
 
import { AppComponentBase } from 'src/app/services/AppComponentBase';
import { inject } from '@angular/core/testing';
@Injectable({
  providedIn: 'root'
})
export class Destinationservices 
extends AppComponentBase {
   DataBase_Span=false;
   Username_Span=false;
   Password_Span=false;
  descriptinsave=environment.apiUrl+"/api/Destination/saveDescriptionInfo";
  savecredentials=environment.apiUrl+"/api/Destination/saveDbCredentialInfo";   
  constructor(private _http: HttpClient,private serviceService:ServiceService,private sourceAccountSettup:SourceAccountSettup,private router: 
    Router,injector : Injector) {super(injector)}
  savedescription(description:Descriptionmodel){
      debugger
    var userId=this.storageService.getItem(environment.storage.userId); 
    var workspaceId=this.storageService.getItem(environment.storage.workspaceId);
    var clientid=this.storageService.getItem(environment.storage.clientId);
    var AccountId=JSON.parse(localStorage.getItem('accountId'));
    var connectorId=JSON.parse(localStorage.getItem('Connectorid'));
    var InputModel={
     "userId":userId,
     "workspaceId":workspaceId,
     "clientId":clientid,
     "AccountId":AccountId,
     "connectorId":connectorId,
     "SourceInfoModel":description
   }
    return  this._http.post(`${this.descriptinsave}`,InputModel);
   }
   Savedbcredentials(){
    debugger
    localStorage.getItem("DbConfigure");
    var DBObject={
      "Hostname":this.sourceAccountSettup.Hostname,
      "Database": this.sourceAccountSettup.Database,
      "PortNumber":this.sourceAccountSettup.PortNumber,
      "UserName":this.sourceAccountSettup.Username ,
      "Password":this.sourceAccountSettup.Password,
       } 
       var CommonParams={
        "userId":this.storageService.getItem(environment.storage.userId),
        "workspaceId":this.storageService.getItem(environment.storage.workspaceId),
        "clientId":this.storageService.getItem(environment.storage.clientId),
        "AccountId":JSON.parse(localStorage.getItem('accountId')),
        "connectorId":JSON.parse(localStorage.getItem('accountId')), 
       }
      var  DbAccount={
         "SourceCommonModel":CommonParams,
         "sOURCE_CNF":DBObject
      }
       localStorage.setItem("DbConfigure",JSON.stringify(DBObject));
       if(this.sourceAccountSettup.Database==null){
        this.DataBase_Span=true;
         
       }
       else if(this.sourceAccountSettup.Database!=null){
        this.DataBase_Span=false;
       }
      if(this.sourceAccountSettup.Username==null){
        this.Username_Span=true;
         
       }
       else if(this.sourceAccountSettup.Username!=null){
        this.Username_Span=false;
       }
        if(this.sourceAccountSettup.Password==null){
        this.Password_Span=true;
         
       }
       else if(this.sourceAccountSettup.Password!=null){
        this.Password_Span=false;
       }
       if(this.sourceAccountSettup.Password!=null && this.sourceAccountSettup.Username!=null && this.sourceAccountSettup.Database!=null){
        this._http.post(this.savecredentials,DbAccount).subscribe((data:any)=>{
        //   this.router.navigate(['extract/AddNewConnection/ViewdataSummary']);
         });
        }
        
  }
}