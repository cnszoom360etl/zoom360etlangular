import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { valueTableListModel } from '../models/enrich/valueTableListModel';
import { ArraySortPipe } from '../shared/pipes/array-pipes';

@Injectable({
  providedIn: 'root'
})
export class ValueTablelListService {
  url='https://localhost:51070';
  _valueTableListUrl= environment.apiUrl+'/​​api​/Enrich​/getValueTableList';
  userId:string='admin';
  workspaceId:string='1';
  ClientId:string=''+1002;
  valueTable:string=null;
   valueTableData:any[]=[];
  _valueTableList: valueTableListModel[]=[];
  _recordLength: number=0;
  _sortToggle: boolean=false;
  _tablecounter:number=0;

  constructor(private _http: HttpClient,private sort: ArraySortPipe) { }
  getValueTableList(){
    debugger
   //var test='?userId='+this.userId+'&workspaceId='+this.workspaceId+'&ClientId='+this.ClientId;
  //  let params = new HttpParams()
  //  .set("userId",this.userId)
  //  .set("workspaceId",this.workspaceId)
  //  .set("ClientId",this.ClientId)
  //  .set("valueTable","");
 
 //wit Post request
  
  var Input={
  userId:this.userId,
  workspaceId:this.workspaceId,
  ClientId:this.ClientId,
  valueTable:""
 }
   return  this._http.post('http://localhost:51070/api/Enrich/getValueTableList',Input)

  }
  sortAllValueTable(field){
    if(this._sortToggle){
       this.valueTableData = this.sort.transform(this.valueTableData,field);
     }
   
     else if(!this._sortToggle){
       this.valueTableData = this.sort.transform2(this.valueTableData,field);
     }
   
     }
}
