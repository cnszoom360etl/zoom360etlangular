import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DropDownLoadService {
  _urltreeDropDown = environment.apiUrl+'/api/CommonDropdownList/GetTreeropdown';
  _getBasicCommonDropdown= environment.apiUrl + '/api/CommonDropdownList/GetDropdownList';
  _getBasicCommonDropdownWithCategory= environment.apiUrl + '/api/CommonDropdownList/GetDropdownWithCategory';
  userId:string='admin';
  subUserId:string;
  workSpaceId:string=''+9;
  client_id:string='1002';
  mainmenuID: string=''+1;
  constructor(private _http: HttpClient) { 
    this.subUserId=localStorage.getItem("userId");
  }
  gettreeDropDownData(dropDownType){
    let params = new HttpParams()
    .set("UserId", this.userId)
    .set("SubUserId", this.subUserId)
    .set("ClientId", this.client_id)
    .set("WorkspaceId", this.workSpaceId)
    .set("DropDownType",dropDownType)
    return this._http.get(`${this._urltreeDropDown}`,{params:params});
    
  //   this._http.get(`${this._urltreeDropDown}`,{params:params}).subscribe((data: TreeDropDownParentModel[]) => {
  //     if(data.length  > 0){
        
  //       this.subMenuSection = data;
  //       }
        
      
  // });        
}
GetDropDowns(dropdownname){
  let params = new HttpParams()
  .set("userId", this.userId)
  //.set("userId", "2")
  .set("dropdownName", dropdownname);
  return this._http.get(`${this._getBasicCommonDropdown}`,{params:params});
}
GetDropDownsWithCategory(dropdownname){
  let params = new HttpParams()
  .set("userId", this.userId)
  //.set("userId", "2")
  .set("dropdownName", dropdownname);
  return this._http.get(`${this._getBasicCommonDropdownWithCategory}`,{params:params});
}
}
