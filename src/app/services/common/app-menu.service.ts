import { Injectable, Injector } from '@angular/core';
import { HttpClient , HttpParams} from '@angular/common/http';
import { sectionModelForSourcesList, SubMenusectionModel, TblSubMenu } from 'src/app/Models/mainmenu.model';
import { environment } from 'src/environments/environment';
import { debounce } from '@fullcalendar/core';
import { AppComponentBase } from 'src/app/services/AppComponentBase';
import { StringFilterUI } from '@syncfusion/ej2-grids';
@Injectable({
  providedIn: 'root'
})
export class AppMenuService extends AppComponentBase {
    //_url = 'https://localhost:44357/api/MianMenu/GetTblMainMenuBySp';
    _mainMenuUrl= environment.apiUrl+'/api/DynamicMenuItem/GetMenulist';
    _urlSubMenu = environment.apiUrl+'/api/SubMenu/GetTblMainMenuBySp';
   _subMenuSectionUrl=environment.apiUrl+'/api/Menu/getsubMenuSection';
   _subMenuSectionitemsUrl=environment.apiUrl+'/api/DynamicMenuItem/GetSubMenuSectionItems';
   _SourceListpagesSectionitemsUrl=environment.apiUrl+'/api/DynamicMenuItem/SectionItemsForSourcePageList';
  userId:string=null;
  workSpaceId:string=null;
  client_id:string=null;
  mainmenuID: string=''+1;
  treeLevel:number=0;
  SubUserId:string='';
  treeNode:number=0;
  id:string=''+1;
  hideSideMenu:boolean=true;
  subMenuSection:SubMenusectionModel[]=[];
  SourcesSectionANdItemList:sectionModelForSourcesList[]=[];
  subMenu : TblSubMenu[]=[];
 
  constructor(private _http: HttpClient,injector : Injector) {
    super(injector)
    this.userId=this.storageService.getItem(environment.storage.userId);
   this.workSpaceId= this.storageService.getItem(environment.storage.clientId);
   this.client_id= this.storageService.getItem(environment.storage.workspaceId);
   }
  getMenuItems(mode_id,UserId:string,workspaceId:string,client_id:string){
    let params = new HttpParams()
    .set("userId", UserId)
    .set("SubUserId",null)
    .set("workSpaceId",workspaceId)
    .set("CLIENT_ID",client_id)
    .set("Mode_Id",mode_id);
    return this._http.get(`${this._mainMenuUrl}`,{params:params});        
    }
    
    // getsubMenuItems(menuID){
    //   let params = new HttpParams().set("userId",this.id).set("mainMenuID", menuID);
    //   this._http.get(`${this._urlSubMenu}`,{params:params}).subscribe((data: TblSubMenu[]) => {
    //     if(data.length  > 0){
    //       this.subMenu = data; 
    //     }
    // });        
    //   }
getsubMenuSection(value){
        let params = new HttpParams()
        .set("userId", this.userId)
        .set("SubUserId",this.SubUserId)
        .set("workSpaceId", this.workSpaceId)
        .set("CLIENT_ID", this.client_id)
        .set("mainMenuID", value)
        .set("treeLevel", ''+this.treeLevel)
        .set("treeNode", ''+this.treeNode);
        debugger
        this._http.get(`${this._subMenuSectionitemsUrl}`,{params:params}).subscribe((data: SubMenusectionModel[]) => {
          if(data.length  > 0){
            
            this.subMenuSection = data;
            }
            
          
      });        
}
getMenuSectionAndItemsForSource(){

   
  var  UserId=this.storageService.getItem(environment.storage.userId);
  var WorkspaceId=this.storageService.getItem(environment.storage.workspaceId);
  var Client_Id=this.storageService.getItem(environment.storage.clientId);
  var MainMenuID="1";
  var TreeLevel="2";
  var TreeNode="1003";
  var SubUserId="";
  let params = new HttpParams()
  .set("userId", UserId)
  .set("SubUserId", SubUserId)
  .set("workSpaceId", WorkspaceId)
  .set("CLIENT_ID", Client_Id)
  .set("mainMenuID",MainMenuID)
  .set("treeLevel", TreeLevel)
  .set("treeNode", TreeNode);
  debugger
  this._http.get(`${this._SourceListpagesSectionitemsUrl}`,{params:params}).subscribe((data:sectionModelForSourcesList[]) => {
    if(data.length  > 0){
      debugger
      this.SourcesSectionANdItemList = data;
      }
      
    
});        
}

}
