import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { filtersModel } from 'src/app/models/extract/filter.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RecentConnectionsService {
url= environment.apiUrl + '/api/ConnectionLog/RecentConnectionLog';
  constructor(private _http:HttpClient) { }
  GetRecentModule(filtersValue:filtersModel){
    debugger
    let params = new HttpParams()
    .set("userId", filtersValue.userId)
    .set("workSpaceId", filtersValue.workspaceId)
    .set("clientId", filtersValue.clientId)
    .set("accountId",filtersValue.accountId)
    .set("workspaceName", filtersValue.workspaceName)
    .set("connectionName", filtersValue.connectionName)
    .set("sourceName", filtersValue.sourceName)
    .set("accessGranted", filtersValue.accessGranted)
    .set("createdBy", filtersValue.createdBy)
    .set("isActive", filtersValue.isActive)
    .set("lastAccessed", filtersValue.lastAccessed)
    .set("destinationEnabled", filtersValue.destinationEnabled)
    .set("TimeFilter", filtersValue.TimeFilter);
      return  this._http.get(`${this.url}`,{params:params}); 
  
  }
}
