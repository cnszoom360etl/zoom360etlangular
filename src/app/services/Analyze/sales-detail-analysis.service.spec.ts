import { TestBed } from '@angular/core/testing';

import { SalesDetailAnalysisService } from './sales-detail-analysis.service';

describe('SalesDetailAnalysisService', () => {
  let service: SalesDetailAnalysisService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SalesDetailAnalysisService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
