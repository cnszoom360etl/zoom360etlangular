import { HttpClient, HttpParams } from '@angular/common/http';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { Injectable } from '@angular/core';


import { environment } from 'src/environments/environment';
import { saveValueTableData, valueTableDataModel } from '../models/enrich/valueTableDataModel';
import { ArraySortPipe } from '../shared/pipes/array-pipes';

@Injectable({
  providedIn: 'root'
})
export class ValueTableDataService {
  _valueTableDataUrl= environment.apiUrl+'/api/Enrich/getValueTableData';
  _saveValueTableDataUrl=environment.apiUrl+'/api/Enrich/SaveValeTableData';
  userId:string='admin';
  workspaceId:string='1';
  clientId:string=''+1002;
  valueTable:string="";
  // valueTableData:any[]=[];
  _valueTableData: valueTableDataModel[]=[];
  valueDataModel:saveValueTableData;
  _recordLength: number=0;
  _sortToggle: boolean=false;
  _tablecounter:number=0;
  Status_message: string;
  valueTableName: string="";
  ClientDate=new Date();
  ClientTime=new Date();
  ClientTimeZone=new Date();
  

  constructor(private _http: HttpClient,private sort: ArraySortPipe) { 
    
  }

  getValueTableData(userId:string,workspaceId:string,clientId:string,valueTable:string){
    debugger
    this.valueTable= JSON.parse(localStorage.getItem("objectKeyName"));
    let params = new HttpParams()
    .set("userId", userId)
    .set("workspaceId",workspaceId)
    .set("clientId",clientId)
    .set("valueTable",valueTable);
    this._http.get(`${this._valueTableDataUrl}`,{params:params}).subscribe((data:valueTableDataModel[]) => {
      debugger
  if(data.length  > 0){
        //this.valueTableData = data;
       this._valueTableData = data;
       this._recordLength = this._valueTableData.length; 
       console.log(this._valueTableData);  
      }   
  });
  }
  
// SaveTabledata(UserData:saveValueTableData){
//   debugger
//     UserData.userId = this.userId;
//     UserData.workspaceId = this.workspaceId;
//     UserData.clientId = this.clientId;
//     UserData.valueTableName = this.valueTableName;
//     // this.valueDataModel=UserData;
//     this._http.post(`${this._saveValueTableDataUrl}`,UserData,{responseType:'text'}).subscribe((data:string) => {
//       debugger
//     this.Status_message = data;

// console.log(this.Status_message);
// });
//     }
 
 
  SaveTabledata(UserData:any){
    debugger
    // UserData.userId = this.userId;
    // UserData.workspaceId = this.workspaceId;
    // UserData.clientId = this.clientId;
    // UserData.valueTableName = this.valueTableName;
    this.valueTableName= JSON.parse(localStorage.getItem("objectKeyName"));
     
    let params = new HttpParams()
    .set("UserId",this.userId)
    .set("workspaceId",this.workspaceId)
    .set("clientId",this.clientId)
    .set("valuetable",this.valueTableName)
    .set("ClientDate",this.ClientDate.toString())
    .set("ClientTime",this.ClientTime.toString())
    .set("ClientTimeZone",this.ClientTimeZone.toString());

    
    this._http.post("http://localhost:51070/api/Enrich/SaveValeTableData",UserData,{params:params,responseType:"text"}).subscribe((data:any) => {
      debugger
    this.Status_message = JSON.parse(data);


console.log(this.Status_message);
});
    }
}
