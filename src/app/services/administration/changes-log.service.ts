import { Injectable } from '@angular/core';
import { ChangeLogListModel } from 'src/app/Models/mainmenu.model';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ArraySortPipe } from 'src/app/shared/pipes/array-pipes';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ChangesLogService {
  _urlGetChangeLog= environment.apiUrl+'/api/ChangeLog/GetChangeLogList';
  userId:string='all';
  client_id:string='all';
  workSpaceId:string='all';
  _sortToggle : boolean=false;
  _recordLength:number=0;
  _tablecounter:number=0;
  _ChangeLogGrid:any[]=[];
  _ChangeLogGridRandom:ChangeLogListModel[]=[];
  constructor(private _http:HttpClient,private sort: ArraySortPipe) { }
  getChangeLogList(){
    let params = new HttpParams()
    .set("userId", this.userId)
    .set("workSpaceId",this.workSpaceId)
    .set("CLIENT_ID", this.client_id)
    this._http.get(`${this._urlGetChangeLog}`,{params:params}).subscribe((data: any[]) => {
      debugger
      if(data.length  > 0){
        this._ChangeLogGrid = data;
        this._ChangeLogGridRandom = data;
        this._recordLength = this._ChangeLogGrid.length;
      }
    });
}
  sortWorkSetup(field){
    if(this._sortToggle){
       this._ChangeLogGrid = this.sort.transform(this._ChangeLogGrid,field);
     }
   
     else if(!this._sortToggle){
       this._ChangeLogGrid = this.sort.transform2(this._ChangeLogGrid,field);
     }
   
     }
}
