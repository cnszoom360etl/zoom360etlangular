import { Injectable } from '@angular/core';
import { DataGovernanceModel } from 'src/app/Models/mainmenu.model';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DataGovernanceService {
  _urlSaveCurrencySetup= environment.apiUrl+'/api/DataGovernance/Save';
  _urlGetDropDowns=environment.apiUrl+'/api/CommonDropdownList/GetDropdownList';
  userId:string='admin';
  workSpaceId:number=11;
  client_id:string=''+1003;
  Status_message:string;
  
  constructor(private _http:HttpClient) { }
  getActiveSourceLoactionList(){
    let params = new HttpParams().set("userId", this.userId).set("dropdownName", 'Active Source Location');
    return this._http.get(`${this._urlGetDropDowns}`,{params:params});
  }
  getWorkspacesList(){
    let params = new HttpParams().set("userId", this.userId).set("dropdownName", 'Workspace');
    return this._http.get(`${this._urlGetDropDowns}`,{params:params});
  }
  getActiveDestinationLocationList(){
    let params = new HttpParams().set("userId", this.userId).set("dropdownName", 'Active Destination Location');
    return this._http.get(`${this._urlGetDropDowns}`,{params:params});
  }
  getPassiveDestinationLocationList(){
    let params = new HttpParams().set("userId", this.userId).set("dropdownName", 'Passive Destination Location');
   return this._http.get(`${this._urlGetDropDowns}`,{params:params})
  }
  
  saveDataGovernance(dataGovernanceModel:DataGovernanceModel){
    dataGovernanceModel.userId = this.userId;
    dataGovernanceModel.workSpaceId = this.workSpaceId;
    dataGovernanceModel.CLIENT_ID = this.client_id;
    
    this._http.post(`${this._urlSaveCurrencySetup}`,dataGovernanceModel,{responseType:'text'}).subscribe((data:string) => {
      debugger
      this.Status_message = data;
      console.log(this.Status_message);
      });
         // this._http
    //       .post(`${this._url}`, "abc").subscribe(results=>{
    //       console.log(results)
    // })

  }
}
