import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ChildWorkspacesModel } from 'src/app/Models/mainmenu.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ChildWorkspacesSetupService {
  _saveChildWorkspaceSetup= environment.apiUrl+'/api/ChildWorkSpace/SaveChildWorkspaces';
  userId:string='admin';
  workSpaceId:number=11;
  Client_id:string=''+1003;
  Status_message:string;
  constructor(private _http:HttpClient) { }
  saveWorkSpaceSetup(childWorkspacesModel:ChildWorkspacesModel){
    childWorkspacesModel.userId = this.userId;
    childWorkspacesModel.workSpaceId = this.workSpaceId;
    childWorkspacesModel.ClientId = this.Client_id;
    this._http.post(`${this._saveChildWorkspaceSetup}`,childWorkspacesModel,{responseType:'text'}).subscribe((data:string) => {
      debugger
      this.Status_message = data;
      console.log(this.Status_message);
      });
         // this._http
    //       .post(`${this._url}`, "abc").subscribe(results=>{
    //       console.log(results)
    // })

  }
}
