import { Injectable } from '@angular/core';
import { NumeralsSetupModel } from 'src/app/Models/mainmenu.model';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NumeralsSetupService {
  _saveNumeralsSetupUrl= environment.apiUrl+'/api/NumeralSetup/SaveNumeralSetup';
  userId:string='admin';
  workSpaceId:number=11;
  Client_id:string=''+1003;
  Status_message:string='';
  constructor(private _http:HttpClient) { }
  saveNumeralsSetup(UserData:NumeralsSetupModel){
    UserData.userId = this.userId;
    UserData.workSpaceId = this.workSpaceId;
    UserData.CLIENT_ID = this.Client_id;
    //var workspaceModel = JSON.stringify(UserData);
    var numeralsSetupModel = UserData;
    this._http.post(`${this._saveNumeralsSetupUrl}`,numeralsSetupModel,{responseType:'text'}).subscribe((data:string) => {
      debugger
      this.Status_message = data;
      console.log(this.Status_message);
      });
         // this._http
    //       .post(`${this._url}`, "abc").subscribe(results=>{
    //       console.log(results)
    // })

  }
}
