import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { QuotaSettingsModel } from 'src/app/Models/mainmenu.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class QuotaSettingsService {
  _quotaSettingSaveUrl= environment.apiUrl+'/api/QuotaSetting/SaveQuotaSetting';
  
  userId:string='admin';
  workSpaceId:number=11;
  client_id:string=''+1003;
  Status_message:string;
  constructor(private _http:HttpClient) { }
  saveQuotaSetting(UserData:QuotaSettingsModel){
    UserData.userId = this.userId;
    UserData.workSpaceId = this.workSpaceId;
    UserData.CLIENT_ID = this.client_id;
    this._http.post(`${this._quotaSettingSaveUrl}`,UserData,{responseType:'text'}).subscribe((data:string) => {
      debugger
      this.Status_message = data;
      console.log(this.Status_message);
      });
    }
}
