import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { TimeZoneSetupModel } from 'src/app/Models/mainmenu.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TimeZoneSetupService {

  _urlDateFormatDropDown=environment.apiUrl+'/api/CommonDropdownList/GetDropdownList';
  _urlTimeFormatDropDown=environment.apiUrl+'/api/CommonDropdownList/GetDropdownList';
  _urlSaveTimeZoneSetup= environment.apiUrl+ '/api/TimeZoneSetup/SaveTimeZoneSetup';

userId:string='admin';
workSpaceId:number=11;
Client_id:string=''+1003;
_DateFormatDropdown:any[]=[];
_TimeFormatDropdown:any[]=[];
timezoneModel: TimeZoneSetupModel;
SetCurrency : boolean=false;
DateFormatDropDownName='Date Format';
TimeFormatDropDownName='Time Format';
timeZoneImage:string='';
Status_message:string;
  constructor(private _http:HttpClient) { }
  getDateFormatList(){
    let params = new HttpParams().set("userId", this.userId).set("dropdownName", this.DateFormatDropDownName);
    this._http.get(`${this._urlDateFormatDropDown}`,{params:params}).subscribe((data: any) => {
      debugger
        this._DateFormatDropdown = data;       
    });
  }
  getTimeFormatList(){
    let params = new HttpParams().set("userId", this.userId).set("dropdownName", this.TimeFormatDropDownName);
    this._http.get(`${this._urlTimeFormatDropDown}`,{params:params}).subscribe((data: any) => {
      debugger
        this._TimeFormatDropdown = data;       
    });
  }
  saveWorkSpaceSetup(UserData:TimeZoneSetupModel){
    UserData.userId = this.userId;
    UserData.workSpaceId = this.workSpaceId;
    UserData.CLIENT_ID = this.Client_id;
    UserData.clockImage=this.timeZoneImage;
    debugger
    //var workspaceModel = JSON.stringify(UserData);
    this._http.post(this._urlSaveTimeZoneSetup,UserData,{responseType:'text'}).subscribe((data:string) => {
      debugger
      this.Status_message = data;
      console.log(this.Status_message);
      });
         // this._http
    //       .post(`${this._url}`, "abc").subscribe(results=>{
    //       console.log(results)
    // })

  }
}
