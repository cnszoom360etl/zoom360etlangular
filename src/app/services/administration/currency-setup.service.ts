import { Injectable } from '@angular/core';
import { HttpParams, HttpClient } from '@angular/common/http';
import { CurrencySetupModel } from 'src/app/Models/mainmenu.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CurrencySetupService {
_urlCurrencyTypeDropDown=environment.apiUrl+'/api/CommonDropdownList/GetDropdownList';
_urlSaveCurrencySetup=environment.apiUrl+'/api/CurrencySetup/SaveCurrencySetup';
_getCurrencySetupSettings=environment.apiUrl+'/api/CurrencySetup/GetCurrencySettings';

userId:string='admin';
workSpaceId:number=11;
client_id:string=''+1003;
currencyImage:string='';
_CurrencyTypeDropdown:any[]=[];
currencySetupModel :CurrencySetupModel;
dropDownParentName='Currency Type(Sign)';
Status_message:string;
  constructor(private _http:HttpClient) { }
  getCurrencyTypeList(){
    let params = new HttpParams().set("userId", this.userId).set("dropdownName", this.dropDownParentName);
    this._http.get(`${this._urlCurrencyTypeDropDown}`,{params:params}).subscribe((data: any) => {
        this._CurrencyTypeDropdown = data;       
    });
  }
  getCurrencySetupSetting(){
    let params = new HttpParams().set("userId", this.userId).set("client_Id",this.client_id).set("workSpaceId",""+this.workSpaceId);
    return this._http.get(`${this._getCurrencySetupSettings}`,{params:params});
}
  saveWorkSpaceSetup(UserData:CurrencySetupModel){
    UserData.userId = this.userId;
    UserData.workSpaceId = this.workSpaceId;
    UserData.CLIENT_ID = this.client_id;
    UserData.currencyImage = this.currencyImage;
    this.currencySetupModel = UserData;
    this._http.post(`${this._urlSaveCurrencySetup}`,this.currencySetupModel,{responseType:'text'}).subscribe((data:string) => {
      debugger
      this.Status_message = data;
      console.log(this.Status_message);
      });
         // this._http
    //       .post(`${this._url}`, "abc").subscribe(results=>{
    //       console.log(results)
    // })

  }
}

