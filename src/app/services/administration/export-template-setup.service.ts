import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DataExportTemplete } from 'src/app/Models/mainmenu.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ExportTemplateSetupService {
  _urlSaveTemplateSetup= environment.apiUrl+'/api/DataExportTemplate/SaveExportTemplate';
  dataExportTemplate= new DataExportTemplete();
  userId:string='admin';
  workSpaceId:number=11;
  client_id:string=''+1003;
  Status_message:string;
  exportXLS:string='';
  exportPPT:string='';
  exportPDF:string='';

  constructor(private _http:HttpClient) { }
  saveTempleteSetup(){
    debugger;
    this.dataExportTemplate.userId = this.userId;
    this.dataExportTemplate.workSpaceId = this.workSpaceId;
    this.dataExportTemplate.CLIENT_ID = this.client_id;
    this.dataExportTemplate.exportXLS = this.exportXLS;
    this.dataExportTemplate.exportPPT = this.exportPPT;
    this.dataExportTemplate.exportPDF = this.exportPDF;
    this._http.post(`${this._urlSaveTemplateSetup}`,this.dataExportTemplate,{responseType:'text'}).subscribe((data:string) => {
      debugger
      this.Status_message = data;
      console.log(this.Status_message);
      });
         // this._http
    //       .post(`${this._url}`, "abc").subscribe(results=>{
    //       console.log(results)
    // })

  }
}
