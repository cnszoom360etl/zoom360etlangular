import { Injectable } from '@angular/core';
import { HttpParams, HttpClient } from '@angular/common/http';
import { WorkspaceSetup, workSpaceParentGrid_Model } from 'src/app/Models/mainmenu.model';
import { ArraySortPipe } from 'src/app/shared/pipes/array-pipes';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WorkSpaceSetupService {

  
  _workSpaceDropDownUrl=environment.apiUrl+'/api/CommonDropdownList/GetDropdownList';
  // _getGridDataUrl='https://localhost:44357/api/WorkSpaceSetup/GetGridWorkspacelist';
  _getWorkspaceSetingsUrl=environment.apiUrl+'/api/WorkspaceSetup/GetWorkspaceSetupSettings';
  _getGridDataUrl=environment.apiUrl+'/api/WorkspaceSetup/GetWorkspaceSetupList';
  _url=environment.apiUrl+'/api/WorkspaceSetup/SaveWorkspaceSetup';
  _workSpaceParentDropdown:any[]=[];
  _workSpaceGrid:any[]=[];
  _workSpaceGridRandom:WorkspaceSetup[]=[];

  _tablecounter=0;
  status:boolean=false;
  _sortToggle : boolean=false;
  _recordLength:number=0;
  userId:string='admin';
  dropDownParentName='parent workspace';
  workSpaceId:number=null;
  client_id:string='1004';
  workspacename='Conference Room';
  parentWorkSpace='all';
  Status_message:string='';
  constructor(private _http:HttpClient,private sort: ArraySortPipe) { }
  
  getWorkSpaceParentList(){
    let params = new HttpParams().set("userId", this.userId).set("dropdownName", this.dropDownParentName);
    this._http.get(`${this._workSpaceDropDownUrl}`,{params:params}).subscribe((data: any[]) => {
      debugger
      this._workSpaceParentDropdown= data;
    });
  }
  getWorkSpaceSettings(){
    let params = new HttpParams().set("userId", this.userId).set("ClientId",this.client_id).set("WorkspaceId",""+this.workSpaceId);
    return this._http.get(`${this._getWorkspaceSetingsUrl}`,{params:params});
}
  getGridWorkSpaceList(){
      let params = new HttpParams()
      .set("userId", this.userId)
      .set("workSpaceId",'all')
      .set("CLIENT_ID", this.client_id)
      .set("workSpaceName",'all')
      .set("parentWorkSpace",'all');
      this._http.get(`${this._getGridDataUrl}`,{params:params}).subscribe((data: any[]) => {
        debugger
        if(data.length  > 0){
          this._workSpaceGrid = data;
          this._workSpaceGridRandom = data;
          this._recordLength = this._workSpaceGrid.length;
        }
      });
  }
  
  saveWorkSpaceSetup(UserData:WorkspaceSetup){
    UserData.userId = this.userId;
    UserData.workSpaceId = this.workSpaceId;
    UserData.Client_Id = this.client_id;
    //var workspaceModel = JSON.stringify(UserData);
    var workspaceModel = UserData;
    this._http.post(`${this._url}`,workspaceModel,{responseType:'text'}).subscribe((data:string) => {
      debugger
      this.Status_message = data;
      console.log(this.Status_message);
      });
         // this._http
    //       .post(`${this._url}`, "abc").subscribe(results=>{
    //       console.log(results)
    // })

  }
  sortWorkSetup(field){
    if(this._sortToggle){
       this._workSpaceGrid = this.sort.transform(this._workSpaceGrid,field);
     }
   
     else if(!this._sortToggle){
       this._workSpaceGrid = this.sort.transform2(this._workSpaceGrid,field);
     }
   
     }
}
