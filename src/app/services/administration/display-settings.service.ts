import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { DisplaySettingsModel } from 'src/app/Models/mainmenu.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DisplaySettingsService {
  _urlSaveCurrencySetup= environment.apiUrl+'/api/DisplaySettings/SaveDisplaySetting';
  _urlWorkspaceThemeDropDowns=environment.apiUrl+'/api/CommonDropdownList/GetDropdownList';
  userId:string='admin';
  workSpaceId:number=11;
  client_id:string=''+1003;
  Status_message:string;
  constructor(private _http:HttpClient) { }
  getThemeColorList(){
    let params = new HttpParams().set("userId", this.userId).set("dropdownName", 'Workspace Theme/Color');
    return this._http.get(`${this._urlWorkspaceThemeDropDowns}`,{params:params});
  }
  saveDisplaySettings(displaySettingsModel:DisplaySettingsModel){
    displaySettingsModel.userId = this.userId;
    displaySettingsModel.workSpaceId = this.workSpaceId;
    displaySettingsModel.CLIENT_ID = this.client_id;
    
    this._http.post(`${this._urlSaveCurrencySetup}`,displaySettingsModel,{responseType:'text'}).subscribe((data:string) => {
      debugger
      this.Status_message = data;
      console.log(this.Status_message);
      });
  }
}
