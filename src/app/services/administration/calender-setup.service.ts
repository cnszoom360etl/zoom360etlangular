import { Injectable } from '@angular/core';
import { HttpParams, HttpClient } from '@angular/common/http';
import { CalenderSetupModel } from 'src/app/Models/mainmenu.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CalenderSetupService {
_getStartDateDropDownUrl= environment.apiUrl+'/api/CommonDropdownList/GetDropdownList';
//_saveCalenderUrl='https://localhost:44357/api/CalenderSetup/SaveCalenderSetup';
_getCalenderSteup=environment.apiUrl+'api/CalenderSetup';
_saveCalenderUrl=environment.apiUrl+'/api/CalenderSetup/SaveCalenderSetup';
userId:string='admin';
workSpaceId:number=11;
client_id:string=''+1003;
dropdownName:string='Weekstart day';
WeekStartDay:any[]=[];
Status_message:string;
  constructor(private _http:HttpClient) { }
  getWeekStarDayList(){
    debugger
    let params = new HttpParams().set("userId", this.userId).set("dropdownName", this.dropdownName);
    this._http.get(`${this._getStartDateDropDownUrl}`,{params:params}).subscribe((data: any[]) => {
        this.WeekStartDay = data;       
    });
  }
  saveWorkSpaceSetup(UserData:CalenderSetupModel){
    UserData.userId = this.userId;
    UserData.workSpaceId = this.workSpaceId;
    UserData.Clientid = this.client_id;
    this._http.post(`${this._saveCalenderUrl}`,UserData,{responseType:'text'}).subscribe((data:string) => {
      debugger
      this.Status_message = data;
      console.log(this.Status_message);
      });
    }
}
