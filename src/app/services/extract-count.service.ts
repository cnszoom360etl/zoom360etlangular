import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { filtersModel } from '../models/extract/filter.model';
import { extractionCountModel } from '../models/extractCountModel';
import { ArraySortPipe } from '../shared/pipes/array-pipes';

@Injectable({
  providedIn: 'root'
})
export class ExtractCountService {
  _extractCountUrl= environment.apiUrl+'/api/AllExtract/getExtractionCount';
  userId:string='admin';
  workspaceId:string='1';
  clientId:string='1002';
  connectorId:string=null;
  accountId:string=null;
  extractCountModel={} as extractionCountModel;
  lastAccess:string;
  //_extractionCountModel: extractionCountModel;
  private _recordLength: any;
  constructor(private _http: HttpClient,private sort: ArraySortPipe) { }
  getextractionCount(filtersValue:filtersModel){
    let params = new HttpParams()
    .set("userId", filtersValue.userId)
    .set("workSpaceId", filtersValue.workspaceId)
    .set("clientId", filtersValue.clientId)
    .set("accountId",filtersValue.accountId)
    .set("workspaceName", filtersValue.workspaceName)
    .set("connectionName", filtersValue.connectionName)
    .set("sourceName", filtersValue.sourceName)
    .set("accessGranted", filtersValue.accessGranted)
    .set("createdBy", filtersValue.createdBy)
    .set("isActive", filtersValue.isActive)
    .set("lastAccessed", filtersValue.lastAccessed)
    .set("destinationEnabled", filtersValue.destinationEnabled)
    .set("TimeFilter", filtersValue.TimeFilter);
    this._http.get(`${this._extractCountUrl}`,{params:params}).subscribe((data:extractionCountModel) => {
        //this.valueTableData = data;
        this.extractCountModel = data;
        this.extractCountModel.enrichmentCompleteCount=this.extractCountModel.enrichmentCompleteCount;
        this.extractCountModel.enrichmentTotalCount=this.extractCountModel.enrichmentTotalCount;
        this.extractCountModel.extractionCompleteCount=this.extractCountModel.extractionCompleteCount;
        this.extractCountModel.extractionTotalCount=this.extractCountModel.extractionTotalCount;
        this.extractCountModel.loadCompleteCount=this.extractCountModel.loadCompleteCount;
        this.extractCountModel.loadTotalCount=this.extractCountModel.loadTotalCount;
         
  });
  }
}
