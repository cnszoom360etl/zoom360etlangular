import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable,Injector } from '@angular/core';
import { AppComponentBase } from 'src/app/services/AppComponentBase';
import { environment } from 'src/environments/environment';
import { ImportFileLogs } from '../utils/DictionaryMapping';
// import { data } from './datasource';

@Injectable({
  providedIn: 'root'
})
export class CSVFileReadingService extends AppComponentBase{
 
Data:object[];
importfileDictionary=new ImportFileLogs();
FileConnectorName:string=null;
sheetname:string=null;
Loadoption:string=null;
tablename:string=null;
tabletype:string=null;
userId:string=this.storageService.getItem(environment.storage.userId);
workspaceId:string="1";
clientId:string=this.storageService.getItem(environment.storage.clientId);
     
removeRow_Above_header:string=null;

  constructor(private _http: HttpClient,injector:Injector) {
      super(injector)
   }
  getFileData(filename:string,change:boolean){
    debugger
    var Mode=JSON.parse(localStorage.getItem("mode"));
     let DatabaseName=filename.slice((filename.lastIndexOf(".") - 1 >>> 0) + 2);
     this.FileConnectorName=this.importfileDictionary.getImportFileName(DatabaseName);
      var filepath="/run/user/1000/gvfs/smb-share:server=192.168.223.100,share=d/zoom_files_dump/"+filename;
      let headers = new Headers();
      let account_id=localStorage.getItem("accountId");
      let Connector_Name=localStorage.getItem("Connectortitle");
      this.sheetname=this.storageService.getItem(environment.storage.sheetname);
      this.removeRow_Above_header=this.storageService.getItem(environment.storage.remove_row_above_header);
        headers.append('Access-Control-Allow-Headers', 'Content-Type');
        headers.append('Access-Control-Allow-Methods', '*');
        headers.append('Access-Control-Allow-Origin', '*'); 
        var obj={
          "filename":filepath,
          "change":change,
          "sheet_name":this.sheetname,
          "extract":{
            "Database":this.FileConnectorName,
            "account_id":account_id,
           },
           "userInfo":{"SOURCE_STG_ID":"","SOURCE_STG_NAME":this.FileConnectorName,"SOURCE_NAME":Connector_Name,"WORKSPACE_NAME":""},
           "config":false,
        }
        return  this._http.post('http://192.168.223.102:4444/getcolumns',obj);
     
    
    
  }

  getRawFileData(filename:string,change:boolean){
    debugger
    var Mode=JSON.parse(localStorage.getItem("mode"));
     
      var filepath="/run/user/1000/gvfs/smb-share:server=192.168.223.100,share=d/zoom_files_dump/extract/"+filename;
      let headers = new Headers();
          
          
        headers.append('Access-Control-Allow-Headers', 'Content-Type');
        headers.append('Access-Control-Allow-Methods', '*');
        headers.append('Access-Control-Allow-Origin', '*'); 
        var obj={
          "filename":filepath,
          "change":change,
          "config":true,
        }
        return  this._http.post('http://192.168.223.102:4444/getcolumns',obj);
     
    
    
  }
  GetContentMappedArray(accountId:string,ScreenName:string){
    const  url=environment.apiUrl+'/api/DynamicEnrichScript/GetMappedListForEditMode';
    let params = new HttpParams().set("userId",this.userId)
    .set("workspaceId",this.workspaceId)
    .set("clientId",this.clientId)
    .set("accountId",accountId)
    .set("EditscreenName",ScreenName);
    return  this._http.get(`${url}`,{params:params});
  }
  GetColumnMappedArray(accountId:string,ScreenName:string){
    debugger
    const  urlColumn=environment.apiUrl+'/api/DynamicEnrichScript/ColumnMappingEditMode';
    let params = new HttpParams().set("userId",this.userId)
    .set("workspaceId",this.workspaceId)
    .set("clientId",this.clientId)
    .set("accountId",accountId)
    .set("EditscreenName",ScreenName);
    return  this._http.get(`${urlColumn}`,{params:params});
  }
  SetColumns(obj){
    debugger
    
    let headers = new Headers();
        
        
      headers.append('Access-Control-Allow-Headers', 'Content-Type');
      headers.append('Access-Control-Allow-Methods', '*');
      headers.append('Access-Control-Allow-Origin', '*'); 
      
      return  this._http.post('http://192.168.223.102:4444/setcolumns',obj,{responseType: 'text'});
    
  }
  GetModelNameList(UserId:string,DropdownType:string){
    debugger
    const  url=environment.apiUrl+'/api/CommonDropdownList/GetDropdownList';
     let params = new HttpParams().set("userId",UserId).set("dropdownName",DropdownType);
  return  this._http.get(`${url}`,{params:params});
  
  }
  GetfieldNameList(UserId:string,DropdownType:string){
    debugger
    const  url=environment.apiUrl+'/api/CommonDropdownList/GetFIELDNameDropdown';
     let params = new HttpParams().set("userId",UserId).set("dropdownName",DropdownType);
  return  this._http.get(`${url}`,{params:params});
  
  }
  GetModelNameListForGrid(UserId:string,workspaceid:string,clintid:string,modelname:string){
    debugger
    const  url=environment.apiUrl+'/api/CommonDropdownList/GetModelNameDropdown';
     let params = new HttpParams()
     .set("userid",UserId)
     .set("workspaceid",workspaceid)
     .set("clientid",clintid)
     .set("modeltype",modelname);
  return  this._http.get(`${url}`,{params:params});
  
  }
  sentCSVFileData(obj){
   
    const _saveDataPython= 'http://192.168.223.102:4444/sendData';
    
    return this._http.post(_saveDataPython,obj);

  }
   getFileDatas(name){
    let params = new HttpParams().set("name",name);
    const  _getCSVData= environment.apiUrl+'/api/CSVHelper/GetCSVFileData';
    return this._http.get(`${_getCSVData}`,{params:params});
   }
   getExtractFileData(name){
    let params = new HttpParams().set("name",name);
    const  _getCSVData= environment.apiUrl+'/api/CSVHelper/getCSVExtractFileDataToJson';
    return this._http.get(`${_getCSVData}`,{params:params});
   }
   ContentMapping(obj){
    debugger
      let headers = new Headers();
      headers.append('Access-Control-Allow-Headers', 'Content-Type');
      headers.append('Access-Control-Allow-Methods', '*');
      headers.append('Access-Control-Allow-Origin', '*'); 
       
      return  this._http.post('http://192.168.223.102:4444/contentMapping',obj,{responseType: 'text'});
    
  }



  getFileColunNameForTransformation(filename:string,change:boolean){
    debugger
    var Mode=JSON.parse(localStorage.getItem("mode"));
     
      var filepath="/run/user/1000/gvfs/smb-share:server=192.168.223.100,share=d/zoom_files_dump/"+filename;
      let headers = new Headers();
          
          
        headers.append('Access-Control-Allow-Headers', 'Content-Type');
        headers.append('Access-Control-Allow-Methods', '*');
        headers.append('Access-Control-Allow-Origin', '*'); 
        var obj={
          "filename":filepath,
          "change":change
        }
        return  this._http.post('http://192.168.223.102:4444/getcolumns',obj);
     
    
    
  }

  
}
