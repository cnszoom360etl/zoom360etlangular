import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ConnectionListExtractionModel } from '../models/connectionListExtractionModel';
import { ArraySortPipe } from '../shared/pipes/array-pipes';

@Injectable({
  providedIn: 'root'
})
export class ConnectionListExtractionService {
  _connectionListExtractUrl= environment.apiUrl+'/api/extractionCount/getconListExtraction';
  UserId:string='admin';
  WorkSpaceId:string='1';
  Client_Id:string='1002';
  AccountId:string=null;
  WorkspaceName:string=null;
  ConnectionName:string=null;
  SourceName:string=null;
  AccessGranted:string=null;
  CreatedBy:string=null;
  IsActive:string=null;
  DestinationEnabled:string=null;
  LastAccessed:string;
  connectionExtractData:any[]=[];
  _ConnectionListExtractionModel: ConnectionListExtractionModel[]=[];

  constructor(private _http: HttpClient,private sort: ArraySortPipe) { }

  getConnectionListextraction(LastAccessed:string){
    debugger
    
    let params = new HttpParams()
    .set("UserId", this.UserId)
    .set("WorkSpaceId",this.WorkSpaceId)
    .set("Client_Id",this.Client_Id)
    .set("AccountId",this.AccountId)
    .set("WorkspaceName",this.WorkspaceName)
    .set("ConnectionName",this.ConnectionName)
    .set("SourceName",this.SourceName)
    .set("AccessGranted",this.AccessGranted)
    .set("CreatedBy",this.CreatedBy)
    .set("IsActive",this.IsActive)
    .set("DestinationEnabled",this.DestinationEnabled)
    .set("LastAccessed",LastAccessed);
    this._http.get(`${this._connectionListExtractUrl}`,{params:params}).subscribe((data:ConnectionListExtractionModel[]) => {
      debugger
  if(data.length  > 0){
        //this.valueTableData = data;
        this.connectionExtractData=data;
      }   
  });
  }
}