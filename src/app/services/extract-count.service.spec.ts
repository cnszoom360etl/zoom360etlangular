import { TestBed } from '@angular/core/testing';

import { ExtractCountService } from './extract-count.service';

describe('ExtractCountService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExtractCountService = TestBed.get(ExtractCountService);
    expect(service).toBeTruthy();
  });
});
