import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AllIssuesModel } from 'src/app/Models/connect.model';
import { filtersModel } from 'src/app/models/extract/filter.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EnrichLogsService {
  _urlEnrichlog= environment.apiUrl+'/api/AllExtract/GetEnrichLog';
  userId:string='admin';
  workSpaceId:number=1;
  client_id:string=''+1002;
  _issueData:any[]=[];
  _issueSearch:AllIssuesModel[]=[];
  _recordLength:number;
  _tablecounter:number=0;
  _sortToggle : boolean=false;
  accountId:string='all';
  constructor(private _http:HttpClient) { }
  getEnrichLog(filtersValue:filtersModel){
    //  this.accountId= localStorage.getItem("accountId");
    this._issueData.splice(0,this._issueData.length); 
    this._recordLength=0;
    let params = new HttpParams()
    .set("userId", filtersValue.userId)
    .set("workSpaceId", filtersValue.workspaceId)
    .set("clientId", filtersValue.clientId)
    .set("accountId", filtersValue.accountId)
    .set("workspaceName",filtersValue.workspaceName)
    .set("connectionName",filtersValue.connectionName)
    .set("sourceName", filtersValue.sourceName)
    .set("accessGranted", filtersValue.accessGranted)
    .set("createdBy", filtersValue.createdBy)
    .set("isActive", filtersValue.isActive)
    .set("lastAccessed", filtersValue.lastAccessed)
    .set("destinationEnabled", filtersValue.destinationEnabled)
    .set("TimeFilter", filtersValue.TimeFilter);
  this._http.get(`${this._urlEnrichlog}`,{params:params}).subscribe((data: AllIssuesModel[]) => { 
    if(data.length  > 0){
        this._issueData= data;
        this._issueSearch = data;
        this._recordLength = data.length;
       // this.filterService._recordLength= data.length;
      }
  });
     }
}
