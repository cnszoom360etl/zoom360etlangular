import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { TransformationJsonPart } from 'src/app/models/extract/ExtractDataModel';
import { environment } from 'src/environments/environment';
import { AppComponentBase } from 'src/app/services/AppComponentBase';
@Injectable({
  providedIn: 'root'
})
export class DynamicEnrichScriptsService extends AppComponentBase {
  
  _enrichment_mapping_load:TransformationJsonPart[]=[];
  _FunctionTemplateUrl=environment.apiUrl+'/api/DynamicEnrichScript/getfunctiontemplate';
  _saveScriptUrl=environment.apiUrl+'/api/DynamicEnrichScript/saveEnrichScript';
  _deleteScriptUrl=environment.apiUrl+'/api/DynamicEnrichScript/DeleteScript';
  _getScriptUrl=environment.apiUrl+'/api/DynamicEnrichScript/getEnrichScript';
  _updateScriptUrl=environment.apiUrl+'/api/DynamicEnrichScript/updateEnrichScript';
  _ListScriptUrl=environment.apiUrl+'/api/DynamicEnrichScript/getScriptList';
  constructor(private _http: HttpClient,injector : Injector) { super(injector)}
  getfunctionTemplate(){
    return  this._http.get(`${this._FunctionTemplateUrl}`);
  }
  saveScript(collection){
    return  this._http.post(`${this._saveScriptUrl}`,collection,{responseType:'text'});
  }
  getScript(usedId,workspaceId,clientId,templateId){
    let params = new HttpParams()
  .set("userId", usedId)
  .set("workspaceId", workspaceId)
  .set("clientId", clientId)
  .set("templateId", templateId)
    return  this._http.get(`${this._getScriptUrl}`,{params:params});
  }
  updateScript(Script,scriptId){
    let params = new HttpParams()
  .set("_id", scriptId);
    return  this._http.put(`${this._updateScriptUrl}`,Script,{params:params});
  }

  getScriptList( ){
    var userid=this.storageService.getItem(environment.storage.userId);
    var workspaceid="1";
    var clientid=this.storageService.getItem(environment.storage.clientId);
    let params = new HttpParams()
  .set("userId",userid)
  .set("workspaceId",workspaceid)
  .set("clientId",clientid)
   
    return  this._http.get(`${this._ListScriptUrl}`,{params:params});
  }
  DeleteScriptList(ScriptId){
    debugger
    var userid=this.storageService.getItem(environment.storage.userId);
    let params = new HttpParams()
    .set("UserId",userid)
    .set("scriptid",ScriptId);
   
    return  this._http.delete(`${this._deleteScriptUrl}`,{params:params,responseType:'text'});
  }
  getEnrichmentscriptJosnFormMongo(usedId,workspaceId,clientId,templateId)
  {
    debugger
   const _getScriptUrl=environment.apiUrl+'/api/DynamicEnrichScript/getEnrichScript';
      let params = new HttpParams()
     .set("userId", usedId)
    .set("workspaceId", workspaceId)
     .set("clientId", clientId)
    .set("templateId", templateId)
    return  this._http.get(`${this._getScriptUrl}`,{params:params});
  }
  
}
