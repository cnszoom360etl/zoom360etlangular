import { TestBed } from '@angular/core/testing';

import { EnrichLogsService } from './enrich-logs.service';

describe('EnrichLogsService', () => {
  let service: EnrichLogsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EnrichLogsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
