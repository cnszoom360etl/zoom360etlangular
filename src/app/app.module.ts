import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { HttpClientModule } from '@angular/common/http';
import { AgGridModule } from 'ag-grid-angular';
import 'ag-grid-enterprise';
import { ApplicationStartupComponent } from './application-startup/application-startup.component';
import { AuthGuard } from './guards/auth.guard';
import { DatePipe } from '@angular/common';
import { AuthurizationResponseComponent } from './authurization-response/authurization-response.component';
 
import {
  GoogleLoginProvider,
  FacebookLoginProvider
} from 'angularx-social-login';
// import {
//   SocialLoginModule,
//   AuthServiceConfig,
//   GoogleLoginProvider,
//   FacebookLoginProvider,
// } from "angular-6-social-login";
// export function getAuthServiceConfigs() {
//   let config = new AuthServiceConfig(
//       [
//         {
//           id: FacebookLoginProvider.PROVIDER_ID,
//           provider: new FacebookLoginProvider("464319688020045")
//         },
//         // {
//         //   id: GoogleLoginProvider.PROVIDER_ID,
//         //   provider: new GoogleLoginProvider("Your-Google-Client-Id")
//         // },
           
//       ]
//   );
//   return config;
// }
@NgModule({
  declarations: [
    AppComponent,
     ApplicationStartupComponent,
    AuthurizationResponseComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AgGridModule.withComponents([]),
     
    HttpClientModule,
   
    // SocialLoginModule
   
  ],
  exports: [TabsModule],
  providers: [AuthGuard,DatePipe,
    //,{
   // provide: AuthServiceConfig,
   // useFactory: getAuthServiceConfigs
  //}
],
  bootstrap: [AppComponent]
})
export class AppModule { }
