import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
 
import{ LoadRoutingModule }from './load-routing.module';    
//  import { LoadComponent } from './load.component';
import { LoadComponent } from '../load/load.component';
import { DashboardDestinationComponent } from '../load/dashboard-destination/dashboard-destination.component';
import { DestinationListComponent } from '../load/destination-list/destination-list.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { ToastModule } from '@syncfusion/ej2-angular-notifications';
@NgModule({
  declarations: [
    LoadComponent,
    DashboardDestinationComponent,
    DestinationListComponent
  ],
  imports: [
    CommonModule,
    LoadRoutingModule,
    FormsModule,
    ToastModule,
    
  ],
 
})
export class LoadModule { }
