import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ToastComponent } from '@syncfusion/ej2-angular-notifications';
import { ButtonComponent } from 'ej-angular2';
 
import { DestinationList, LoadDataSave, LoadDataSaveupdated,DB_Credentials_Info_For_Load_Json } from 'src/app/models/extract/access-microsoft-sqlserver';
 
import { DestinationExtractModel } from 'src/app/models/Load/destinationExtractModel';
import { ToastMessage } from 'src/app/models/MessageTypes/toast-message';
import { AppComponentBase } from 'src/app/services/AppComponentBase';
import { LookupvalueListservice } from 'src/app/services/Enrich/LookValuesList';
import { ExtractData } from 'src/app/services/extract/ExtractSqlAccountData';
import { Transformation_Mapping_Loading_Using_Auto_ETL } from 'src/app/services/extract/JsonFor_Transformation_mapping_Loading';
import { ServiceService } from 'src/app/services/extract/service.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-destination-list',
  templateUrl: './destination-list.component.html',
  styleUrls: ['./destination-list.component.css']
})
export class DestinationListComponent extends AppComponentBase implements OnInit {
  //for toast
  public testToast=new ToastMessage();
  @ViewChild('defaulttoast',{static:true})
  public toastObj: ToastComponent;
  @ViewChild('toastBtnShow',{static:true})
  public btnEleShow: ButtonComponent;
  public position=this.testToast.position;
  // toast end
  _connectionDestination:any[];
  allcheckbox:boolean=false;
  limit:string = 'All';
  workspaceName:string=null;
  connectionName:string=null;
  LoadObject:LoadDataSaveupdated[]=[];
  LoadDBJsonInMongo:DB_Credentials_Info_For_Load_Json[]=[];
  sourceName:string=null;
  accessGranted:string=null;
  createdBy:string=null;
  isActive:string=null;
  lastAccessed:string=null;
  destinationEnabled:string=null;
  appearanceLogo_toggle:boolean=false;
  appearanceLogo_arrow:boolean=false;
  workspaceName_toggle:boolean=false;
  workspaceName_arrow:boolean=false;
  connectorName_toggle:boolean=false;
  connectorName_arrow:boolean=false;
  bstatus_toggle=false;
  bstatus_arrow:boolean=false;
  _tablecounter:number=0;
  _recordLenght:number=0;
  LoadData:DestinationExtractModel[]=[];
  ErrorMessage:string;
  Errorheading=false;
  constructor(private lookupvalueListservice:LookupvalueListservice,private extractDataservices:ExtractData,
    private router: Router, public serviceService:ServiceService,
    public transformation_Mapping_Loading_Using_Auto_ETL_services:Transformation_Mapping_Loading_Using_Auto_ETL,injector : Injector) {
      super(injector) }

  ngOnInit(){
    this.GetalldestinationList();
  }
   GetalldestinationList(){
     debugger
    this.lookupvalueListservice.GetDestinationList().subscribe((data:any)=>{
      this._connectionDestination=data;
      this._recordLenght=  this._connectionDestination.length;
    });
   }
   checkAll(ev) {
    this._connectionDestination.forEach(x => x.state = ev.target.checked)
    
  }

  findCount(e,accountId:string,connectorId:string,connectorname:string) {
    debugger
   if(e.target.checked)
   {
     this.LoadData.push({
       CoonectorId:connectorId,
       ConnectorName:connectorname,
       AccountId:accountId,
     });
   
    //  this.count+=1;
     this._tablecounter+=1;
     this.allcheckbox=false;
   }
   else{
     var find=this.LoadData.find(x=>x.AccountId==accountId);
     var RemoveIndex=this.LoadData.indexOf(find);
     this.LoadData.splice(RemoveIndex,1);
    //  this.count-=1;
    
     this._tablecounter-=1;
     this.allcheckbox=false;
   }
  }
 functionCount(e){
   debugger
   if(e.target.checked)
   {
   for(var i=0;i< this._connectionDestination.length;i++)
   {
     var id= this._connectionDestination[i].accountID;
     var connectorname= this._connectionDestination[i].connectorName ;
     var connectorid= this._connectionDestination[i].connectorID;
     this.allcheckbox=true;
     this.findCount(e,id,connectorid,connectorname);
    }
     this.allcheckbox=true;
     this._tablecounter= this._connectionDestination.length;
      
   }
   else if(e.target.checked==false){
     for(var i=0;i<this._connectionDestination.length;i++)
     {
       var id=this._connectionDestination[i].accountID;
       var connectorname=this._connectionDestination[i].connectorName;
       var connectorid=this._connectionDestination[i].connectorID;
       this.allcheckbox=false;
       this.findCount(e,id,connectorid,connectorname);
     }
       this.allcheckbox=false;
        this._tablecounter=0;
   }

  }


   applySort(fieldName){
    if(fieldName=='connectorName'){
      this.appearanceLogo_arrow=true;
      this.appearanceLogo_toggle=!this.appearanceLogo_toggle;
      this.lookupvalueListservice._sortToggle=!this.lookupvalueListservice._sortToggle;
      this.lookupvalueListservice.sortAllDestinationList(fieldName);
    }
    else if(fieldName=='workspaceName'){
      this.workspaceName_arrow=true;
      this.workspaceName_toggle=!this.workspaceName_toggle;
      this.lookupvalueListservice._sortToggle=!this.lookupvalueListservice._sortToggle;
      this.lookupvalueListservice.sortAllDestinationList(fieldName);
    }
    else if(fieldName=='accountDisplayName'){
      this.connectorName_arrow=true;
      this.connectorName_toggle=!this.connectorName_toggle;
      this.lookupvalueListservice._sortToggle=!this.lookupvalueListservice._sortToggle;
      this.lookupvalueListservice.sortAllDestinationList(fieldName);
    }
    else if(fieldName=='accessGranted'){
      
      this.bstatus_arrow=true;
      this.bstatus_toggle=!this.bstatus_toggle;
      this.lookupvalueListservice._sortToggle=!this.lookupvalueListservice._sortToggle;
      this.lookupvalueListservice.sortAllDestinationList(fieldName);
    }
   
  }
  reloadPage(){
    this.appearanceLogo_arrow=false;
    this.workspaceName_arrow=false;
    this.connectorName_arrow=false;
    this.bstatus_toggle=false;
    this.lookupvalueListservice.GetDestinationList().subscribe((data:any)=>{
      this._connectionDestination=data;
    });
   }
   GetDataForDestinationConnector(){
      debugger
     if(this.LoadData.length>0)
     {
          this.extractDataservices.LoadDataList=this.LoadData;
          var UserId=this.storageService.getItem(environment.storage.userId);
          var Workspaceid=this.storageService.getItem(environment.storage.workspaceId);
          var Clientid=this.storageService.getItem(environment.storage.clientId);
          for(var L=0;L<this.LoadData.length;L++)   
              {
                var connectorid='214';
                this.extractDataservices.GetLoadSourceList(this.LoadData[L].AccountId,UserId,Workspaceid,Clientid,this.LoadData[L].CoonectorId,this.LoadData[L].ConnectorName).subscribe((data:DestinationList[])=>{
                   debugger
                  if(data.length>0){
                    debugger
                    this.LoadObject.push({
                      
                      Desaccountdisplayname:data[0].accountdisplayname,
                      Desfiledname:"Hostname",
                      DesConnectorType:data[0].connectorname, 
                      DesFilename:"",
                      DesFilepath:"",
                      Desfieldvalue:{
                        DeshostName:data[1].fieldvalue,
                        DesdatabaseName:data[0].fieldvalue,
                        Desport:data[3].fieldvalue,
                        Desusername:data[4].fieldvalue,
                        Despassword:data[2].fieldvalue,
                      },   
                         
                      Destablename:data[0].objectname,
                       
                    })
                    this.LoadDBJsonInMongo.push({
                       
                      Database:data[0].connectorname,
                      serviceName:"orcl.CNSE.COM.PK",
                      host:data[1].fieldvalue,
                      dbname:data[0].fieldvalue,
                      user:data[4].fieldvalue,
                      password:data[2].fieldvalue,
                      insert:data[0].dataoption,
                      tablename:data[0].objectname,
                      port:data[3].fieldvalue
                     })
                    }
                 if(this.LoadData.length==this.LoadDBJsonInMongo.length)
                 {
                   this.transformation_Mapping_Loading_Using_Auto_ETL_services.DB_JSON_Loading(this.LoadDBJsonInMongo).subscribe((data:any)=>{
                     debugger
                    var msg=data;
                     this.testToast.toast[1].content=msg;
                     this.toastObj.show(this.testToast.toast[1]);
                   })
                 }
                   
                })
                console.log(this.LoadObject);
              }




          //  if(this.serviceService.IdsCollectionArray.length==0)
          //  {
          //    this.ErrorMessage="Please Select At least One Source From Connection List"
          //    this.Errorheading=true;
          //  }
          // else
          //      {
          //        this.extractDataservices.Runforextractdata();
          //      }
          //  /extract/extraction/connectionslist
          //this.router.navigate(['extract/extraction/connectionslist']);
     }

   }

}
