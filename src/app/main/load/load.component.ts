import { Component, OnInit } from '@angular/core';
import { AppMenuService } from 'src/app/Services/common/app-menu.service';
 

@Component({
  selector: 'app-load',
  templateUrl: './load.component.html',
  styleUrls: ['./load.component.css']
})
export class LoadComponent implements OnInit {
  mainmenuID:number=3;
  constructor(public MenuService: AppMenuService) { }

  ngOnInit(): void {
    this.MenuService.getsubMenuSection(this.mainmenuID);
  }

}
