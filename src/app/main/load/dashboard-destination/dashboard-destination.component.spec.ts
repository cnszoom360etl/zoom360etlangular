import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardDestinationComponent } from './dashboard-destination.component';

describe('DashboardDestinationComponent', () => {
  let component: DashboardDestinationComponent;
  let fixture: ComponentFixture<DashboardDestinationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardDestinationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardDestinationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
