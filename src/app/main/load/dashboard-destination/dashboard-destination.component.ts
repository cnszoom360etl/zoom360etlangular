import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard-destination',
  templateUrl: './dashboard-destination.component.html',
  styleUrls: ['./dashboard-destination.component.css']
})
export class DashboardDestinationComponent implements OnInit {

Files=false;
RelationalDatabases=false;
FileDatalake=false;
ResentUseDb=true;
ResentModules:any[]=[];
FileClasshightLight:string;
RecentDashBox:string="box-active";
DatabasefileBox:string;
businessfileBox:string;
  constructor() { }

  ngOnInit(): void {
  }

  FilesShow(){
    debugger
    this.Files=true;
    this.ResentUseDb=false;
    this.RelationalDatabases=false;
    this.FileDatalake=false;
    this.FileClasshightLight="box-active";
    this.RecentDashBox="simple-box";
    this.DatabasefileBox="simple-box";
    this.businessfileBox="simple-box";
    
    }
    ResentShow(){
    this.Files=false;
    this.ResentUseDb=true;
    this.RelationalDatabases=false;
    this.FileDatalake=false;
    this.FileClasshightLight="simple-box";
    this.RecentDashBox="box-active";
    this.DatabasefileBox="simple-box";
    this.businessfileBox="simple-box";
    }
    Databaseshow(){
    this.Files=false;
    this.ResentUseDb=false;
    this.RelationalDatabases=true;
    this.FileDatalake=false;
    this.FileClasshightLight="simple-box";
    this.RecentDashBox="simple-box";
    this.DatabasefileBox="box-active";
    this.businessfileBox="simple-box";
    }
    ShowBusinessPlateForm(){
    this.Files=false;
    this.ResentUseDb=false;
    this.RelationalDatabases=false;
    this.FileDatalake=true;
    this.FileClasshightLight="simple-box";
    this.RecentDashBox="simple-box";
    this.DatabasefileBox="simple-box";
    this.businessfileBox="box-active";
    }
}
