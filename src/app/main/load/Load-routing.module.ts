import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LoadComponent } from './load.component';
import { DashboardDestinationComponent } from './dashboard-destination/dashboard-destination.component';
import { DestinationListComponent } from './destination-list/destination-list.component';

const routes: Routes = [
{ path: 'destination', component: LoadComponent,
  children:[
          {path:'destinationsourcelist', component: DashboardDestinationComponent},
          {path:'destinationlist', component: DestinationListComponent},
          ]
},
]
@NgModule({
  declarations: [
    
  ],
  imports: [
     CommonModule,
    RouterModule.forChild(routes),
  ],
  exports:[RouterModule]
})
export class LoadRoutingModule { }
