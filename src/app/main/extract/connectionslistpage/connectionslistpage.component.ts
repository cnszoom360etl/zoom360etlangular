import { Component, OnInit ,ViewEncapsulation, ViewChild, Injector} from '@angular/core';
import { FieldSettingsModel, DragEventArgs } from '@syncfusion/ej2-dropdowns';
import { DataManager } from '@syncfusion/ej2-data';
import { ListBoxComponent } from '@syncfusion/ej2-angular-dropdowns';
import { GetAllConnectionsService } from 'src/app/Services/extract/get-all-connections.service';
import { ConectorPipline } from 'src/app/services/extract/ConnectionPiplineSave';
import { Router } from '@angular/router';
import { JsonForPipline, jsonpipelineobject } from 'src/app/models/extract/connectorPipeline';
import { ExtractData } from 'src/app/services/extract/ExtractSqlAccountData';
import { DestinationExtractModel } from 'src/app/models/Load/destinationExtractModel';
import { LookupvalueListservice } from 'src/app/services/Enrich/LookValuesList';
import { environment } from 'src/environments/environment';
import { AppComponentBase } from 'src/app/services/AppComponentBase';
 




@Component({
  selector: 'app-connectionslistpage',
  templateUrl:'./connectionslistpage.component.html',
  styleUrls: ['./connectionslistpage.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ConnectionslistpageComponent extends AppComponentBase implements OnInit {
  limit:string = 'All';
  workspaceName:string=null;
  connectionName:string=null;
  sourceName:string=null;
  accessGranted:string=null;
  createdBy:string=null;
  isActive:string=null;
  lastAccessed:string=null;
  allcheckbox:boolean=false;
  singleCheckbox:boolean=false;
  _tablecounter:number=0;
  destinationEnabled:string=null;
  connectorIdsArray:any[]=[];
  connectionIdsArray:any[]=[];
  pipelineJson:JsonForPipline[]=[];
  accountName:string;
  ErrorMessage:string;
  PiplineJsonObject:jsonpipelineobject[]=[];
  pipelinelistarray:any[]=[];
  connectoridsarray:any[]=[];
  lengthofpipeline:number=0;
  connectorids:any[]=[];
  Errorheading=false;
  // connectoridslist:string[]=[];

  // Load Variables and Array Decleration
  _connectionDestination:any[];
  allcheckboxload:boolean=false;
  limitload:string = 'All';
  workspaceNameload:string=null;
  connectionNameload:string=null;
  sourceNameload:string=null;
  accessGrantedload:string=null;
  createdByload:string=null;
  isActiveload:string=null;
  lastAccessedload:string=null;
  destinationEnabledload:string=null;
  appearanceLogo_toggle:boolean=false;
  appearanceLogo_arrow:boolean=false;
  workspaceName_toggle:boolean=false;
  workspaceName_arrow:boolean=false;
  connectorName_toggle:boolean=false;
  connectorName_arrow:boolean=false;
  bstatus_toggle=false;
  bstatus_arrow:boolean=false;
  _tablecounterload:number=0;
  _recordLenght:number=0;
  LoadData:DestinationExtractModel[]=[];
  @ViewChild('listbox1',{static:true})
  public listObj1: ListBoxComponent;
  @ViewChild('listbox2',{static:true})
   public listObj2: ListBoxComponent;

  constructor(public allConService:GetAllConnectionsService,public conectorPipline:ConectorPipline ,
    private router: Router,public extractData:ExtractData,
    private lookupvalueListservice:LookupvalueListservice,injector : Injector)

  {super(injector) }

  ngOnInit() {
    //this.getAllConections(this.limit);
    this.GetPipelineRecordList();
    this.GetalldestinationList();
  }
  GetPipelineRecordList(){
    var userId=this.storageService.getItem(environment.storage.userId);
    var ClientId=this.storageService.getItem(environment.storage.clientId);
    var workspaceId=this.storageService.getItem(environment.storage.workspaceId);
    var PipelineId=null;
  this.conectorPipline.GetConnectorList(PipelineId,userId,workspaceId,ClientId).subscribe((data:any)=>{
    debugger
    this.pipelinelistarray=data;
    this.lengthofpipeline=this.pipelinelistarray.length;
      })
  }
  ConnectionUpdate(DisplayName:string,ConnectorIds:string,piplineId:string,ConnectorName:string,accountIds:string){

    debugger
    localStorage.setItem("Code",JSON.stringify(ConnectorIds));
    localStorage.setItem("Name",JSON.stringify(ConnectorName));
    localStorage.setItem("DisplayName",JSON.stringify(DisplayName));
    localStorage.setItem("PipelineID",JSON.stringify(piplineId));
    localStorage.setItem("accountIds",JSON.stringify(accountIds));
    this.router.navigate(['extract/extraction/pipeline']);
  }
  checkAll(ev) {
    this.pipelinelistarray.forEach(x => x.state = ev.target.checked)

  }
  findCount(index,e,connectorIds:string,connectorName:string,accountsIds:string,piplineid:string) {
    debugger
      var Accountids=accountsIds.split(',');
      var connectorname=connectorName.split(',');
      var connectoridslist=connectorIds.split(',');
      for(var i=0;i<connectoridslist.length;i++)
      {
          this.connectionIdsArray.push(connectoridslist[i])
      }
    
      if(e.target.checked)
        {

          this.pipelineJson.push({
            accountId:Accountids,
            connectorName:connectorname,
            ConnectorId:connectoridslist,
            piplineid:piplineid,
            accessgranted:""
          })
          // this.singleCheckbox=true;
          this._tablecounter+=1;
        }
    else{
        debugger
      var find=this.pipelineJson.find(x=>x.piplineid==piplineid);
      var RemoveIndex=this.pipelineJson.indexOf(find);
      this.pipelineJson.splice(RemoveIndex,1);
            this.allcheckbox=false;
          this._tablecounter-=1;
          }

  }
  functionCount(e){

    if(e.target.checked)
    {
      this.allcheckbox=true;
      this._tablecounter= this.pipelinelistarray.length;
    }
    else{
      this.allcheckbox=false;
      this._tablecounter= 0;
    }


    }
    RunForExtract(){
        debugger
        if(this.LoadData.length>0)
      {
        this.Errorheading=false;
         this.extractData.LoadDataList=this.LoadData;
       if(this.pipelineJson.length>0)
       {
        this.Errorheading=false;
         for(var u=0;u<this.pipelineJson.length;u++)
        {
            for(var h=0;h<this.connectionIdsArray.length;h++)
              {
              this.PiplineJsonObject.push({
                piplineid:this.pipelineJson[u].piplineid[h],
                accountid:this.pipelineJson[u].accountId[h],
                connectorname:this.pipelineJson[u].connectorName[h],
                connectorId:this.pipelineJson[u].ConnectorId[h],
                displayName:"",
                accessgranted:""
              })
              }

              // var find=this.PiplineJsonObject.find(x=>x.connectorName[u]=="Undefined");
              // var RemoveIndex=this.PiplineJsonObject.indexOf(find);
        
              // this.PiplineJsonObject.splice(RemoveIndex,1);

          }
          for(var w=0;w<this.PiplineJsonObject.length;w++)
          {
            debugger
            var find=this.PiplineJsonObject.find(x=>x.connectorname==undefined);
            if(find !=undefined){
              var RemoveIndex=this.PiplineJsonObject.indexOf(find);
              this.PiplineJsonObject.splice(RemoveIndex,1);
            }
           
          }
         
          console.log(this.PiplineJsonObject);
          this.extractData.serviceService.IdsCollectionArray=this.PiplineJsonObject;
          this.extractData.Runforextractdata();
        }
        else {
          this.Errorheading=true;
          this.ErrorMessage="Select the At least one extraction"
        }
        }
        else{
          this.Errorheading=true;
          this.ErrorMessage="Select the At least one destination"
        }
      }

     //Load List  Page

     GetalldestinationList(){
      debugger
     this.lookupvalueListservice.GetDestinationList().subscribe((data:any)=>{
       this._connectionDestination=data;
       this._recordLenght=  this._connectionDestination.length;
     });
    }
    checkAllload(ev) {
     this._connectionDestination.forEach(x => x.state = ev.target.checked)
     
   }
 
   findCountload(e,accountId:string,connectorId:string,connectorname:string) {
     debugger
    if(e.target.checked)
    {
      this.LoadData.push({
        CoonectorId:connectorId,
        ConnectorName:connectorname,
        AccountId:accountId,
      });
    
     //  this.count+=1;
      this._tablecounterload+=1;
      this.allcheckboxload=false;
    }
    else{
      var find=this.LoadData.find(x=>x.AccountId==accountId);
      var RemoveIndex=this.LoadData.indexOf(find);
      this.LoadData.splice(RemoveIndex,1);
     //  this.count-=1;
     
      this._tablecounterload-=1;
      this.allcheckboxload=false;
    }
   }
  functionCountload(e){
    debugger
    if(e.target.checked)
    {
    for(var i=0;i< this._connectionDestination.length;i++)
    {
      var id= this._connectionDestination[i].accountID;
      var connectorname= this._connectionDestination[i].connectorName ;
      var connectorid= this._connectionDestination[i].connectorID;
      this.allcheckboxload=true;
      this.findCountload(e,id,connectorid,connectorname);
     }
      this.allcheckboxload=true;
      this._tablecounterload= this._connectionDestination.length;
       
    }
    else if(e.target.checked==false){
      for(var i=0;i<this._connectionDestination.length;i++)
      {
        var id=this._connectionDestination[i].accountID;
        var connectorname=this._connectionDestination[i].connectorName;
        var connectorid=this._connectionDestination[i].connectorID;
        this.allcheckboxload=false;
        this.findCountload(e,id,connectorid,connectorname);
      }
        this.allcheckboxload=false;
         this._tablecounterload=0;
    }
 
   }
 
 
    applySort(fieldName){
     if(fieldName=='connectorName'){
       this.appearanceLogo_arrow=true;
       this.appearanceLogo_toggle=!this.appearanceLogo_toggle;
       this.lookupvalueListservice._sortToggle=!this.lookupvalueListservice._sortToggle;
       this.lookupvalueListservice.sortAllDestinationList(fieldName);
     }
     else if(fieldName=='workspaceName'){
       this.workspaceName_arrow=true;
       this.workspaceName_toggle=!this.workspaceName_toggle;
       this.lookupvalueListservice._sortToggle=!this.lookupvalueListservice._sortToggle;
       this.lookupvalueListservice.sortAllDestinationList(fieldName);
     }
     else if(fieldName=='accountDisplayName'){
       this.connectorName_arrow=true;
       this.connectorName_toggle=!this.connectorName_toggle;
       this.lookupvalueListservice._sortToggle=!this.lookupvalueListservice._sortToggle;
       this.lookupvalueListservice.sortAllDestinationList(fieldName);
     }
     else if(fieldName=='accessGranted'){
       
       this.bstatus_arrow=true;
       this.bstatus_toggle=!this.bstatus_toggle;
       this.lookupvalueListservice._sortToggle=!this.lookupvalueListservice._sortToggle;
       this.lookupvalueListservice.sortAllDestinationList(fieldName);
     }
    
   }
   reloadPage(){
     this.appearanceLogo_arrow=false;
     this.workspaceName_arrow=false;
     this.connectorName_arrow=false;
     this.bstatus_toggle=false;
     this.lookupvalueListservice.GetDestinationList().subscribe((data:any)=>{
       this._connectionDestination=data;
     });
    }
   

}
// interface ModifiedRecords {
//   addedRecords: { [key: string]: Object }[];
//   deletedRecords: { [key: string]: Object }[];
//   changedRecords: { [key: string]: Object }[];
// }
