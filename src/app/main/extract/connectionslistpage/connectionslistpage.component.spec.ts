import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectionslistpageComponent } from './connectionslistpage.component';

describe('ConnectionslistpageComponent', () => {
  let component: ConnectionslistpageComponent;
  let fixture: ComponentFixture<ConnectionslistpageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnectionslistpageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectionslistpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
