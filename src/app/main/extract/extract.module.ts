import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { ExtractRoutingModule } from './extract-routing.module';
 
import { ConnectComponent } from './connect.component';
import { HomeComponent } from './home/home.component';
import { DataStreamComponent } from './data-stream/data-stream.component';
import { AllExtractsComponent } from './all-extracts/all-extracts.component';
import { AllIssuesComponent } from './all-issues/all-issues.component';
import { AllConnectionsComponent } from './all-connections/all-connections.component';
import { ConnectFiltersComponent } from './connect-filters/connect-filters.component';
 
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { ConnectorsComponent } from './connectors/connectors.component';
import { OverviewAllComponent } from './connectors/overview-all/overview-all.component';
import { InstagramConfigurationComponent } from './connectors/Instagram/instagram-configuration/instagram-configuration.component';
import { LocalDataRetentionComponent } from './connectors/Instagram/local-data-retention/local-data-retention.component';
import { PrimaryStorageComponent } from './connectors/Instagram/primary-storage/primary-storage.component';
import { MappingRulesComponent } from './connectors/Instagram/mapping-rules/mapping-rules.component';
import { SecondaryStorageComponent } from './connectors/Instagram/secondary-storage/secondary-storage.component';
import { SetupNewConnectionComponent } from './AddNewDataStream/setup-new-connection/setup-new-connection.component';
import { DatastreamSqlComponent } from './AddNewDataStream/datastream-sql/datastream-sql.component';
import { TempletForConnectionComponent } from './AddNewDataStream/templet-for-connection/templet-for-connection.component';
import { SourceObjectComponent } from './AddNewDataStream/source-object/source-object.component';
import { ExecutionPlanComponent } from './AddNewDataStream/execution-plan/execution-plan.component';
import { ExtractComponent } from './AddNewDataStream/extract/extract.component';
import { FieldsSelectionComponent } from './AddNewDataStream/fields-selection/fields-selection.component';
import { FiltersComponent } from './AddNewDataStream/filters/filters.component';
import { NewDataStreamComponent } from './AddNewDataStream/new-data-stream/new-data-stream.component';
import { SelectDataStreamComponent } from './AddNewDataStream/select-data-stream/select-data-stream.component';
import { SideBarComponent } from './AddDatastreamSideBar/side-bar/side-bar.component';
 
import { MongoDbSettupComponent } from './DataBase/mongo-db-settup/mongo-db-settup.component';
import { FilesUploadeComponent } from './Filse/files-uploade/files-uploade.component';
import { FBPublicPageComponent } from './Social-media/facebook/fbpublic-page/fbpublic-page.component';
import { ConnectorTypesComponent } from './AddNewConnection/connector-types/connector-types.component';
import { ConfigurationComponent } from './AddNewConnection/configuration/configuration.component';
import { DescriptionComponent } from './AddNewConnection/description/description.component';
import { MainComponentComponent } from './AddNewConnection/NewConnectionMainComponent/main-component/main-component.component';
import { SideWizardMenuComponent } from './AddNewConnection/side-wizard-menu/side-wizard-menu.component';
import { SourceAccountSettupComponent } from './AddNewConnection/source-account-settup/source-account-settup.component';
import { SummaryPageComponent } from './AddNewConnection/summary-page/summary-page.component';
import { TemplateComponent } from './AddNewConnection/template/template.component';
import { SelectDestinationComponent } from './Destination/bodycontent/destinationtype/select-destination/select-destination.component';
import { DestinationComponent } from './Destination/bodycontent/destination/destination.component';
import { SidemenuComponent } from './Destination/bodycontent/sidemenu/sidemenu.component';
import { DesTemplateComponent } from './Destination/bodycontent/des-template/des-template.component';
import { DesDescriptionComponent } from './Destination/bodycontent/des-description/des-description.component';
import { DesConfigurationComponent } from './Destination/bodycontent/des-configuration/des-configuration.component';
import { LoadConnectorsDirective } from './load-connectors.directive';
import { ConnectionslistpageComponent } from './connectionslistpage/connectionslistpage.component';
 import { DropDownListModule, ListBoxComponent, ListBoxModule, MultiSelectModule } from '@syncfusion/ej2-angular-dropdowns';
import { AddNewConPipelineComponent } from './add-new-con-pipeline/add-new-con-pipeline.component';
import { SyncfusionDragControlComponent } from './syncfusion-drag-control/syncfusion-drag-control.component';
import { EJAngular2Module } from 'ej-angular2';
import { LeftStatusBarComponent } from './FileUploader/left-status-bar/left-status-bar.component';
import { FileUploadComponent } from './FileUploader/file-upload/file-upload.component';
import { MaindashbordComponent } from './FileUploader/maindashbord/maindashbord.component';
import { ButtonModule, CheckBoxModule  } from '@syncfusion/ej2-angular-buttons';

import { DialogModule } from '@syncfusion/ej2-angular-popups';
import { EnrichModelMappingComponent } from './FileUploader/enrich-model-mapping/enrich-model-mapping.component';

import { UploaderModule } from '@syncfusion/ej2-angular-inputs';
import { EnrichCSViewerComponent } from './FileUploader/enrich-csviewer/enrich-csviewer.component';
import { ContentmappingComponent } from './FileUploader/contentmapping/contentmapping.component';
import { EnrichAgGridCSVViewerComponent } from './FileUploader/enrich-ag-grid-csvviewer/enrich-ag-grid-csvviewer.component';
import { AgGridModule } from 'ag-grid-angular';
import { SourceSummarPageComponent } from './source-summar-page/source-summar-page.component';
import { LogsfileviewerAggridComponent } from './logsfileviewer-aggrid/logsfileviewer-aggrid.component';
import { TransformationlistComponent } from './connectors/transformationlist/transformationlist.component';
import { DestinationListComponent } from '../load/destination-list/destination-list.component';
import { ContentmappingListComponent } from './connectors/contentmapping-list/contentmapping-list.component';
import { TransformationScriptListComponent } from './FileUploader/transformation-script-list/transformation-script-list.component';
import { DropDownButtonModule } from '@syncfusion/ej2-angular-splitbuttons';
import { ProgressBarAllModule } from '@syncfusion/ej2-angular-progressbar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {ProgressBarModule} from 'primeng/progressbar';
import {ToastModule} from 'primeng/toast';
import { ConnectionPagesDirective } from 'src/app/shared/directives/connection-pages.directive';
import { SharedModule } from 'src/app/shared/shared.module';
import { WidgetModule } from 'src/app/widget/widget.module';
import {  CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
@NgModule({
  declarations:[
    ConnectComponent,
    HomeComponent,
    DataStreamComponent,
    AllExtractsComponent,
    AllIssuesComponent, 
    AllConnectionsComponent,
    ConnectFiltersComponent,
    ConnectorsComponent,OverviewAllComponent,
    InstagramConfigurationComponent,
    LocalDataRetentionComponent,
    PrimaryStorageComponent,
    SecondaryStorageComponent,
    MappingRulesComponent,
    DatastreamSqlComponent,
    ExecutionPlanComponent,
    ExtractComponent,
    FieldsSelectionComponent,
    FiltersComponent,
    NewDataStreamComponent,
    SelectDataStreamComponent,
    SetupNewConnectionComponent,
    SourceObjectComponent,
    TempletForConnectionComponent,
    SideBarComponent,
    ConnectionPagesDirective,
    MongoDbSettupComponent,
    SetupNewConnectionComponent,
    FilesUploadeComponent,
    FBPublicPageComponent,
    ConnectorTypesComponent,
    ConfigurationComponent,
    DescriptionComponent,
    MainComponentComponent,
    SideWizardMenuComponent,
    SourceAccountSettupComponent,
    SummaryPageComponent,
    TemplateComponent,
    SelectDestinationComponent,
    DestinationComponent,
    SidemenuComponent,
    DesTemplateComponent,
    DesDescriptionComponent,
    DesConfigurationComponent,
    LoadConnectorsDirective,
    ConnectionslistpageComponent,
    AddNewConPipelineComponent,
    SyncfusionDragControlComponent,
    LeftStatusBarComponent,
    FileUploadComponent,
    MaindashbordComponent,
    EnrichModelMappingComponent ,
    EnrichCSViewerComponent,
    ContentmappingComponent,
    EnrichAgGridCSVViewerComponent,
    SourceSummarPageComponent,
    LogsfileviewerAggridComponent,
    TransformationlistComponent,
    ContentmappingListComponent,
    TransformationScriptListComponent,
   ],
  imports: [
    SharedModule,
    CommonModule,
    ExtractRoutingModule,
    ProgressBarAllModule,
    
    WidgetModule,
    NgSelectModule,
    FormsModule,
    ListBoxModule,
    EJAngular2Module.forRoot(),
    DropDownListModule,
    CheckBoxModule,
    DialogModule,
    UploaderModule,
   AgGridModule.withComponents([]),
   DropDownButtonModule,
    MultiSelectModule,
    ButtonModule,
   // BrowserAnimationsModule,
    ProgressBarModule,
    
    
  ],
  providers: [DatePipe],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  entryComponents:[
    MongoDbSettupComponent,
    SetupNewConnectionComponent,
    FilesUploadeComponent,
    FBPublicPageComponent,
  ]
})
export class ExtractModule { }
