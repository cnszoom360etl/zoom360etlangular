import { Component, OnInit ,ViewChild, ComponentFactoryResolver, ViewContainerRef, Injector} from '@angular/core';
import { Router } from '@angular/router';
import { SourceAccount, updatesourceaccountStep2 } from 'src/app/models/extract/access-microsoft-sqlserver';
import { SourceAccountSettup } from 'src/app/services/extract/AddNewConnectionServices/SourceAccountSettup';
import { AddNewConnectoinWizardStyle } from 'src/app/services/extract/AddNewConnectionWizardStyle';
import { UpdateConnectorPages } from 'src/app/services/extract/ConnectorService/UpdateConnectorPages';
import { ConnectionPagesDirective } from 'src/app/shared/directives/connection-pages.directive';
import { componentDictionary, groupingDictionary } from 'src/app/utils/DictionaryMapping';
    import { isNullOrUndefined } from 'util';
    import { environment } from 'src/environments/environment';
    import { AppComponentBase } from 'src/app/services/AppComponentBase';

@Component({
  selector: 'app-source-account-settup',
  templateUrl: './source-account-settup.component.html',
  styleUrls: ['./source-account-settup.component.css']
})
export class SourceAccountSettupComponent extends AppComponentBase implements OnInit {
  _component: any;
  componentsList = [];
  buttonDisabled=true;
  EmailAuthorizationStaus=false;
  EmailAuthurizationtextarea=false;
  EmailAuthurizationTextAreaMsg:string="";
  SourceAccountModel=new SourceAccount();
   
  EmailAuthurizationArea=true;
  SettupNewConnection=false;
  ComponentDictionary=new componentDictionary();
  ComponentGroupingDictionary=new groupingDictionary();
  savebtn=false;
  workspacedropdownlist:any[]=[];
  imgname:string;
  configure:string="Save";
  // @ViewChild(LoadConnectorsDirective , { static: true }) tabHost: LoadConnectorsDirective;
  @ViewChild(ConnectionPagesDirective , { static: true }) tabHost: ConnectionPagesDirective;
  Acount_id=localStorage.getItem("value");
  UpdateSourceAccountLst:updatesourceaccountStep2[]=[];
  constructor(
    private viewContainerRef: ViewContainerRef,
    private componentFactoryResolver: ComponentFactoryResolver,
     private router: Router,
     public _updateSourceAccount:UpdateConnectorPages,
     public sourceAccountSettup:SourceAccountSettup,
     public addNewConnectoinWizardStyle:AddNewConnectoinWizardStyle,
     injector : Injector
    
  ) {
    super(injector)
    this.imgname=localStorage.getItem('src');

  }

  ngOnInit(): void {
    
        
        this.getWorspaceDropdown()
        this.GetSelectedComponent()
      
      
      
  
  }
  ReversClass(){
     

    this.addNewConnectoinWizardStyle.SourceAccountClass="AddNewConnectionSourcewizard"
    this.addNewConnectoinWizardStyle.ConfiguureWizard=true;
    this.router.navigate(['extract//AddNewConnection/Template']);
  
  }
   
  ShowSettupNewConButton(){
    this.EmailAuthurizationArea=false;
    this.SettupNewConnection=true;
  }
  ShowEmailAuthorization(){
    this.EmailAuthurizationArea=true;
    this.SettupNewConnection=false;
  }
  GetSelectedComponent(){
    debugger
    // var nameOfComponent=JSON.parse(localStorage.getItem('ConnectorName'));
    var idOfComponent=JSON.parse(localStorage.getItem('Connectorid'));
    var Coonectortitle=localStorage.getItem("Connectortitle");
    var nameOfComponent=this.ComponentGroupingDictionary.getgroupofComponent(Coonectortitle);
    this._component=this.ComponentDictionary.getComponent(Coonectortitle);
    if(nameOfComponent=="FileComponent"){
       this.savebtn=true;
       
     }
     if(nameOfComponent=="Socialmedia"){
      this.savebtn=true;
      if(this.Acount_id!=null)
      {
        this.UpdateSocialMedia()
      }
      else{
        
      }
     
    }
     if(nameOfComponent=="DBConnectorCredentials")
     {
       if(this.Acount_id!=null)
       {
        this.configure="Update"
        this.UpdateDBCredential()
       }
       else
       {
       this.sourceAccountSettup.Hostname="192.168.223.111";
       this.sourceAccountSettup.Database=null;
       this.sourceAccountSettup.Username=null;
       this.sourceAccountSettup.Password=null;
       this.sourceAccountSettup.PortNumber=null;
       this.sourceAccountSettup.status=false;
       
       }

      
     }
     
     
    if(this._component!=undefined){
      this.loadTabComponent(this._component);

    }
    
  }
  loadTabComponent(_selectedTab) {
    debugger
     if (!isNullOrUndefined(this.componentsList)) {
      this.componentsList.map((cm, i) => {
        let tmp = this.viewContainerRef;
        this.viewContainerRef.remove(this.viewContainerRef.indexOf(cm));
        this.viewContainerRef.remove(i);
        this.componentsList.splice(i, 1);
      });
    }

     

    this.viewContainerRef.detach();
    this.viewContainerRef.clear();
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(_selectedTab);
    this.viewContainerRef = this.tabHost.viewContainerRef;

    let componentRef = this.viewContainerRef.createComponent(componentFactory);
    this.componentsList.push(componentRef);
  }

  getWorspaceDropdown(){
    debugger
    var UserId= this.storageService.getItem(environment.storage.userId);
    var DropdownType='workspace'
    debugger
    this.sourceAccountSettup.SourceAccountSettup(UserId,DropdownType).subscribe((data:any)=>{
      debugger
      if(data.length > 0){
      this.workspacedropdownlist=data;
    }
    });
  }
  AddEmialForAuthorize() { 
    debugger
    var Email=(<HTMLInputElement>document.getElementById('Email')).value;
 var AccountAuthurization = (<HTMLInputElement>document.querySelector('input[name = AccountAuthurization]:checked')).value;
    var UserData={
       Email,
      AccountAuthurization
    }
    debugger
     
    }  
    SaveAuthorizData(SourceAccountSettupData:SourceAccount){
    debugger
    if(SourceAccountSettupData.Email!=undefined){
      this.sourceAccountSettup.SourceAccountSettupEmailAuthorization(SourceAccountSettupData).subscribe((data:any)=>{
        debugger
            this.EmailAuthurizationtextarea=true;
             this.EmailAuthurizationTextAreaMsg=data.trim();
             this.router.navigate(['extract//AddNewConnection/ViewdataSummary']);

      })
    }
     }

     SaveCredential(){
       this.sourceAccountSettup.Savedbcredentials();
     }
     UpdateDBCredential()
     {
      var UserId=this.storageService.getItem(environment.storage.userId);
      var Workspaceid=this.storageService.getItem(environment.storage.workspaceId);
      var Clientid=this.storageService.getItem(environment.storage.clientId);
      var connectorId=JSON.parse(localStorage.getItem('ConnectorId'));
      this._updateSourceAccount.RefreshConnectionListPageEditMode(this.Acount_id,UserId,Workspaceid,Clientid,connectorId).subscribe((data:updatesourceaccountStep2[])=>{
        debugger
        if(data.length > 0){
         // this.AccountSourceButtom="Update And Next";
          this.UpdateSourceAccountLst=data;
          
       let resultkeyvalue = this._updateSourceAccount.UpdateSourceAccountLst.map(a => a.fieldvalue);
       this.sourceAccountSettup.Hostname=resultkeyvalue[1];
       this.sourceAccountSettup.Database=resultkeyvalue[0];
       this.sourceAccountSettup.Username=resultkeyvalue[4];
       this.sourceAccountSettup.Password=resultkeyvalue[2];
       this.sourceAccountSettup.PortNumber=resultkeyvalue[3];
       this.sourceAccountSettup.status=Boolean(this._updateSourceAccount.UpdateSourceAccountLst.map(a => a.connectivitystatus[0]));    
           
    
    
      }
      });
     }
     UpdateSocialMedia(){
      var UserId=this.storageService.getItem(environment.storage.userId);
      var Workspaceid=this.storageService.getItem(environment.storage.workspaceId);
      var Clientid=this.storageService.getItem(environment.storage.clientId);
      var connectorId=JSON.parse(localStorage.getItem('ConnectorId'));
      this._updateSourceAccount.RefreshConnectionListPageEditMode(this.Acount_id,UserId,Workspaceid,Clientid,connectorId).subscribe((data:updatesourceaccountStep2[])=>{
        debugger
        if(data.length > 0){
           this.UpdateSourceAccountLst=data;
          this.sourceAccountSettup.SourceAccountModel.Email=this.UpdateSourceAccountLst.map(a=>a.emailid)[0];
          this.sourceAccountSettup.SourceAccountModel.AccountAuthurization=this.UpdateSourceAccountLst.map(a=>a.authorizationgrant)[0];
          this.sourceAccountSettup.SourceAccountModel.connectivitystatus=Boolean(this.UpdateSourceAccountLst.map(a=>a.connectivitystatus)[0]);
        }
      });
     }
}

