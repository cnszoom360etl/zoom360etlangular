import { Component, Injector, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponentBase } from 'src/app/services/AppComponentBase';
import { AddNewConnectoinWizardStyle } from 'src/app/services/extract/AddNewConnectionWizardStyle';
import { environment } from 'src/environments/environment';
 

@Component({
  selector: 'app-side-wizard-menu',
  templateUrl: './side-wizard-menu.component.html',
  styleUrls: ['./side-wizard-menu.component.css']
})
export class SideWizardMenuComponent extends AppComponentBase {
  ConnectorHeading:string=null;
  constructor(public addNewConnectoinWizardStyle:AddNewConnectoinWizardStyle,private router: Router,injector : Injector) { super(injector)}

  ngOnInit(): void {
    debugger
    let path = window.location.href;
    this.ConnectorHeading=this.storageService.getItem(environment.storage.ConnectorHeadingForNewAndUpdate);
    var rest = path.substring(path.lastIndexOf("/") + 1);
    this.checkUrlComponentName(rest);
  }
  checkUrlComponentName(rest){
    debugger
     if(rest=="des"){
      //this.router.navigate(['/extract/AddnewDataStream/des']);
      this.addNewConnectoinWizardStyle.ConnectionTypeWizardClass="AddNewConnectionTemplatewizardtick";
      this.addNewConnectoinWizardStyle.TempleteAccountWizard=false;
      // this.WizardControlsService.SourceAccountClass="css-skkt0a";

    }
    else if(rest=="Template"){
      this.router.navigate(['/extract/AddnewDataStream/Template']);
      this.addNewConnectoinWizardStyle.ConnectionTypeWizardClass="AddNewConnectionTemplatewizardtick";
      this.addNewConnectoinWizardStyle.TempleteAccountWizard=false;
       this.addNewConnectoinWizardStyle.TempleteWizardClass="AddNewConnectionTemplatewizardtick";
      this.addNewConnectoinWizardStyle.SourceAccountDisable=false;
      // this.WizardControlsService.TempleteWizardClass="css-Templatewizard";
    }
    else if(rest=="Configuration"){
      this.router.navigate(['/extract/AddnewDataStream/Micruration']);
      this.addNewConnectoinWizardStyle.ConnectionTypeWizardClass="AddNewConnectionTemplatewizardtick";
      this.addNewConnectoinWizardStyle.TempleteAccountWizard=false;
       this.addNewConnectoinWizardStyle.TempleteWizardClass="AddNewConnectionTemplatewizardtick";
       this.addNewConnectoinWizardStyle.SourceAccountDisable=false;
      this.addNewConnectoinWizardStyle.SourceAccountClass="AddNewConnectionSourcewizardtick";
      this.addNewConnectoinWizardStyle.ConfiguureWizard=false;
    }
     
  }

}
