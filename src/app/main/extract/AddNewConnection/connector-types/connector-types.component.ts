import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ToastComponent } from '@syncfusion/ej2-angular-notifications';
import { ButtonComponent } from 'ej-angular2';
import { ToastMessage } from 'src/app/models/MessageTypes/toast-message';
import { ConnectorStatusService } from 'src/app/services/extract/AddNewConnectionServices/connector-status.service';
import { SourceAccountSettup } from 'src/app/services/extract/AddNewConnectionServices/SourceAccountSettup';
import { AddNewConnectoinWizardStyle } from 'src/app/services/extract/AddNewConnectionWizardStyle';
import { ConnectorTypeDashbord } from 'src/app/services/extract/ConnectorService/Connectortype';
import { FileUploadService } from 'src/app/services/extract/FilesValidations';
import { ServiceService } from 'src/app/services/extract/service.service';
import { environment } from 'src/environments/environment';
import { AppComponentBase } from 'src/app/services/AppComponentBase';

@Component({
  selector: 'app-connector-types',
  templateUrl: './connector-types.component.html',
  styleUrls: ['./connector-types.component.css']
})
export class ConnectorTypesComponent extends AppComponentBase implements OnInit {
//for toast
public testToast=new ToastMessage();
@ViewChild('defaulttoast',{static:true})
public toastObj: ToastComponent;
@ViewChild('toastBtnShow',{static:true})
public btnEleShow: ButtonComponent;
public position=this.testToast.position;
// toast end

  Files=false;
  RelationalDatabases=false;
  FileDatalake=false;
  ResentUseDb=true;
  ResentModules:any[]=[];
  FileClasshightLight:string;
  RecentDashBox:string="box-active";
  DatabasefileBox:string;
  businessfileBox:string

  constructor(public addNewConnectoinWizardStyle:AddNewConnectoinWizardStyle,private router: Router,private fileUploadService:FileUploadService,
    private connectorTypeDashbord:ConnectorTypeDashbord,
    private serviceService:ServiceService,
    public sourceAccountSettupservis:SourceAccountSettup,
    public connectorStatusService:ConnectorStatusService,
    injector : Injector
    ) { super(injector)}

  ngOnInit(): void {
    this.GetResendModule();
    this.addNewConnectoinWizardStyle.ConnectionTypeWizardClass="css-Connectorwizard";
    this.addNewConnectoinWizardStyle.TempleteAccountWizard=true;
    this.addNewConnectoinWizardStyle.SourceAccountDisable=true;
    this.addNewConnectoinWizardStyle.ConfiguureWizard=true;
    
  }
  GoSocialMedia(FileName,id){
    localStorage.removeItem('src');
    debugger
     
    this.addNewConnectoinWizardStyle.ConnectionTypeWizardClass="AddNewConnectorType";
    this.addNewConnectoinWizardStyle.TempleteAccountWizard=false;
    var mydivval=document.getElementById(id);
    var myimg=mydivval.getElementsByTagName('img')[0];
    var accountId=myimg.sizes;
    var ConnectorId=myimg.id;
    var name=myimg.alt;
    var ConnectorName=myimg.name;
    var Connectortitle=myimg.title;
    localStorage.setItem('src',FileName);
    JSON.stringify(localStorage.setItem("accountId",accountId));
    localStorage.removeItem("value")
    JSON.stringify(localStorage.setItem("Connectortitle",Connectortitle));
    localStorage.setItem('ConnectorName',JSON.stringify(ConnectorName));
    localStorage.setItem('ConnectorId',JSON.stringify(ConnectorId));
     
    this.connectorStatusService.getConnectorStatus(ConnectorId).subscribe((test:any)=>{
      debugger
      var data=JSON.parse(test);
      if(data=="1")
      {
        localStorage.removeItem('src');
        debugger
         
        this.router.navigate(['extract//AddNewConnection/des']);
      }
      else
      {
          this.testToast.toast[2].content=data;
          this.toastObj.show(this.testToast.toast[2]);
      }
    })
    
  }
  GetInfoDBConnector(FileName,id){
     
    localStorage.clear();
    debugger
    this.addNewConnectoinWizardStyle.ConnectionTypeWizardClass="AddNewConnectorType";
    this.addNewConnectoinWizardStyle.TempleteAccountWizard=false;
    var mydivval=document.getElementById(id);
    var myimg=mydivval.getElementsByTagName('img')[0];
    var mycomponentName=myimg.sizes;
    var ConnectorId=myimg.id;
    var name=myimg.alt;
    var ConnectorName=myimg.name;
    var Connectortitle=myimg.title;
    var accountId=myimg.sizes;
    localStorage.setItem('src',FileName);
    JSON.stringify(localStorage.setItem("accountId",accountId));
    JSON.stringify(localStorage.setItem("Connectortitle",Connectortitle));
    localStorage.setItem('ConnectorName',JSON.stringify(ConnectorName));
    localStorage.setItem('ConnectorId',JSON.stringify(ConnectorId));
    this.connectorStatusService.getConnectorStatus(ConnectorId).subscribe((test:any)=>{
      debugger
      var data=JSON.parse(test);
      if(data=="1")
      {
        
        debugger
         
        this.router.navigate(['extract//AddNewConnection/des']);
      }
      else
      {
        this.testToast.toast[2].content=data;
        this.toastObj.show(this.testToast.toast[2]);
      }
    })

    }
    FilesShow(){
      debugger
      this.Files=true;
      this.ResentUseDb=false;
      this.RelationalDatabases=false;
      this.FileDatalake=false;
      this.FileClasshightLight="box-active";
      this.RecentDashBox="simple-box"; 
      this.DatabasefileBox="simple-box"; 
      this.businessfileBox="simple-box"; 
       
    }
    ResentShow(){
      this.Files=false;
      this.ResentUseDb=true;
      this.RelationalDatabases=false;
      this.FileDatalake=false;
      this.FileClasshightLight="simple-box";
      this.RecentDashBox="box-active"; 
      this.DatabasefileBox="simple-box"; 
      this.businessfileBox="simple-box"; 
    }
    Databaseshow(){
      this.Files=false;
      this.ResentUseDb=false;
      this.RelationalDatabases=true;
      this.FileDatalake=false;
      this.FileClasshightLight="simple-box";
      this.RecentDashBox="simple-box"; 
      this.DatabasefileBox="box-active"; 
      this.businessfileBox="simple-box";
    }
    ShowBusinessPlateForm(){
      this.Files=false;
      this.ResentUseDb=false;
      this.RelationalDatabases=false;
      this.FileDatalake=true;
      this.FileClasshightLight="simple-box";
      this.RecentDashBox="simple-box"; 
      this.DatabasefileBox="simple-box"; 
      this.businessfileBox="box-active";
    }
    Fileuploade(value,FileName,id){
      debugger
      localStorage.removeItem('src');
      JSON.stringify(localStorage.setItem("FileType",value));
      this.sourceAccountSettupservis.FileType=value;
     
    var mydivval=document.getElementById(id);
    var myimg=mydivval.getElementsByTagName('img')[0];
     var imgpath=myimg.src;
     var z =imgpath.substring(imgpath.lastIndexOf('/')+1);
    var ConnectorName=myimg.name;
    var Connectortitle=myimg.title;
    var ConnectorId=myimg.id;
    var accountId=myimg.sizes;
    localStorage.setItem('src',FileName);
    JSON.stringify(localStorage.setItem("accountId",accountId));
    JSON.stringify(localStorage.setItem("Connectortitle",Connectortitle));
    localStorage.setItem('ConnectorName',JSON.stringify(ConnectorName));
    localStorage.setItem('ConnectorId',JSON.stringify(ConnectorId));
    this.connectorStatusService.getConnectorStatus(ConnectorId).subscribe((test:any)=>{
      debugger
      var data=JSON.parse(test);
      if(data=="1")
      {
        
        debugger
        this.addNewConnectoinWizardStyle.ConnectionTypeWizardClass="AddNewConnectorType";
        this.addNewConnectoinWizardStyle.TempleteAccountWizard=false;
        this.router.navigate(['extract//AddNewConnection/des']);
      }
      else
      {
          this.testToast.toast[2].content=data;
          this.toastObj.show(this.testToast.toast[2]);
      }
    })
   
    }

    GetResendModule(){
      debugger
      var UserId=this.storageService.getItem(environment.storage.userId);
      var Workspaceid=this.storageService.getItem(environment.storage.workspaceId);
      var Clientid=this.storageService.getItem(environment.storage.clientId);
      var GetConnectorIds="182";
     
      this.connectorTypeDashbord.ResentModule(UserId,Workspaceid,Clientid).subscribe((data:any)=>{
        if(data.length >0){
          debugger
         this.ResentModules=data;
     
        }
        });
     
      
     //}
  
    }
}
