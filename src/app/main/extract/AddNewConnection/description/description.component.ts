import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ToastComponent } from '@syncfusion/ej2-angular-notifications';
import { ButtonComponent } from 'ej-angular2';
import { updatesourceaccountStep2 } from 'src/app/models/extract/access-microsoft-sqlserver';
import { Descriptionmodel } from 'src/app/models/extract/description-model';
import { ToastMessage } from 'src/app/models/MessageTypes/toast-message';
import { SourceAccountSettup } from 'src/app/services/extract/AddNewConnectionServices/SourceAccountSettup';
import { AddNewConnectoinWizardStyle } from 'src/app/services/extract/AddNewConnectionWizardStyle';
import { UpdateConnectorPages } from 'src/app/services/extract/ConnectorService/UpdateConnectorPages';
import { environment } from 'src/environments/environment';
import { AppComponentBase } from 'src/app/services/AppComponentBase';
@Component({
  selector: 'app-description',
  templateUrl: './description.component.html',
  styleUrls: ['./description.component.css']
})
export class DescriptionComponent extends AppComponentBase implements OnInit {

  public testToast=new ToastMessage();
  @ViewChild('defaulttoast',{static:true})
  public toastObj: ToastComponent;
  @ViewChild('toastBtnShow',{static:true})
  public btnEleShow: ButtonComponent;
  public position=this.testToast.position;

  DescriptionModel=new Descriptionmodel();
  UpdateSourceAccountLst:updatesourceaccountStep2[]=[];
  workspacedropdownlist:any[]=[];
  Accountdisplayname=true;
  AccountSourceButtom:string="Save and Next";
  imgname:string;
  Acount_id=localStorage.getItem("value");
  constructor(  public sourceAccountSettup:SourceAccountSettup,public _updateSourceAccount:UpdateConnectorPages,
    public addNewConnectoinWizardStyle:AddNewConnectoinWizardStyle,
    private router: Router,injector : Injector) { 
      super(injector)
       this.imgname=localStorage.getItem('src');

    }

  ngOnInit(): void {
    this.getWorspaceDropdown()
     if(this.Acount_id!=null)
     {
      this.addNewConnectoinWizardStyle.ConnectionTypeWizardClass="AddNewConnectorType";
      this.addNewConnectoinWizardStyle.TempleteAccountWizard=false;
      this.addNewConnectoinWizardStyle.TempleteWizardClass="AddNewConnectionTemplatewizard";
      this.addNewConnectoinWizardStyle.SourceAccountDisable=true;
      this.addNewConnectoinWizardStyle.ConfiguureWizard=true;
      this.geteditmode();
      this.ControlEditDataOnRefreshPage()
     }
     
    
    
  }
  getWorspaceDropdown(){
    debugger
    var UserId=this.storageService.getItem(environment.storage.userId);
    var DropdownType='workspace'
    debugger
    this.sourceAccountSettup.SourceAccountSettup(UserId,DropdownType).subscribe((data:any)=>{
      debugger
      if(data.length > 0){
      this.workspacedropdownlist=data;
    }
    });
  }
  ReversClass(){
    this.addNewConnectoinWizardStyle.ConnectionTypeWizardClass="css-Connectorwizard"
    this.addNewConnectoinWizardStyle.TempleteAccountWizard=true;
     this.router.navigate(['extract/AddNewConnection/ConnectorType']);
 
 }
 nextmove(Description:Descriptionmodel){
  localStorage.getItem("description");
  localStorage.setItem("description",JSON.stringify(Description));
  this.sourceAccountSettup.SaveDescriptionInfo(Description).subscribe((data:any)=>{
    if(data.length==1)
    {
      this.addNewConnectoinWizardStyle.TempleteWizardClass="AddNewConnectionTemplatewizardtick"
      this.addNewConnectoinWizardStyle.SourceAccountDisable=false;
      this.router.navigate(['extract/AddNewConnection/Template']);

    }
    else
    {
      this.testToast.toast[2].content=data;
      this.toastObj.show(this.testToast.toast[2]);
    }
 
  })
 }
 SaveDescription(Description:Descriptionmodel){
  debugger
  localStorage.getItem("description");
  localStorage.setItem("description",JSON.stringify(Description));
  this.sourceAccountSettup.SaveDescriptionInfo(Description).subscribe((data:any)=>{
   
 })
 }
 geteditmode()
 {
  this.AccountSourceButtom="Update and Next";
  this.DescriptionModel.accountName=this._updateSourceAccount.UpdateSourceAccountLst.map(a=>a.accountdisplayname)[0];
  this.DescriptionModel.workspace=this._updateSourceAccount.UpdateSourceAccountLst.map(a=>a.workspacename)[0];
  this.DescriptionModel.enableConnectoin=Boolean(this._updateSourceAccount.UpdateSourceAccountLst.map(a=>a.enableconnection)[0]);
  this.DescriptionModel.visibilitymode=Boolean(this._updateSourceAccount.UpdateSourceAccountLst.map(a=>a.visibileconnection)[0]);
  this.DescriptionModel.commentsection=this._updateSourceAccount.UpdateSourceAccountLst.map(a=>a.comments)[0];
  this.DescriptionModel.specialcomments=this._updateSourceAccount.UpdateSourceAccountLst.map(a=>a.spcomments)[0];
 }
 ControlEditDataOnRefreshPage(){
  var UserId=this.storageService.getItem(environment.storage.userId);
  var Workspaceid=this.storageService.getItem(environment.storage.workspaceId);
  var Clientid=this.storageService.getItem(environment.storage.clientId);
  var connectorId=JSON.parse(localStorage.getItem('ConnectorId'));
  this._updateSourceAccount.RefreshConnectionListPageEditMode(this.Acount_id,UserId,Workspaceid,Clientid,connectorId).subscribe((data:updatesourceaccountStep2[])=>{
    debugger
    if(data.length > 0){
      this.AccountSourceButtom="Update And Next";
      this.UpdateSourceAccountLst=data;
      this.DescriptionModel.accountName=this.UpdateSourceAccountLst.map(a=>a.accountdisplayname)[0];
      this.DescriptionModel.workspace=this.UpdateSourceAccountLst.map(a=>a.workspacename)[0];
      this.DescriptionModel.enableConnectoin=Boolean(this.UpdateSourceAccountLst.map(a=>a.enableconnection)[0]);
      this.DescriptionModel.visibilitymode=Boolean(this.UpdateSourceAccountLst.map(a=>a.visibileconnection)[0]);
      this.DescriptionModel.commentsection=this.UpdateSourceAccountLst.map(a=>a.comments)[0];
      this.DescriptionModel.specialcomments=this.UpdateSourceAccountLst.map(a=>a.spcomments)[0];
       


  }
  });
 }
}
