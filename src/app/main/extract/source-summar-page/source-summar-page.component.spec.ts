import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SourceSummarPageComponent } from './source-summar-page.component';

describe('SourceSummarPageComponent', () => {
  let component: SourceSummarPageComponent;
  let fixture: ComponentFixture<SourceSummarPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SourceSummarPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SourceSummarPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
