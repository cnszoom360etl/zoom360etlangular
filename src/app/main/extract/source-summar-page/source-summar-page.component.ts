import { Component, Injector, OnInit } from '@angular/core';
 
import { environment } from 'src/environments/environment';
import { AppComponentBase } from 'src/app/services/AppComponentBase';
import { inject } from '@angular/core/testing';
import { updatesourceaccountStep2 } from 'src/app/models/extract/access-microsoft-sqlserver';
import { UpdateConnectorPages } from 'src/app/services/extract/ConnectorService/UpdateConnectorPages';
import { stringmappingDictionary } from 'src/app/utils/ExtractMapping';

@Component({
  selector: 'app-source-summar-page',
  templateUrl: './source-summar-page.component.html',
  styleUrls: ['./source-summar-page.component.css']
})
export class SourceSummarPageComponent extends AppComponentBase implements OnInit {
 GetConncetorType=new stringmappingDictionary();
 UserId:string=null;
 Workspaceid:string=null;
 Clientid:string=null;
 UpdateSourceAccountLst:updatesourceaccountStep2[]=[];
 Account_id:string="";
 DBInfo=false;
 FileInfo=false;
 FileName:string;
 FileType:string;
 Connector_Title:string="";
 src:string="";
 connectorid:string="";
 account_display_name:string="";
 workspace_to_use:string="";
 enable_connection:boolean;
 source_name:string="";
 source_hostname:string="";
 source_DBname:string="";
 source_username:string="";
 source_password:string="";
 portnumber:string="";
 
  constructor(private updateConnectorPages:UpdateConnectorPages,injector : Injector) { super(injector)}

  ngOnInit() {
    debugger
   this.Account_id=JSON.parse(localStorage.getItem("Account_Id"));
   this.Connector_Title=localStorage.getItem("Connectortitle");
   this.src=localStorage.getItem("src");
   this.connectorid=localStorage.getItem("ConnectorId");
   this.UserId=this.storageService.getItem(environment.storage.userId);
   this.Workspaceid=this.storageService.getItem(environment.storage.workspaceId);
   this.Clientid=this.storageService.getItem(environment.storage.clientId);
  this.getdata();

  }

  getdata(){
    debugger
    this.updateConnectorPages.ViewSummary(this.Account_id,this.UserId,this.Workspaceid,this.Clientid,this.connectorid).subscribe((data:updatesourceaccountStep2[])=>{
      debugger
      if(data.length > 0){
        debugger
         this.UpdateSourceAccountLst=data;
         var gettype=this.GetConncetorType.getStringKey(this.UpdateSourceAccountLst[0].connectorname)
         if(gettype=="DBExtraction")
         {
           this.DBInfo=true;
           this.FileInfo=false;
          let resultkeyname = this.UpdateSourceAccountLst.map(a => a.filedname);
          let resultkeyvalue = this.UpdateSourceAccountLst.map(a => a.fieldvalue);
          for(let i=0;i<this.UpdateSourceAccountLst.length;i++)
          {
            
            this.account_display_name=this.UpdateSourceAccountLst[0].accountdisplayname;
            this.workspace_to_use="Null";
            this.enable_connection=Boolean(this.UpdateSourceAccountLst[0].enableconnection);
            this.source_name=this.UpdateSourceAccountLst[0].connectorname; 
            this.source_hostname=resultkeyvalue[1]; 
            this.source_DBname=resultkeyvalue[0]; 
            this.source_username=resultkeyvalue[4]; 
            this.source_password=resultkeyvalue[2]; 
            this.portnumber=resultkeyvalue[3];
 
           }
         }
         else if(gettype=="FileExtraction")
         {
              this.FileInfo=true;
              this.DBInfo=false;
              for(let i=0;i<this.UpdateSourceAccountLst.length;i++)
              {
                
                this.account_display_name=this.UpdateSourceAccountLst[0].accountdisplayname;
                this.workspace_to_use="Null";
                this.enable_connection=Boolean(this.UpdateSourceAccountLst[0].enableconnection);
                this.source_name=this.UpdateSourceAccountLst[0].connectorname; 
                this.FileName=this.UpdateSourceAccountLst[0].filedname; 
                 
     
               }
    
         }
        
         
        }
    });
  }

}
