import { Component, Injector, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UpdateConnectorPages } from 'src/app/services/extract/ConnectorService/UpdateConnectorPages';
import { WizardControls } from 'src/app/services/extract/WizardControlsFromServices';
import { environment } from 'src/environments/environment';
import { AppComponentBase } from 'src/app/services/AppComponentBase';
 
@Component({
  selector: 'app-execution-plan',
  templateUrl: './execution-plan.component.html',
  styleUrls: ['./execution-plan.component.css']
})
export class ExecutionPlanComponent 
extends AppComponentBase implements OnInit {
  Acount_id=localStorage.getItem("value");
  btnvalue="Save And Next"
  constructor(public WizardControlsService:WizardControls,
     private router: Router,
     public  updateConnectorPages:UpdateConnectorPages,injector : Injector) {super(injector) }

  ngOnInit(): void {
   
    if(this.Acount_id==null)
    {
      this.btnvalue="Save And Next"
    }else
    {
      this.btnvalue="Update And Next"
    }
  }
  MoveNextStep8(){
    this.WizardControlsService.ExtractWizard=false;
    this.WizardControlsService.ExecitionPlanWizard=false;
    this.WizardControlsService.ExecitionPlanWizardClass="css-ExecutionPlanWizardtick"
    var UserId=this.storageService.getItem(environment.storage.userId);
    var Workspaceid=this.storageService.getItem(environment.storage.workspaceId);
    var Clientid=this.storageService.getItem(environment.storage.clientId);
    var ConnectorId=localStorage.getItem("ConnectorId");
    if(this.Acount_id==null){
      this.router.navigate(['extract/AddnewDataStream/Extract']);
    }else{
      this.updateConnectorPages.upadteExtractStep8(this.Acount_id,UserId,Workspaceid,Clientid,ConnectorId);
     }
  }
  ReversClass(){
    this.WizardControlsService.ExecitionPlanWizard=true;
    this.WizardControlsService.FilterSelectionWizard=false;
    this.WizardControlsService.FilterSelectionClass="css-FilterSelectionwizard"
    // this.WizardControlsService.ExecitionPlanWizardClass="css-ExecutionPlanWizardtick"
    this.router.navigate(['extract/AddnewDataStream/FieldsSelection']);
  }
}
