import { Component, Inject, Injector, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { GetAllConnectionsService } from 'src/app/Services/extract/get-all-connections.service';
import { FiltersService } from 'src/app/Services/common/filters.service';
import { ConnectionListModel } from 'src/app/Models/connect.model';
import { UpdateConnectorPages } from 'src/app/services/extract/ConnectorService/UpdateConnectorPages';
import { ExtractSqlTableData } from 'src/app/models/extract/ExtractDataModel';
import { ExtractData } from 'src/app/services/extract/ExtractSqlAccountData';
import { ServiceService } from 'src/app/services/extract/service.service';
import { googleoauth } from 'src/app/services/Enrich/googleOauthToken';
import { Router } from '@angular/router';
import { AutoEtlServics } from 'src/app/services/extract/AutoETLProcess';
import { stringmappingDictionary } from 'src/app/utils/ExtractMapping';
 
import {
  ProgressBar,
  ITextRenderEventArgs,
  AnimationModel,
  FontModel,
  ILoadedEventArgs,
  ProgressTheme
} from '@syncfusion/ej2-progressbar';
import { MessageService } from 'primeng/api';
import { filtersModel } from 'src/app/models/extract/filter.model';
import { AppComponentBase } from 'src/app/services/AppComponentBase';
import { supportsPassiveEventListeners } from '@angular/cdk/platform';
import { inject } from '@angular/core/testing';
import { environment } from 'src/environments/environment';
 
 
@Component({
  selector: 'app-all-connections',
  templateUrl: './all-connections.component.html',
  styleUrls: ['./all-connections.component.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [MessageService]
  
})
export class AllConnectionsComponent extends AppComponentBase implements OnInit  {
  //   progress bar  start

  public type: string = 'Linear';
  public width: string = '100%';
  public height: string = '40px';
  public trackThickness: number = 24;
  public progressThickness: number = 24;
  public min: number = 0;
  public max: number = 60;
  public value1: number = 80;
  public showProgressValue: boolean = true;
  public role1: string = 'Success';
  public duration:any="Today";
  public animation: AnimationModel = { enable: true, duration: 2000, delay: 0 };
  public labelStyle1: FontModel = {
    textAlignment: 'Center',
    text: '40% Complete (Success)',
    color: '#ffffff'
  };
  @ViewChild('linear1',{static:true})
  public linear1: ProgressBar;
  // progress bar End
// Filter Work from Aneeb  /// Start
filterValuesModel={} as filtersModel;

///  End 
  screenName:string='connections';
  updateConnections:ConnectionListModel[]=[];
  allcheckbox:boolean=false;
  _tablecounter:number=0;
  limit:string = 'All';
  workspaceName:string=null;
  connectionName:string=null;
  sourceName:string=null;
  accessGranted:string=null;
  createdBy:string=null;
  isActive:string=null;
  lastAccessed:string=null;
  destinationEnabled:string=null;
  appearanceLogo_toggle:boolean=false;
  appearanceLogo_arrow:boolean=false;
  workspaceName_toggle:boolean=false;
  workspaceName_arrow:boolean=false;
  connectorName_toggle:boolean=false;
  connectorName_arrow:boolean=false;
  bstatus_toggle=false;
  bstatus_arrow:boolean=false;
  // progress bar
  value: number = 0;
  // End progress bar 
  ErrorMessage:string;
   Errorheading=false;
   _StatusForExtraction:any[]=[]
  ConnectionInfoForExtraction:ExtractSqlTableData[]=[];
  constructor(public allConService:GetAllConnectionsService, private filterService:FiltersService,
    private updateConnectorPages:UpdateConnectorPages,public extractData:ExtractData, public autoETL:AutoEtlServics,private router: Router,injector:Injector,
     public serviceService:ServiceService,
       private fbtoken:googleoauth,
       private messageService: MessageService
        
    ) {
      super(injector)

   }

  ngOnInit(): void {
    this.filterService._screenName='connections';
    let searchParams = new URLSearchParams(window.location.search);
    if (searchParams.has('code')) {
        var code = searchParams.get('code');
        var stringlen="https://graph.facebook.com/v9.0/oauth/access_token?client_id=411387786868121&redirect_uri=http://localhost:4200/extract/extraction/connectionslist&client_secret=63e3267e5f4be512b2ec585f89b0c52d&code="+code ;
        //this.fbtoken.fbUrl(stringlen);
    }
    // this.getAllConections(this.filterValuesModel);
    this.allConService._tablecounter=0;
    
    this._tablecounter=0;
    this.Errorheading=false;
    this.ErrorMessage="";

    
    
  }
getAllConections(filtersValue:filtersModel){
  debugger
     this.allConService.getAllConnectionswithLimit(filtersValue);
}
checkAll(ev) {
   
  this.allConService._connectionData.forEach(x => x.state = ev.target.checked)
  
}
findCount(index,e,AccountId:string,connectorname:string,DisplayName:string,connectorId:string,grantaccess:string,Grouping:string) {
  if(e.target.checked)
  {
    // if(grantaccess!="no"){
      this.ConnectionInfoForExtraction.push({
        accountid:AccountId,
        connectorname:connectorname,
        connectorId:connectorId,
        displayName:DisplayName,
        piplineid:"",
        accessgranted:grantaccess
        
      });
    
    
    this.autoETL.allcheckbox=false;
    this._tablecounter+=1;
    var elemCheckBox= document.getElementById('check'+(index+1)) as HTMLInputElement;
    var sourceConnectionId = 'sourceConnectionId';
    var  test= this.allConService._connectionData[index][sourceConnectionId];
    var result= this.updateConnections.findIndex(x=>x.sourceConnectionId==test);
    if(result==-1){
        this.updateConnections.push(this.allConService._connectionData[index]);
        }
    else{
        this.updateConnections.splice(result,1);
        this.updateConnections.push(this.allConService._connectionData[index]);
        }
  }
  else{
    var find=this.ConnectionInfoForExtraction.find(x=>x.accountid==AccountId);
    var RemoveIndex=this.ConnectionInfoForExtraction.indexOf(find);
    
    this.ConnectionInfoForExtraction.splice(RemoveIndex,1);
    this._tablecounter-=1;
    var elemCheckBox= document.getElementById('check'+(index+1)) as HTMLInputElement;
    var sourceConnectionId = 'sourceConnectionId';
    var  test= this.allConService._connectionData[index][sourceConnectionId];
    var result= this.updateConnections.findIndex(x=>x.sourceConnectionId==test);
    if(result!=-1){
      this.updateConnections.splice(result,1);
        }
    this.autoETL.allcheckbox=false;
    
  }
 }
functionCount(e){
  
  if(e.target.checked)
  {
    this.autoETL.allcheckbox=true;
    this._tablecounter= this.allConService._recordLength;
    for(let i=0;i<this.allConService._connectionData.length;i++){
      
      var sourceConnectionId = 'sourceConnectionId';
      var  test= this.allConService._connectionData[i][sourceConnectionId];
      var result= this.updateConnections.findIndex(x=>x.sourceConnectionId==test);
      if(result==-1){
          this.updateConnections.push(this.allConService._connectionData[i]);
          }
      else{
          this.updateConnections.splice(result,1);
          this.updateConnections.push(this.allConService._connectionData[i]);
          }
          this.ConnectionInfoForExtraction.push({
            accountid:this.allConService._connectionData[i].sourceConnectionId,
            connectorname:this.allConService._connectionData[i].connectorName,
            connectorId:this.allConService._connectionData[i].sourceConnectorId,
            displayName:this.allConService._connectionData[i].accountDisplayName,
            piplineid:"",
            accessgranted:this.allConService._connectionData[i].accessgranted
            
          });
  }      
  }
  else{
       this.autoETL.allcheckbox=false;
       this._tablecounter=0;
       this.ConnectionInfoForExtraction=[];
       this.updateConnections.splice(0,this.updateConnections.length);
  }
 }
applySort(fieldName){
  if(fieldName=='connectorName'){
    this.appearanceLogo_arrow=true;
    this.appearanceLogo_toggle=!this.appearanceLogo_toggle;
    this.allConService._sortToggle=!this.allConService._sortToggle;
    this.allConService.sortAllissues(fieldName);
  }
  else if(fieldName=='workspaceName'){
    this.workspaceName_arrow=true;
    this.workspaceName_toggle=!this.workspaceName_toggle;
    this.allConService._sortToggle=!this.allConService._sortToggle;
    this.allConService.sortAllissues(fieldName);
  }
  else if(fieldName=='account_Display_Name'){
    this.connectorName_arrow=true;
    this.connectorName_toggle=!this.connectorName_toggle;
    this.allConService._sortToggle=!this.allConService._sortToggle;
    this.allConService.sortAllissues(fieldName);
  }
  else if(fieldName=='accessgranted'){
    
    this.bstatus_arrow=true;
    this.bstatus_toggle=!this.bstatus_toggle;
    this.allConService._sortToggle=!this.allConService._sortToggle;
    this.allConService.sortAllissues(fieldName);
  }
 
}
ToggleDestination(index){
  debugger
var elemCheckBox= document.getElementById('check'+(index+1)) as HTMLInputElement;
elemCheckBox.checked=true;
var sourceConnectionId = 'sourceConnectionId';
var  test= this.allConService._connectionData[index][sourceConnectionId];
var result= this.updateConnections.findIndex(x=>x.sourceConnectionId==test);
if(result==-1){
  this.updateConnections.push(this.allConService._connectionData[index]);
  this._tablecounter +=1;
}
else{
  this.updateConnections.splice(result,1);
  this.updateConnections.push(this.allConService._connectionData[index]);
}

}
reloadPage(){
  this.appearanceLogo_arrow=false;
  this.workspaceName_arrow=false;
  this.connectorName_arrow=false;
  this.bstatus_toggle=false;
  this.getAllConections(this.filterValuesModel);
  }
  ConnectionUpdate(sourceConnectionId,connectorName,sourceConnectorId,appearanceLogo)
  {
    debugger
  localStorage.getItem("src");
  
  var UserId=this.storageService.getItem(environment.storage.userId);
  var Workspaceid="1";
  var Clientid=this.storageService.getItem(environment.storage.clientId);
  var ConnectorId=sourceConnectorId;
  JSON.stringify(localStorage.setItem("accountId",sourceConnectionId));
  JSON.stringify(localStorage.setItem("value",sourceConnectionId));
  JSON.stringify(localStorage.setItem("Connectortitle",connectorName));
  JSON.stringify(localStorage.setItem("src",appearanceLogo));
  JSON.stringify(localStorage.setItem("ConnectorId",sourceConnectorId));

  this.updateConnectorPages.SourceAccountEditMode(sourceConnectionId,UserId,Workspaceid,Clientid,ConnectorId);
}
// OLD Work 
  RunForExtract(){
    debugger
    var Userid= this.storageService.getItem(environment.storage.userId);
    var Workspaceid=this.storageService.getItem(environment.storage.workspaceId);
    var CLIENT_ID = this.storageService.getItem(environment.storage.clientId);
    var ACCOUNT_ID =localStorage.getItem("Account_Id");
    if(this.ConnectionInfoForExtraction.length==0)
    {
      this.Errorheading=true;
      this.ErrorMessage="Select at least 1 row for extract";
      return;
    }
    else{
      this.serviceService.IdsCollectionArray=this.ConnectionInfoForExtraction;
      if(this.extractData.LoadDataList.length==0){
        this.router.navigate(['load/destination/destinationlist']);
      }
      else {
        this.Errorheading=false;
        this.extractData.Runforextractdata();
      }
     
     
      
    }
  }


//   New AUTO ETL PROCSS

AutoETL(){
  debugger
  var Userid= this.storageService.getItem(environment.storage.userId);
  var Workspaceid=this.storageService.getItem(environment.storage.workspaceId);
  var CLIENT_ID = this.storageService.getItem(environment.storage.clientId);
  var ACCOUNT_ID =localStorage.getItem("Account_Id");
  if(this.ConnectionInfoForExtraction.length==0)
  {
    this.autoETL.MessageStatusForSHowHide=true;
    this.autoETL.Message="Select at least 1 Connection for extract";
    return;
  }
  else{
    this.serviceService.AutoETLConnectionArrayForExtact=this.ConnectionInfoForExtraction;
    this.autoETL.MessageStatusForSHowHide=true;
    this.autoETL.Message="In Progress";
    this.autoETL.AutoETLextractdata();
    this.ConnectionInfoForExtraction=[];
     }
}
  ShowDetailOnSummary(sourceConnectionId,connectorName,sourceConnectorId,appearanceLogo){
  
  JSON.stringify(localStorage.setItem("Account_Id",sourceConnectionId));
  JSON.stringify(localStorage.setItem("Connectortitle",connectorName));
  JSON.stringify(localStorage.setItem("src",appearanceLogo));
  JSON.stringify(localStorage.setItem("ConnectorId",sourceConnectorId));
  this.router.navigate(['extract/extraction/view-detail']);
  }
  // RightsChecking(connectorId){
  //   var Userid="1";
  //   var Workspaceid="1";
  //   var CLIENT_ID = this.storageService.getItem(environment.storage.clientId);
  //   var ACCOUNT_ID =localStorage.getItem("Account_Id");
  //   this.extractData.AccessGrantValidation(connectorId,ACCOUNT_ID,Userid,Workspaceid,CLIENT_ID).subscribe((data:any)=>{
  //     debugger
    
  //    for(var i=0;i<data.length;i++)
  //    {
  //     if(data[i]==1)
  //     {
        
  //     }
  //     else{
  //       this.Errorheading=true;
  //       this.ErrorMessage=data[i]
  //       return;
  //     }
  
  //    }
    
  //   });
  // }
  public load(args: ILoadedEventArgs): void {
    let selectedTheme: string = location.hash.split('/')[1];
    selectedTheme = selectedTheme ? selectedTheme : 'Material';
    args.progressBar.theme = <ProgressTheme>(
      (selectedTheme.charAt(0).toUpperCase() + selectedTheme.slice(1))
        .replace(/-dark/i, 'Dark')
        .replace(/contrast/i, 'Contrast')
    );
    if (args.progressBar.theme === 'Material') {
      args.progressBar.trackColor = '#eee';
    }
    if (selectedTheme === 'highcontrast') {
      args.progressBar.labelStyle.color = '#000000';
      args.progressBar.trackColor = '#969696';
    }
  }

  

}
