import { Component, OnInit } from '@angular/core';
import { AppMenuService } from 'src/app/Services/common/app-menu.service';
import { GetAllConnectionsService } from 'src/app/Services/extract/get-all-connections.service';

@Component({
  selector: 'app-connectors',
  templateUrl: './connectors.component.html',
  styleUrls: ['./connectors.component.css']
})
export class ConnectorsComponent implements OnInit {
source_logo:string;
source_Name:string;
  constructor(public AllConnectionsService:GetAllConnectionsService,public sectionandItemList:AppMenuService) { }

  ngOnInit(): void {
   this.source_logo=localStorage.getItem("source_logo");
    this.source_Name=localStorage.getItem("source_name");
    this.GetsectionAndItemsList();
  }

  GetsectionAndItemsList()
  {
    this.sectionandItemList.getMenuSectionAndItemsForSource();

  }

}
