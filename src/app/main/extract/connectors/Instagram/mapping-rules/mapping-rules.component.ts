import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { GridComponent, PageSettingsModel } from '@syncfusion/ej2-angular-grids';
import { ToastComponent } from '@syncfusion/ej2-angular-notifications';
import { ButtonComponent } from 'ej-angular2';
 

 
import { dropdownModel, fieldNameList, GetTargetColumnNameList } from 'src/app/models/common/dropdownmodel';
import { ToastMessage } from 'src/app/models/MessageTypes/toast-message';
import { FieldMappingRuleTemplateModel, ObjectFieldsList } from 'src/app/Models/SourceSettings.Model';
 import { ColumnMappingEditMode, ColumnMappingJsonModel, SyncGridColumnModel, SyncMappingGridModel } from 'src/app/models/SyncGridColumnModel';
import { CSVFileReadingService } from 'src/app/services/csvfile-reading.service';
import { fileuploaderwizrad } from 'src/app/services/extract/FileuploaderServices/fileuploadwizzardstyleservices';
import { Transformation_Mapping_Loading_Using_Auto_ETL } from 'src/app/services/extract/JsonFor_Transformation_mapping_Loading';
import { InstagramConfigurationService } from 'src/app/Services/Instagram/instagram-configuration.service';
import { environment } from 'src/environments/environment';
import { AppComponentBase } from 'src/app/services/AppComponentBase';
@Component({
  selector: 'app-mapping-rules',
  templateUrl: './mapping-rules.component.html',
  styleUrls: ['./mapping-rules.component.css']
})
export class MappingRulesComponent extends AppComponentBase implements OnInit {

//for toast
public testToast=new ToastMessage();
@ViewChild('defaulttoast',{static:true})
public toastObj: ToastComponent;
@ViewChild('toastBtnShow',{static:true})
public btnEleShow: ButtonComponent;
public position=this.testToast.position;
// toast end



  @ViewChild('grid',{static:true}) 
  public gridInstance : GridComponent ;  
@ViewChild('template',{static:true}) 
  public toolbarTemplate: any;
  syncMappingGridModel=new SyncMappingGridModel();
  targetcolumnvalue:any[]=[];
  fieldtypevalue:AnalyserNode[]=[];
  columnmappingModel:ColumnMappingJsonModel[]=[];
  _modelList:any[]=[];
  _gridmodeldata:any[]=[];
  _filesnameList:dropdownModel[]=[];
  _fieldnameList:any[]=[];
  _bit:string;
  _getTargetColumnNameList:GetTargetColumnNameList[]=[];
  TargetColumndropdownname:string;
  _visibilityList = [
    {dropdownText:'no',dropdownValue:'no',selected:false},
    {dropdownText:'yes',dropdownValue:'first_name',selected:true},
    ];
public headerList:SyncMappingGridModel[]=[];
headerListForfilters:SyncMappingGridModel[]=[];
public pageSettings: PageSettingsModel;
public fieldsvalues:object;
public fieldsvalues2:object;
public workspaceName:string;
filename:string;
ModelName:string;
_ColumnMapping:ColumnMappingEditMode[]=[];
columnmappingJson:any[]=[];
mappedtargetcolumn:fieldNameList[]=[];
unmappedtargetcolumn:fieldNameList[]=[];
mappedfieldvalue:fieldNameList[]=[];
unmappedfieldvalue:fieldNameList[]=[];
FiltersClassessChangeOnselectedAll="css-xpr64j";
Mapped="css-b2qa21";
upmapped="css-b2qa21";
NewFileNameField:boolean=true;
exitORNewfile:boolean;
///  Usman Work  from Mapping Rules Screen in Extract 
dropdownName:string='Mapped Field'; 
targetColumnselectedvalue:any[]=[];
fieldColumnselectedvalue:any[]=[];
showHideGridDropdown:boolean[]=[];
showHideGridButton:boolean[]=[];
disableTrue:boolean=true;
defaultMappingRule:string[]=[];
mappingRules:any[]=[];
updatedMappingRules:any[]=[];
selectedMappingTemplate:string='Master';
mappingTemplate:dropdownModel[]=[];
mappingNewTemplateName:string=null;
mappingField:dropdownModel[]=[];
modeldropdownlist:string;
templateTextbox:boolean=false;
bgColor="d5eff6";
Colorid:string;
Length:number;
allcheckbox=false;
count:number=0;
bit:any;
change=false;
workspacedropdownlist:any[]=[];
countervalue:number=0;
UpadteSourceObjecList:ObjectFieldsList[]=[];
SourceObjectList:any[]=[];
VisibilityStatus:any[]=[];
toggle:any[]=[];
 dropdownfilename:string;
 SourceFilename:string=null;
///
 
public data: string[] = ['Snooker', 'Tennis', 'Cricket', 'Football', 'Rugby']
test:any='<ejs-dropdownlist id="_workspace" Name="workspaceDropDown" [dataSource]="_workspaces" [fields]="fieldsvalues" placeholder="Select column name" ></ejs-dropdownlist>';
constructor(private csvFileReadingService: CSVFileReadingService,public fileuploadwizardstyle:fileuploaderwizrad,private router: Router,private instagramConfigurationService:InstagramConfigurationService,
  private transformation_Mapping_Loading_Using_Auto_ETL_services:Transformation_Mapping_Loading_Using_Auto_ETL,
  injector : Injector
  ) {
    super(injector)
  this.fieldsvalues = { dataSource: this._modelList,text:'dropdownText',value:'dropdownValue'};
  this.fieldsvalues2 = { dataSource: this._modelList,text:'dropdownText',value:'dropdownText'};
 }

ngOnInit() {
  debugger
    this.filename=localStorage.getItem("sourcefile_name");
    this.getModelDropdownLiat()
    this.getData();
    this.getfieldnameDropdownLiat()
    this.getfilenamenameList()
    //this.defineColumns();
  this.pageSettings = { pageSize: 6 };
  
}
// defineColumns() {
//   this.gridInstance.columns=[
//     {
//    field:'SourceColumn',
//    headerText:'Source Column',
//   },
//   {
//    headerText:'Target Column',
//    template:this.test
   
//   },
  
// ]
// }
ShowHideNewFileNameField(){
  if(this.exitORNewfile==true)
  {
    this.NewFileNameField==false;
  }
  else
  {
    this.NewFileNameField==true;
  }
}
 getModelDropdownLiat()
 {
   debugger
   var userId=this.storageService.getItem(environment.storage.userId);
   var dropdowntext="Model Name"; 
   this.csvFileReadingService.GetModelNameList(userId,dropdowntext).subscribe((data:any)=>{
     debugger
      this.ModelName=data[0].dropdownText;
      
      
      this._modelList=data;
   })

 }
 getfieldnameDropdownLiat()
 {
    
   var userId=this.storageService.getItem(environment.storage.userId);
   var dropdowntext="Field Type"; 
   this.csvFileReadingService.GetfieldNameList(userId,dropdowntext).subscribe((data:any)=>{
       this._fieldnameList=data;
   })

 }
 getfilenamenameList()
 {
    
 var list= JSON.parse(localStorage.getItem("allFiles"))
 this.filename=list[0].name;
 localStorage.setItem("FileName",JSON.stringify(this.filename));
 for(var i=0;i<list.length;i++)
 {
  
   this._filesnameList.push({
     dropdownValue:list[i].name,
     dropdownText:list[i].name
   })
 }

 }
 onChange(event:any)
 {
  
   debugger
  
   
    this.modeldropdownlist= event.itemData.dropdownText;
    var userid=this.storageService.getItem(environment.storage.userId);
    var workspaceid="1";
    var clientid=this.storageService.getItem(environment.storage.clientId);
    this.csvFileReadingService.GetModelNameListForGrid(userid,workspaceid,clientid,this.modeldropdownlist).subscribe((data:any)=>{
      debugger
     this._gridmodeldata=data;
     this.mappingFilter('all');
    //  for(let u=0;u<this._getTargetColumnNameList.length;u++)
    //   {
    //   this.targetColumnselectedvalue[this._getTargetColumnNameList[u].Index]=this._getTargetColumnNameList[u].target_value;
    // }

    })
   //}
  
  }
  OnChangeTargetColumnName(event:any,indexs,sourcecolumn:string=null,visibility:boolean){
    debugger
    
    this.TargetColumndropdownname= event.itemData.dropdownText;
    var find=this._getTargetColumnNameList.find(x=>x.source_Column==sourcecolumn);
    var getindexlength=this._getTargetColumnNameList.indexOf(find);
    var findtargetcol=this._getTargetColumnNameList.find(x=>x.Index==indexs);
    var findtargetcollength=this._getTargetColumnNameList.indexOf(find);
    if(findtargetcollength==indexs)
    { 
      this.targetcolumnvalue[indexs]=event.itemData.dropdownText;
    }
    else{
      this.targetcolumnvalue.push(event.itemData.dropdownText);
    }
    this.headerList[indexs].mappingBit="yes";
    this.headerListForfilters[indexs].mappingBit="yes";
    if(getindexlength!=-1)
    {
     
      this._getTargetColumnNameList[getindexlength].target_value=this.TargetColumndropdownname;
    }
    else{
      this._getTargetColumnNameList.push({
        Index:indexs,
        source_Column:sourcecolumn,
        target_value: this.TargetColumndropdownname,
        field_type:{dropdownText:"",dropdownvalue:""
        },
        visibility:visibility
      })
     
    }
      
     
      
     
     
     
    

  }
  OnChangeFieldName(event:any,indexs,sourcecolumn_name:string,visibility:boolean){
    debugger
    
    var dropdownname= event.itemData.dropdownText;
    var dropdownvalue= event.itemData.dropdownValue;
    var find=this._getTargetColumnNameList.find(x=>x.source_Column==sourcecolumn_name);
    var getindexlength=this._getTargetColumnNameList.indexOf(find);
    var findField=this._getTargetColumnNameList.find(x=>x.Index==indexs);
    var findFieldlength=this._getTargetColumnNameList.indexOf(find);
    if(findFieldlength==indexs)
    {
      this.fieldtypevalue[indexs]=event.itemData.dropdownValue;
       
    }
    else {
      this.fieldtypevalue.push(
       
        event.itemData.dropdownValue
      );
    }
    
    if(getindexlength!=-1)
    {
     
      this._getTargetColumnNameList[getindexlength].field_type.dropdownText=dropdownname;
      this._getTargetColumnNameList[getindexlength].field_type.dropdownvalue=dropdownvalue;
    }
    else {
      this._getTargetColumnNameList.push({
        Index:indexs,
        source_Column:"",
        target_value:"",
        field_type:{dropdownText:dropdownname,dropdownvalue:dropdownvalue},
        visibility:visibility
      })
      ; 
    }
       
      
   
    

  }
  ChangetheVisibiltySetting(indexs,sourcecolumn:string=null,visibility:boolean){
    debugger
    var find=this._getTargetColumnNameList.find(x=>x.source_Column==sourcecolumn);
    var getindexlength=this._getTargetColumnNameList.indexOf(find);
    if(getindexlength!=-1)
    {
      this._getTargetColumnNameList[getindexlength].visibility=visibility;
    }
    else{
      this._getTargetColumnNameList.push({
        Index:indexs,
        source_Column:sourcecolumn,
        target_value:"",
        field_type:{dropdownText:"",dropdownvalue:""
        },
        visibility:visibility
      })
    }
   
   


  }
 changefilename(event:any){
   debugger
   this.headerList=[];
  var filenametext= event.itemData.dropdownText;
  localStorage.setItem("FileName",JSON.stringify(filenametext));
  this.csvFileReadingService.getFileData(filenametext,this.change).subscribe((data:any)=>{
    debugger
     
   
     var data=data;
      console.log(data);
       
  for(var i=0;i<data.columns.length;i++){
    this.headerList.push(
      {
        SourceColumn:data.columns[i],
        keycolumn:true,
        visibilty:false,
        mappingBit:""
        
      });
  }
      
       });

   
 }
 

getData(){
  debugger
   
  this.headerList=[];
  this.headerListForfilters=[];
  this.SourceFilename=localStorage.getItem("sourcefile_name");
  this.csvFileReadingService.getRawFileData(this.SourceFilename,this.change).subscribe((data:any)=>{
    debugger
     var data=data;
      console.log(data);
       
  for(var i=0;i<data.columns.length;i++){
    debugger
    this.headerList.push(
      {
        SourceColumn:data.columns[i],
        keycolumn:true,
        visibilty:true,
        mappingBit:""
        
      });
      this.headerListForfilters.push(
        {
          SourceColumn:data.columns[i],
          keycolumn:true,
          visibilty:true,
          mappingBit:""
          
        });
  }
      
       });
  
}
 /// Edit the Column Mapping Screen function 
 getDataForEditMode(){
  debugger
   
  this.headerList=[];
  this.headerListForfilters=[];
  this.SourceFilename=localStorage.getItem("sourcefile_name");
  this.csvFileReadingService.getRawFileData(this.SourceFilename,this.change).subscribe((data:any)=>{
    debugger
     var data=data;
      console.log(data);
       
  for(var i=0;i<data.columns.length;i++){
    debugger
    this.headerList.push(
      {
        SourceColumn:data.columns[i],
        keycolumn:true,
        visibilty:true,
        mappingBit:""
        
      });
      this.headerListForfilters.push(
        {
          SourceColumn:data.columns[i],
          keycolumn:true,
          visibilty:true,
          mappingBit:""
          
        });
  }
     this.EditColumnMapping();
       });
  
}
 EditColumnMapping()
 {
   debugger
  var accountId=this.SourceFilename.substring(0,7);
  var ScreenName="ColoumnMapped";
  this.csvFileReadingService.GetColumnMappedArray(accountId,ScreenName).subscribe((data:any)=>{
    if(data.length > 0)
    {
      debugger
      console.log(data);
      this.columnmappingJson=JSON.parse(data);
      this.ModelName=this.columnmappingJson[0].modelName;
     // this.onChange(this.columnmappingJson[0].modelName);
     
      /// we can get the list form Mongo db and match the column with sources list then get the matted column index for 
      // showing the lookup table and filed 
      for(let y=0;y<this.columnmappingJson[0].column_mapping.length;y++)
      {
        debugger
        var FindIndex=this.headerList.findIndex(e=>e.SourceColumn === this.columnmappingJson[0].column_mapping[y].source_name);
        if(FindIndex!=-1)
        {
           
          this._getTargetColumnNameList.push({
            Index:FindIndex,
            source_Column:this.columnmappingJson[0].column_mapping[y].source_name,
            target_value:this.columnmappingJson[0].column_mapping[y].target_name,
            field_type:{dropdownText:this.columnmappingJson[0].column_mapping[y].field_type,dropdownvalue:this.columnmappingJson[0].column_mapping[y].field_type},
            visibility:this.columnmappingJson[0].column_mapping[y].visibility,
            
          })
          //this._SourceColumnName.push(this._getTargetColumnNameList[y].source_column_name);
          this.headerListForfilters[FindIndex].mappingBit="yes";
           
        } 
       
     
      }
      for(let g=0;g<this._getTargetColumnNameList.length;g++)
      {
        debugger
        this.targetColumnselectedvalue[this._getTargetColumnNameList[g].Index]=this._getTargetColumnNameList[g].target_value;
         this.fieldColumnselectedvalue[this._getTargetColumnNameList[g].Index]=this._getTargetColumnNameList[g].field_type.dropdownText;
          this.headerList[this._getTargetColumnNameList[g].Index].visibilty=this._getTargetColumnNameList[g].visibility;
      }
       
       
    }
    
  
  })

 }



// Usman Work   form Mapping rules component
columnmapping(){
     debugger
    this.columnmappingModel=[];
   
    for(var i=0;i<this._getTargetColumnNameList.length;i++)
    {
      this.columnmappingModel.push({
        source_name:this._getTargetColumnNameList[i].source_Column,
        target_name:this._getTargetColumnNameList[i].target_value,
        field_type:this._getTargetColumnNameList[i].field_type.dropdownText,
        visibility:this._getTargetColumnNameList[i].visibility
      })
    }
    this.transformation_Mapping_Loading_Using_Auto_ETL_services.JSON_For_Column_Mapping(this.columnmappingModel,this.ModelName).subscribe((data:any)=>{
     debugger
      var msg=data;
      this.testToast.toast[1].content=msg;
      this.toastObj.show(this.testToast.toast[1]);

    })
    if(this.columnmappingModel.length>0)
    {
      var filename=localStorage.getItem("sourcefile_name")
      var obj={
        "filename":"/run/user/1000/gvfs/smb-share:server=192.168.223.100,share=d/zoom_files_dump/extract/"+filename,
        "content":this.columnmappingModel,
        "skiprows":"0",
        "header_skiprows":"0",
         }
      this.csvFileReadingService.SetColumns(obj).subscribe((data:any)=>{
        debugger
        var d=JSON.parse(data);
        var directoryfilename=d.filename;
        var filenameaftermapping=directoryfilename.substring(directoryfilename.lastIndexOf('/'));

        localStorage.setItem("sourcefile_name",filenameaftermapping);
      })
      
    }
  
  
}



// saveMappingTemplate(){
//   debugger
//  if(this.mappingNewTemplateName==null){
//    this.instagramConfigurationService.SaveMappingRuleTemplate(this.updatedMappingRules,this.selectedMappingTemplate).subscribe((data:string)=>{
//      console.log(data);
//    });
//  }
//  else{
//    this.instagramConfigurationService.SaveMappingRuleTemplate(this.mappingRules,this.mappingNewTemplateName).subscribe((data:string)=>{
      
//    });
//  }



// }
// getdropdown(){
// this.instagramConfigurationService.GetMapedField(this.dropdownName).subscribe((data:dropdownModel[])=>{
//   this.mappingField =data;
//     });
   
//}
// getMappingRules(){
// this.instagramConfigurationService.getMappingRule(this.selectedMappingTemplate).subscribe((data:FieldMappingRuleTemplateModel[])=>
// {
//   debugger
//   this.mappingRules=data;    
//   this.Length=data.length;
//   for(let i=0;i<this.Length;i++){
//     this.showHideGridDropdown[i]=false;
//     this.showHideGridButton[i]=true;
//     if(this.mappingRules[i].keyColumn=='1'){
//       this.mappingRules[i].keyColumn=true;
//     }
//     else{
//       this.mappingRules[i].keyColumn =false;
//     }
    
//   }
   
//   this.getdropdown();
// });
// }
mappingFilter(value){

 
debugger
if(value=='unmapped')
{
  this.targetColumnselectedvalue=[];
  this.fieldColumnselectedvalue=[];
  this.upmapped="css-xpr64j";   /// height light the  selected button 
  this.Mapped="css-b2qa21";
  this.FiltersClassessChangeOnselectedAll="css-b2qa21";
  this.headerList = this.headerListForfilters.filter(bitmapped => bitmapped.mappingBit !="yes");
  for(var i=0;i <this._getTargetColumnNameList.length;i++)
  {
   
   
  this.fieldColumnselectedvalue[i]="";
  this.targetColumnselectedvalue[i]="";
  }
}
else if(value=='Mapped'){
  this.targetColumnselectedvalue=[];
  this.fieldColumnselectedvalue=[];
  this._ColumnMapping=[];
  this.upmapped="css-b2qa21";
  this.Mapped="css-xpr64j";
  this.FiltersClassessChangeOnselectedAll="css-b2qa21";
  //this.headerList = this.headerListForfilters.filter(bitmapped => bitmapped.mappingBit == "yes");
  for(var i=0;i <this._getTargetColumnNameList.length;i++)
  {
    var find=this.headerListForfilters.find(x=>x.SourceColumn==this._getTargetColumnNameList[i].source_Column);
      this._ColumnMapping.push({
        Index:this._getTargetColumnNameList[i].Index,
        SourceColumn:find.SourceColumn,
        keycolumn:find.keycolumn,
        visibilty:find.visibilty,
        mappingBit:find.mappingBit,
        target_name:this._getTargetColumnNameList[i].target_value,
        field_type:this._getTargetColumnNameList[i].field_type.dropdownText,

      })
   }
    this.headerList = this._ColumnMapping;
    for(let x=0;x<this._ColumnMapping.length;x++)
    {
      this.targetColumnselectedvalue[x]=this._ColumnMapping[x].target_name;
      this.fieldColumnselectedvalue[x]=this._ColumnMapping[x].field_type;
      this.headerList[x].visibilty=this._getTargetColumnNameList[x].visibility;
    }

  
 
   
}
else if(value=='all'){
  this.targetColumnselectedvalue=[];
  this.fieldColumnselectedvalue=[];
  this.upmapped="css-b2qa21";
  this.Mapped="css-b2qa21";
  this.FiltersClassessChangeOnselectedAll="css-xpr64j";
  this.headerList = this.headerListForfilters.filter(bitmapped => bitmapped.mappingBit != "no");
  for(var i=0;i <this._getTargetColumnNameList.length;i++)
  {
   
   
  this.fieldColumnselectedvalue[this._getTargetColumnNameList[i].Index]=this._getTargetColumnNameList[i].field_type.dropdownvalue;
  this.targetColumnselectedvalue[this._getTargetColumnNameList[i].Index]=this._getTargetColumnNameList[i].target_value;
  this.headerList[this._getTargetColumnNameList[i].Index].visibilty=this._getTargetColumnNameList[i].visibility;
  }
  
  //this.getData();
  
}
}
 
CleanMapping()
{
  this._getTargetColumnNameList=[];
  
  this._ColumnMapping=[];
  this.ClearMappingData();
}
ClearMappingData(){
 debugger
 this.headerList=[];
 this.headerListForfilters=[];
 this._getTargetColumnNameList=[];
  this.fieldColumnselectedvalue=[];
  this.targetColumnselectedvalue=[];
  
  
  this.SourceFilename=localStorage.getItem("sourcefile_name");
  this.csvFileReadingService.getRawFileData(this.SourceFilename,this.change).subscribe((data:any)=>{
    debugger
     var data=data;
      console.log(data);
       
  for(var i=0;i<data.columns.length;i++){
    debugger
    this.headerList.push(
      {
        SourceColumn:data.columns[i],
        keycolumn:true,
        visibilty:true,
        mappingBit:""
        
      });
      this.headerListForfilters.push(
        {
          SourceColumn:data.columns[i],
          keycolumn:true,
          visibilty:true,
          mappingBit:""
          
        });
  }
 
       });
     
}
RestoreDefaultMapping()
{
  this.headerList=[];
 this.headerListForfilters=[];
 this._getTargetColumnNameList=[];
  this.fieldColumnselectedvalue=[];
  this.targetColumnselectedvalue=[];
  this.getDataForEditMode();
}

}
