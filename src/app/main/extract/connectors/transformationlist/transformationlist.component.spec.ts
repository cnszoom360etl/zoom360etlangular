import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransformationlistComponent } from './transformationlist.component';

describe('TransformationlistComponent', () => {
  let component: TransformationlistComponent;
  let fixture: ComponentFixture<TransformationlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransformationlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransformationlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
