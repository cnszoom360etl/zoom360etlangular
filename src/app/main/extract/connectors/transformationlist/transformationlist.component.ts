import { Component, ElementRef, Injector, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ToastComponent } from '@syncfusion/ej2-angular-notifications';
import { ButtonComponent } from 'ej-angular2';
import { environment } from 'src/environments/environment';
import { AppComponentBase } from 'src/app/services/AppComponentBase';
import { DialogComponent, DialogUtility } from '@syncfusion/ej2-angular-popups';
import { EmitType } from '@syncfusion/ej2-base';
import { ScriptDetails } from 'src/app/models/extract/ExtractDataModel';
import { ToastMessage } from 'src/app/models/MessageTypes/toast-message';
import { DynamicEnrichScriptsService } from 'src/app/services/Enrich/dynamic-enrich-scripts.service';
import { Transformation_Mapping_Loading_Using_Auto_ETL } from 'src/app/services/extract/JsonFor_Transformation_mapping_Loading';
@Component({
  selector: 'app-transformationlist',
  templateUrl: './transformationlist.component.html',
  styleUrls: ['./transformationlist.component.css']
})
export class TransformationlistComponent extends AppComponentBase{
  public DialogObj;
  scriptIdForDelete:string=null;
public onOpenDialog = function(scriptId: string): void {
  debugger
    
     
    this.DialogObj = DialogUtility.confirm({
    title: 'Confirmation Dialog',
    content: "Are you sure you want to delete this Script !",
    okButton: {  text: 'OK', click: this.DeleteScript.bind(scriptId) },
    cancelButton: {  text: 'Cancel', click: this.cancelClick.bind(this) },
    showCloseIcon: true,
    closeOnEscape: true,
    animationSettings: { effect: 'Zoom' }
});
}

  //for toast
  public testToast=new ToastMessage();
  @ViewChild('defaulttoast',{static:true})
  public toastObj: ToastComponent;
  @ViewChild('toastBtnShow',{static:true})
  public btnEleShow: ButtonComponent;
  public position=this.testToast.position;
  // toast end

  GetScriptList:any[]=[];
  totalLenght:number=0;
  allcheckbox:boolean=false;
  singleCheckbox:boolean=false;
  _tablecounter:number=0;
  MessageStatusForSHowHide=false;
  Message:string;
  Transformationfunction:any[]=[];
  transformationfunctionSelection:any[]=[];
  _functionsdetailsList:ScriptDetails[]=[];
  JsonScriptForTransformation:any;
  constructor(private dynamicEnrichScriptsService:DynamicEnrichScriptsService,
    private router: Router,
    private transformation_Mapping_Loading_Using_Auto_ETL_services:Transformation_Mapping_Loading_Using_Auto_ETL,injector : Injector) { super(injector)}

  ngOnInit() {
    this.getScriptList();
  }
   okClick(e): void {
    debugger
    var value=e;
     
}

private cancelClick(): void {
    //Hide the dialog
    this.DialogObj.hide();
}
        



 getScriptList()
 {
   this.dynamicEnrichScriptsService.getScriptList().subscribe((data:any)=>{
     debugger
     this.GetScriptList=JSON.parse(data);
     this.totalLenght=this.GetScriptList.length;

   })

 }
 checkAll(ev) {
  this.GetScriptList.forEach(x => x.state = ev.target.checked)
  
}
findCount(e,scriptId:string,scriptName:string) {
  debugger
   if(e.target.checked==true)
     {
      this._functionsdetailsList.push({
       
        scriptId:scriptId,
      })
      this._tablecounter+=1;
     }
     else {
      var find=this._functionsdetailsList.find(x=>x.scriptId==scriptId);
      var RemoveIndex=this._functionsdetailsList.indexOf(find);
      this._functionsdetailsList.splice(RemoveIndex,1);
      this._tablecounter-=1;
      }
     
     
  
   
 }
 functionCount(e){
  debugger
  if(e.target.checked==true)
  {
    this.allcheckbox=true;
    this._tablecounter= this.GetScriptList.length;
    for(let i=0;i<this.GetScriptList.length;i++){
      
     this.findCount(e.target.checked,this.GetScriptList[i].scriptId,this.GetScriptList[i].scriptName)
      }      
  }
  else{
    this.allcheckbox=false;
    this._tablecounter=0;
    for(let i=0;i<this.GetScriptList.length;i++){
      
     this.findCount(e.target.checked,this.GetScriptList[i].scriptId,this.GetScriptList[i].scriptName)
      }
  }
 }
 GetFunctionId(Id:string)
 {
  localStorage.setItem("Script_Id",JSON.stringify(Id));
  this.router.navigate(['/extract/source/addNewScript']);
 }
 Addnew()
 {
  localStorage.removeItem("Script_Id");
  localStorage.setItem("ImportFile",JSON.stringify("extractmode"));
  this.router.navigate(['/extract/source/addNewScript']);
 }

 GetscriptJson()
 {
   debugger
   this.transformationfunctionSelection=[];
   this.Transformationfunction=[];
   var functionIds_lenght=this._functionsdetailsList.length;
   if(this._functionsdetailsList.length>0)
   {
     this.MessageStatusForSHowHide=false;
    var userid=this.storageService.getItem(environment.storage.userId);
    var workspaceid=this.storageService.getItem(environment.storage.workspaceId);
    var clientId=this.storageService.getItem(environment.storage.clientId);
    //var _Id=this._functionsdetailsList[0].scriptId;
    for(let c=0;c<this._functionsdetailsList.length;c++)
    {
      this.dynamicEnrichScriptsService.getEnrichmentscriptJosnFormMongo(userid,workspaceid,clientId,this._functionsdetailsList[c].scriptId).subscribe((data:any)=>{
        debugger
        this.JsonScriptForTransformation=JSON.parse(data);
        this.transformationfunctionSelection.push(this.JsonScriptForTransformation.ScriptList.length)
        for(var l=0;l<this.JsonScriptForTransformation.ScriptList.length;l++)
        {
          this.Transformationfunction.push(this.JsonScriptForTransformation.ScriptList[l])
        }
        
        // this.dynamicEnrichScriptsService._enrichment_mapping_load.push({
        //   AccountId:"",
        //   Transformation:this.JsonScriptForTransformation.ScriptList,
        //   columnmapping:"",
        //   contentmapping:"",
        //   load:""
        // })
         console.log(this.Transformationfunction);
         if(functionIds_lenght==this.transformationfunctionSelection.length)
    {
        this.transformation_Mapping_Loading_Using_Auto_ETL_services.JSON_For_Transformation(this.Transformationfunction).subscribe((data:any)=>{
        var msg=data;
        })
    }
   
      })
    }
    
    
   
   }
   else
   {
    // this.MessageStatusForSHowHide=true;
    this.Message="Select the function for transformation";
    this.testToast.toast[2].content=this.Message;
    this.toastObj.show(this.testToast.toast[2]);
   }
  
 }
 DeleteScript(scriptId:string){
   debugger
  
  this.dynamicEnrichScriptsService.DeleteScriptList(scriptId).subscribe((data:any)=>{
    debugger
     
    this.getScriptList();
    // this.testToast.toast[0].content=data;
    // this.toastObj.show(this.testToast.toast[0])

     
  })

}
}
