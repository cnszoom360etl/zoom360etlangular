import { Component, Injector, OnInit } from '@angular/core';
import { filtersModel } from 'src/app/models/extract/filter.model';
import { AppComponentBase } from 'src/app/services/AppComponentBase';
import { GetAllExtractsService } from 'src/app/Services/extract/get-all-extracts.service';


@Component({
  selector: 'app-overview-all',
  templateUrl: './overview-all.component.html',
  styleUrls: ['./overview-all.component.css']
})
export class OverviewAllComponent extends AppComponentBase implements OnInit {
  limit:string = 'All';
  workspaceName:string='all';
  connectionName:string='all';
  sourceName:string='all';
  accessGranted:string='all';
  createdBy:string='all';
  isActive:string='all';
  lastAccessed:string='all';
  destinationEnabled:string='all';
  filtersValues={} as filtersModel;
  constructor(public AllExtractService:GetAllExtractsService, injector:Injector) {
    super(injector);
   }

  ngOnInit(): void {
    this.filtersValues={
      userId: this.clientDetailService.getuserID(),
      workspaceId:this.clientDetailService.getWorkspaceID(),
      clientId: this.clientDetailService.getClientID(),
      workspaceName:null,
      connectionName:null,
      sourceName:null,
      accessGranted:null,
      createdBy:null,
      isActive:null,
      lastAccessed:null,
      destinationEnabled:null,
      accountId:null,
      TimeFilter:'Today'
    };
    this.getAllExtract();
    
  }
  getAllExtract(){
    this.AllExtractService.getAllextract(this.filtersValues);
  }
}
