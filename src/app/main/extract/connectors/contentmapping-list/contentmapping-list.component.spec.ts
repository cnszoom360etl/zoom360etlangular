import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentmappingListComponent } from './contentmapping-list.component';

describe('ContentmappingListComponent', () => {
  let component: ContentmappingListComponent;
  let fixture: ComponentFixture<ContentmappingListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentmappingListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentmappingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
