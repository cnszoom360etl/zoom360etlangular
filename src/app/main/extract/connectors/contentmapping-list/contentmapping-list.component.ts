import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { GridComponent, PageSettingsModel } from '@syncfusion/ej2-angular-grids';
import { ToastComponent } from '@syncfusion/ej2-angular-notifications';
import { ButtonComponent } from 'ej-angular2';
import { dropdownModel, fieldNameList, GetlookuptableAndColumnNameList, GetTargetColumnNameList } from 'src/app/models/common/dropdownmodel';
import { ToastMessage } from 'src/app/models/MessageTypes/toast-message';
import { FieldMappingRuleTemplateModel, ObjectFieldsList } from 'src/app/Models/SourceSettings.Model';
import { ArrayForMappedFilter, MappedLookupTableModel, SyncMappingGridModel } from 'src/app/models/SyncGridColumnModel';
import { CSVFileReadingService } from 'src/app/services/csvfile-reading.service';
import { Lookupvalueservice } from 'src/app/services/Enrich/lookupvalue';
import { fileuploaderwizrad } from 'src/app/services/extract/FileuploaderServices/fileuploadwizzardstyleservices';
import { Transformation_Mapping_Loading_Using_Auto_ETL } from 'src/app/services/extract/JsonFor_Transformation_mapping_Loading';
import { InstagramConfigurationService } from 'src/app/Services/Instagram/instagram-configuration.service';
import { environment } from 'src/environments/environment';
import { AppComponentBase } from 'src/app/services/AppComponentBase';
@Component({
  selector: 'app-contentmapping-list',
  templateUrl: './contentmapping-list.component.html',
  styleUrls: ['./contentmapping-list.component.css']
})
export class ContentmappingListComponent extends AppComponentBase implements OnInit {
  // Work on this screen umar and usman 
//for toast
public testToast=new ToastMessage();
@ViewChild('defaulttoast',{static:true})
public toastObj: ToastComponent;
@ViewChild('toastBtnShow',{static:true})
public btnEleShow: ButtonComponent;
public position=this.testToast.position;
// toast end



  @ViewChild('grid',{static:true}) 
  public gridInstance : GridComponent ;  
  @ViewChild('template',{static:true}) 
  public toolbarTemplate: any;
  syncMappingGridModel=new SyncMappingGridModel();
  _modelList:any[]=[];
  _gridmodeldata:any[]=[];
  _MappedFilterArray:ArrayForMappedFilter[]=[];
  _filesnameList:dropdownModel[]=[];
  _fieldnameList:any[]=[];
  _getTargetColumnNameList:GetlookuptableAndColumnNameList[]=[];
  lookup_table_name:string;
  looktableList:any[]=[];
   _SourceColumnName:any[]=[];
public headerList:SyncMappingGridModel[]=[];
headerListForfilters:SyncMappingGridModel[]=[];
public pageSettings: PageSettingsModel;
public fieldsvalues:object;
public workspaceName:string;
filename:string;
ModelName:string;
mappedtargetcolumn:fieldNameList[]=[];
unmappedtargetcolumn:fieldNameList[]=[];
mappedfieldvalue:fieldNameList[]=[];
unmappedfieldvalue:fieldNameList[]=[];
FiltersClassessChangeOnselectedAll="css-xpr64j";
Mapped="css-b2qa21";
upmapped="css-b2qa21";
///  Usman Work  from Mapping Rules Screen in Extract 
dropdownName:string='Mapped Field'; 
lookuptableselectedvalue:any[]=[];
fieldColumnselectedvalue:any[]=[];
showHideGridDropdown:boolean[]=[];
showHideGridButton:boolean[]=[];
disableTrue:boolean=true;
defaultMappingRule:string[]=[];
mappingRules:any[]=[];
_Selectedfilename:string;
updatedMappingRules:any[]=[];
selectedMappingTemplate:string='Master';
mappingTemplate:dropdownModel[]=[];
mappingNewTemplateName:string=null;
mappingField:dropdownModel[]=[];
contentJson:any[]=[];
templateTextbox:boolean=false;
bgColor="d5eff6";
Colorid:string;
Length:number;
allcheckbox=false;
count:number=0;
change=false;
// mappedcontentname:MappedLookupTableModel[]=[];
 
workspacedropdownlist:any[]=[];
countervalue:number=0;
customAndexistingTable:any[]=[];
UpadteSourceObjecList:ObjectFieldsList[]=[];
SourceObjectList:any[]=[];
VisibilityStatus:any[]=[];
toggle:any[]=[];
sourcefilename:string="";
 
///
test:any='<ejs-dropdownlist id="_workspace" Name="workspaceDropDown" [dataSource]="_workspaces" [fields]="fieldsvalues" placeholder="Select column name" ></ejs-dropdownlist>';
constructor(private csvFileReadingService: CSVFileReadingService,public fileuploadwizardstyle:fileuploaderwizrad,private router: Router,
  private instagramConfigurationService:InstagramConfigurationService,private lookupvalueservice:Lookupvalueservice,
  private transformation_Mapping_Loading_Using_Auto_ETL_services:Transformation_Mapping_Loading_Using_Auto_ETL,injector : Injector
  
  
  ) {
    super(injector)
  this.fieldsvalues = { dataSource: this._modelList,text:'dropdownText',value:'dropdownValue'};
 }
 ngOnInit() {
      debugger
          this.sourcefilename=localStorage.getItem("sourcefile_name");
          
         this.getData();
       
          this.getModelDropdownLiat();
  // this.getfieldnameDropdownLiat()
   
         this.GetLookupTablesList();
         this.getfilenamenameList();
         
        // this.defineColumns();
      
  

  this.pageSettings = { pageSize: 6 };
  
}
//  use syncfusion  grid which condition is that if source column null table html not load 
defineColumns() {
  this.gridInstance.columns=[
    {
   field:'SourceColumn',
   headerText:'Source Column',
  },
  {
   headerText:'Target Column',
   template:this.test
  //  template:'<select><option>test</option></select>'
   // textAlign:'left',
   // width:120
  },
  
]
}
// load model dropdown list 
 getModelDropdownLiat()
 {
   debugger
   var userId=this.storageService.getItem(environment.storage.userId);
   var dropdowntext="Model Name"; 
   this.csvFileReadingService.GetModelNameList(userId,dropdowntext).subscribe((data:any)=>{
     debugger
          this.ModelName=data[0].dropdownText;
          this._modelList=data;
       })

 }
 // load field name list 
 getfieldnameDropdownLiat()
 {
    
   var userId=this.storageService.getItem(environment.storage.userId);
   var dropdowntext="Field Type"; 
   this.csvFileReadingService.GetfieldNameList(userId,dropdowntext).subscribe((data:any)=>{
       this._fieldnameList=data;
   })

 }
 // load file name list 
 getfilenamenameList()
 {
    
 var list= JSON.parse(localStorage.getItem("allFiles"))
 this.filename=list[0].name;
 for(var i=0;i<list.length;i++)
 {
  
   this._filesnameList.push({
     dropdownValue:list[i].name,
     dropdownText:list[i].name
   })
 }

 }
 /// load the look up table list 
 GetLookupTablesList()
 {
   debugger
   var UserId=this.storageService.getItem(environment.storage.userId);
   var WorkspaceId="Use Existing";
   this.lookupvalueservice.getexistingLookupTable(UserId,WorkspaceId).subscribe((data:any)=>{
     debugger
   this._gridmodeldata=data;
   
 });
}
/// this function work for content mapped array ,copy  from column mapping 
  OnChangeTargetColumnName(event:any,indexs,source_column_name:string=null,fieldcolumntype:string=null){
    debugger
    //Arry use for geting the selected lookup table field on same inedx 
    this._SourceColumnName.push(source_column_name);
    this.lookup_table_name= event.itemData.dropdownText;
    var find=this._getTargetColumnNameList.find(x=>x.source_column_name==source_column_name);
    var getindexlength=this._getTargetColumnNameList.indexOf(find);
    this.looktableList.push(event.itemData.dropdownText);
    //this.fieldColumnselectedvalue.push(this.fieldColumnselectedvalue)
    this.headerList[indexs].mappingBit="yes";
    this.headerListForfilters[indexs].mappingBit="yes";
    if(getindexlength!=-1)
    {
      this._getTargetColumnNameList[getindexlength].lookup_table_name=event.itemData.dropdownText
      //this._getTargetColumnNameList[getindexlength].lookup_field_name=this.fieldColumnselectedvalue[indexs]
    }
    else{
      this._getTargetColumnNameList.push({
        Index:indexs,
        source_column_name:source_column_name,
        lookup_table_name: this.lookup_table_name,
        lookup_field_name:this.fieldColumnselectedvalue[indexs],
      })

    }
    this.getexistingAndcustomtabledropdownfield(indexs,source_column_name);
    }
  OnChangeFieldName(event:any,indexs){
    debugger
    var dropdownname= event.itemData.dropdownText;
    var dropdownvalue= event.itemData.dropdownValue;
    var find=this._getTargetColumnNameList.find(x=>x.Index==indexs);
    var getindexlength=this._getTargetColumnNameList.indexOf(find);
    if(getindexlength==indexs)
    {
      this._getTargetColumnNameList[indexs].lookup_field_name=dropdownname;
      //this._getTargetColumnNameList[indexs].fieldName.dropdownvalue=dropdownvalue;
    }
  }
 changefilename(event:any){
   debugger
   this.headerList=[];
  var filenametext= event.itemData.dropdownText;
  this.csvFileReadingService.getFileData(filenametext,this.change).subscribe((data:any)=>{
    debugger
     var data=data;
      console.log(data);
       
  for(var i=0;i<data.columns.length;i++){
    this.headerList.push(
      {
        SourceColumn:data.columns[i],
        keycolumn:true,
        visibilty:false,
        mappingBit:""
         
         
       
      });
  }
      
       });

   
 }
GetFileContentInEditMode()
{
  debugger
  var accountId=this.sourcefilename.substring(0,7);
  var ScreenName="ContentMapped";
  this.csvFileReadingService.GetContentMappedArray(accountId,ScreenName).subscribe((data:any)=>{
    if(data.length > 0)
    {
      debugger
      console.log(data);
      this.contentJson=JSON.parse(data);
      /// we can get the list form Mongo db and match the column with sources list then get the matted column index for 
      // showing the lookup table and filed 
      for(let y=0;y<this.contentJson[0].content_mapping.length;y++)
      {
        debugger
        var FindIndex=this.headerList.findIndex(e=>e.SourceColumn === this.contentJson[0].content_mapping[y].source_column_name);
        if(FindIndex!=-1)
        {
           
          this._getTargetColumnNameList.push({
            Index:FindIndex,
            source_column_name:this.contentJson[0].content_mapping[y].source_column_name,
            lookup_field_name:this.contentJson[0].content_mapping[y].lookup_field_name,
            lookup_table_name:this.contentJson[0].content_mapping[y].lookup_table_name,
          })
          this._SourceColumnName.push(this._getTargetColumnNameList[y].source_column_name);
          this.headerListForfilters[FindIndex].mappingBit="yes";
           
        } 
       
     
      }
       
    }
   // this.mappingFilter('all');
    for(let g=0;g<this._getTargetColumnNameList.length;g++)
      {
        debugger
        
        /// Show Table name in edit Mode and after table name load in dropdown in client side 
        // then hit the function for loading the field against the lookup table 
        this.lookuptableselectedvalue[this._getTargetColumnNameList[g].Index]=this._getTargetColumnNameList[g].lookup_table_name;
        this.getexistingAndcustomtabledropdownfield(this._getTargetColumnNameList[g].Index,this._getTargetColumnNameList[g].source_column_name)
        
      }
     
  
  })
}
getData(){
  debugger
  this.headerList=[];
  this.headerListForfilters=[];
  this._getTargetColumnNameList=[];
 
   
  this.csvFileReadingService.getRawFileData(this.sourcefilename,this.change).subscribe((data:any)=>{
    debugger
     var data=data;
      console.log(data);
      for(var i=0;i<data.columns.length;i++){
    debugger
    // save data in two arrays one for actual data second for mapped and unmapped filter 
  // if use one data filter showing but actual data not showing 
    this.headerList.push(
      {
        SourceColumn:data.columns[i],
        keycolumn:true,
        visibilty:false,
        mappingBit:""
        
      });
      this.headerListForfilters.push(
        {
          SourceColumn:data.columns[i],
          keycolumn:true,
          visibilty:false,
          mappingBit:""
          
        });

      }
 /// Edit mode open in this  Page look up mapped 
      // this.GetFileContentInEditMode();
       });;
      
}
getDataForContentEditMode(){
  debugger
  this.headerList=[];
  this.headerListForfilters=[];
  this._getTargetColumnNameList=[];
 
   
  this.csvFileReadingService.getRawFileData(this.sourcefilename,this.change).subscribe((data:any)=>{
    debugger
     var data=data;
      console.log(data);
      for(var i=0;i<data.columns.length;i++){
    debugger
    // save data in two arrays one for actual data second for mapped and unmapped filter 
  // if use one data filter showing but actual data not showing 
    this.headerList.push(
      {
        SourceColumn:data.columns[i],
        keycolumn:true,
        visibilty:false,
        mappingBit:""
        
      });
      this.headerListForfilters.push(
        {
          SourceColumn:data.columns[i],
          keycolumn:true,
          visibilty:false,
          mappingBit:""
          
        });

      }
 /// Edit mode open in this  Page look up mapped 
      this.GetFileContentInEditMode();
       });;
      
}
/// load the Field Name against the Look up table  for All Filter
getexistingAndcustomtabledropdownfield(index:number,sourcecolumn){
  var UserId=this.storageService.getItem(environment.storage.userId);
  var WorkspaceId="LOOKUP FIELD";
  
  this.lookupvalueservice.getexistingLookupfield(UserId,WorkspaceId).subscribe((data:any)=>{
    debugger
   
  
  this.customAndexistingTable[index]=data;
  // this.fieldColumnselectedvalue[index]=data[index].dropdownValue;
  //this._getTargetColumnNameList[index].lookup_field_name=data[index].dropdownValue;
for(let o=0;o<this._SourceColumnName.length;o++)
{
  var find=this._getTargetColumnNameList.find(x=>x.source_column_name==this._SourceColumnName[o]);
  var getindexlength=this._getTargetColumnNameList.indexOf(find);
     if(getindexlength!=-1)
    {
         this._getTargetColumnNameList[getindexlength].lookup_field_name=data[getindexlength].dropdownValue;
         this.fieldColumnselectedvalue[getindexlength]=this._getTargetColumnNameList[getindexlength].lookup_field_name
     }
}
 
   // Show data in Edit Mode for Field
  if(this._getTargetColumnNameList.length!=0)
  {
    for(let z=0;z<this._getTargetColumnNameList.length;z++)
    {
      this.fieldColumnselectedvalue[this._getTargetColumnNameList[z].Index]=this._getTargetColumnNameList[z].lookup_field_name;
      
    }
    this.mappingFilter('all');   // use for restore data for edit 
  }
  
});
}
/// load the Field Name against the Look up table  for Mapped Filter
getexistingAndcustomtabledropdownfieldForMapped(index,sourcecolumn){
  var UserId=this.storageService.getItem(environment.storage.userId);
  var WorkspaceId="LOOKUP FIELD";
  
  this.lookupvalueservice.getexistingLookupfield(UserId,WorkspaceId).subscribe((data:any)=>{
    debugger
   
  this.fieldColumnselectedvalue[index]=data[index].dropdownValue;
   this.customAndexistingTable[index]=data;
  //    var find=this._getTargetColumnNameList.find(x=>x.source_column_name==sourcecolumn);
  //   var getindexlength=this._getTargetColumnNameList.indexOf(find);
  //  if(getindexlength!=-1)
  // {
  //       this._getTargetColumnNameList[getindexlength].lookup_field_name=data[getindexlength].dropdownValue;
  //       this.fieldColumnselectedvalue[getindexlength]=this._getTargetColumnNameList[getindexlength].lookup_field_name
  // }
   // Show data in Edit Mode for Field
  if(this._MappedFilterArray.length!=0)
  {
    for(let z=0;z<this._MappedFilterArray.length;z++)
    {
      this.fieldColumnselectedvalue[this._MappedFilterArray[z].Index]=this._MappedFilterArray[z].mappingFieldname;
    }
  }
  
});
}
// Usman Work   form Mapping rules component
contentmapping(){
   
  this.fileuploadwizardstyle.ConnectionTypeWizardClass="ConnectorType";
  this.fileuploadwizardstyle.TempleteWizardClass="ColumnMappingtick";
  this.fileuploadwizardstyle.SourceAccountDisable=false;
  this.router.navigate(['/extract/Uploader/contentmapping']);
}
saveMappingTemplate(){
  debugger
 if(this.mappingNewTemplateName==null){
   this.instagramConfigurationService.SaveMappingRuleTemplate(this.updatedMappingRules,this.selectedMappingTemplate).subscribe((data:string)=>{
     console.log(data);
   });
 }
 else{
   this.instagramConfigurationService.SaveMappingRuleTemplate(this.mappingRules,this.mappingNewTemplateName).subscribe((data:string)=>{
     console.log(data);
   });
 }
}
getdropdown(){
this.instagramConfigurationService.GetMapedField(this.dropdownName).subscribe((data:dropdownModel[])=>{
  this.mappingField =data;
    });
}
 
getMappingRules(){
this.instagramConfigurationService.getMappingRule(this.selectedMappingTemplate).subscribe((data:FieldMappingRuleTemplateModel[])=>
{
  debugger
  this.mappingRules=data;    
  this.Length=data.length;
  for(let i=0;i<this.Length;i++){
    this.showHideGridDropdown[i]=false;
    this.showHideGridButton[i]=true;
    if(this.mappingRules[i].keyColumn=='1'){
      this.mappingRules[i].keyColumn=true;
    }
    else{
      this.mappingRules[i].keyColumn =false;
    }
    
  }
  console.log(this.toggle);
  this.getdropdown();
});
}
mappingFilter(value){
 debugger
if(value=='unmapped')
{
  this.fieldColumnselectedvalue=[];
  this.lookuptableselectedvalue=[];
  this.upmapped="css-xpr64j";   /// height light the  selected button 
  this.Mapped="css-b2qa21";
  this.FiltersClassessChangeOnselectedAll="css-b2qa21";
  this.headerList = this.headerListForfilters.filter(bitmapped => bitmapped.mappingBit !="yes");
 
  // for(var i=0;i <this._getTargetColumnNameList.length;i++)
  // {
   
  // }
}
else if(value=='Mapped'){
  this.fieldColumnselectedvalue=[];
   this.lookuptableselectedvalue=[];
   this._MappedFilterArray=[];
  this.upmapped="css-b2qa21";
  this.Mapped="css-xpr64j";
  this.FiltersClassessChangeOnselectedAll="css-b2qa21";
  //this.headerList = this.headerListForfilters.filter(bitmapped => bitmapped.mappingBit == "yes");
  for(var i=0;i <this._getTargetColumnNameList.length;i++)
  {
    var find=this.headerListForfilters.find(x=>x.SourceColumn==this._getTargetColumnNameList[i].source_column_name);
      this._MappedFilterArray.push({
        Index:this._getTargetColumnNameList[i].Index,
        SourceColumn:find.SourceColumn,
        keycolumn:find.keycolumn,
        visibilty:find.visibilty,
        mappingBit:find.mappingBit,
        mappingTablename:this._getTargetColumnNameList[i].lookup_table_name,
        mappingFieldname:this._getTargetColumnNameList[i].lookup_field_name,

      })
     
    
  }
  this.headerList=this._MappedFilterArray;
  for(let m=0;m<this._MappedFilterArray.length;m++)
  {
    this.lookuptableselectedvalue[m]=this._MappedFilterArray[m].mappingTablename;
    this.getexistingAndcustomtabledropdownfieldForMapped(m,this._MappedFilterArray[m].SourceColumn)
  }
}
else if(value=='all'){
  this.fieldColumnselectedvalue=[];
   this.lookuptableselectedvalue=[];
  this.upmapped="css-b2qa21";
  this.Mapped="css-b2qa21";
  this.FiltersClassessChangeOnselectedAll="css-xpr64j";
  this.headerList = this.headerListForfilters.filter(bitmapped => bitmapped.mappingBit != "no");
  for(var i=0;i <this._getTargetColumnNameList.length;i++)
  {
    this.fieldColumnselectedvalue[this._getTargetColumnNameList[i].Index]=this._getTargetColumnNameList[i].lookup_field_name;
    this.lookuptableselectedvalue[this._getTargetColumnNameList[i].Index]=this._getTargetColumnNameList[i].lookup_table_name;
  }
}
}
 

Contentmapping(){
  debugger
   this._getTargetColumnNameList;
  this.transformation_Mapping_Loading_Using_Auto_ETL_services.JSON_For_Content_Mapping(this._getTargetColumnNameList).subscribe((data:any)=>{
         var msg=data;
         this.testToast.toast[1].content=msg;
         this.toastObj.show(this.testToast.toast[1]);
         this.lookuptableselectedvalue=[];
  })
      
       
  
}
Columnmapping(){
  localStorage.setItem("PageBit",JSON.stringify("Edit"));
  this.fileuploadwizardstyle.ApplyTransformationDisabled=true;
  this.fileuploadwizardstyle.SourceAccountDisable=true;
  this.fileuploadwizardstyle.TempleteWizardClass="ColumnMapping";
  this.router.navigate(['/extract/Uploader/columnmapping']);
 }
 CleanMapping()
 {
    
   this.ClearMappingData();
 }
 ClearMappingData(){
  debugger
  this.headerList=[];
  this.headerListForfilters=[];
  this._getTargetColumnNameList=[];
   this.fieldColumnselectedvalue=[];
   this.lookuptableselectedvalue=[];
   
  this.csvFileReadingService.getRawFileData(this.sourcefilename,this.change).subscribe((data:any)=>{
    debugger
     var data=data;
      console.log(data);
      for(var i=0;i<data.columns.length;i++){
    debugger
     
    this.headerList.push(
      {
        SourceColumn:data.columns[i],
        keycolumn:true,
        visibilty:false,
        mappingBit:""
        
      });
      this.headerListForfilters.push(
        {
          SourceColumn:data.columns[i],
          keycolumn:true,
          visibilty:false,
          mappingBit:""
          
        });
     }
    });;
      
}
RestoreDefaultMapping()
{
  this.headerList=[];
  this.headerListForfilters=[];
  this._getTargetColumnNameList=[];
   this.fieldColumnselectedvalue=[];
   this.lookuptableselectedvalue=[];
   this._SourceColumnName=[];
   this._MappedFilterArray=[];
  // this.getData();
  this.getDataForContentEditMode();
}
}
