import { Component, EventEmitter, Injector, OnInit, Output } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FiltersService } from 'src/app/Services/common/filters.service';
import { GetDataStreamService } from 'src/app/Services/extract/get-data-stream.service';
import { GetAllExtractsService } from 'src/app/Services/extract/get-all-extracts.service';
import { AllissuesService } from 'src/app/Services/extract/allissues.service';
import { GetAllConnectionsService } from 'src/app/Services/extract/get-all-connections.service';
import { dropdownModel } from 'src/app/models/common/dropdownmodel';
import { CheckBoxSelectionService, FilteringEventArgs } from '@syncfusion/ej2-angular-dropdowns';
import { ItemModel } from '@syncfusion/ej2-angular-splitbuttons';
import { filtersModel } from 'src/app/models/extract/filter.model';
import { AppComponentBase } from 'src/app/services/AppComponentBase';
import { ExtractServices } from 'src/app/services/extract/ConnectorService/ExtractDataServicesStep8';
import { environment } from 'src/environments/environment';
import { buttonGroupmodel } from 'src/app/models/extract/ExtractDataModel';
 

@Component({
  selector: 'app-connect-filters',
  templateUrl: './connect-filters.component.html',
  styleUrls: ['./connect-filters.component.css'],
  providers: [CheckBoxSelectionService]
})
export class ConnectFiltersComponent extends AppComponentBase implements OnInit {
  @Output() filterValuesOutput = new EventEmitter();
  @Output() PeriodOutput = new EventEmitter();
  filternames:string[]=['Workspace','Destination Enabled','Created By','Access Granted','Data Sources','connector type','Enabled','Last Accessed'];
  btnCancel:boolean=false;
  datastreamfilterStatus: boolean;
  allconnectionfilterStatus: boolean;
  allextractfilterStatus: boolean;
  allIssuefilterStatus: boolean;
  _buttongroupList:buttonGroupmodel[]=[];
  sourceTypeValue:String;
  userId:string=null;
  workspaceId:string=null;
  clientId:string=null;
  ButtonGroupFilterContent:string="Show Filters";
  sourceNameValue: string;
  selectedConnection:string;
  _selectedtext: String;
  workspaceName:string=null;
  connectionName:string=null;
  sourceName:string=null;
  accessGranted:string=null;
  createdBy:string=null;
  isActive:string=null;
  lastAccessed:string=null;
  destinationEnabled:string=null;
  accountId:string;
  filterToggle:boolean=false;
  showfilterText:string='More Filter';
  connectorTypeDropDown:any;
  fieldsvaluesforConnectorType: Object;
  fieldsvaluesforworkspace: Object;
  fieldsvaluesfordatasources: Object;
  fieldsvaluesforAccessgranted: Object;
  fieldsvaluesforEnabled: Object;
  fieldsvaluesforDestinationEnabled: Object;
  fieldsvaluesforlastAccessed: Object;
  fieldsvaluesforcreatedBy: Object;
  filtersValues={} as filtersModel;
  buttonlist=[
    {dropdownValue:'Today',dropdownText:"Today"},
    {dropdownValue:'Yesterday',dropdownText:"Yesterday"},
    {dropdownValue:'This week',dropdownText:"This week"},
    {dropdownValue:'This month',dropdownText:"This month"},
    {dropdownValue:'This quarter',dropdownText:"This quarter"},
  ]
  /// end 
  //Connection
  _workspace:dropdownModel[]=[];
  _destinationEnabled:dropdownModel[]=[];
  _CreatedBy:dropdownModel[]=[];
  _AccessGranted:dropdownModel[]=[];
  _DataSources:dropdownModel[]=[];
  _connectortype:dropdownModel[]=[];
  _Enabled:dropdownModel[]=[];
  _lastAccessed:dropdownModel[]=[];
   /// Filter  Marge from Aneeb ///
  
   
   selectedFilterType:string;
   SelectButtonGroup:string;
   items:ItemModel[]=[
     {text:'Basic'},
     {text:'Advance'},
     {text:'Customize'},
     {text:'Hide all'}
   ];
   fieldsvalues: Object;
   fieldsvalues2: Object;
   public mode: string;
    
  constructor(public _Filter: FiltersService,public _gridDataService:GetDataStreamService,public _extractServices:ExtractServices,
    public AllExtractService:GetAllExtractsService,public _allissueSer:AllissuesService, public allConService:GetAllConnectionsService,injector : Injector
    ) {
      super(injector)
     }

  ngOnInit(): void {
    this.filtersValues={
      userId: this.clientDetailService.getuserID(),
      workspaceId:this.clientDetailService.getWorkspaceID(),
      clientId: this.clientDetailService.getClientID(),
      workspaceName:null,
      connectionName:null,
      sourceName:null,
      accessGranted:null,
      createdBy:null,
      isActive:null,
      lastAccessed:null,
      destinationEnabled:null,
      accountId:null,
      TimeFilter:'Today'
    };
    this.filterValuesOutput.emit(this.filtersValues);

    this.mode="CheckBox";
    this.datastreamfilterStatus = this._Filter.datastreamfilterStatus;
    this.allconnectionfilterStatus = this._Filter.allconnectionfilterStatus;
    this.allextractfilterStatus = this._Filter.allextractfilterStatus;
    this.allIssuefilterStatus =this._Filter.allIssuefilterStatus;
    this._gridDataService._recordLength=0;
    this.AllExtractService._recordLength=0;
    this._allissueSer._recordLength=0;
    this.userId=this.storageService.getItem(environment.storage.userId);
    this.workspaceId=this.storageService.getItem(environment.storage.workspaceId);
    this.clientId=this.storageService.getItem(environment.storage.clientId);
    this.getbuttongrouplist();
    //Connections
    this.getDestinationEabled();

  }
//DataStream
searchValue(){
        debugger
        var SearchValue = ((document.getElementById("SearchText") as HTMLInputElement).value);
        if(SearchValue ==""){
          window.location.reload();
        }
        else{
        this._gridDataService.search(SearchValue);
      }
     }
  
//reset button
resetButton(){
this.btnCancel=true;
}
reloadWindow(){
  window.location.reload();
}

/// Filter Function From Aneeb 
getDestinationEabled(){
  for(let i=0;i<this.filternames.length;i++){
    if(this.filternames[i]=='Workspace'){
      this._Filter.GetDropDowns(this.filternames[i])
      .subscribe((data: dropdownModel[]) => {
        
        if(data.length  > 0){
          this._workspace = data;
          this.fieldsvalues = { dataSource: this._workspace,text:'dropdownText',value:'dropdownText'};
        }
    });
  }
  else if(this.filternames[i]=='Destination Enabled'){
  this._Filter.GetDropDowns(this.filternames[i])
  .subscribe((data: dropdownModel[]) => {
    if(data.length  > 0){
        for(let i=0;i<data.length;i++)
      {
            if(data[i].dropdownText=="0")
            {
              data[i].dropdownText="no"
            }
            else
            {
              data[i].dropdownText="yes"
            }
    }
      this._destinationEnabled = data;

      this.fieldsvalues2 = { text:'dropdownText',value:'dropdownValue',selected:'selected'};
    }
});
}
else if(this.filternames[i]=='Created By'){
this._Filter.GetDropDowns(this.filternames[i])
.subscribe((data: dropdownModel[]) => {
  if(data.length  > 0){
    this._CreatedBy = data;
    this.fieldsvalues2 = { dataSource: this._CreatedBy,text:'dropdownText',value:'dropdownValue'};
  }
});
}
else if(this.filternames[i]=='Access Granted'){
debugger
this._Filter.GetDropDowns(this.filternames[i])
.subscribe((data: dropdownModel[]) => {
  if(data.length  > 0){
    for(let i=0;i<data.length;i++)
    {
            if(data[i].dropdownText=="0")
            {
              data[i].dropdownText="no"
            }
            else
            {
              data[i].dropdownText="yes"
            }
    }
    this._AccessGranted = data;
    this.fieldsvalues2 = { text:'dropdownText',value:'dropdownValue',selected:'selected'};
  }
});
}
else if(this.filternames[i]=='Data Sources'){
this._Filter.GetDropDowns(this.filternames[i])
.subscribe((data: dropdownModel[]) => {
  if(data.length  > 0){
    this._DataSources = data;
    this.fieldsvalues = { dataSource: this._DataSources,text:'dropdownText',value:'dropdownText'};
  }
});
}
else if(this.filternames[i]=='connector type'){
this._Filter.GetDropDowns(this.filternames[i])
.subscribe((data:dropdownModel[]) => {
  if(data.length  > 0){
    this._connectortype = data;
    this.fieldsvalues = { dataSource: this._connectortype,text:'dropdownText',value:'dropdownText'};
  }
});
}
else if(this.filternames[i]=='Enabled'){
debugger
this._Filter.GetDropDowns(this.filternames[i])
.subscribe((data: dropdownModel[]) => {
  if(data.length  > 0){
    for(let i=0;i<data.length;i++)
    {
            if(data[i].dropdownText=="0")
            {
              data[i].dropdownText="no"
            }
            else
            {
              data[i].dropdownText="yes"
            }
    }
    this._Enabled = data;

    this.fieldsvalues2 = { text:'dropdownText',value:'dropdownValue',selected:'selected'};
  }
  });
}
else if(this.filternames[i]=='Last Accessed'){
  this._Filter.GetDropDowns(this.filternames[i])
  .subscribe((data: dropdownModel[]) => {
    if(data.length  > 0){
      this._lastAccessed = data;
      this.fieldsvalues = { dataSource: this._lastAccessed,text:'dropdownText',value:'dropdownText'};
    }
    });
  }

  }
}

SearchConnection(){
  debugger
  var SearchValue = ((document.getElementById("SearchConnection") as HTMLInputElement).value);
  if(this._Filter.datastreamfilterStatus==true){
    if(SearchValue ==""){
      window.location.reload();
    }
    else{
    this._gridDataService.search(SearchValue);
    }
  }
  
else if(this._Filter.allconnectionfilterStatus==true){
          if(SearchValue ==""){
            window.location.reload();
          }
          else{
          this.allConService.searchAllConnections(SearchValue);
          }
        }
else if(this._Filter.allextractfilterStatus==true){
        if(SearchValue ==""){
          window.location.reload();
        }
        else{
        this.AllExtractService.searchAllExtract(SearchValue);
      }
 }
else if(this._Filter.allIssuefilterStatus==true){
 }
 if(SearchValue ==""){
  window.location.reload();
}
else{
this._allissueSer.searchAllissue(SearchValue);
}
}
 
applyFilter(){
  this.filterValuesOutput.emit(this.filtersValues);
}


//AllExtract
searchValueExtracts(){
 // var SearchValue = ((document.getElementById("SearchExtract") as HTMLInputElement).value);
}


//Allissues
searchValueIssue(){
          var SearchValue = ((document.getElementById("SearchIssue") as HTMLInputElement).value);
          if(SearchValue ==""){
            window.location.reload();
          }
          else{
          this._allissueSer.searchAllissue(SearchValue);
        }
}
filterData(){
         var SearchValue = ((document.getElementById("Connections") as HTMLInputElement).value);
         console.log(SearchValue);
       }
        
SelectOption(event : any){
  var val=event.value;
    

}
Makelist(args,name){
  debugger
  console.log('source name',this.filtersValues.sourceName);
  if(name=='connectionName'){
  this.filtersValues.connectionName=args.value.toString();
  }
  else if(name=='workspacesName'){
    this.filtersValues.workspaceName=args.value.toString();
  }
  else if(name=='datasource'){
    this.filtersValues.sourceName=args.value.toString();
  }
  else if(name=='accessgranted'){
    debugger
    this.filtersValues.accessGranted=args.value.toString();
  }
  else if(name=='Isactive'){
    this.filtersValues.isActive=args.value.toString();
  }
  else if(name=='destinationenable'){
    this.filtersValues.destinationEnabled=args.value.toString();
  }
  else if(name=='Lastaccessd'){
    this.filtersValues.lastAccessed=args.value.toString();
  }
  else if(name=='createdBy'){
    this.filtersValues.createdBy=args.value.toString();
  }

}
    /// Filter Show From Aneeb 
  showFilters(va){
    debugger
 
    if(va.item.properties.text=='Basic'){
      this._Filter.BasicFilterToggle=true;
      this._Filter.AdvanceFilterToggle=false;
      this._Filter.CustomizeFilterToggle=false;
      this.ButtonGroupFilterContent='Basic'
    }
    else if(va.item.properties.text=='Advance'){
      this._Filter.BasicFilterToggle=true;
      this._Filter.AdvanceFilterToggle=true;
      this._Filter.CustomizeFilterToggle=false;
      this.ButtonGroupFilterContent='Advance'
    }
    else if(va.item.properties.text=='Customize'){
      this._Filter.BasicFilterToggle=true;
      this._Filter.AdvanceFilterToggle=true;
      this._Filter.CustomizeFilterToggle=true;
      this.ButtonGroupFilterContent='Customize'
    
  }
  else if(va.item.properties.text=='Hide all')
  {
    this._Filter.BasicFilterToggle=false;
    this._Filter.AdvanceFilterToggle=false;
    this._Filter.CustomizeFilterToggle=false;
    this.ButtonGroupFilterContent='Show Filters'
  }
}
// heigh light color button group from mahran 
// selected(event){
//   debugger
// if(event=="Today"){
//     /// Filter Classess
// this.Today="css-xpr64j";
// this.Yesterday="css-b2qa21";
// this.This_Week="css-b2qa21";
// this.thisyear="css-b2qa21";
// this.This_Quarter="css-b2qa21";
// this.PeriodOutput.emit(event);
// return
// /// end 
// }
// else if(event=="Yesterday"){
//   /// Filter Classess
// this.Today="css-b2qa21";
// this.Yesterday="css-xpr64j";
// this.This_Week="css-b2qa21";
// this.This_Month="css-b2qa21";
// this.This_Quarter="css-b2qa21";
// this.PeriodOutput.emit(event);
// return
// /// end 
// }
// else if(event=="This_Week"){
//     /// Filter Classess
// this.Today="css-b2qa21";
// this.Yesterday="css-b2qa21";
// this.This_Week="css-xpr64j";
// this.This_Month="css-b2qa21";
// this.This_Quarter="css-b2qa21";
// this.PeriodOutput.emit(event);
// return
// /// end 
// }
// else if(event=="This_Month"){
//   /// Filter Classess
// this.Today="css-b2qa21";
// this.Yesterday="css-b2qa21";
// this.This_Week="css-b2qa21";
// this.This_Month="css-xpr64j";
// this.This_Quarter="css-b2qa21";
// this.PeriodOutput.emit(event);
// return
// /// end 
// } 
// else{
//   this.Today="css-b2qa21";
// this.Yesterday="css-b2qa21";
// this.This_Week="css-b2qa21";
// this.This_Month="css-b2qa21";
//   this.This_Quarter="css-xpr64j";
//   this.PeriodOutput.emit(event);
// }   


//    this.PeriodOutput.emit(event);
// }
selected(event){
  debugger
  this.filtersValues.TimeFilter=event;
 this.filterValuesOutput.emit(this.filtersValues);
}   
/// button group db driven by umar 
getbuttongrouplist()
{
  debugger
  this._extractServices.GetButtongroup(this.userId,this.clientId,this.workspaceId).subscribe((data:buttonGroupmodel[])=>{
    debugger
    this._buttongroupList=data;
  })
}
}