import { Component, OnInit ,ViewEncapsulation, ViewChild} from '@angular/core';
import { FieldSettingsModel, DragEventArgs,ToolbarSettingsModel } from '@syncfusion/ej2-dropdowns';
import { DataManager } from '@syncfusion/ej2-data';
import { ListBoxComponent } from '@syncfusion/ej2-angular-dropdowns';
import { ConectorPipline } from 'src/app/services/extract/ConnectionPiplineSave';
import { GetAllConnectionsService } from 'src/app/Services/extract/get-all-connections.service';
import { SetValueupdatePipelineCon, updatePipelineCon } from 'src/app/models/extract/connectorPipeline';
import { listconnection } from 'src/app/models/extract/Step6ObjectFieldListModelClass';
import { Router } from '@angular/router';
 
 

@Component({
  selector: 'app-add-new-con-pipeline',
  templateUrl: './add-new-con-pipeline.component.html',
  styleUrls: ['./add-new-con-pipeline.component.css']
  
})
export class AddNewConPipelineComponent implements OnInit {
  limit:string = 'All';
  workspaceName:string=null;
  connectionName:string=null;
  sourceName:string=null;
  accessGranted:string=null;
  createdBy:string=null;
  isActive:string=null;
  lastAccessed:string=null;
  destinationEnabled:string=null;
  connectorIdsArray:any[]=[];
  connectionIdsArray:any[]=[];
  accountName:string;
  pipelinelistarray:any[]=[];
  PipelineNameArray:any[]=[];
  arraylist:listconnection[]=[];
_connectionData: any[]=[];
  lengthofpipeline:number=0;
  SavebtnValue:string="Save";
  jsonArrayForPipelineUpdate:updatePipelineCon[]=[];
  pipelineId:string;
  Setvalue:SetValueupdatePipelineCon[]=[];
  @ViewChild('listbox1',{static:true}) 
  public listObj1: ListBoxComponent; 
  @ViewChild('listbox2',{static:true})
   public listObj2: ListBoxComponent;
   
   constructor(public allConService:GetAllConnectionsService,public conectorPipline:ConectorPipline ,private router:Router) 
   {}
    
  ngOnInit() {
    this.getAllConections(this.limit);
    this.pipelineId=JSON.parse(localStorage.getItem("PipelineID"));
    if(this.pipelineId!=null)
    {
      debugger
     this.SavebtnValue="Update"
     var connectorIds=JSON.parse(localStorage.getItem("Code"));
     var connectorsName=JSON.parse(localStorage.getItem("Name"));
     var ConDisplayName=JSON.parse(localStorage.getItem("DisplayName"));
     var accountids=JSON.parse(localStorage.getItem("accountIds"));
     var CodeArr = connectorIds.split(',');
     var NameArr = connectorsName.split(',');
     var accountsIds=accountids.split(',');
     this.jsonArrayForPipelineUpdate.push({
      Name:NameArr,
      Code:CodeArr,
      Id:accountsIds

     })
    
     var arrayToString = JSON.stringify(Object.assign({}, this.jsonArrayForPipelineUpdate));
     console.log(arrayToString);
    for(var i=0;i<this.jsonArrayForPipelineUpdate.length;i++){
      debugger
        for(var j=0;j<NameArr.length;j++)
        {
          this.Setvalue.push({
            Name:this.jsonArrayForPipelineUpdate[i].Name[j],
            Code:this.jsonArrayForPipelineUpdate[i].Code[j],
            Id:this.jsonArrayForPipelineUpdate[i].Id[j],
            iconCss:""
           })
        }

        
    
    }
     
    this.dataB=new DataManager({
      json:this.Setvalue    
    });
    this.accountName=ConDisplayName;
    localStorage.removeItem("PipelineID")
    }
     else {
      this.dataB=new DataManager({
    
      });
      
     }

    
  }


  getAllConections(limit){
    debugger
       this.allConService.getAllConnectionswithfilter(this.workspaceName,this.connectionName,this.sourceName,this.accessGranted
      ,this.createdBy,this.isActive,this.lastAccessed,this.destinationEnabled,5).subscribe((data:any) => { 
        if(data.length  > 0){
            debugger
            this._connectionData= data;
            this.arraylist=[];
             for(var x=0;x<this._connectionData.length; x++)
             {
               this.arraylist.push({
                 pic:' ',
                 iconCss: 'e-list-icons e-list-user-settings',
                 
                 Name:this._connectionData[x].connectorName,
                 Code:this._connectionData[x].sourceConnectorId,
                 Id:this._connectionData[x].sourceConnectionId
                
                  
               })
              
             }
             for(var z=0;z<this.Setvalue.length;z++){
              var find=this.arraylist.find(x=>x.Code==this.Setvalue[z].Code);
              if(find !=undefined)
              {
                var RemoveIndex=this.arraylist.indexOf(find);
                this.arraylist.splice(RemoveIndex,1);
              }
              
             }
             
             this.dataA=new DataManager({
              json: this.arraylist
            });
             
          }
      });;
  }
  public dataA: DataManager = new DataManager({
     
});
public fields: FieldSettingsModel = {iconCss:'iconCss', text: 'Name'};

 public dataB: DataManager = new DataManager({
     
});
 
public modifiedDataA: ModifiedRecords = { addedRecords: [], deletedRecords: [], changedRecords: [] };
public modifiedDataB: ModifiedRecords = { addedRecords: [], deletedRecords: [], changedRecords: [] };
public saveChanges(): void {
  debugger
    this.dataA.saveChanges(this.modifiedDataA, this.fields.text);
    this.dataB.saveChanges(this.modifiedDataB, this.fields.text);
    this.modifiedDataA.addedRecords = []; this.modifiedDataB.addedRecords = [];
    this.PipelineNameArray=[];
    this.connectorIdsArray=[];
    this.connectionIdsArray=[];
    for(var i=0;i<this.dataB.dataSource.json.length;i++){
      this.PipelineNameArray.push(this.dataB.dataSource.json[i].Name)
      this.connectorIdsArray.push(this.dataB.dataSource.json[i].Code);
      this.connectionIdsArray.push(this.dataB.dataSource.json[i].Id);
    }
    this.dataB.dataSource.json=[];
    this.modifiedDataB.addedRecords = [];
    this.modifiedDataB.deletedRecords = [];
    this.modifiedDataB.changedRecords = [];
    var Connamelist=this.PipelineNameArray.toString();
    var connectoridList=this.connectorIdsArray.toString();
    var connectionidList=this.connectionIdsArray.toString();
    var id=this.pipelineId;
    this.conectorPipline.SaveConnectorList(connectoridList,connectionidList,this.accountName,id,Connamelist).subscribe((data:any)=>{
      debugger
      var result=data;
      this.getAllConections(this.limit);

       this.dataB=new DataManager({
         
         });
         this.accountName="";
         this.router.navigate(['extract/extraction/pipelinelist']);
        //  extract/extraction/pipelinelist
    });
    
}
public onDropGroupA(args: DragEventArgs): void {
    args.items.forEach((item: { [key: string]: Object; }): void => {
        /*Preventing item manipulation while doing drag and drop within same list box.*/
        
        if (!this.listObj1.getDataByValue(item[this.fields.text] as string)) {
            this.modifiedDataB.addedRecords.push(item);
            this.modifiedDataA.deletedRecords.push(item);
        }
    });
}
public onDropGroupB(args: DragEventArgs): void {
    args.items.forEach((item: { [key: string]: Object; }): void => {
        if (!this.listObj2.getDataByValue(item[this.fields.text] as string)) {
            this.modifiedDataA.addedRecords.push(item);
            this.modifiedDataB.deletedRecords.push(item);
        }
    });
}

}
interface ModifiedRecords {
  addedRecords: { [key: string]: Object }[];
  deletedRecords: { [key: string]: Object }[];
  changedRecords: { [key: string]: Object }[];
}