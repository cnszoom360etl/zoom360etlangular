import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNewConPipelineComponent } from './add-new-con-pipeline.component';

describe('AddNewConPipelineComponent', () => {
  let component: AddNewConPipelineComponent;
  let fixture: ComponentFixture<AddNewConPipelineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewConPipelineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewConPipelineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
