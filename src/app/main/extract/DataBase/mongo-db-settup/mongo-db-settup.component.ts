import { Component, OnInit } from '@angular/core';
 
 
import { SourceAccountSettup } from 'src/app/services/extract/AddNewConnectionServices/SourceAccountSettup';
import { ConnectionVarification } from 'src/app/services/extract/ConnectionVarification';
import { SqlConnector } from 'src/app/services/extract/ConnectorService/SqlConnectorServices';


@Component({
  selector: 'app-mongo-db-settup',
  templateUrl: './mongo-db-settup.component.html',
  styleUrls: ['./mongo-db-settup.component.css']
})
export class MongoDbSettupComponent implements OnInit {
  imgname:string;
  status=false;
  constructor(public _SqlConnector:SqlConnector,public sourceAccountSettup:SourceAccountSettup,private connectionVarification:ConnectionVarification) {
    this.imgname=localStorage.getItem('src');
   }

  ngOnInit(): void {
    this. getAllHostname();
    this.sourceAccountSettup.DataBase_Span=false;
    this.sourceAccountSettup.Password_Span=false;
    this.sourceAccountSettup.Username_Span=false;
     
  }
  getAllHostname(){
    debugger
    var UserId='admin';
    var DropdownType='Hostname'
    debugger
    this._SqlConnector.getHostName(UserId,DropdownType);
  }
  VarifyConnection(){
    
    var obj={
      "Hostname":this.sourceAccountSettup.Hostname,
       "Database": this.sourceAccountSettup.Database ,
       "UserName":this.sourceAccountSettup.Username ,
       "Password":this.sourceAccountSettup.Password,
       "PortNumber":this.sourceAccountSettup.PortNumber,
       }
      this.connectionVarification.ConnectionStatus(obj).subscribe((data:any)=>{
        debugger
        var msg=data;
        if(data==true)
        {
          this.sourceAccountSettup.status=true;
          this.sourceAccountSettup.saveAndupdatebuttonDisabled=false;
          this.sourceAccountSettup.connectionStatusErrorShow=false;
        }
        else {
          this.sourceAccountSettup.status=false;
          this.sourceAccountSettup.saveAndupdatebuttonDisabled=true;
          this.sourceAccountSettup.connectionStatusErrorShow=true;

        }
      })
    


  }
}
