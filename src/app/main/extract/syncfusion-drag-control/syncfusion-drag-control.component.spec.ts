import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SyncfusionDragControlComponent } from './syncfusion-drag-control.component';

describe('SyncfusionDragControlComponent', () => {
  let component: SyncfusionDragControlComponent;
  let fixture: ComponentFixture<SyncfusionDragControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SyncfusionDragControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SyncfusionDragControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
