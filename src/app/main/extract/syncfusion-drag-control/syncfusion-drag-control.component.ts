import { Component, ViewEncapsulation ,OnInit, ViewChild} from '@angular/core';
import { FieldSettingsModel, ToolbarSettingsModel } from '@syncfusion/ej2-dropdowns';
import { DataManager } from '@syncfusion/ej2-data';
import { DragEventArgs, ListBoxComponent } from '@syncfusion/ej2-angular-dropdowns';
import { listconnection, ShowingImageWithConnectorArray } from 'src/app/models/extract/Step6ObjectFieldListModelClass';
import { SetValueupdatePipeline, SetValueupdatePipelineCon, updatePipelineCon } from 'src/app/models/extract/connectorPipeline';
import { GetAllConnectionsService } from 'src/app/Services/extract/get-all-connections.service';
import { ConectorPipline } from 'src/app/services/extract/ConnectionPiplineSave';
import { Router } from '@angular/router';
 

 
@Component({
  selector: 'app-syncfusion-drag-control',
  templateUrl: './syncfusion-drag-control.component.html',
  styleUrls: ['./syncfusion-drag-control.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SyncfusionDragControlComponent implements OnInit {
  limit:string = 'All';
  workspaceName:string=null;
  connectionName:string=null;
  sourceName:string=null;
  accessGranted:string=null;
  createdBy:string=null;
  isActive:string=null;
  lastAccessed:string=null;
  destinationEnabled:string=null;
  connectorIdsArray:any[]=[];
  connectionIdsArray:any[]=[];
  accountName:string;
  pipelinelistarray:any[]=[];
  PipelineNameArray:any[]=[];
  arraylist:listconnection[]=[];
_connectionData: any[]=[];
  lengthofpipeline:number=0;
  ValueArrayWithImage:ShowingImageWithConnectorArray[]=[];
  SavebtnValue:string="Save";
  jsonArrayForPipelineUpdate:updatePipelineCon[]=[];
  pipelineId:string;
  Setvalue:SetValueupdatePipelineCon[]=[];  ////old worl array  changed by sir 
  setvalueupdatefornewcontrol:SetValueupdatePipeline[]=[];
  GetdataBValue:any[]=[];
  GetdataAValue:any[]=[];

  @ViewChild('listbox1',{static:true}) 
  public listObj1: ListBoxComponent; 
  @ViewChild('listbox2',{static:true})
   public listObj2: ListBoxComponent;
   text1:string;
  constructor(public allConService:GetAllConnectionsService,public conectorPipline:ConectorPipline ,private router:Router) { }

  ngOnInit() {
    this.getAllConections(this.limit);
    this.pipelineId=JSON.parse(localStorage.getItem("PipelineID"));
    if(this.pipelineId!=null)
    {
      debugger
     this.SavebtnValue="Update"
     var connectorIds=JSON.parse(localStorage.getItem("Code"));
     var connectorsName=JSON.parse(localStorage.getItem("Name"));
     var ConDisplayName=JSON.parse(localStorage.getItem("DisplayName"));
     var accountids=JSON.parse(localStorage.getItem("accountIds"));
     var CodeArr = connectorIds.split(',');
     var NameArr = connectorsName.split(',');
     var accountsIds=accountids.split(',');
     this.jsonArrayForPipelineUpdate.push({
      Name:NameArr,
      Code:CodeArr,
      Id:accountsIds

     })
    
     var arrayToString = JSON.stringify(Object.assign({}, this.jsonArrayForPipelineUpdate));
     console.log(arrayToString);
    for(var i=0;i<this.jsonArrayForPipelineUpdate.length;i++){
      debugger
        for(var j=0;j<NameArr.length;j++)
        {
          // this.Setvalue.push({
          //   Name:this.jsonArrayForPipelineUpdate[i].Name[j],
          //   Code:this.jsonArrayForPipelineUpdate[i].Code[j],
          //   Id:this.jsonArrayForPipelineUpdate[i].Id[j],
          //   iconCss:""
          //  })
          this.setvalueupdatefornewcontrol.push({
             
            Name:this.jsonArrayForPipelineUpdate[i].Name[j],
            pic:this.jsonArrayForPipelineUpdate[i].Name[j].replace(/\s/g, ""),
            description:this.jsonArrayForPipelineUpdate[i].Id[j],
            Code:this.jsonArrayForPipelineUpdate[i].Code[j],
            Id:this.jsonArrayForPipelineUpdate[i].Id[j],
           })
        }

        
    
    }
     
    this.dataB=this.setvalueupdatefornewcontrol;
    this.GetdataBValue=this.setvalueupdatefornewcontrol;
    this.accountName=ConDisplayName;
    localStorage.removeItem("PipelineID")
    }
     else {
      this.dataB={}; 
      
     }
  }
  
  public data: { [key: string]: Object }[] = [
    { text: 'JavaScript', pic: 'javascript', description: 'It is a lightweight interpreted or JIT-compiled programming language.' },
    { text: 'TypeScript', pic: 'typeScript', description: 'It is a typed superset of Javascript that compiles to plain JavaScript.' },
    { text: 'Angular', pic: 'angular', description: 'It is a TypeScript-based open-source web application framework.' },
    { text: 'React', pic: 'react', description: 'A JavaScript library for building user interfaces. It can also render on the server using Node.' },
    { text: 'Vue', pic: 'vue', description: 'A progressive framework for building user interfaces. it is incrementally adoptable.' }
];
getAllConections(limit){
  debugger
     this.allConService.getAllConnectionswithfilter(this.workspaceName,this.connectionName,this.sourceName,this.accessGranted
    ,this.createdBy,this.isActive,this.lastAccessed,this.destinationEnabled,5).subscribe((data:any) => { 
      if(data.length  > 0){
          debugger
          this._connectionData= data;
          this.arraylist=[];
           for(var x=0;x<this._connectionData.length; x++)
           {
             this.arraylist.push({
               pic:' ',
               iconCss: 'e-list-icons e-list-user-settings',
               
               Name:this._connectionData[x].connectorName,
               Code:this._connectionData[x].sourceConnectorId,
               Id:this._connectionData[x].sourceConnectionId
              
                
             })
             this.arraylist.push({
              pic:' ',
              iconCss: 'e-list-icons e-list-user-settings',
              
              Name:this._connectionData[x].connectorName,
              Code:this._connectionData[x].sourceConnectorId,
              Id:this._connectionData[x].sourceConnectionId
             
               
            })


            this.ValueArrayWithImage.push({
              Name:this._connectionData[x].connectorName,
              pic:this._connectionData[x].connectorName.replace(/\s/g, ""),
              description:this._connectionData[x].sourceConnectorId,
              Code:this._connectionData[x].sourceConnectorId,
               Id:this._connectionData[x].sourceConnectionId
             })
            
           }
           for(var z=0;z<this.setvalueupdatefornewcontrol.length;z++){
            var find=this.ValueArrayWithImage.find(x=>x.Code==this.setvalueupdatefornewcontrol[z].Code);
            if(find !=undefined)
            {
              var RemoveIndex=this.ValueArrayWithImage.indexOf(find);
              this.ValueArrayWithImage.splice(RemoveIndex,1);
            }
            
           }
           
            //this.dataA=this.arraylist;
          this.dataA=this.ValueArrayWithImage;
           
           
        }
    });;
}
public dataA ={}; 
   

// public dataB: DataManager = new DataManager({
     
// });
 public dataB={};
public fields: FieldSettingsModel = { text: 'Name'};
public toolbarSettings: ToolbarSettingsModel = { items: ['moveUp', 'moveDown','moveFrom', 'moveTo', 'moveAllTo', 'moveAllFrom']}
public modifiedDataA: ModifiedRecords = { addedRecords: [], deletedRecords: [], changedRecords: [] };
public modifiedDataB: ModifiedRecords = { addedRecords: [], deletedRecords: [], changedRecords: [] };
public saveChanges(): void {
  debugger
   
   
    
    // this.modifiedDataA.addedRecords = []; this.modifiedDataB.addedRecords = [];
 
    this.PipelineNameArray=[];
    this.connectorIdsArray=[];
    this.connectionIdsArray=[];
    for(var i=0;i<this.GetdataBValue.length;i++){
      this.PipelineNameArray.push(this.GetdataBValue[i].Name)
      this.connectorIdsArray.push(this.GetdataBValue[i].Code);
      this.connectionIdsArray.push(this.GetdataBValue[i].Id);
    }
    this.GetdataBValue=[];
    // this.modifiedDataB.addedRecords = [];
    // this.modifiedDataB.deletedRecords = [];
    // this.modifiedDataB.changedRecords = [];
    var Connamelist=this.PipelineNameArray.toString();
    var connectoridList=this.connectorIdsArray.toString();
    var connectionidList=this.connectionIdsArray.toString();
    var id=this.pipelineId;
    this.conectorPipline.SaveConnectorList(connectoridList,connectionidList,this.accountName,id,Connamelist).subscribe((data:any)=>{
      debugger
      var result=data;
      this.getAllConections(this.limit);

      this.dataB={};
      this.accountName="";
        
      this.router.navigate(['extract/extraction/pipelinelist']);
    });
    
}

public onDropGroupA(args): void {
  debugger
  if (args.eventName === "moveTo" || "moveFrom" || "moveAllTo" || "moveAllFrom") { 
   
    if(args.eventName !="moveAllFrom")
    {
      for(var i=0;i<args.items.length;i++)
      {
        this.GetdataBValue.push(args.items[i]);
      }
    }
    else{
      this.GetdataBValue=[];
     
  }  
  
 
    
    
} 
  
}
public onDropGroupB(argsB): void {
 debugger

 if (argsB.eventName === "moveTo" || "moveFrom" || "moveAllTo" || "moveAllFrom") { 
   
    if(argsB.eventName !="moveAllFrom")
  {
    for(var i=0;i<argsB.items.length;i++)
    {
      this.GetdataAValue.push(argsB.items[i]);
      var find=this.GetdataBValue.find(x=>x.Id==argsB.items[i].Id);
      var RemoveIndex=this.GetdataBValue.indexOf(find);
      this.GetdataBValue.splice(RemoveIndex,1);
    }
  }
  
   
  else{
    this.GetdataAValue=[];
  }
 
    
    
} 
   
}

getvalue(value:any){
debugger
var test=value;
}

}
interface ModifiedRecords {
  addedRecords: { [key: string]: Object }[];
  deletedRecords: { [key: string]: Object }[];
  changedRecords: { [key: string]: Object }[];
}
