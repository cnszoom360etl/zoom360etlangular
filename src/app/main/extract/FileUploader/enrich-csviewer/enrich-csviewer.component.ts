import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { EditService, EditSettingsModel, FilterService, GridComponent, GroupService, InfiniteScrollService, PageService, PageSettingsModel, RowDataBoundEventArgs, SortService, ToolbarItems, VirtualScrollService, } from '@syncfusion/ej2-angular-grids';
import { SyncGridColumnModel } from 'src/app/models/SyncGridColumnModel';
import { CSVFileReadingService } from 'src/app/services/csvfile-reading.service';
import { DataManager, Query, JsonAdaptor,WebApiAdaptor } from '@syncfusion/ej2-data';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-enrich-csviewer',
  templateUrl: './enrich-csviewer.component.html',
  styleUrls: ['./enrich-csviewer.component.css'],
  // encapsulation: ViewEncapsulation.None,
   providers: [PageService,
    SortService,
    FilterService,
    GroupService,
    EditService,
  InfiniteScrollService,
  ]
})
export class EnrichCSViewerComponent implements OnInit {
  @ViewChild('grid',{static:true}) 
    public gridInstance : GridComponent ;  
    public toolbarOptions: ToolbarItems[];
  public columnNameList:SyncGridColumnModel[]=[];
  public data: object[]=[];
  public pageSettings: PageSettingsModel;
  public editSettings: Object;
  public filterSettings: Object;
  public editOptions: EditSettingsModel;
  constructor(private csvFileReadingService: CSVFileReadingService) { }


  ngOnInit() {
    
    this.filterSettings = { type: 'Menu' };
    this.toolbarOptions = ['Add', 'Edit', 'Delete', 'Update', 'Cancel','Search'];
    this.pageSettings={pageSize:100}
    this.editOptions = { allowEditing: true, allowAdding: true, allowDeleting: true, mode: 'Normal' };
    // this.editSettings = { allowEditing: true, allowAdding: true, allowDeleting: true , newRowPosition: 'Top' };
  }
  getData(){
    this.gridInstance.showSpinner();
    var filename=JSON.parse(localStorage.getItem("previewfilename"));
    this.csvFileReadingService.getFileDatas(filename).subscribe((Data:any)=>{
    
    
    var test=JSON.parse(Data);
    debugger
    let xyz=[];
    var columnList= Object.keys(test[0]);
    xyz.push({field:columnList[0],headerText:columnList[0],isPrimaryKey:true});
    for(var i=1;i<columnList.length;i++){
      this.columnNameList.push({columnName:columnList[i]});
      xyz.push({field:columnList[i],headerText:columnList[i],});
    }
    this.gridInstance.columns=xyz;
    //  this.data=new DataManager({ json: test, adaptor: new JsonAdaptor() }).executeLocal(new Query().take(43));
    this.data=test;
    // this.gridInstance.dataSourceChange();
     this.gridInstance.refresh();
     this.gridInstance.hideSpinner();

     })
  }
  save(){
    debugger
    // console.log(this.gridInstance.);
   // console.log(this.data);
   var t=JSON.stringify(this.data);
    this.csvFileReadingService.sentCSVFileData(this.data).subscribe((message:any)=>{
      console.log(message);
    })
  }
}
