import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { GridComponent, PageSettingsModel } from '@syncfusion/ej2-angular-grids';

 
 
import { dropdownModel, fieldNameList, GetTargetColumnNameList } from 'src/app/models/common/dropdownmodel';
import { FieldMappingRuleTemplateModel, ObjectFieldsList } from 'src/app/Models/SourceSettings.Model';
 import { ColumnMappingJsonModel, SyncGridColumnModel, SyncMappingGridModel } from 'src/app/models/SyncGridColumnModel';
import { AppComponentBase } from 'src/app/services/AppComponentBase';
import { CSVFileReadingService } from 'src/app/services/csvfile-reading.service';
import { fileuploaderwizrad } from 'src/app/services/extract/FileuploaderServices/fileuploadwizzardstyleservices';
import { InstagramConfigurationService } from 'src/app/Services/Instagram/instagram-configuration.service';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-enrich-model-mapping',
  templateUrl: './enrich-model-mapping.component.html',
  styleUrls: ['./enrich-model-mapping.component.css']
})
export class EnrichModelMappingComponent extends AppComponentBase {
  @ViewChild('grid',{static:true}) 
    public gridInstance : GridComponent ;  
  @ViewChild('template',{static:true}) 
    public toolbarTemplate: any;
    syncMappingGridModel=new SyncMappingGridModel();
    targetcolumnvalue:any[]=[];
    fieldtypevalue:AnalyserNode[]=[];
    JsonObjectModel:ColumnMappingJsonModel[]=[];
    _modelList:any[]=[];
    _gridmodeldata:any[]=[];
    _filesnameList:dropdownModel[]=[];
    _fieldnameList:any[]=[];
    _bit:string;
    _getTargetColumnNameList:GetTargetColumnNameList[]=[];
    TargetColumndropdownname:string;
    _visibilityList = [
      {dropdownText:'no',dropdownValue:'no',selected:false},
      {dropdownText:'yes',dropdownValue:'first_name',selected:true},
      ];
 public headerList:SyncMappingGridModel[]=[];
  headerListForfilters:SyncMappingGridModel[]=[];
 public pageSettings: PageSettingsModel;
 public fieldsvalues:object;
 public workspaceName:string;
 filename:string;
 ModelName:string;
  mappedtargetcolumn:fieldNameList[]=[];
  unmappedtargetcolumn:fieldNameList[]=[];
  mappedfieldvalue:fieldNameList[]=[];
  unmappedfieldvalue:fieldNameList[]=[];
  FiltersClassessChangeOnselectedAll="css-xpr64j";
  Mapped="css-b2qa21";
  upmapped="css-b2qa21";
  NewFileNameField:boolean=true;
  exitORNewfile:boolean;
 ///  Usman Work  from Mapping Rules Screen in Extract 
 dropdownName:string='Mapped Field'; 
 targetColumnselectedvalue:any[]=[];
 fieldColumnselectedvalue:any[]=[];
  showHideGridDropdown:boolean[]=[];
  showHideGridButton:boolean[]=[];
  disableTrue:boolean=true;
  defaultMappingRule:string[]=[];
  mappingRules:any[]=[];
  updatedMappingRules:any[]=[];
  selectedMappingTemplate:string='Master';
  mappingTemplate:dropdownModel[]=[];
  mappingNewTemplateName:string=null;
  mappingField:dropdownModel[]=[];
  modeldropdownlist:string;
  templateTextbox:boolean=false;
  bgColor="d5eff6";
  Colorid:string;
  Length:number;
  allcheckbox=false;
  count:number=0;
  bit:any;
  change=false;
  workspacedropdownlist:any[]=[];
  countervalue:number=0;
  UpadteSourceObjecList:ObjectFieldsList[]=[];
  SourceObjectList:any[]=[];
  VisibilityStatus:any[]=[];
  toggle:any[]=[];
   dropdownfilename:string;
 ///
test:any='<ejs-dropdownlist id="_workspace" Name="workspaceDropDown" [dataSource]="_workspaces" [fields]="fieldsvalues" placeholder="Select column name" ></ejs-dropdownlist>';
  constructor(private csvFileReadingService: CSVFileReadingService,public fileuploadwizardstyle:fileuploaderwizrad,private router: Router,private instagramConfigurationService:InstagramConfigurationService,injector : Injector) {
    super(injector)
    this.fieldsvalues = { dataSource: this._modelList,text:'dropdownText',value:'dropdownValue'};
 
   }

  ngOnInit() {
    debugger
    this._bit=JSON.parse(localStorage.getItem("PageBit"));
      this.getModelDropdownLiat()
      this.getfieldnameDropdownLiat()
      this.getfilenamenameList()
    
     
    
    
    
    
   
  
    this.pageSettings = { pageSize: 6 };
    
  }
  defineColumns() {
    this.gridInstance.columns=[
      {
     field:'SourceColumn',
     headerText:'Source Column',
    },
    {
     headerText:'Target Column',
     template:this.test
    //  template:'<select><option>test</option></select>'
     // textAlign:'left',
     // width:120
    },
    
  ]
  }
  ShowHideNewFileNameField(){
    if(this.exitORNewfile==true)
    {
      this.NewFileNameField==false;
    }
    else
    {
      this.NewFileNameField==true;
    }
  }
   getModelDropdownLiat()
   {
     debugger
     var userId=this.storageService.getItem(environment.storage.userId);
     var dropdowntext="Model Name"; 
     this.csvFileReadingService.GetModelNameList(userId,dropdowntext).subscribe((data:any)=>{
       debugger
        this.ModelName=data[0].dropdownText;
        if(this._bit=="Edit")
        {
          this.onChange(this.ModelName);
        }
        
        this._modelList=data;
     })

   }
   getfieldnameDropdownLiat()
   {
      
     var userId=this.storageService.getItem(environment.storage.userId);
     var dropdowntext="Field Type"; 
     this.csvFileReadingService.GetfieldNameList(userId,dropdowntext).subscribe((data:any)=>{
         this._fieldnameList=data;
     })

   }
   getfilenamenameList()
   {
      
   var list= JSON.parse(localStorage.getItem("allFiles"))
   this.filename=list[0].name;
   localStorage.setItem("FileName",JSON.stringify(this.filename));
   for(var i=0;i<list.length;i++)
   {
    
     this._filesnameList.push({
       dropdownValue:list[i].name,
       dropdownText:list[i].name
     })
   }

   }
   onChange(event:any)
   {
    
     debugger
     
     this.headerList=[];
     this.headerListForfilters=[];
     this.targetcolumnvalue=[];
     this.fieldtypevalue=[];
     this._getTargetColumnNameList=[];
     if(event.itemData==undefined)
     {
      this.modeldropdownlist= event;
     }
     else
     {
      this.modeldropdownlist= event.itemData.dropdownText;
     }
    
    var userid=this.storageService.getItem(environment.storage.userId);
    var workspaceid="1";
    var clientid=this.storageService.getItem(environment.storage.clientId);
    this.csvFileReadingService.GetModelNameListForGrid(userid,workspaceid,clientid,this.modeldropdownlist).subscribe((data:any)=>{
      debugger

      if(this._bit=="Edit")
      {
        
        this.EditMode();
         
      }
      else{
        this.getData();
      }  
    this._gridmodeldata=data;
    })
    }
    OnChangeTargetColumnName(event:any,indexs,sourcecolumn:string=null,visibility:boolean){
      debugger
      
      this.TargetColumndropdownname= event.itemData.dropdownText;
      var find=this._getTargetColumnNameList.find(x=>x.source_Column==sourcecolumn);
      var getindexlength=this._getTargetColumnNameList.indexOf(find);
      var findtargetcol=this._getTargetColumnNameList.find(x=>x.Index==indexs);
      var findtargetcollength=this._getTargetColumnNameList.indexOf(find);
      if(findtargetcollength==indexs)
      { 
        this.targetcolumnvalue[indexs]=event.itemData.dropdownText;
      }
      else{
        this.targetcolumnvalue.push(event.itemData.dropdownText);
      }
      this.headerList[indexs].mappingBit="yes";
      this.headerListForfilters[indexs].mappingBit="yes";
      if(getindexlength!=-1)
      {
       
        this._getTargetColumnNameList[getindexlength].target_value=this.TargetColumndropdownname;
      }
      else{
        this._getTargetColumnNameList.push({
          Index:indexs,
          source_Column:sourcecolumn,
          target_value: this.TargetColumndropdownname,
          field_type:{dropdownText:"",dropdownvalue:""
          },
          visibility:visibility
        })
       
      }
        
       
        
       
       
       
      

    }
    OnChangeFieldName(event:any,indexs,sourcecolumn_name:string,visibility:boolean){
      debugger
      
      var dropdownname= event.itemData.dropdownText;
      var dropdownvalue= event.itemData.dropdownValue;
      var find=this._getTargetColumnNameList.find(x=>x.source_Column==sourcecolumn_name);
      var getindexlength=this._getTargetColumnNameList.indexOf(find);
      var findField=this._getTargetColumnNameList.find(x=>x.Index==indexs);
      var findFieldlength=this._getTargetColumnNameList.indexOf(find);
      if(findFieldlength==indexs)
      {
        this.fieldtypevalue[indexs]=event.itemData.dropdownValue;
         
      }
      else {
        this.fieldtypevalue.push(
         
          event.itemData.dropdownValue
        );
      }
      
      if(getindexlength!=-1)
      {
       
        this._getTargetColumnNameList[getindexlength].field_type.dropdownText=dropdownname;
        this._getTargetColumnNameList[getindexlength].field_type.dropdownvalue=dropdownvalue;
      }
      else {
        this._getTargetColumnNameList.push({
          Index:indexs,
          source_Column:"",
          target_value:"",
          field_type:{dropdownText:dropdownname,dropdownvalue:dropdownvalue},
          visibility:visibility
        })
        ; 
      }
         
        
     
      

    }
    ChangetheVisibiltySetting(indexs,sourcecolumn:string=null,visibility:boolean){
      debugger
      var find=this._getTargetColumnNameList.find(x=>x.source_Column==sourcecolumn);
      var getindexlength=this._getTargetColumnNameList.indexOf(find);
      if(getindexlength!=-1)
      {
        this._getTargetColumnNameList[getindexlength].visibility=visibility;
      }
      else{
        this._getTargetColumnNameList.push({
          Index:indexs,
          source_Column:sourcecolumn,
          target_value:"",
          field_type:{dropdownText:"",dropdownvalue:""
          },
          visibility:visibility
        })
      }



    }
   changefilename(event:any){
     debugger
     this.headerList=[];
    var filenametext= event.itemData.dropdownText;
    localStorage.setItem("FileName",JSON.stringify(filenametext));
    this.csvFileReadingService.getFileData(filenametext,this.change).subscribe((data:any)=>{
      debugger
       
     
       var data=data;
        console.log(data);
         
    for(var i=0;i<data.columns.length;i++){
      this.headerList.push(
        {
          SourceColumn:data.columns[i],
          keycolumn:true,
          visibilty:false,
          mappingBit:""
          
        });
    }
        
         });

     
   }

  getData(){
    debugger
    this.headerList=[];
    this.headerListForfilters=[];
    
    this.csvFileReadingService.getFileData(this.filename,this.change).subscribe((data:any)=>{
      debugger
       var data=data;
        console.log(data);
         
    for(var i=0;i<data.columns.length;i++){
      debugger
      this.headerList.push(
        {
          SourceColumn:data.columns[i],
          keycolumn:true,
          visibilty:true,
          mappingBit:""
          
        });
        this.headerListForfilters.push(
          {
            SourceColumn:data.columns[i],
            keycolumn:true,
            visibilty:true,
            mappingBit:""
            
          });
    }
        
         });;
    
  }
  // Usman Work   form Mapping rules component
  contentmapping(){
      debugger
      localStorage.setItem("PageBit",JSON.stringify("Add"));
      this.JsonObjectModel=[];
      localStorage.setItem("columnmappingList",JSON.stringify(this.headerList));
      localStorage.setItem("targetcolumnlist",JSON.stringify(this.targetcolumnvalue));
      localStorage.setItem("fieldtypelist",JSON.stringify(this.fieldtypevalue));
      localStorage.setItem("targetcolumnslist",JSON.stringify(this._getTargetColumnNameList));
      
    for(var i=0;i<this._getTargetColumnNameList.length;i++)
    {
      this.JsonObjectModel.push({
        source_name:this._getTargetColumnNameList[i].source_Column,
        target_name:this._getTargetColumnNameList[i].target_value,
        field_type:this._getTargetColumnNameList[i].field_type.dropdownText,
        visibility:this._getTargetColumnNameList[i].visibility
      })
    }
    // var skiprow=JSON.parse(localStorage.getItem("SkipRows"));
    // var first_row_have_header=JSON.parse(localStorage.getItem("first_row_have_header"));
    // var header_skiprows=JSON.parse(localStorage.getItem("remove_row_above_header"));
    var skiprow=this.storageService.getItem(environment.storage.SkipRows);
    var first_row_have_header=JSON.parse(localStorage.getItem("first_row_have_header"));
    var header_skiprows=this.storageService.getItem(environment.storage.remove_row_above_header);
   
     var obj={
       filename:"/run/user/1000/gvfs/smb-share:server=192.168.223.100,share=d/zoom_files_dump/"+this.filename,
       skiprows:skiprow,
       content:this.JsonObjectModel,
       first_row_have_header:first_row_have_header,
       header_skiprows:header_skiprows,
      

     }
        this.csvFileReadingService.SetColumns(obj).subscribe((data:any)=>{
           debugger
          var msg=JSON.parse(data);
          // if(msg.message=="Success")
          // {
            this.fileuploadwizardstyle.ConnectionTypeWizardClass="ConnectorType";
          this.fileuploadwizardstyle.TempleteWizardClass="ColumnMappingtick";
          this.fileuploadwizardstyle.SourceAccountDisable=false;
          this.router.navigate(['/extract/Uploader/contentmapping']);

         // }
          // else 
          // {
          //   alert("Something went wronge")
          // }
          

        })

    
  }
  saveMappingTemplate(){
    debugger
   if(this.mappingNewTemplateName==null){
     this.instagramConfigurationService.SaveMappingRuleTemplate(this.updatedMappingRules,this.selectedMappingTemplate).subscribe((data:string)=>{
       console.log(data);
     });
   }
   else{
     this.instagramConfigurationService.SaveMappingRuleTemplate(this.mappingRules,this.mappingNewTemplateName).subscribe((data:string)=>{
       console.log(data);
     });
   }
  
 
 
 }
 getdropdown(){
  this.instagramConfigurationService.GetMapedField(this.dropdownName).subscribe((data:dropdownModel[])=>{
    this.mappingField =data;
      });
}
 getMappingRules(){
  this.instagramConfigurationService.getMappingRule(this.selectedMappingTemplate).subscribe((data:FieldMappingRuleTemplateModel[])=>
  {
    debugger
    this.mappingRules=data;    
    this.Length=data.length;
    for(let i=0;i<this.Length;i++){
      this.showHideGridDropdown[i]=false;
      this.showHideGridButton[i]=true;
      if(this.mappingRules[i].keyColumn=='1'){
        this.mappingRules[i].keyColumn=true;
      }
      else{
        this.mappingRules[i].keyColumn =false;
      }
      
    }
    console.log(this.toggle);
    this.getdropdown();
  });
}
mappingFilter(value){

   
  debugger
  if(value=='unmapped')
  {
    this.upmapped="css-xpr64j";   /// height light the  selected button 
    this.Mapped="css-b2qa21";
    this.FiltersClassessChangeOnselectedAll="css-b2qa21";
    this.headerList = this.headerListForfilters.filter(bitmapped => bitmapped.mappingBit !="yes");
    for(var i=0;i <this._getTargetColumnNameList.length;i++)
    {
     
     
    this.fieldColumnselectedvalue[i]="";
    this.targetColumnselectedvalue[i]="";
    }
}
  else if(value=='Mapped'){
    this.upmapped="css-b2qa21";
    this.Mapped="css-xpr64j";
    this.FiltersClassessChangeOnselectedAll="css-b2qa21";
    this.headerList = this.headerListForfilters.filter(bitmapped => bitmapped.mappingBit == "yes");
    for(var i=0;i <this._getTargetColumnNameList.length;i++)
    {
     
     
    this.fieldColumnselectedvalue[i]=this._getTargetColumnNameList[i].field_type.dropdownvalue;
    this.targetColumnselectedvalue[i]=this._getTargetColumnNameList[i].target_value;
    }
    
   
     
  }
  else if(value=='all'){
    this.upmapped="css-b2qa21";
    this.Mapped="css-b2qa21";
    this.FiltersClassessChangeOnselectedAll="css-xpr64j";
    this.headerList = this.headerListForfilters.filter(bitmapped => bitmapped.mappingBit != "no");
    for(var i=0;i <this._getTargetColumnNameList.length;i++)
    {
     
     
    this.fieldColumnselectedvalue[i]=this._getTargetColumnNameList[i].field_type.dropdownvalue;
    this.targetColumnselectedvalue[i]=this._getTargetColumnNameList[i].target_value;
    }
    
    //this.getData();
    
  }
}
ClearAll(){
  this.mappingRules.forEach((element) => {
    element.targetColumn=null;
    element.keyColumn = false;
    element.vivisibilityStatus = null;
  });

}
uploadImage()
{
  
  this.fileuploadwizardstyle.ApplyTransformationDisabled=true;
  this.fileuploadwizardstyle.FilePreviewDisabled=true;
  this.fileuploadwizardstyle.SourceAccountDisable=true;
  this.fileuploadwizardstyle.TempleteAccountWizard=true;
  this.fileuploadwizardstyle.ConnectionTypeWizardClass="css-fileupload";
  localStorage.setItem("PageBit",JSON.stringify("Edit"));
  this.router.navigate(['/extract/Uploader/FileUpload']);

}
EditMode(){
  debugger
  this.headerList=[];
  this.headerListForfilters=[];
  this.targetcolumnvalue=[];
  this.fieldtypevalue=[];
  this._getTargetColumnNameList=[];

  this.headerList=JSON.parse(localStorage.getItem("columnmappingList"));
  this.headerListForfilters=JSON.parse(localStorage.getItem("columnmappingList"));
  this.targetcolumnvalue=JSON.parse(localStorage.getItem("targetcolumnlist"));
  this.fieldtypevalue=JSON.parse(localStorage.getItem("fieldtypelist"));
  this._getTargetColumnNameList=JSON.parse(localStorage.getItem("targetcolumnslist"));
  for(let y=0;y<this._getTargetColumnNameList.length;y++)
  {
    this.fieldColumnselectedvalue[this._getTargetColumnNameList[y].Index]=this._getTargetColumnNameList[y].field_type.dropdownvalue;
    this.targetColumnselectedvalue[this._getTargetColumnNameList[y].Index]=this._getTargetColumnNameList[y].target_value;
  }
  // for(var i=0;i<this.fieldtypevalue.length;i++)
  // { 
  //   this.fieldColumnselectedvalue[i]=this.fieldtypevalue[i];
  // }
  // for(var i=0;i<this.targetcolumnvalue.length;i++)
  // {
  //   this.targetColumnselectedvalue[i]=this.targetcolumnvalue[i];
  // }


}
 

 
  
}
