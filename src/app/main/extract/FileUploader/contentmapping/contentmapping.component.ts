import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { GridComponent, PageSettingsModel } from '@syncfusion/ej2-angular-grids';
 
 

 
 
 
import { dropdownModel, fieldNameList, GetlookuptableAndColumnNameList, GetTargetColumnNameList } from 'src/app/models/common/dropdownmodel';
import { FieldMappingRuleTemplateModel, ObjectFieldsList } from 'src/app/Models/SourceSettings.Model';
import { SyncMappingGridModel } from 'src/app/models/SyncGridColumnModel';
import { AppComponentBase } from 'src/app/services/AppComponentBase';
import { CSVFileReadingService } from 'src/app/services/csvfile-reading.service';
import { Lookupvalueservice } from 'src/app/services/Enrich/lookupvalue';
import { fileuploaderwizrad } from 'src/app/services/extract/FileuploaderServices/fileuploadwizzardstyleservices';
import { InstagramConfigurationService } from 'src/app/Services/Instagram/instagram-configuration.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-contentmapping',
  templateUrl: './contentmapping.component.html',
  styleUrls: ['./contentmapping.component.css']
})
export class ContentmappingComponent extends AppComponentBase implements OnInit {
  @ViewChild('grid',{static:true}) 
  public gridInstance : GridComponent ;  
  @ViewChild('template',{static:true}) 
  public toolbarTemplate: any;
  syncMappingGridModel=new SyncMappingGridModel();
  _modelList:any[]=[];
  _gridmodeldata:any[]=[];
  _filesnameList:dropdownModel[]=[];
  _fieldnameList:any[]=[];
  _getTargetColumnNameList:GetlookuptableAndColumnNameList[]=[];
  lookup_table_name:string;
  looktableList:any[]=[];
   
public headerList:SyncMappingGridModel[]=[];
headerListForfilters:SyncMappingGridModel[]=[];
public pageSettings: PageSettingsModel;
public fieldsvalues:object;
public workspaceName:string;
filename:string;
ModelName:string;
mappedtargetcolumn:fieldNameList[]=[];
unmappedtargetcolumn:fieldNameList[]=[];
mappedfieldvalue:fieldNameList[]=[];
unmappedfieldvalue:fieldNameList[]=[];
FiltersClassessChangeOnselectedAll="css-xpr64j";
Mapped="css-b2qa21";
upmapped="css-b2qa21";
///  Usman Work  from Mapping Rules Screen in Extract 
dropdownName:string='Mapped Field'; 
lookuptableselectedvalue:any[]=[];
fieldColumnselectedvalue:any[]=[];
showHideGridDropdown:boolean[]=[];
showHideGridButton:boolean[]=[];
disableTrue:boolean=true;
defaultMappingRule:string[]=[];
mappingRules:any[]=[];
_Selectedfilename:string;
updatedMappingRules:any[]=[];
selectedMappingTemplate:string='Master';
mappingTemplate:dropdownModel[]=[];
mappingNewTemplateName:string=null;
mappingField:dropdownModel[]=[];
 
templateTextbox:boolean=false;
bgColor="d5eff6";
Colorid:string;
Length:number;
allcheckbox=false;
count:number=0;
change=true;

workspacedropdownlist:any[]=[];
countervalue:number=0;
customAndexistingTable:any[]=[];
UpadteSourceObjecList:ObjectFieldsList[]=[];
SourceObjectList:any[]=[];
VisibilityStatus:any[]=[];
toggle:any[]=[];
///
test:any='<ejs-dropdownlist id="_workspace" Name="workspaceDropDown" [dataSource]="_workspaces" [fields]="fieldsvalues" placeholder="Select column name" ></ejs-dropdownlist>';
constructor(private csvFileReadingService: CSVFileReadingService,public fileuploadwizardstyle:fileuploaderwizrad,private router: Router,
  private instagramConfigurationService:InstagramConfigurationService,
  private lookupvalueservice:Lookupvalueservice,injector : Injector) {
    super(injector)

  this.fieldsvalues = { dataSource: this._modelList,text:'dropdownText',value:'dropdownValue'};
 }
 ngOnInit() {
      debugger
 
        
          this.getModelDropdownLiat()
  // this.getfieldnameDropdownLiat()
         this.GetLookupTablesList()
         this.getfilenamenameList()
         
       
        
  

  this.pageSettings = { pageSize: 6 };
  
}
defineColumns() {
  this.gridInstance.columns=[
    {
   field:'SourceColumn',
   headerText:'Source Column',
  },
  {
   headerText:'Target Column',
   template:this.test
  //  template:'<select><option>test</option></select>'
   // textAlign:'left',
   // width:120
  },
  
]
}
 getModelDropdownLiat()
 {
   debugger
   var userId=this.storageService.getItem(environment.storage.userId);
   var dropdowntext="Model Name"; 
   this.csvFileReadingService.GetModelNameList(userId,dropdowntext).subscribe((data:any)=>{
     debugger
     var bit=JSON.parse(localStorage.getItem("PageBit"));
        if(bit=="Edit")
        {
          this.EditMode();
          this.ModelName=data[0].dropdownText;
          this._modelList=data;
       
        }
        else{
          this.getData();
          this.ModelName=data[0].dropdownText;
          this._modelList=data;
        }
      
   })

 }
 getfieldnameDropdownLiat()
 {
    
   var userId=this.storageService.getItem(environment.storage.userId);
   var dropdowntext="Field Type"; 
   this.csvFileReadingService.GetfieldNameList(userId,dropdowntext).subscribe((data:any)=>{
       this._fieldnameList=data;
   })

 }
 getfilenamenameList()
 {
    
 var list= JSON.parse(localStorage.getItem("allFiles"))
 this.filename=list[0].name;
 for(var i=0;i<list.length;i++)
 {
  
   this._filesnameList.push({
     dropdownValue:list[i].name,
     dropdownText:list[i].name
   })
 }

 }
 GetLookupTablesList()
 {
   debugger
   var UserId=this.storageService.getItem(environment.storage.userId);
   var WorkspaceId="Use Existing";
   this.lookupvalueservice.getexistingLookupTable(UserId,WorkspaceId).subscribe((data:any)=>{
   this._gridmodeldata=data;
   
 });
}
  OnChangeTargetColumnName(event:any,indexs,source_column_name:string=null,fieldcolumntype:string=null){
    debugger
   
    this.lookup_table_name= event.itemData.dropdownText;
    var find=this._getTargetColumnNameList.find(x=>x.source_column_name==source_column_name);
    var getindexlength=this._getTargetColumnNameList.indexOf(find);
    this.looktableList.push(event.itemData.dropdownText);
    //this.fieldColumnselectedvalue.push(this.fieldColumnselectedvalue)
    this.headerList[indexs].mappingBit="yes";
    this.headerListForfilters[indexs].mappingBit="yes";
    if(getindexlength!=-1)
    {
      this._getTargetColumnNameList[getindexlength].lookup_table_name=event.itemData.dropdownText
    }
    else{
      this._getTargetColumnNameList.push({
        Index:indexs,
        source_column_name:source_column_name,
        lookup_table_name: this.lookup_table_name,
        lookup_field_name:this.fieldColumnselectedvalue[indexs],
      })

    }
    this.getexistingAndcustomtabledropdownfield(indexs,source_column_name);
    }
  OnChangeFieldName(event:any,indexs){
    debugger
    var dropdownname= event.itemData.dropdownText;
    var dropdownvalue= event.itemData.dropdownValue;
    var find=this._getTargetColumnNameList.find(x=>x.Index==indexs);
    var getindexlength=this._getTargetColumnNameList.indexOf(find);
    if(getindexlength==indexs)
    {
      this._getTargetColumnNameList[indexs].lookup_field_name=dropdownname;
      //this._getTargetColumnNameList[indexs].fieldName.dropdownvalue=dropdownvalue;
    }
  }
 changefilename(event:any){
   debugger
   this.headerList=[];
  var filenametext= event.itemData.dropdownText;
  this.csvFileReadingService.getFileData(filenametext,this.change).subscribe((data:any)=>{
    debugger
     var data=data;
      console.log(data);
       
  for(var i=0;i<data.columns.length;i++){
    this.headerList.push(
      {
        SourceColumn:data.columns[i],
        keycolumn:true,
        visibilty:false,
        mappingBit:""
         
         
       
      });
  }
      
       });

   
 }

getData(){
  debugger
  this.headerList=[];
  this.headerListForfilters=[];
  this.csvFileReadingService.getFileData(this.filename,this.change).subscribe((data:any)=>{
    debugger
     var data=data;
      console.log(data);
       
  for(var i=0;i<data.columns.length;i++){
    debugger
    this.headerList.push(
      {
        SourceColumn:data.columns[i],
        keycolumn:true,
        visibilty:false,
        mappingBit:""
        
      });
      this.headerListForfilters.push(
        {
          SourceColumn:data.columns[i],
          keycolumn:true,
          visibilty:false,
          mappingBit:""
          
        });
  }
      
       });;
  
}
getexistingAndcustomtabledropdownfield(index,sourcecolumn){
  var UserId=this.storageService.getItem(environment.storage.userId);
  var WorkspaceId="LOOKUP FIELD";
  
  this.lookupvalueservice.getexistingLookupfield(UserId,WorkspaceId).subscribe((data:any)=>{
    debugger
  this.fieldColumnselectedvalue[index]=data[index].dropdownValue;
  
  this.customAndexistingTable[index]=data;
    var find=this._getTargetColumnNameList.find(x=>x.source_column_name==sourcecolumn);
    var getindexlength=this._getTargetColumnNameList.indexOf(find);
  if(getindexlength!=-1)
  {
    this._getTargetColumnNameList[getindexlength].lookup_field_name=data[getindexlength].dropdownValue;
  }
  
  
});
}
// Usman Work   form Mapping rules component
contentmapping(){
   
  this.fileuploadwizardstyle.ConnectionTypeWizardClass="ConnectorType";
  this.fileuploadwizardstyle.TempleteWizardClass="ColumnMappingtick";
  this.fileuploadwizardstyle.SourceAccountDisable=false;
  this.router.navigate(['/extract/Uploader/contentmapping']);
}
saveMappingTemplate(){
  debugger
 if(this.mappingNewTemplateName==null){
   this.instagramConfigurationService.SaveMappingRuleTemplate(this.updatedMappingRules,this.selectedMappingTemplate).subscribe((data:string)=>{
     console.log(data);
   });
 }
 else{
   this.instagramConfigurationService.SaveMappingRuleTemplate(this.mappingRules,this.mappingNewTemplateName).subscribe((data:string)=>{
     console.log(data);
   });
 }
}
getdropdown(){
this.instagramConfigurationService.GetMapedField(this.dropdownName).subscribe((data:dropdownModel[])=>{
  this.mappingField =data;
    });
}
getMappingRules(){
this.instagramConfigurationService.getMappingRule(this.selectedMappingTemplate).subscribe((data:FieldMappingRuleTemplateModel[])=>
{
  debugger
  this.mappingRules=data;    
  this.Length=data.length;
  for(let i=0;i<this.Length;i++){
    this.showHideGridDropdown[i]=false;
    this.showHideGridButton[i]=true;
    if(this.mappingRules[i].keyColumn=='1'){
      this.mappingRules[i].keyColumn=true;
    }
    else{
      this.mappingRules[i].keyColumn =false;
    }
    
  }
  console.log(this.toggle);
  this.getdropdown();
});
}
mappingFilter(value){
 debugger
if(value=='unmapped')
{
  this.upmapped="css-xpr64j";   /// height light the  selected button 
  this.Mapped="css-b2qa21";
  this.FiltersClassessChangeOnselectedAll="css-b2qa21";
  this.headerList = this.headerListForfilters.filter(bitmapped => bitmapped.mappingBit !="yes");
  for(var i=0;i <this._getTargetColumnNameList.length;i++)
  {
   this.fieldColumnselectedvalue[i]="";
   this.lookuptableselectedvalue[i]="";
  }
}
else if(value=='Mapped'){
  this.upmapped="css-b2qa21";
  this.Mapped="css-xpr64j";
  this.FiltersClassessChangeOnselectedAll="css-b2qa21";
  this.headerList = this.headerListForfilters.filter(bitmapped => bitmapped.mappingBit == "yes");
  for(var i=0;i <this._getTargetColumnNameList.length;i++)
  {
   this.fieldColumnselectedvalue[i]=this._getTargetColumnNameList[i].lookup_field_name;
   this.lookuptableselectedvalue[i]=this._getTargetColumnNameList[i].lookup_table_name;
  }
}
else if(value=='all'){
  this.upmapped="css-b2qa21";
  this.Mapped="css-b2qa21";
  this.FiltersClassessChangeOnselectedAll="css-xpr64j";
  this.headerList = this.headerListForfilters.filter(bitmapped => bitmapped.mappingBit != "no");
  for(var i=0;i <this._getTargetColumnNameList.length;i++)
  {
    this.fieldColumnselectedvalue[i]=this._getTargetColumnNameList[i].lookup_field_name;
    this.lookuptableselectedvalue[i]=this._getTargetColumnNameList[i].lookup_table_name;
  }
}
}
 

GoForPreview(){
  debugger
   this._getTargetColumnNameList;
   localStorage.setItem("lookuptablelist",JSON.stringify(this.lookuptableselectedvalue));
   localStorage.setItem("headerlist",JSON.stringify(this.headerList));
   localStorage.setItem("ImportFile",JSON.stringify("importfile"));
   localStorage.setItem("lookuptables",JSON.stringify(this._getTargetColumnNameList));
  
    var obj={
         filename:"/run/user/1000/gvfs/smb-share:server=192.168.223.100,share=d/zoom_files_dump/"+this.filename,
         content:this._getTargetColumnNameList,
       }
     
       this.csvFileReadingService.ContentMapping(obj).subscribe((data:any)=>{
         debugger
         var msg=JSON.parse(data);
         if(msg.filename!=undefined)
         {
          var rest = msg.filename.substring(msg.filename.lastIndexOf("/") + 1);
          localStorage.setItem("previewfilename",JSON.stringify(rest));
          this.fileuploadwizardstyle.ConnectionTypeWizardClass="ConnectorType";
          this.fileuploadwizardstyle.TempleteWizardClass="ColumnMappingtick";
          this.fileuploadwizardstyle.SourceAccountDisable=false;
          this.fileuploadwizardstyle.SourceAccountClass="contentmappingtick";
          this.fileuploadwizardstyle.ApplyTransformationDisabled=false;
          this.router.navigate(['/extract/Uploader/scriptlist']);
         }
         else {
           alert("Something went Wrong");
         }
       
       })
  
}
Columnmapping(){
  localStorage.setItem("PageBit",JSON.stringify("Edit"));
  this.fileuploadwizardstyle.ApplyTransformationDisabled=true;
  this.fileuploadwizardstyle.FilePreviewDisabled=true;
  this.fileuploadwizardstyle.SourceAccountDisable=true;
  this.fileuploadwizardstyle.TempleteWizardClass="ColumnMapping";
  this.router.navigate(['/extract/Uploader/columnmapping']);
}

EditMode(){
  debugger
  this.lookuptableselectedvalue=[];
  this.looktableList= JSON.parse(localStorage.getItem("lookuptablelist"));
  this.headerList= JSON.parse(localStorage.getItem("headerlist"));
  this.headerListForfilters= JSON.parse(localStorage.getItem("headerlist"));
  this._getTargetColumnNameList=JSON.parse(localStorage.getItem("lookuptables"));
  for(var i=0;i<this._getTargetColumnNameList.length;i++)
  {
    this.lookuptableselectedvalue[this._getTargetColumnNameList[i].Index]=this._getTargetColumnNameList[i].lookup_table_name;
  }
   
}

}
