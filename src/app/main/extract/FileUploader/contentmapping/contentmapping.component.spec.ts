import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentmappingComponent } from './contentmapping.component';

describe('ContentmappingComponent', () => {
  let component: ContentmappingComponent;
  let fixture: ComponentFixture<ContentmappingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentmappingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentmappingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
