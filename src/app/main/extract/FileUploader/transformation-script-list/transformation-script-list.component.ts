import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ToastComponent } from '@syncfusion/ej2-angular-notifications';
import { ButtonComponent } from 'ej-angular2';
import * as FileSaver from 'file-saver';
import { ScriptDetails } from 'src/app/models/extract/ExtractDataModel';
import { ToastMessage } from 'src/app/models/MessageTypes/toast-message';
import { AppComponentBase } from 'src/app/services/AppComponentBase';
import { DynamicEnrichScriptsService } from 'src/app/services/Enrich/dynamic-enrich-scripts.service';
import { fileuploaderwizrad } from 'src/app/services/extract/FileuploaderServices/fileuploadwizzardstyleservices';
import { Transformation_Mapping_Loading_Using_Auto_ETL } from 'src/app/services/extract/JsonFor_Transformation_mapping_Loading';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-transformation-script-list',
  templateUrl: './transformation-script-list.component.html',
  styleUrls: ['./transformation-script-list.component.css']
})
export class TransformationScriptListComponent extends AppComponentBase implements OnInit {
  //for toast
  public testToast=new ToastMessage();
  @ViewChild('defaulttoast',{static:true})
  public toastObj: ToastComponent;
  @ViewChild('toastBtnShow',{static:true})
  public btnEleShow: ButtonComponent;
  public position=this.testToast.position;
  // toast end
  GetScriptList:any[]=[];
  totalLenght:number=0;
  allcheckbox:boolean=false;
  singleCheckbox:boolean=false;
  _tablecounter:number=0;
  MessageStatusForSHowHide=false;
  Message:string;
  saveUsername:boolean;
  Transformationfunction:any[]=[];
  transformationfunctionSelection:any[]=[];
  _functionsdetailsList:ScriptDetails[]=[];
  JsonScriptForTransformation:any;
  constructor(public fileuploadwizardstyle:fileuploaderwizrad,
    private dynamicEnrichScriptsService:DynamicEnrichScriptsService,
    private router: Router,
    private transformation_Mapping_Loading_Using_Auto_ETL_services:Transformation_Mapping_Loading_Using_Auto_ETL,injector : Injector) {super(injector) }

  ngOnInit() {
    this.getScriptList();
  }
 getScriptList()
 {
   this.dynamicEnrichScriptsService.getScriptList().subscribe((data:any)=>{
     debugger
     this.GetScriptList=JSON.parse(data);
     this.totalLenght=this.GetScriptList.length;

   })

 }
 checkAll(ev) {
   debugger
  this.GetScriptList.forEach(x => x.state = ev.target.checked)
  
}
findCount(e,scriptId:string,scriptName:string) {
  debugger
   if(e.target.checked==true)
     {
      this._functionsdetailsList.push({
       
        scriptId:scriptId,
      })
      this._tablecounter+=1;
     }
     else {
      var find=this._functionsdetailsList.find(x=>x.scriptId==scriptId);
      var RemoveIndex=this._functionsdetailsList.indexOf(find);
      this._functionsdetailsList.splice(RemoveIndex,1);
      this._tablecounter-=1;
      }
     
     
  
   
 }
 functionCount(e){
  debugger
  if(e.target.checked==true)
  {
    this.allcheckbox=true;
    this._tablecounter= this.GetScriptList.length;
    for(let i=0;i<this.GetScriptList.length;i++){
      
     this.findCount(e.target.checked,this.GetScriptList[i].scriptId,this.GetScriptList[i].scriptName)
      }      
  }
  else{
    this.allcheckbox=false;
    this._tablecounter=0;
    for(let i=0;i<this.GetScriptList.length;i++){
      
     this.findCount(e.target.checked,this.GetScriptList[i].scriptId,this.GetScriptList[i].scriptName)
      }
  }
 }
 GetFunctionId(Id:string)
 {
  localStorage.setItem("Script_Id",JSON.stringify(Id));
  this.router.navigate(['/extract/source/addNewScript']);
 }
 Addnew()
 {
  localStorage.removeItem("Script_Id");
  localStorage.setItem("ImportFile",JSON.stringify("importmode"));
  this.router.navigate(['/extract/Uploader/addNewScript']);
 }

 GetscriptJson()
 {
   debugger
   this.transformationfunctionSelection=[];
   var functionIds_lenght=this._functionsdetailsList.length;
   if(this._functionsdetailsList.length>0)
   {
     this.MessageStatusForSHowHide=false;
    var userid=this.storageService.getItem(environment.storage.userId);
    var workspaceid=this.storageService.getItem(environment.storage.workspaceId);
    var clientId=this.storageService.getItem(environment.storage.clientId);
    //var _Id=this._functionsdetailsList[0].scriptId;
    for(let c=0;c<this._functionsdetailsList.length;c++)
    {
      this.dynamicEnrichScriptsService.getEnrichmentscriptJosnFormMongo(userid,workspaceid,clientId,this._functionsdetailsList[c].scriptId).subscribe((data:any)=>{
        debugger
        this.JsonScriptForTransformation=JSON.parse(data);
        this.transformationfunctionSelection.push(this.JsonScriptForTransformation.length)
        for(var l=0;l<this.JsonScriptForTransformation.ScriptList.length;l++)
        {
          this.Transformationfunction.push(this.JsonScriptForTransformation.ScriptList[l])
        }
          
         if(functionIds_lenght==this.transformationfunctionSelection.length)
        {
          let file_name=JSON.parse(localStorage.getItem("previewfilename"));
          let account_id=JSON.parse(localStorage.getItem("accountId"));
          let sourcename=localStorage.getItem("Connectortitle");
           var userInfo={"SOURCE_STG_ID":"","SOURCE_STG_NAME":"Umar Farooq","SOURCE_NAME":sourcename,"WORKSPACE_NAME":""}
          var obj={
            
            "account_id":"0000",
            "filename":"/run/user/1000/gvfs/smb-share:server=192.168.223.100,share=d/zoom_files_dump/"+file_name,
            "scriptList":this.Transformationfunction,
            "userInfo":userInfo
          }
          
         
        this.transformation_Mapping_Loading_Using_Auto_ETL_services.ApplyTransformationOnFile(obj).subscribe((data:any)=>{
            debugger
            var msg=data;
            if(msg.filename!=undefined)
            {
              var rest = msg.filename.substring(msg.filename.lastIndexOf("/") + 1);
              localStorage.setItem("previewfilename",JSON.stringify(rest));
              this.fileuploadwizardstyle.ConnectionTypeWizardClass="ConnectorType";
              this.fileuploadwizardstyle.TempleteWizardClass="ColumnMappingtick";
              this.fileuploadwizardstyle.SourceAccountDisable=false;
              this.fileuploadwizardstyle.SourceAccountClass="contentmappingtick";
              this.fileuploadwizardstyle.ApplyTransformationDisabled=false;
              this.fileuploadwizardstyle.ApplyTransformationClass="applytransformationtick";
              this.fileuploadwizardstyle.FilePreviewDisabled=false
              this.router.navigate(['/extract/Uploader/preview']);
            }
            else
            {
              alert("Somthing went wrong")
            }
            
        })
    }
   
      })
    }
    
    
   
   }
   else
   {
    //this.MessageStatusForSHowHide=true;
    this.Message="Select the function for transformation";
    this.testToast.toast[2].content=this.Message;
    this.toastObj.show(this.testToast.toast[2]);
   }
  
 }
 contentmapping()
 {
  localStorage.setItem("PageBit",JSON.stringify("Edit"));
  this.fileuploadwizardstyle.ConnectionTypeWizardClass="ConnectorType";
  this.fileuploadwizardstyle.TempleteWizardClass="ColumnMappingtick";
  this.fileuploadwizardstyle.SourceAccountDisable=false;
  this.fileuploadwizardstyle.SourceAccountClass="contentmapping";
  this.fileuploadwizardstyle.ApplyTransformationDisabled=true;
  this.fileuploadwizardstyle.FilePreviewDisabled=true;
  this.router.navigate(['/extract/Uploader/contentmapping']);

 }
 FilePreview()
 {
  this.fileuploadwizardstyle.ConnectionTypeWizardClass="ConnectorType";
  this.fileuploadwizardstyle.TempleteWizardClass="ColumnMappingtick";
  this.fileuploadwizardstyle.SourceAccountDisable=false;
  this.fileuploadwizardstyle.SourceAccountClass="contentmappingtick";
  this.fileuploadwizardstyle.ApplyTransformationDisabled=false;
  this.fileuploadwizardstyle.ApplyTransformationClass="applytransformationtick";
  this.fileuploadwizardstyle.FilePreviewDisabled=false
  this.router.navigate(['/extract/Uploader/preview']);
 }

 DeleteScript(scriptId:string){
  debugger
 
 this.dynamicEnrichScriptsService.DeleteScriptList(scriptId).subscribe((data:any)=>{
   debugger
    
   this.getScriptList();
   // this.testToast.toast[0].content=data;
   // this.toastObj.show(this.testToast.toast[0])

    
 })

}
}
