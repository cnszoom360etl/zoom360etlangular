import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransformationScriptListComponent } from './transformation-script-list.component';

describe('TransformationScriptListComponent', () => {
  let component: TransformationScriptListComponent;
  let fixture: ComponentFixture<TransformationScriptListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransformationScriptListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransformationScriptListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
