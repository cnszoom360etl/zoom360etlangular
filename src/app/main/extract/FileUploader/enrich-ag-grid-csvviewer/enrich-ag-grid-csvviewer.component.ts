import { ThrowStmt } from '@angular/compiler';
import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AgGridAngular } from 'ag-grid-angular';
import { AppComponentBase } from 'src/app/services/AppComponentBase';
 
 
import { CSVFileReadingService } from 'src/app/services/csvfile-reading.service';
import { fileuploaderwizrad } from 'src/app/services/extract/FileuploaderServices/fileuploadwizzardstyleservices';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-enrich-ag-grid-csvviewer',
  templateUrl: './enrich-ag-grid-csvviewer.component.html',
  styleUrls: ['./enrich-ag-grid-csvviewer.component.css']
})
export class EnrichAgGridCSVViewerComponent extends AppComponentBase {
  @ViewChild('agGrid',{static:true}) agGrid: AgGridAngular;
  private gridApi;
  private gridColumnApi;
  public columnDefs=[];
  public defaultColDef;
  public rowData:any[]=[];
  public sideBar;
  public rowGroupPanelShow;
  public rowSelection;
  public enableRowGroup;
  public rowClassRules;
  public pivotPanelShow;
  public statusBar;
  public skipHeaderOnAutoSize;
  public chartThemeOverrides;
  public headerHeight;
  public rowHeight;
  public rowStyle;
  public propertyNames:any[]=[];
  constructor(private csvFileReadingService: CSVFileReadingService,public addNewConnectoinWizardStyle:fileuploaderwizrad,private router: Router,injector : Injector) {
    super(injector)
    this.defaultColDef = {
      minWidth: 150,
      maxWidth: 300,
      filter: true,
      resizable: true,
      sortable: true,
      enablePivot: true,
      enableValue: true,
      autoBind:false,
      editable: true
     // floatingFilter: true,
    };
   
    // this.rowStyle = { background: 'black' };
    this.sideBar = {
      toolPanels: [
          {
              id: 'columns',
              labelDefault: 'Columns',
              labelKey: 'columns',
              iconKey: 'columns',
              toolPanel: 'agColumnsToolPanel',
          },
          {
              id: 'filters',
              labelDefault: 'Filters',
              labelKey: 'filters',
              iconKey: 'filter',
              toolPanel: 'agFiltersToolPanel',
          }
      ],
      position: 'Right',
      defaultToolPanel: '',
  }
  this.statusBar = {
    statusPanels: [
      {
        statusPanel: 'agTotalAndFilteredRowCountComponent',
        align: 'left',
      },
      { statusPanel: 'agFilteredRowCountComponent' },
      { statusPanel: 'agSelectedRowCountComponent' },
      { statusPanel: 'agAggregationComponent',
      statusPanelParams: {
        // possible values are: 'count', 'sum', 'min', 'max', 'avg'
        aggFuncs: ['count', 'sum', 'min', 'max', 'avg']
    } 
  },
    ],
  };
    this.chartThemeOverrides = {
      common: {
        title: {
          enabled: true,
          text: 'Digital Sales Analysis',
        },
        legend: { position: 'bottom' },
      },
      column: { axes: { category: { label: { rotation: 0 } } } },
    };
    this.rowGroupPanelShow = 'always';
    this.rowSelection = 'multiple';
    this.enableRowGroup=true;
    this.pivotPanelShow = 'always';
    this.skipHeaderOnAutoSize=true
    this.headerHeight=40;
    this.rowHeight = 40;
    
   }

  ngOnInit() {
        

    }
  onGridReady(params){
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    // var name=JSON.parse(localStorage.getItem("FileName"));
    var name=JSON.parse(localStorage.getItem("previewfilename"));
    this.getData(name);
//this.GetData(2,1002);
  }
  getData(name:string){
    debugger
    this.agGrid.api.showLoadingOverlay();
    this.csvFileReadingService.getFileDatas(name).subscribe((data:any) => {
      debugger
      var test=JSON.parse(data);
      this.propertyNames = Object.keys(test[0]);
      let xyz=[];
    for(let i=0;i<this.propertyNames.length;i++){
      xyz.push(
    { headerName: this.propertyNames[i],
     field: this.propertyNames[i],
     cellClass: 'font-style',
     suppressSizeToFit:true,
     filter: 'agMultiColumnFilter',
     filterParams: {
       buttons: ['clear', 'apply'],
     },
     enableRowGroup:true,
     cellStyle: {'font-size':'12px','font-weight':'400'}
   });
   
  }
  //Header Definations
  // xyz.forEach(function (colDef, index) {
  //   colDef.headerName = abc[index].columntext;
  // });
   this.columnDefs=xyz;
  this.gridApi.setColumnDefs(this.columnDefs);
  //Apply States
  this.rowData = test;  
  });
  }
  save(upload:string){
     debugger
    console.log(this.rowData);
    var tablename=this.storageService.getItem(environment.storage.tablename);
    var Loadoption=this.storageService.getItem(environment.storage.loadoption);
   
    var obj={
      "destination_name":tablename,
      "content":this.rowData,
      "destination":upload,
      "insert":Loadoption,
      "userInfo":{
        "SOURCE_STG_ID":"",
        "SOURCE_STG_NAME":"",
        "SOURCE_NAME":tablename,
        "WORKSPACE_NAME":"1"
        }
      }
      this.csvFileReadingService.sentCSVFileData(obj).subscribe((tablename:any)=>{
        console.log(tablename);
         
      })
   }
   scriptlist()
   {
     localStorage.setItem("PageBit",JSON.stringify("Edit"));
     this.addNewConnectoinWizardStyle.FilePreviewDisabled=true;
     this.addNewConnectoinWizardStyle.ApplyTransformationClass="applytransformation";
     this.router.navigate(['/extract/Uploader/scriptlist']);

   }
}
