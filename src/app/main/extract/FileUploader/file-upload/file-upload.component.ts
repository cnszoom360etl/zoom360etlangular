import { Component, Injector, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { EmitType, detach } from '@syncfusion/ej2-base';
import { UploaderComponent, SelectedEventArgs, FileInfo, RemovingEventArgs, UploadingEventArgs } from '@syncfusion/ej2-angular-inputs';
import { Router } from '@angular/router';
import { fileuploaderwizrad } from 'src/app/services/extract/FileuploaderServices/fileuploadwizzardstyleservices';
import { environment } from 'src/environments/environment';
import { AppComponentBase } from 'src/app/services/AppComponentBase';
import { inject } from '@angular/core/testing';
 
 
 

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class FileUploadComponent extends AppComponentBase {
  HidetheNextButton:boolean=false;
  counter:number=0;
  filenamesequence:string="";
  filename:string="";
  SkipsRowsNumber:number=0;
  allFiles : FileInfo[];
  SkpiRowsfromabovethe=false;
  SetCustomTableName=false;
  SetExistingTableName=true;
  SkipsRowsfromtheheader:number=0;
  customTableName:string="";
  SheetName:string="";
  loadoption:string="replace";
  Existingtablename:string="test";
  SkipsRowsfromtheheadertoggle=true;
  LoadDatatoggle=true;
  tabletype:string=null;
    _bit:string="";
  @ViewChild('UploadFiles',{static:true})
  public uploadObj: UploaderComponent;

  public path: Object = {
      saveUrl:environment.apiUrl+"/api/DocFilesUpload/Save",
      removeUrl: 'https://ej2.syncfusion.com/services/api/uploadbox/Remove',
      };

  public dropElement: HTMLElement = document.getElementsByClassName('control-fluid')[0] as HTMLElement;

  public allowExtensions: string = '.xlsx ,.csv';
  constructor(public fileuploadwizardstyle:fileuploaderwizrad,private router: Router,  injector : Injector) {
  super(injector)
    this.fileuploadwizardstyle.FilePreviewDisabled=true;
}
ngOnInit() {
   debugger
  this._bit=JSON.parse(localStorage.getItem("PageBit"));
  if(this._bit=="Edit")
  {
    debugger
    //  this.SkipsRowsNumber=JSON.parse(localStorage.getItem("SkipRows"));
    
     this.SkipsRowsNumber=this.storageService.getItem(environment.storage.SkipRows);
     this.SkipsRowsfromtheheadertoggle=JSON.parse(localStorage.getItem("first_row_have_header"));
    this.LoadDatatoggle= this.storageService.getItem(environment.storage.ExistAndCustomtableToggle);
     if(this.SkipsRowsfromtheheadertoggle==true)
     {
      this.SkpiRowsfromabovethe=false;
     }
     else{
      this.SkpiRowsfromabovethe=true;
      // this.SkipsRowsfromtheheader=JSON.parse(localStorage.getItem("remove_row_above_header"));
      this.SkipsRowsfromtheheader=this.storageService.getItem(environment.storage.remove_row_above_header);

     }
     if(this.LoadDatatoggle==true)
     {
       this.Existingtablename="test";
       this.SetExistingTableName=true;
       this.SetCustomTableName=false;
       this.customTableName="";
     }
     else{
       this.customTableName=this.storageService.getItem(environment.storage.tablename);
       this.SetExistingTableName=false;
       this.SetCustomTableName=true;
     }
     this.loadoption=this.storageService.getItem(environment.storage.loadoption);
     this.SheetName=this.storageService.getItem(environment.storage.sheetname);
      
     
  }
  else {
    this.SkipsRowsfromtheheader=0;
    this.SkipsRowsfromtheheadertoggle=true;
    this.SkipsRowsNumber=0;
  }
     
   }
  public onSelected(args : SelectedEventArgs) : void {
      debugger
       
      args.filesData.splice(20);
      
      //  this.filenamesequence+this.counterthis.filename;
      var obj={
          
      }
      let filesData : FileInfo[] = this.uploadObj.getFilesData();
      
      this.allFiles= filesData.concat(args.filesData);
      localStorage.setItem("allFiles",JSON.stringify(this.allFiles))
      
      if (this.allFiles.length > 20) {
          for (let i : number = 0; i < this.allFiles.length; i++) {
              if (this.allFiles.length > 20) {
                this.allFiles.shift();
              }
          }
          args.filesData = this.allFiles;
          args.modifiedFilesData = args.filesData;
      }
      args.isModified = true;
  }

  public onFileRemove(args: RemovingEventArgs): void {
    debugger
   var getrow=args.filesData.splice(20);
   var findfilename=args.filesData[0].name;

    for(var i=0;i<this.allFiles.length;i++)
    {

      var find=this.allFiles.find(x=>x.name==findfilename);
      var RemoveIndex=this.allFiles.indexOf(find);
      this.allFiles.splice(RemoveIndex,1);
    }
      args.postRawFile = false;
      localStorage.setItem("allFiles",JSON.stringify(this.allFiles))
  }


  public onUploadBegin(args: UploadingEventArgs) {
    
}
onUploadStatus(args) { 
  debugger
   
  args.statusText=args.response.statusText;
  this.HidetheNextButton=true;
}
 next(){
   debugger
  // localStorage.setItem("SkipRows",JSON.stringify(this.SkipsRowsNumber));
  localStorage.setItem("first_row_have_header",JSON.stringify(this.SkipsRowsfromtheheadertoggle));
  // localStorage.setItem("remove_row_above_header",JSON.stringify(this.SkipsRowsfromtheheader));
  if(this.LoadDatatoggle==true)
  {
    this.storageService.setItem(environment.storage.tablename,this.Existingtablename);
  }
  else
  {
    this.storageService.setItem(environment.storage.tablename,this.customTableName);
  }
  this.storageService.setItem(environment.storage.SkipRows,this.SkipsRowsNumber);
  this.storageService.setItem(environment.storage.remove_row_above_header,this.SkipsRowsfromtheheader);
  this.storageService.setItem(environment.storage.tabletype,this.tabletype);

  this.storageService.setItem(environment.storage.sheetname,this.SheetName);
  this.storageService.setItem(environment.storage.loadoption,this.loadoption);
  this.storageService.setItem(environment.storage.ExistAndCustomtableToggle,this.LoadDatatoggle);
  this.fileuploadwizardstyle.ConnectionTypeWizardClass="ConnectorType";
  this.fileuploadwizardstyle.TempleteAccountWizard=false;
  this.fileuploadwizardstyle.TempleteWizardClass="ColumnMapping"
  localStorage.removeItem("columnmappingList");
  localStorage.removeItem("targetcolumnlist");
  localStorage.removeItem("fieldtypelist");
  localStorage.removeItem("targetcolumnslist");
  localStorage.removeItem("PageBit");
  this.router.navigate(['/extract/Uploader/columnmapping']);
 }
 chnageheaderstate(event){
   debugger
if(event.target.checked==false)
  {
  this.SkpiRowsfromabovethe=true;
  }
  else {
    this.SkpiRowsfromabovethe=false;
  }
 }
 chnageDestinationForLoadData(event){
  debugger
  if(event.target.checked==false)
    {
    this.SetExistingTableName=false;
    this.SetCustomTableName=true;
    this.tabletype="custom";

    }
    else {
    this.SetExistingTableName=true;
    this.SetCustomTableName=false;
    this.tabletype="existing";
    }
 }
 GetValue(e:any)
 {
   debugger
   this.loadoption=e.target.value;
 }
}
