import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AddNewConnectoinWizardStyle } from 'src/app/services/extract/AddNewConnectionWizardStyle';
import { fileuploaderwizrad } from 'src/app/services/extract/FileuploaderServices/fileuploadwizzardstyleservices';

@Component({
  selector: 'app-left-status-bar',
  templateUrl: './left-status-bar.component.html',
  styleUrls: ['./left-status-bar.component.css']
})
export class LeftStatusBarComponent implements OnInit {

  constructor(public addNewConnectoinWizardStyle:fileuploaderwizrad,private router: Router,public ectoinWizardStyle:AddNewConnectoinWizardStyle) { }

  ngOnInit(): void {
    debugger
    let path = window.location.href;
    var rest = path.substring(path.lastIndexOf("/") + 1);
    this.checkUrlComponentName(rest);
  }
  checkUrlComponentName(rest){
    debugger
    
     if(rest=="columnmapping"){
      this.router.navigate(['/extract/Uploader/columnmapping']);
      this.addNewConnectoinWizardStyle.ConnectionTypeWizardClass="ConnectorType";
      this.addNewConnectoinWizardStyle.TempleteAccountWizard=false;
      this.addNewConnectoinWizardStyle.SourceAccountDisable=true;
      // this.WizardControlsService.SourceAccountClass="css-skkt0a";

    }
    else if(rest=="FileUpload"){
      this.router.navigate(['/extract/Uploader/FileUpload']);
      this.addNewConnectoinWizardStyle.ConnectionTypeWizardClass="css-fileupload"
      this.addNewConnectoinWizardStyle.TempleteAccountWizard=true;
      this.addNewConnectoinWizardStyle.SourceAccountDisable=true;
      // this.WizardControlsService.TempleteWizardClass="css-Templatewizard";
    }
    else if(rest=="contentmapping"){
      this.router.navigate(['/extract/Uploader/contentmapping']);
      this.addNewConnectoinWizardStyle.ConnectionTypeWizardClass="ConnectorType";
      this.addNewConnectoinWizardStyle.TempleteAccountWizard=false;
       this.addNewConnectoinWizardStyle.TempleteWizardClass="ColumnMappingtick";
       this.addNewConnectoinWizardStyle.SourceAccountDisable=false;
      this.addNewConnectoinWizardStyle.SourceAccountClass="contentmapping";
      this.addNewConnectoinWizardStyle.ApplyTransformationDisabled=true;
      this.addNewConnectoinWizardStyle.FilePreviewDisabled=true;
      // this.addNewConnectoinWizardStyle.ConfiguureWizard=false;
    }
    else if(rest=="preview"){
      this.router.navigate(['/extract/Uploader/preview']);
      this.addNewConnectoinWizardStyle.ConnectionTypeWizardClass="ConnectorType";
      this.addNewConnectoinWizardStyle.TempleteAccountWizard=false;
       this.addNewConnectoinWizardStyle.TempleteWizardClass="ColumnMappingtick";
       this.addNewConnectoinWizardStyle.SourceAccountDisable=false;
      this.addNewConnectoinWizardStyle.SourceAccountClass="contentmappingtick";
      this.addNewConnectoinWizardStyle.ApplyTransformationDisabled=false;
      this.addNewConnectoinWizardStyle.ApplyTransformationClass="FilePreviewtick";
      this.addNewConnectoinWizardStyle.FilePreviewDisabled=true;
    }
    else if(rest=="scriptlist"){
      this.router.navigate(['/extract/Uploader/scriptlist']);
      this.addNewConnectoinWizardStyle.ConnectionTypeWizardClass="ConnectorType";
      this.addNewConnectoinWizardStyle.TempleteAccountWizard=false;
       this.addNewConnectoinWizardStyle.TempleteWizardClass="ColumnMappingtick";
       this.addNewConnectoinWizardStyle.SourceAccountDisable=false;
      this.addNewConnectoinWizardStyle.SourceAccountClass="contentmappingtick";
      this.addNewConnectoinWizardStyle.ApplyTransformationDisabled=false;
      this.addNewConnectoinWizardStyle.FilePreviewDisabled=true;
    }
  }

}
