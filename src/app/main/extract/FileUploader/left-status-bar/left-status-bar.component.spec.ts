import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeftStatusBarComponent } from './left-status-bar.component';

describe('LeftStatusBarComponent', () => {
  let component: LeftStatusBarComponent;
  let fixture: ComponentFixture<LeftStatusBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeftStatusBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeftStatusBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
