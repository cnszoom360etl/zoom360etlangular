
import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AnimationModel, ProgressBar } from '@syncfusion/ej2-angular-progressbar';

import { filtersModel } from 'src/app/models/extract/filter.model';
import { AppComponentBase } from 'src/app/services/AppComponentBase';
import { FiltersService } from 'src/app/Services/common/filters.service';
import { googleoauth } from 'src/app/services/Enrich/googleOauthToken';
import { ExtractCountService } from 'src/app/services/extract-count.service';
import { ExtractStatusComponent } from 'src/app/widget/extract-status/extract-status.component';

import { WedgetAllExtractComponent } from 'src/app/widget/wedget-all-extract/wedget-all-extract.component';
import { WedgetAllissuesComponent } from 'src/app/widget/wedget-allissues/wedget-allissues.component';
import { WedgetEnrichLogComponent } from 'src/app/widget/wedget-enrich-log/wedget-enrich-log.component';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent extends AppComponentBase {
  @ViewChild('connectionLogSummery',{static:true}) public connectionLogSummery: ExtractStatusComponent;
  @ViewChild('extractLogSummery',{static:true}) public extractLogSummery: WedgetAllExtractComponent;
  @ViewChild('issueLogSummery',{static:true}) public issueLogSummery: WedgetAllissuesComponent;
  @ViewChild('enrichLogSummery',{static:true}) public enrichLogSummery: WedgetEnrichLogComponent;
  
  allcheckbox: boolean=false;
  checkbox:boolean=false;
  screenName:string='Summary';
  limit:string = 'All';
  workspaceName:string='all';
  connectionName:string='all';
  sourceName:string='all';
  accessGranted:string='all';
  createdBy:string='all';
  isActive:string='all';
  lastAccessed:string='all';
  destinationEnabled:string='all';
  filterValuesModel={} as filtersModel;
  
  // filterToggle:boolean=false;
  // showfilterText:string='Show Filter';
  // selectedFilterType:string;
  // SelectButtonGroup:string;
  // items:ItemModel[]=[
  //   {text:'Basic'},
  //   {text:'Advance'},
  //   {text:'Customize'},
  //   {text:'Hide all'}
  // ];

  getstatus=environment.apiUrl+'/api/DynamicEnrichScript/ETLStatus';
  interval:NodeJS.Timer;
  ExtractStatus:number=0;
  TransformStatus:number=0;
  LoadStatus:number=0;
  progressColor:string='#219B9C';
  showProgressValue:boolean=true;
  public type1: string = 'Linear';
  public width: string = '100%';
  public height1: string = '20';
  public trackThickness1: number = 4;
  public progressThickness1: number = 4;
  public min1: number = 0;
  public max1: number = 100;
  ///
  public type2: string = 'Linear';
  public height2: string = '20';
  public trackThickness2: number = 4;
  public progressThickness2: number = 4;
  public min2: number = 0;
  public max2: number = 100;
  ///
  public type3: string = 'Linear';
  public height3: string = '20';
  public trackThickness3: number = 4;
  public progressThickness3: number = 4;
  public min3: number = 0;
  public max3: number = 100;
  ///
  public type4: string = 'Linear';
  public height4: string = '20';
  public trackThickness4: number = 4;
  public progressThickness4: number = 4;
  public min4: number = 0;
  public max4: number = 100;
  //circular
    public Cheight: string = '40';
    public Cwidth: string = '40';
    public isIndeterminate3: boolean = true;

    public Ctype3: string = 'Circular';
    public Cmin3: number = 0;
    public Cmax3: number = 100;
    public Cvalue3: number = 20;
   

    public Ctype1: string = 'Circular';
    public Cmin1: number = 0;
    public Cmax1: number = 100;
    public Cvalue1: number = 20;

    public Ctype2: string = 'Circular';
    public Cmin2: number = 0;
    public Cmax2: number = 100;
    public Cvalue2: number = 20;

    public Ctype4: string = 'Circular';
    public Cmin4: number = 0;
    public Cmax4: number = 100;
    public Cvalue4: number = 20;
  // public value2: number = 100;
//   public secondaryProgress2: number = 62;
   public animation: AnimationModel = { enable: true, duration: 1000, delay: 0 };

//linear
  @ViewChild('linear1',{static:true})
  public linear1: ProgressBar;
  @ViewChild('linear2',{static:true})
  public linear2: ProgressBar;
  @ViewChild('linear3',{static:true})
  public linear3: ProgressBar;
  @ViewChild('linear4',{static:true})
  public linear4: ProgressBar;

 //Circular
 @ViewChild('circular1',{static:true})
  public circular1: ProgressBar;
  @ViewChild('circular2',{static:true})
  public circular2: ProgressBar;
  @ViewChild('circular4',{static:true})
  public circular4: ProgressBar;

  constructor(private Googleoauth:googleoauth,private router: Router,
    public _filter:FiltersService ,public _SummaryCounter:ExtractCountService,injector : Injector)
    {super(injector) }

  ngOnInit(): void {
    localStorage.removeItem("sourceaccount_Id");
    this._SummaryCounter.extractCountModel={
      extractionCompleteCount:0,
      extractionTotalCount:0,
      enrichmentCompleteCount:0,
      enrichmentTotalCount:0,
      loadCompleteCount:0,
      loadTotalCount:0
      }
  //this.AllExtractService.accountId='all';
  // this.getAllExtract(this.filterValuesModel);
  
  // this.getStatus();
 
  debugger
  let searchParams = new URLSearchParams(window.location.search);
    if (searchParams.has('code')) {
        var code = searchParams.get('code');
        this.Googleoauth.GetToken(code);
    }
  }
  // getAllExtract(filtersValue:filtersModel){
  //   this.AllExtractService.getAllextract(filtersValue.workspaceName,filtersValue.connectionName,filtersValue.sourceName,filtersValue.accessGranted
  //     ,filtersValue.createdBy,filtersValue.isActive,filtersValue.lastAccessed,filtersValue.destinationEnabled,5);
  // }
  
//   getStatus(){
//     this.interval = setInterval(() => {
//   this._http.get(`${this.getstatus}`).subscribe((data:any)=>{
//     var data=JSON.parse(data);
//       this.ExtractStatus=data[0].Extract;
//       this.TransformStatus=data[0].Transform;
//       this.LoadStatus=data[0].Load;
//       this.linear1.refresh();
//       this.linear2.refresh();
//       this.linear3.refresh();
//       this.linear4.refresh();
//   });
// }, 2000);
// }
  // showFilters(){
    
  //   this.filterToggle=!this.filterToggle;
  //   if(this.filterToggle==false){
  //     this.showfilterText='Show Filter';
  //   }
  //   else{
  //     this.showfilterText='Hide Filter';
  //   }
    
  // }
   
  ImportFile()
  {
    var add="Add";
  
   localStorage.setItem("PageBit",JSON.stringify(add));
   this.router.navigate(['/extract/Uploader/FileUpload']);
  }
  AddnewConnector()
  {

   this.storageService.setItem(environment.storage.ConnectorHeadingForNewAndUpdate,"Add New Connection");
   this.router.navigate(['/extract/AddNewConnection/ConnectorType']);
  }
  // reloadPage(){
  //   this.appearanceLogo_arrow=false;
  //   this.workspaceName_arrow=false;
  //   this.connectorName_arrow=false;
  //   this.bstatus_toggle=false;
  //   this.getAllExtract(this.filterValuesModel);
  //  }
   
  /// Filter Show From Aneeb 
//   showFilters(va){
//     debugger
 
//     if(va.item.properties.text=='Basic'){
//       this._filter.BasicFilterToggle=true;
//       this._filter.AdvanceFilterToggle=false;
//       this._filter.CustomizeFilterToggle=false;
//     }
//     else if(va.item.properties.text=='Advance'){
//       this._filter.BasicFilterToggle=true;
//       this._filter.AdvanceFilterToggle=true;
//       this._filter.CustomizeFilterToggle=false;
//     }
//     else if(va.item.properties.text=='Customize'){
//       this._filter.BasicFilterToggle=true;
//       this._filter.AdvanceFilterToggle=true;
//       this._filter.CustomizeFilterToggle=true;
    
//   }
//   else if(va.item.properties.text=='Hide all')
//   {
//     this._filter.BasicFilterToggle=false;
//     this._filter.AdvanceFilterToggle=false;
//     this._filter.CustomizeFilterToggle=false;
//   }
// }
// selected(event){
//     const element = document.getElementsByClassName('css-1fzqn28');
// for (var i = 0; i < element.length; i++) {
//   element[i].classList.remove('css-1fzqn28');
//      }
//    var ele2= document.getElementById(event) as HTMLInputElement;
//    ele2.classList.add('css-1fzqn28');
// }
getData(filtersValue:filtersModel){
  debugger
  this._SummaryCounter.getextractionCount(filtersValue);
  this.extractLogSummery.getallExtractsSummery(filtersValue);
  this.issueLogSummery.getAllIssues(filtersValue);
  this.enrichLogSummery.getEnrichLog(filtersValue);
  this.connectionLogSummery.GetRecentModule(filtersValue);
}

}
