import { Component, OnInit,Input, Output, EventEmitter, ViewEncapsulation, ViewChild, Injector } from '@angular/core';
import { UploaderComponent, SelectedEventArgs, FileInfo, RemovingEventArgs, UploadingEventArgs } from '@syncfusion/ej2-angular-inputs';
import { HttpEventType, HttpClient } from '@angular/common/http';
import { FileUploadService } from 'src/app/services/extract/FilesValidations';
import { SourceAccountSettup } from 'src/app/services/extract/AddNewConnectionServices/SourceAccountSettup';
import { FileUpload } from 'src/app/services/extract/AddNewConnectionServices/fileupload';
import { googleoauth } from 'src/app/services/Enrich/googleOauthToken';
import { Router } from '@angular/router';
import { EmitType } from '@syncfusion/ej2-base';
import { environment } from 'src/environments/environment';
import { AppComponentBase } from 'src/app/services/AppComponentBase';
@Component({
  selector: 'app-files-uploade',
  templateUrl: './files-uploade.component.html',
  styleUrls: ['./files-uploade.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class FilesUploadeComponent extends AppComponentBase implements OnInit {
  @Output() public onUploadFinished = new EventEmitter();
  file:string;
  FileErrorMessage=false;
  FileEmptyMessageError=false;
  FilesName:string[];
  FileEmptyMsg:string;
  imgname:string;
  selectedfileCount:number;
  allFiles : FileInfo[];
  public progress: number;
  public message: string;
  FileType:string;
  @ViewChild('UploadFiles',{static:true})
  public uploadObj: UploaderComponent;
   
  public path: Object = {
      saveUrl:environment.apiUrl+"/api/SourceDescriptionAndConfigurtionController/FilesUploader",
      removeUrl: 'https://ej2.syncfusion.com/services/api/uploadbox/Remove',
     };

  public dropElement: HTMLElement = document.getElementsByClassName('control-fluid')[0] as HTMLElement;
  public allowExtensions: string = '.doc, .docx, .xls, .xlsx ,.csv';
  constructor(public fileUploadService:FileUploadService,
    public sourceAccountSettupservis:SourceAccountSettup,
    public fileUpload:FileUpload,
    private router: Router,
    private Googleoauth:googleoauth,
    injector : Injector
    ) { 
      super(injector)
     var fileextention=localStorage.getItem("FileType");
      this.FileType=fileextention;
      this.imgname=localStorage.getItem('src');
    }

  ngOnInit(): void {

    let searchParams = new URLSearchParams(window.location.search);
    if (searchParams.has('code')) {
        var code = searchParams.get('code');
         
        this.Googleoauth.uploadfile(code);
    }
  }
                      // File Uploder Work With HTMl working Fine But Change it in Syncfusion for multiple and 
                       // Single  file upload 
   
  // filesName(fileInput:any,files)
  // {
  //   if (files.length === 0) {
  //     return;
  //   }
  //   debugger
  //   this.file = fileInput.target.files[0].name.split('.').pop();
     
  //    if(this.FileType =="."+this.file){
  //     this.FileEmptyMessageError=false;
  //      let fileToUpload = <File>files[0];
  //      const formData = new FormData();
  //      formData.append('file', fileToUpload, fileToUpload.name);
  //      }
  //      else{
  //       this.FileEmptyMsg="Please choose selected file type";
  //       this.FileEmptyMessageError=true;
  //     }
  // }
  // uploadFile(selectedFile){
  //   debugger
  //   if(selectedFile.length>0)
  //   {
  //     this.FileEmptyMessageError=false;
  //     let fileToUpload = <File>selectedFile[0];
  //     const formData = new FormData();
  //     formData.append('file', fileToUpload, fileToUpload.name);
  //     let UserId = this.storageService.getItem(environment.storage.userId);
  //      let WORKSPACEID = "1";
  //      let CLIENTID = this.storageService.getItem(environment.storage.clientId);
  //      let AccountId =JSON.parse(localStorage.getItem('accountId'));
  //      let ConnectorId =JSON.parse(localStorage.getItem('ConnectorId'));
       
  //     this.fileUpload.uploadfile(formData,UserId,WORKSPACEID,CLIENTID,ConnectorId,AccountId).subscribe(event => {
  //       if (event.type === HttpEventType.UploadProgress)
  //         this.progress = Math.round(100 * event.loaded / event.total);
  //       else if (event.type === HttpEventType.Response) {
  //         this.message = 'Upload success.';
  //         this.onUploadFinished.emit(event.body);
  //       }
  //     });
  //   }
  //   else{
  //     this.FileEmptyMsg="Please choose the file";
  //     this.FileEmptyMessageError=true;
  //   }
    
  // }

  public onSelected(args : SelectedEventArgs) : void {
    debugger
    this.FilesName=[];
    args.filesData.splice(20);
    this.selectedfileCount=args.filesData.length;
    //  this.filenamesequence+this.counterthis.filename;
    var obj={
        
    }
    debugger
    let filesData : FileInfo[] = this.uploadObj.getFilesData();
    
    this.allFiles= filesData.concat(args.filesData);
    localStorage.setItem("allFiles",JSON.stringify(this.allFiles))
  
    if (this.allFiles.length > 20) {
        for (let i : number = 0; i < this.allFiles.length; i++) {
            if (this.allFiles.length > 20) {
              // this.FilesName.push(this.allFiles[0].name)
              this.allFiles.shift();
            }
        }
        args.filesData = this.allFiles;
        args.modifiedFilesData = args.filesData;
    }
    for (let i : number = 0; i < this.allFiles.length; i++) {
        this.FilesName.push(this.allFiles[i].name)
         }
    args.isModified = true;
}

public onFileRemove(args: RemovingEventArgs): void {
  debugger
  this.FilesName=[];
 var getrow=args.filesData.splice(20);
 var findfilename=args.filesData[0].name;

  for(var i=0;i<this.allFiles.length;i++)
  {

    var find=this.allFiles.find(x=>x.name==findfilename);
    var RemoveIndex=this.allFiles.indexOf(find);
    this.allFiles.splice(RemoveIndex,1);
  }
  for(var i=0;i<this.allFiles.length;i++)
  {
    this.FilesName.push(this.allFiles[i].name)
    
  }
  this.selectedfileCount=args.filesData.length;
    args.postRawFile = false;
    localStorage.setItem("allFiles",JSON.stringify(this.allFiles))
}


public onUploadBegin(args: UploadingEventArgs) {
  debugger
       let UserId = this.storageService.getItem(environment.storage.userId);
       let WORKSPACEID = this.storageService.getItem(environment.storage.workspaceId);
       let CLIENTID = this.storageService.getItem(environment.storage.clientId);
       var arrayincommaseperate=this.FilesName.toString();
       let AccountId =JSON.parse(localStorage.getItem('accountId'));
       let ConnectorId =JSON.parse(localStorage.getItem('ConnectorId'));
       args.customFormData = [{'UserId': UserId},{'WORKSPACEID':WORKSPACEID},{'CLIENTID':CLIENTID},
         {'ConnectorId':ConnectorId},{'AccountId':AccountId},{'selectedfileCount':this.selectedfileCount},{'filename':arrayincommaseperate}];
        

       // check whether the file is uploading from paste.
  // if (args.fileData.fileSource === 'paste') {
  //     let newName: string = getUniqueID(args.fileData.name.substring(0, args.fileData.name.lastIndexOf('.'))) + '.png';
  //     args.customFormData = [{'fileName': newName}];
  // }
}
onUploadStatus(args) { 
debugger
// var header = args.response.headers; 
// var statusMessage = header.slice(header.indexOf('status'), header.indexOf('date')); 
// statusMessage = statusMessage.slice(statusMessage.indexOf(',') + 1); 
args.statusText ;
 
}
}
