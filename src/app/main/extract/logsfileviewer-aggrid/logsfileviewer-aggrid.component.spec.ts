import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogsfileviewerAggridComponent } from './logsfileviewer-aggrid.component';

describe('LogsfileviewerAggridComponent', () => {
  let component: LogsfileviewerAggridComponent;
  let fixture: ComponentFixture<LogsfileviewerAggridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogsfileviewerAggridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogsfileviewerAggridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
