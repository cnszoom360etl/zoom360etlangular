import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnalyzeRoutingModule } from './aiinsights-routing.module';
 
import { AgGridModule } from 'ag-grid-angular';
import 'ag-grid-enterprise';
 
import { AIinsightsComponent } from './aiinsights.component';
// Import FusionCharts library and chart modules
import * as FusionCharts from "fusioncharts";
import * as charts from "fusioncharts/fusioncharts.charts";
import * as FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";
import { FusionChartsModule } from 'angular-fusioncharts';
import { KpiGraphComponent } from 'src/app/widget/kpi-graph/kpi-graph.component';
import { SharedModule } from 'src/app/shared/shared.module';

// Pass the fusioncharts library and chart modules
FusionChartsModule.fcRoot(FusionCharts, charts, FusionTheme);
@NgModule({
  declarations: [
    AIinsightsComponent,
    KpiGraphComponent
  ],
  imports: [
    SharedModule,
    CommonModule,
    AnalyzeRoutingModule,
    FusionChartsModule,
    AgGridModule.withComponents([]),
  ]
})
export class aiinsightsModule { }
