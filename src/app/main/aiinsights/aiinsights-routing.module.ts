import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
 
import { AIinsightsComponent } from './aiinsights.component';
import { KpiGraphComponent } from 'src/app/widget/kpi-graph/kpi-graph.component';

const routes: Routes = [
  { path: 'insights', component:AIinsightsComponent,
    children:[
    { path: 'graph', component:KpiGraphComponent}
    ]
  }
]
@NgModule({
  declarations: [],
  imports: [
     CommonModule,
    RouterModule.forChild(routes),
  ],
  exports:[RouterModule]
})
export class AnalyzeRoutingModule { }
