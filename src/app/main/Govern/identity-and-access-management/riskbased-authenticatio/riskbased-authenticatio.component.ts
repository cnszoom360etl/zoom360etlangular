import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { dropdownModel } from 'src/app/models/common/dropdownmodel';
import { RiskBaseAuthenticationModel } from 'src/app/models/Govern/IdentityAccessManagment.model';

import { RiskBasedAuthenticationService } from 'src/app/services/Govern/IdentityAccessManagement/risk-based-authentication.service';

@Component({
  selector: 'app-riskbased-authenticatio',
  templateUrl: './riskbased-authenticatio.component.html',
  styleUrls: ['./riskbased-authenticatio.component.css']
})
export class RiskbasedAuthenticatioComponent implements OnInit {
  riskBasedAuth: FormGroup;
  dropdownNames:string[]=["Device Type","Connection Type","Time Zone","time slot"];
  _deviceType:dropdownModel[]=[];
  _connectionType:dropdownModel[]=[];
  _timezone:dropdownModel[]=[];
  _timeSlot:dropdownModel[]=[];
  MACAccessToggle:boolean=false;
  IpAccessToggle:boolean=false;
  TimeZoneAccessToggle:boolean=false;
  TimeSlotAccessToggle:boolean=false;
  DeviceTypeAccesToggle:boolean=false;
  ConnectionTypeToggle:boolean=false;
  SupervisorAccessToggle:boolean=false
  constructor(private fb: FormBuilder,public _riskBasedAuthenticationService: RiskBasedAuthenticationService) { }

  ngOnInit(): void {
	  this.getdropdown();
    this.createForm();
  }
  createForm() {
    debugger
    this.riskBasedAuth = this.fb.group({
     RBAActivation :['1'],
		 RBAAccess :['1'],
		 SupervisorSummary :[true],
		 UserRiskSummary :[true],
		 IPSummary :['1'],
		 IPVerification :['1'],
		 IPAccess :[false],
		 MacAccess :[false],
		 MacVerification :['1'],
		 TimeZoneAccess :[false],
		 TimeZoneVerification :['1'],
		 TimeSlotAccess :[false],
		 StartTimeVerification :['1'],
		 EndTimeVerification :['1'],
		 DeviceTypeAcces :[false],
		 DeviceTypeVerification :['1'],
		 ConnectionTypeAccess :[false],
		 ConnectionTypeVerification :['1'],
		 SupervisorAccess :[false],
		 SupervisorVerification :['1']
      // dateFormat: ['', Validators.required ],
      // dateCollectedData: [false, Validators.required ],
    });
}
  SaveConfig(identityControlData: RiskBaseAuthenticationModel){
    this._riskBasedAuthenticationService.saveRiskBasedAuthSetup(identityControlData);
  }
getdropdown(){
	for(let i=0;i<this.dropdownNames.length;i++){
	 if(this.dropdownNames[i]=='Device Type'){
		 debugger
	   this._riskBasedAuthenticationService.getDropDown(this.dropdownNames[i])
	   .subscribe((data: dropdownModel[]) => {
		 if(data.length  > 0){
		   this._deviceType = data;
		 }
	 });
   }
  else if(this.dropdownNames[i]=='Connection Type'){
   this._riskBasedAuthenticationService.getDropDown(this.dropdownNames[i])
   .subscribe((data: dropdownModel[]) => {
	 if(data.length  > 0){
	   this._connectionType = data;
	 		}
			});
	}
	else if(this.dropdownNames[i]=='Time Zone'){
		this._riskBasedAuthenticationService.getDropDown(this.dropdownNames[i])
		.subscribe((data: dropdownModel[]) => {
		  if(data.length  > 0){
			this._timezone = data;
		  }
 });
   }
   else if(this.dropdownNames[i]=='time slot'){
	   
	this._riskBasedAuthenticationService.getDropDown(this.dropdownNames[i])
	.subscribe((data: dropdownModel[]) => {
		debugger
	  if(data.length  > 0){
		  debugger
		this._timeSlot = data;
	  }
});
}
 }
 }
MacAccess(e){
	 if(e.target.checked){
		 this.MACAccessToggle=true;
	 }
	 else{
		this.MACAccessToggle=false;
		this.riskBasedAuth.patchValue({MacVerification:''});
	 }
 }
IpAccess(e){
	if(e.target.checked){
		this.IpAccessToggle=true;
	}
	else{
	   this.IpAccessToggle=false;
	   this.riskBasedAuth.patchValue({IPVerification:''});
	}
}
TimeZoneAccess(e){
	if(e.target.checked){
		this.TimeZoneAccessToggle=true;
	}
	else{
	   this.TimeZoneAccessToggle=false;
	   this.riskBasedAuth.patchValue({TimeZoneVerification:''});
	}
}
TimeSlotAccess(e){
	if(e.target.checked){
		this.TimeSlotAccessToggle=true;
	}
	else{
	   this.TimeSlotAccessToggle=false;
	   this.riskBasedAuth.patchValue({StartTimeVerification:''});
	   this.riskBasedAuth.patchValue({EndTimeVerification:''});
	}
}
DeviceTypeAccess(e){
	if(e.target.checked){
		this.DeviceTypeAccesToggle=true;
	}
	else{
	   this.DeviceTypeAccesToggle=false;
	   this.riskBasedAuth.patchValue({DeviceTypeVerification:''});
	}
	
}
ConnectionType(e){
	if(e.target.checked){
		this.ConnectionTypeToggle=true;
	}
	else{
	   this.ConnectionTypeToggle=false;
	   this.riskBasedAuth.patchValue({ConnectionTypeVerification:''});
	}
}
SupervisorAccess(e){
	if(e.target.checked){
		this.SupervisorAccessToggle=true;
	}
	else{
	   this.SupervisorAccessToggle=false;
	   this.riskBasedAuth.patchValue({SupervisorVerification:''});
	}
}
}
