import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { PasswordControlSetupModel } from 'src/app/models/Govern/IdentityAccessManagment.model';
import { PasswordControlService } from 'src/app/services/Govern/IdentityAccessManagement/password-control.service';

@Component({
  selector: 'app-password-control',
  templateUrl: './password-control.component.html',
  styleUrls: ['./password-control.component.css']
})
export class PasswordControlComponent implements OnInit {
  LoginIdFormat:boolean=false;
  passwordControl: FormGroup;
  constructor(private fb: FormBuilder,public _passwordControlService:PasswordControlService) { }

  ngOnInit(): void {
    this.createForm();
  }
  createForm() {
    debugger
    this.passwordControl = this.fb.group({
      PasswordLength: ['1'],
      StrongPassword: ['1'],
      PasswordField: ['1'],
      PasswordExpired: ['1'],
      FirstLoginOption: [true],
      FirstLoginDays: [1],
      UserCreatedDays:[1],
      LoginAttempts:[1],
      LastPasswordChanged:[1],
      NotifyUserPasswordExpiry:["1"],
      NotifyUserPasswordExpiryDays:[1],
      NotifySupervisorPasswordExpiry:["1"],
      NotifySupervisorPasswordExpiryDays:[1],
      // dateFormat: ['', Validators.required ],
      // dateCollectedData: [false, Validators.required ],
    });
}
  SaveConfig(identityControlData: PasswordControlSetupModel){
    this._passwordControlService.savePasswordControlSetup(identityControlData);
  }
  setLoginIdFormat(value){
    this.setLoginIdFormat= value;
    //if(value==false){
    //  this.TimezoneSetup.patchValue({dateFormat:''});
    //}
  }
}
