import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AccessLockingModel } from 'src/app/models/Govern/IdentityAccessManagment.model';
import { TimeZoneSetupModel } from 'src/app/Models/mainmenu.model';
import { AccessLockingService } from 'src/app/services/Govern/IdentityAccessManagement/access-locking.service';

@Component({
  selector: 'app-access-locking',
  templateUrl: './access-locking.component.html',
  styleUrls: ['./access-locking.component.css']
})
export class AccessLockingComponent implements OnInit {
  DATE_FORMAT_TYPE_toggle:boolean=false;
  dataFormatTypeToggle:boolean=false;
  timeZoneToggle:boolean=false;
  dateFormatReportsToggle:boolean=false;
  dateFormatVisulizationToggle:boolean=false;
  timeFormatReportsToggle:boolean=false;
  timeFormatVisualizationToggle:boolean=false;
  accesslockingSetup: FormGroup;
  constructor(private fb: FormBuilder,public _accessLockingService:AccessLockingService) { }
  ngOnInit(): void {
    this.createForm();
  }
  createForm() {
    this.accesslockingSetup = this.fb.group({
      UserLocked: [true],
      FailedAttempt: [1],
      FailedTimeInterval: [1],
      UnlockOption: ['1'],
      UnlockTimeInterval: ['1'],
      UnlockAdministrator: [true],
      UnlockSupervisor: [true],
    });
}
SaveConfig(accessLockingData: AccessLockingModel){
this._accessLockingService.saveAccessLockingSetup(accessLockingData);
 
}
setdateFormatVisible(value){
  this.dataFormatTypeToggle= value;
  if(value==false){
    this.accesslockingSetup.patchValue({dateFormat:''});
  }
}
}
