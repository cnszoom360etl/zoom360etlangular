import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, } from '@angular/forms';
import { dropdownModel } from 'src/app/models/common/dropdownmodel';
import { IdentityControlModel } from 'src/app/models/Govern/IdentityAccessManagment.model';
import { IdentityControlService } from 'src/app/services/Govern/IdentityAccessManagement/identity-control.service';

@Component({
  selector: 'app-identity-control',
  templateUrl: './identity-control.component.html',
  styleUrls: ['./identity-control.component.css']
})
export class IdentityControlComponent implements OnInit {

  DATE_FORMAT_TYPE_toggle:boolean=false;
  LoginIdFormat:boolean=false;
  timeZoneToggle:boolean=false;
  dateFormatReportsToggle:boolean=false;
  dateFormatVisulizationToggle:boolean=false;
  timeFormatReportsToggle:boolean=false;
  timeFormatVisualizationToggle:boolean=false;
  IdentityControl: FormGroup;
  dropdownNames:string[]=["Available Formats","Mandatory Fields"];
  _availableFormats:dropdownModel[]=[];
  _mandatoryFields:dropdownModel[]=[];
  constructor(private fb: FormBuilder,public _identityControlService: IdentityControlService) {
    
   }
  ngOnInit(): void {
    this.getdropdown();
    this.createForm();
  }
  createForm() {
    debugger
    this.IdentityControl = this.fb.group({
      loginidFormatType: ['1'],
      DefinedFormats :['1'],
		  AvailableFormats :['1'],
		  MandtoryField :['1'],
		  UniqueMandtoryField :[true],
		  LoginActivation :['1'],
		  ApprovalSuperVisor :[true],
		  ApprovalEmail :[true],
		  ApprovalQRCode :[true],
		  LoginCaseSensitive :['1'],
	  	LoginAuthentication :[true],
		  SSOAuthentication :[true],
		  SSLCertificate :['1'],
      // dateFormat: ['', Validators.required ],
      // dateCollectedData: [false, Validators.required ],
    });
}
getdropdown(){
   for(let i=0;i<this.dropdownNames.length;i++){
    if(this.dropdownNames[i]=='Available Formats'){
      this._identityControlService.getDropDown(this.dropdownNames[i])
      .subscribe((data: dropdownModel[]) => {
        if(data.length  > 0){
          this._availableFormats = data;
        }
    });
  }
 else if(this.dropdownNames[i]=='Mandatory Fields'){
  this._identityControlService.getDropDown(this.dropdownNames[i])
  .subscribe((data: dropdownModel[]) => {
    if(data.length  > 0){
      this._mandatoryFields = data;
    }
});
  }
}
}
SaveConfig(identityControlData: IdentityControlModel){
  this._identityControlService.saveIndentityControlSetup(identityControlData);
}
setLoginIdFormat(value){
  this.setLoginIdFormat= value;
  //if(value==false){
  //  this.TimezoneSetup.patchValue({dateFormat:''});
  //}
}

}
