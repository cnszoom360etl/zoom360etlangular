import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { dropdownModel } from 'src/app/models/common/dropdownmodel';
import { MultiFactorSetupeModel } from 'src/app/models/Govern/IdentityAccessManagment.model';
import { MultiFectorAuthenticationService } from 'src/app/services/Govern/IdentityAccessManagement/multi-fector-authentication.service';

@Component({
  selector: 'app-multi-factor-authentication',
  templateUrl: './multi-factor-authentication.component.html',
  styleUrls: ['./multi-factor-authentication.component.css']
})
export class MultiFactorAuthenticationComponent implements OnInit {
  accesslockingSetup: FormGroup;
  dropdownName='Security Question';
  _securityQuestion:dropdownModel[]=[];
  constructor(private fb: FormBuilder,public _multiFectorAuthenticationService: MultiFectorAuthenticationService) { }

  ngOnInit(): void {
this.getdropdown();
    this.createForm();
  }
  createForm() {
    this.accesslockingSetup = this.fb.group({
     SecurityQuestion:[],
		 SQFirstTimeLogin:[true],
		 SQPasswordUpdation:[true],
		 SecurityQestionOption:['What was your driving instructors first name?'],
		 SecurityQestionAnswer:['1'],
		 PasscodeAuthentication:['1'],
		 PAFirsttimeLogin:[true],
		 PAPasswordUpdation:[true],
		 PasscodeEmail:[true],
		 PasscodeSMS:[true],
		 PasscodeSinglepart:[true],
		 PasscodeTwopart:[true],
		 PasscodeValidityTime:[1],
		 PasscodeValidityAttempts:[1],
		 CryptographicTokens:['1'],
		 CTFirsttimeLogin:[true],
		 CTPasswordUpdation:[true],
		 QRCode:['1'],
		 QRFirsttimeLogin:[true],
		 QRPasswordUpdation:[true],
		 FaceID:['1'],
		 FIFirsttimeLogin:[true],
		 FIPasswordUpdation:[true],
		 SupervisoryApproval:['1'],
		 SAFirsttimeLogin:[true],
		 SAPasswordUpdation:[true]
    });
  }
    SaveConfig(multiFactorData: MultiFactorSetupeModel){
      debugger
    this._multiFectorAuthenticationService.saveMultiFactorSetup(multiFactorData);   
       }
getdropdown(){
      this._multiFectorAuthenticationService.getDropDown(this.dropdownName)
      .subscribe((data: dropdownModel[]) => {
        if(data.length  > 0){
          this._securityQuestion = data;
        }
    });
  }
}
