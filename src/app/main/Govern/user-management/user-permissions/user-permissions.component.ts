import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { dropdownModel, UAMDropdownModel } from 'src/app/models/common/dropdownmodel';
import { TreeDropDownParentModel } from 'src/app/models/common/TreeDropDownModel';
import { SaveUserAccessManagementModel } from 'src/app/models/Govern/UserAccessManagment.model';
import { DropDownLoadService } from 'src/app/services/common/drop-down-load.service';
import { UserAccessManagementService } from 'src/app/services/Govern/UserManagement/user-access-management.service';

@Component({
  selector: 'app-user-permissions',
  templateUrl: './user-permissions.component.html',
  styleUrls: ['./user-permissions.component.css']
})
export class UserPermissionsComponent implements OnInit {

  dataList: any;
  fieldsvalues: Object;
  dropdownNames:string[]=["Workspaces","Child Workspaces","Display Modes"];
  _workspaces:UAMDropdownModel[]=[];
  _childWorkspaces:UAMDropdownModel[]=[];
  _displayMode:UAMDropdownModel[]=[];
  _timeSlot:UAMDropdownModel[]=[];
  _deviceType :UAMDropdownModel[]=[];
  SaveUserPermissions: FormGroup;
  sp_SaveUserProfile:string='SAVEUSERPERMISSIONS';
  dropDownName='Module Allowed';
  //data:TreeDropDownParentModel[]=[];
  public fields: Object;
  public treeSettings: Object;
  // dataSource
  public data: { [key: string]: Object }[];
  //  = [
  //   {
  //      nodeId: '01', nodeText: 'Music',selected:false,
  //      nodeChild: [
  //          { nodeId: '01-01', nodeText: 'Gouttes.mp3' }
  //      ]
  //   },
  //   {
  //      nodeId: '02', nodeText: 'Videos', expanded: true,selected:false,
  //      nodeChild: [
  //          { nodeId: '02-01', nodeText: 'Naturals.mp4' },
  //          { nodeId: '02-02', nodeText: 'Wild.mpeg' },
  //      ]
  //   },
  //   {
  //      nodeId: '03', nodeText: 'Documents',
  //      nodeChild: [
  //          { nodeId: '03-01', nodeText: 'Environment Pollution.docx',selected:true },
  //          { nodeId: '03-02', nodeText: 'Global Water, Sanitation, & Hygiene.docx',selected:true},
  //          { nodeId: '03-03', nodeText: 'Global Warming.ppt',selected:true},
  //          { nodeId: '03-04', nodeText: 'Social Network.pdf',selected:false},
  //          { nodeId: '03-05', nodeText: 'Youth Empowerment.pdf',selected:true},
  //      ]
  //   },
  // ];
    //binding data source through fields property
 //  public fields: Object = { dataSource: this.data, value: 'nodeId', text: 'nodeText', child: 'nodeChild',selected:'selected' };// treeSettings
//public treeSettings: Object = { autoCheck: true };
  constructor(private fb: FormBuilder,private userAccessManagementService: UserAccessManagementService,public dropDownLoadService:DropDownLoadService) {
  //   this.dataList = [
  //     { skill: 'ASP.NET' }, { skill: 'ActionScript' }, { skill: 'Basic' },
  //     { skill: 'C++' }, { skill: 'C#' }, { skill: 'dBase' }, { skill: 'Delphi' },
  //     { skill: 'ESPOL' }, { skill: 'F#' }, { skill: 'FoxPro' }, { skill: 'Java' },
  //     { skill: 'J#' }, { skill: 'Lisp' }, { skill: 'Logo' }, { skill: 'PHP' }
  // ];
  
  this.dropDownLoadService.gettreeDropDownData(this.dropDownName).subscribe((datalist:any[])=>{
       this.data = datalist;
       this.fields = { dataSource: this.data, value: 'dropdownValue', text: 'dropdownText', child: 'treeDropDownChildItems',selected:'selected' };// treeSettings
     this.treeSettings = { autoCheck: true };
  });
  
   }

  ngOnInit() {  
    this.createForm();
    this.getdropdown();
//this.GetUserPermissions();
  }
  getdropdown(){
    for(let i=0;i<this.dropdownNames.length;i++){
     if(this.dropdownNames[i]=='Workspaces'){
       debugger
       this.userAccessManagementService.getDropDown(this.dropdownNames[i])
       .subscribe((data: UAMDropdownModel[]) => {
       if(data.length  > 0){
         this._workspaces = data;
         this.fieldsvalues = { dataSource: this._workspaces,text:'dropdownText',value:'dropdownValue',selected:"selected"};
       }
     });
     }
     else if(this.dropdownNames[i]=='Child Workspaces'){
      this.userAccessManagementService.getDropDown(this.dropdownNames[i])
      .subscribe((data: UAMDropdownModel[]) => {
        if(data.length  > 0){
        this._childWorkspaces = data;
        this.fieldsvalues = { dataSource: this._childWorkspaces,text:'dropdownText',value:'dropdownValue',selected:"selected"};
        }
   });
     }
    else if(this.dropdownNames[i]=='Display Modes'){
     this.userAccessManagementService.getDropDown(this.dropdownNames[i])
     .subscribe((data: UAMDropdownModel[]) => {
      this._displayMode = data;
      this.fieldsvalues = { dataSource: this._displayMode,text:'dropdownText',value:'dropdownValue',selected:'selected'};
        });
    }
  }
}
createForm() {
    this.SaveUserPermissions = this.fb.group({
      Useractive:[true],
      UserLocked:[false],
     
    });
  }
  SaveConfig(saveUserAccessManagementModel: SaveUserAccessManagementModel,module){
    debugger
    var workspaces1= document.getElementById('workspaces1') as HTMLInputElement;
    var childWorkspace1= document.getElementById('childworkspaces1') as HTMLInputElement;
    var displayMode1= document.getElementById('displayMode1') as HTMLInputElement;
    var dataOperation1= document.getElementById('dataOperation1') as HTMLInputElement;
    saveUserAccessManagementModel.Workspace= workspaces1.value;
    saveUserAccessManagementModel.ChildWorkspace= childWorkspace1.value;
    saveUserAccessManagementModel.DisplayMode= displayMode1.value;
   saveUserAccessManagementModel.ModuleAllowed= module.toString();
    saveUserAccessManagementModel.DataOperaion= dataOperation1.value;
    saveUserAccessManagementModel.ProcedureName= this.sp_SaveUserProfile;
    debugger
  this.userAccessManagementService.saveUserAccessManagementSetup(saveUserAccessManagementModel);
  }
  GetUserPermissions(){
    this.userAccessManagementService.getUserPermissions().subscribe((data:any)=>{
      this.SaveUserPermissions.patchValue({
        Useractive: data.userActive,
        UserLocked: data.UserLocked,
       });
    });
  }
}
