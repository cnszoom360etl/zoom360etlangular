import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { UAMDropdownModel } from 'src/app/models/common/dropdownmodel';
import { SaveUserAccessManagementModel } from 'src/app/models/Govern/UserAccessManagment.model';
import { UserAccessManagementService } from 'src/app/services/Govern/UserManagement/user-access-management.service';

@Component({
  selector: 'app-user-restrictions',
  templateUrl: './user-restrictions.component.html',
  styleUrls: ['./user-restrictions.component.css']
})
export class UserRestrictionsComponent implements OnInit {
  SaveUserRestriction: FormGroup;
  sp_SaveUserProfile:string='SAVEUSERRESTRICTIONS';
  dropdownName='Supervisors';
  fieldsvalues: Object;
  _superVisor:UAMDropdownModel[]=[];
  shown:boolean=false;
  constructor(private fb: FormBuilder,private userAccessManagementService: UserAccessManagementService) { }

  ngOnInit() {
    this.createForm();
  }
  getdropdown(){
    this.userAccessManagementService.getDropDown(this.dropdownName)
    .subscribe((data:UAMDropdownModel[]) => {
     this._superVisor = data;
     this.fieldsvalues = { dataSource: this._superVisor,text:'dropdownText',value:'dropdownValue',selected:"selected"};
  });
}
  createForm() {
    this.SaveUserRestriction = this.fb.group({
      RegistarionAccessMode:['1'],
      SupervisorMode:[false],
    });
  }
  SaveConfig(saveUserAccessManagementModel: SaveUserAccessManagementModel){
    var SupervisorName= document.getElementById('SupervisorName') as HTMLInputElement;
    saveUserAccessManagementModel.SuperVisorName= SupervisorName.value;
    saveUserAccessManagementModel.ProcedureName= this.sp_SaveUserProfile;
  this.userAccessManagementService.saveUserAccessManagementSetup(saveUserAccessManagementModel);
  }
showSupervisor(e){
  debugger
          if(e.target.checked)
          {
            this.shown=true;
          }
          else{
              this.shown=false
          }
    }
getUserRistrications(){
  this.userAccessManagementService.getUserRestriction().subscribe((data:any)=>{
    this.SaveUserRestriction.patchValue({
      RegistarionAccessMode: data.RegistarionAccessMode,
      SupervisorMode: data.SupervisorMode,
     });
  });
}
}
