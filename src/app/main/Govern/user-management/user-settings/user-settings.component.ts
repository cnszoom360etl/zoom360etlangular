import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { dropdownModel } from 'src/app/models/common/dropdownmodel';
import { SaveUserAccessManagementModel } from 'src/app/models/Govern/UserAccessManagment.model';
import { UserAccessManagementService } from 'src/app/services/Govern/UserManagement/user-access-management.service';

@Component({
  selector: 'app-user-settings',
  templateUrl: './user-settings.component.html',
  styleUrls: ['./user-settings.component.css']
})
export class UserSettingsComponent implements OnInit {

  dataList: any;
  fieldsvalues: Object;
  dropdownNames:string[]=["Default Page","Display Theme","Display Modes"];
  _defaultPage:dropdownModel[]=[];
  _displayTheme:dropdownModel[]=[];
  _displayMode:dropdownModel[]=[];
  _timeSlot:dropdownModel[]=[];
  _deviceType :dropdownModel[]=[];
  sp_SaveUserProfile:string='SAVEUSERSETTINGS';
  SaveUserSettings: FormGroup;
  defaultPageValue:any;
  defaultTheme:any;
  displayMode:any;

  // dataSource
  
// defining fieldMapping

public treeSettings: Object = { autoCheck: true }
  constructor(private fb: FormBuilder,private userAccessManagementService: UserAccessManagementService) {
   }
  ngOnInit() {  
    this.getdropdown();
    this.getselectedvalues();
    this.createForm();
  }
  createForm() {
    this.SaveUserSettings = this.fb.group({
    });
  }
  getdropdown(){
    for(let i=0;i<this.dropdownNames.length;i++){
     if(this.dropdownNames[i]=='Default Page'){
       debugger
       this.userAccessManagementService.getDropDown(this.dropdownNames[i])
       .subscribe((data: dropdownModel[]) => {
       if(data.length  > 0){
         debugger
         this._defaultPage = data;
         this.fieldsvalues = { dataSource: this._defaultPage,text:'dropdownText',value:'dropdownValue'};
       }
     });
     }
     else if(this.dropdownNames[i]=='Display Theme'){
       debugger
      this.userAccessManagementService.getDropDown(this.dropdownNames[i])
      .subscribe((data: dropdownModel[]) => {
        console.log(data)
        if(data.length  > 0){
          debugger
        this._displayTheme = data;
        this.fieldsvalues = { dataSource: this._displayTheme,text:'dropdownText',value:'dropdownValue'};
        }
   });
     }
    else if(this.dropdownNames[i]=='Display Modes'){
      debugger
     this.userAccessManagementService.getDropDown(this.dropdownNames[i])
     .subscribe((data: dropdownModel[]) => {
     if(data.length  > 0){
  debugger
       this._displayMode = data;
       this.fieldsvalues = { dataSource: this._displayMode,text:'dropdownText',value:'dropdownValue'};
         }
        });
    }
   }
   }
   SaveConfig(saveUserAccessManagementModel: SaveUserAccessManagementModel){
    debugger
    var displayMode= document.getElementById('displayMode') as HTMLInputElement;
    var defaultPage= document.getElementById('defaultPage') as HTMLInputElement;
    var displaytheme= document.getElementById('displaytheme') as HTMLInputElement;
   
    saveUserAccessManagementModel.DefaultDisplaymode= displayMode.value;
    saveUserAccessManagementModel.DefaultPage= defaultPage.value;
    saveUserAccessManagementModel.DisplayTheme= displaytheme.value;
    
    saveUserAccessManagementModel.ProcedureName= this.sp_SaveUserProfile;
    debugger
  this.userAccessManagementService.saveUserAccessManagementSetup(saveUserAccessManagementModel);
  }
  getselectedvalues(){
    this.defaultPageValue='2';
    this.defaultTheme='2';
    this.displayMode='3';
  }
}
