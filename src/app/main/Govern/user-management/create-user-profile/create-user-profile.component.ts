
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MultiSelectComponent } from '@syncfusion/ej2-angular-dropdowns';
import { UAMDropdownModel } from 'src/app/models/common/dropdownmodel';
import { SaveUserAccessManagementModel } from 'src/app/models/Govern/UserAccessManagment.model';
import { UserAccessManagementService } from 'src/app/services/Govern/UserManagement/user-access-management.service';

@Component({
  selector: 'app-create-user-profile',
  templateUrl: './create-user-profile.component.html',
  styleUrls: ['./create-user-profile.component.css']
})
export class CreateUserProfileComponent implements OnInit {
  @ViewChild('checkbox', {static: true}) mulObj: MultiSelectComponent; 
 
btnclick() {  
    console.log(this.mulObj.value); 
} 
  dropdownName='Time Zone';
  sp_SaveUserProfile:string='SAVEUSERPROFILE';
  ProcedureName='';
  TimeZone:string="";
  dataList: any;
  fieldsvalues: Object;
  SaveUserProfile: FormGroup;
  _timezone:UAMDropdownModel[]=[];
  constructor(private fb: FormBuilder,private userAccessManagementService: UserAccessManagementService) {
 
   
   }

  ngOnInit() {
    this.createForm();
    this.getdropdown();
  }
  createForm() {
    this.SaveUserProfile = this.fb.group({
      EmailAddress: ["usman@gmail.com"],
      FirstName: ["usman"],
      LastName: ["zulfiqar"],
      PhoneNo: ['1'],
    });
  }
  SaveConfig(saveUserAccessManagementModel: SaveUserAccessManagementModel){
    debugger
    var timeZone= document.getElementById('timeZone') as HTMLInputElement;
    saveUserAccessManagementModel.TimeZone = timeZone.value;
    saveUserAccessManagementModel.ProcedureName= this.sp_SaveUserProfile;
  this.userAccessManagementService.saveUserAccessManagementSetup(saveUserAccessManagementModel);
  }
  getdropdown(){
       this.userAccessManagementService.getDropDown(this.dropdownName)
       .subscribe((data:UAMDropdownModel[]) => {
         debugger
        this._timezone = data;
        this.fieldsvalues = { dataSource: this._timezone,text:'dropdownText',value:'dropdownValue',selected:"selected"};
     });
}
GetUserProfile(){
  this.userAccessManagementService.getUserProfile().subscribe((data:any)=>{
    this.SaveUserProfile.patchValue({
      EmailAddress: data.workSpace_Name,
      FirstName: data.workSpaceDisplayName,
      LastName: data.workSpaceParentName,
      PhoneNo: data.childWorkSpaceRule,
     });
  });
}
}
