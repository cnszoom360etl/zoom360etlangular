import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { SaveUserAccessManagementModel } from 'src/app/models/Govern/UserAccessManagment.model';
import { UserAccessManagementService } from 'src/app/services/Govern/UserManagement/user-access-management.service';

@Component({
  selector: 'app-user-notfication-setting',
  templateUrl: './user-notfication-setting.component.html',
  styleUrls: ['./user-notfication-setting.component.css']
})
export class UserNotficationSettingComponent implements OnInit {

  SaveUserNotification: FormGroup;
  sp_SaveUserProfile:string='SAVEUSERNOTIFICATIONS';
  constructor(private fb: FormBuilder,private userAccessManagementService: UserAccessManagementService) { }

  ngOnInit() {
    this.createForm();
  }
  createForm() {
    this.SaveUserNotification = this.fb.group({
      NewsLetter:[true],
      SystemNotification:[true],
      AccessNotification:[true]
    });
  }
  SaveConfig(saveUserAccessManagementModel: SaveUserAccessManagementModel){
    debugger
    saveUserAccessManagementModel.ProcedureName= this.sp_SaveUserProfile;
  this.userAccessManagementService.saveUserAccessManagementSetup(saveUserAccessManagementModel);
  }
}
