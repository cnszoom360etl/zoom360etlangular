import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { SaveUserAccessManagementModel } from 'src/app/models/Govern/UserAccessManagment.model';
import { UserAccessManagementService } from 'src/app/services/Govern/UserManagement/user-access-management.service';

@Component({
  selector: 'app-create-user-password',
  templateUrl: './create-user-password.component.html',
  styleUrls: ['./create-user-password.component.css']
})
export class CreateUserPasswordComponent implements OnInit {
  SaveUserPassword: FormGroup;
  sp_SaveUserProfile:string='SAVEUSERPASSWORD';
  constructor(private fb: FormBuilder,private userAccessManagementService: UserAccessManagementService) { }

  ngOnInit() {
    this.createForm();
  }
  createForm() {
    this.SaveUserPassword = this.fb.group({
      OldPassword: ["usman@gmail.com"],
      NewPassword: ["usman"],
      RetypeNewPassword: ["zulfiqar"],
      OverwriteExisting:[true]
    });
  }
  SaveConfig(saveUserAccessManagementModel: SaveUserAccessManagementModel){
   debugger
   saveUserAccessManagementModel.ProcedureName= this.sp_SaveUserProfile;
  this.userAccessManagementService.saveUserAccessManagementSetup(saveUserAccessManagementModel);
  }
}
