import { Component, OnInit } from '@angular/core';
import { AppMenuService } from 'src/app/Services/common/app-menu.service';
import { GetAllConnectionsService } from 'src/app/services/extract/get-all-connections.service';
 

@Component({
  selector: 'app-govern',
  templateUrl: './govern.component.html',
  styleUrls: ['./govern.component.css']
})
export class GovernComponent implements OnInit {

  mainmenuID:number=11;
  accountId=null;
  constructor(public MenuService: AppMenuService,public getAllConnectionsService:GetAllConnectionsService) {
      
     }

  ngOnInit(): void {
    JSON.stringify(localStorage.setItem("accountId",this.accountId));
    this.MenuService.getsubMenuSection(this.mainmenuID);
  }
}
