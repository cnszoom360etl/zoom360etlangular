import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApplicationStartupComponent } from '../application-startup/application-startup.component';
import { AuthurizationResponseComponent } from '../authurization-response/authurization-response.component';
import { EtlStatusbarComponent } from './etl-statusbar/etl-statusbar.component';
 
import { MainComponent } from './main.component';

const routes: Routes = [
  {path:'', redirectTo:'extract', pathMatch:'full'},
  {path:'ETLStatus', component:EtlStatusbarComponent},
  {path:'',component:MainComponent,
  children:[
  {
    path: 'auth',
    loadChildren: () => import('../auth/auth.module').then(m => m.AuthModule)
  },
  // {
  //   path: '',
  //   loadChildren: () => import('./front/front.module').then(m => m.FrontModule), canActivate: [AuthGuard]
  // },
  {
    path: 'extract',
    loadChildren: () => import('../main/extract/extract.module').then(m => m.ExtractModule),
  },
  {
    path: 'enrich',
    loadChildren: () => import('../main/enrich/enrich.module').then(m => m.EnrichModule),
  },
  {
    path: 'load',
    loadChildren: () => import('../main/load/Load.module').then(m => m.LoadModule),
  }, 
  {
    path: 'analyze',
    loadChildren: () => import('../main/analyze/analyze.module').then(m => m.AnalyzeModule), 
  },
  {
    path: 'manage',
    loadChildren: () => import('../main/administration/administration.module').then(m => m.AdministrationModule),
  },
  {
    path: 'publish',
    loadChildren: () => import('../main/publish/publish.module').then(m => m.PublishModule),
  },
  {
    path: 'aiinsights',
    loadChildren: () => import('../main/aiinsights/aiinsights.module').then(m => m.aiinsightsModule),
  },
  {
    path: 'govern',
    loadChildren: () => import('../main/Govern/Govern-module').then(m => m.GovernModule),
  },
  {
    path: 'kpiachivements',
    loadChildren: () => import('../main/KPIAchivements/KPIAchivements.module').then(m => m.KPIAchivementsModule)
  },
  { path: '**', redirectTo: 'auth/login' },
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }

