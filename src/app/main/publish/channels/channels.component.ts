import { Component, OnInit } from '@angular/core';
import { DashboardService } from 'src/app/services/extract/dashboard/Dashboard.service';


@Component({
  selector: 'app-channels',
  templateUrl: './channels.component.html',
  styleUrls: ['./channels.component.css']
})
export class ChannelsComponent implements OnInit {

  cate : any[] = [];
  imp:any[] = [];
  clicks: any[] = [];
  ctr: any[] = [];

  sscate : any[] = [];
  ssimp:any[] = [];
  ssclicks: any[] = [];
  ssctr: any[] = [];


  mscate : any[] = [];
  msimp:any[] = [];
  msclicks: any[] = [];
  msctr: any[] = [];
  ngOnInit(): void {
  }

  constructor(
    private dashboardService: DashboardService ) 
    {

    dashboardService.get().subscribe(res => {
      res.forEach(element => {
        if (element.platform == "Facebook") {
        this.cate.push({label:element.period_Display_Date})
        this.imp.push({value: element.imperssions})
        this.clicks.push({value:element.clicks})
        this.ctr.push({value:element.ctr})
        } else if (element.platform == "Spreadsheet") {
          debugger;
          this.sscate.push({label:element.period_Display_Date})
          this.ssimp.push({value: element.imperssions})
          this.ssclicks.push({value:element.clicks})
          this.ssctr.push({value:element.ctr})
        } else if (element.platform == "MS SQL Server") {
          debugger;
          this.mscate.push({label:element.period_Display_Date})
          this.msimp.push({value: element.imperssions})
          this.msclicks.push({value:element.clicks})
          this.msctr.push({value:element.ctr})
        }
        
});
     
    })
   }


   data = {
    chart: {
      // caption:"",
      // subcaption: "China (2012-15)",
      // yaxisname: "Units Sold",
      // syaxisname: "Share of market",
      snumbersuffix: "k",
      drawcustomlegendicon: "0",
      showvalues: "0",
      rotatelabels: "1",
      theme: "fusion"
    },
    categories: [
        
      {
        category: this.cate
      }
    ],
    dataset: [
      {
        seriesname: "Impressions",
        data:this.imp
      },
      {
        seriesname: "Clicks",
        data: this.clicks
      },

      {
        seriesname: "CTR(%)",
        renderas: "line",
        parentyaxis: "S",
        data: this.ctr
      }
    ]
  };

  ssdata = {
    chart: {
      // caption:"",
      // subcaption: "China (2012-15)",
      // yaxisname: "Units Sold",
      // syaxisname: "Share of market",
      snumbersuffix: "k",
      drawcustomlegendicon: "0",
      showvalues: "0",
      rotatelabels: "1",
      theme: "fusion"
    },
    categories: [
      
        
      {
        category: this.sscate
      }
    ],
    dataset: [
      {
        seriesname: "Impressions",
        data:this.ssimp
      },
      {
        seriesname: "Clicks",
        data: this.ssclicks
      },
      {
        seriesname: "CTR(%)",
        renderas: "line",
        parentyaxis: "S",
        data: this.ssctr
      }
    ]
  };

  msdata = {
    chart: {
      // caption:"",
      // subcaption: "China (2012-15)",
      // yaxisname: "Units Sold",
      // syaxisname: "Share of market",
      snumbersuffix: "k",
      drawcustomlegendicon: "0",
      showvalues: "0",
      rotatelabels: "1",
      theme: "fusion"
    },
    categories: [
      
        
      {
        category: this.mscate
      }
    ],
    dataset: [
      {
        seriesname: "Impressions",
        data:this.msimp
      },
      {
        seriesname: "Clicks",
        data: this.msclicks
      },
      {
        seriesname: "CTR(%)",
        renderAs: "line",
        parentyaxis: "S",
        data: this.msctr
      }
    ]
  };


  clickdata={

    chart: {
      palettecolors:"#29C3BE",
      showlables: "0",
      showvalues: "0",
      showYAxisvalue:"0",
      theme: "fusion"
    },
    data: this.clicks
  }
  ssclickdata={

    chart: {
      palettecolors:"#29C3BE",
      showlables: "0",
      showvalues: "0",
      showYAxisvalue:"0",
      theme: "fusion"
    },
    data: this.ssclicks
  }

  msclickdata={

    chart: {
      palettecolors:"#29C3BE",
      showlables: "0",
      showvalues: "0",
      showYAxisvalue:"0",
      theme: "fusion"
    },
    data: this.msclicks
  }

  width= 100+'%';
  height = 250;
  // type = "msline";
  type = "mscombidy2d";
  dataFormat = "json";
  dataSource = this.data;

  ssdataSource = this.ssdata;
  msdataSource = this.msdata;

  ClicksdataSource = this.clickdata

  ssClicksdataSource = this.ssclickdata

  msClicksdataSource = this.msclickdata




}
