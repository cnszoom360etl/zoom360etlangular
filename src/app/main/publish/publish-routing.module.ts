import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { PublishComponent } from './publish.component';
import { CreateDashboardComponent } from './create-dashboard/create-dashboard.component';
import { ChannelsComponent } from './channels/channels.component';
import { PublishDashBoardComponent } from './publish-dash-board/publish-dash-board.component';
import { GetStartedComponent } from './publish-dash-board/get-started/get-started.component';
import { PreBuiltComponent } from './publish-dash-board/pre-built/pre-built.component';
import { KpiLibraryComponent } from './publish-dash-board/kpi-library/kpi-library.component';



const routes: Routes = [
  { path: 'publisheddashboard', component:PublishComponent,
  children:[
    {path:'createdashBoard', component:CreateDashboardComponent},
    { path: 'digitalsalesdashboard', component:ChannelsComponent}
  ]
},
{ path: 'createdashboard', component: PublishDashBoardComponent,
children:[
  {path:'getstarted', component:GetStartedComponent},
  {path:'prebuilt', component:PreBuiltComponent},
  {path:'kpilibrary', component:KpiLibraryComponent},
  {path: '', redirectTo:'getstarted', pathMatch:'full'},
]
},

]
@NgModule({
  declarations: [],
  imports: [
     CommonModule,
    RouterModule.forChild(routes),
  ],
  exports:[RouterModule]
})
export class PublishRoutingModule { }
