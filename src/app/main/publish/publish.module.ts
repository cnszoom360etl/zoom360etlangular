import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AgGridModule } from 'ag-grid-angular';
import 'ag-grid-enterprise';
 
import { PublishRoutingModule } from './publish-routing.module';

import { CreateDashboardComponent } from './create-dashboard/create-dashboard.component';
import { PublishComponent } from './publish.component';
// Import FusionCharts library and chart modules
import * as FusionCharts from "fusioncharts";
import * as charts from "fusioncharts/fusioncharts.charts";
import * as FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";
import { FusionChartsModule } from 'angular-fusioncharts';
import { ChannelsComponent } from './channels/channels.component';
import { PublishDashBoardComponent } from './publish-dash-board/publish-dash-board.component';
import { GetStartedComponent } from './publish-dash-board/get-started/get-started.component';
import { PreBuiltComponent } from './publish-dash-board/pre-built/pre-built.component';
import { SharedModule } from 'src/app/shared/shared.module';
// import { KpiLibraryComponent } from './publish-dash-board/kpi-library/kpi-library.component';

// Pass the fusioncharts library and chart modules
FusionChartsModule.fcRoot(FusionCharts, charts, FusionTheme);
@NgModule({
  declarations: [
    PublishComponent,
    CreateDashboardComponent,
    ChannelsComponent,
    PublishDashBoardComponent,
    GetStartedComponent,
    PreBuiltComponent,
    // KpiLibraryComponent
  ],
  imports: [
    SharedModule,
    CommonModule,
    PublishRoutingModule,
    FusionChartsModule,
    AgGridModule.withComponents([]),
  ]
})
export class PublishModule { }
