import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EtlStatusbarComponent } from './etl-statusbar.component';

describe('EtlStatusbarComponent', () => {
  let component: EtlStatusbarComponent;
  let fixture: ComponentFixture<EtlStatusbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EtlStatusbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EtlStatusbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
