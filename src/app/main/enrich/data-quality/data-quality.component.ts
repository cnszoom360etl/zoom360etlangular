import { Component, Injector, OnInit } from '@angular/core';
 
import { DatalabelingList, DataLabelingSaveRecord, DataQualityList } from 'src/app/models/enrich/DataqualityModel';
import { ClientDateTimeService } from 'src/app/services/client-date-time.service';
import { DataQualityService } from 'src/app/services/Enrich/DataQuality';
import { environment } from 'src/environments/environment';
import { AppComponentBase } from 'src/app/services/AppComponentBase';
@Component({
  selector: 'app-data-quality',
  templateUrl: './data-quality.component.html',
  styleUrls: ['./data-quality.component.css']
})
export class DataQualityComponent extends AppComponentBase implements OnInit {
 DataqualityModel:DataQualityList[]=[];
 dataqualitylistrecord:number;
 userId:string;
  workspaceid:string;
  clientId:string;
  clientDate:string;
  clientTime:string;
  clientTimeZone:string;
 selectedData:DatalabelingList[]=[];
 dataLabelingSaveRecord=new DataLabelingSaveRecord;
 _workspaces = [
   {dropdownText:'ID',dropdownValue:'id',selected:false},
   {dropdownText:'First Name',dropdownValue:'first_name',selected:true},
   {dropdownText:'Last Name',dropdownValue:'last_name',selected:false},
   {dropdownText:'Phone',dropdownValue:'phone3',selected:false}
  ];
  public mode: string;
  public filterPlaceholder: string;
public fields:Object = { text:'dropdownText',value:'dropdownValue',selected:'selected'};
 public checkWaterMark: string = 'Select countries';
  constructor( private dataQualityService:DataQualityService,injector : Injector) {
    super(injector)
    this.userId= this.storageService.getItem(environment.storage.userId);
    this.workspaceid=this.storageService.getItem(environment.storage.workspaceId);
    this.clientId=this.storageService.getItem(environment.storage.clientId);
    this.clientDate=this.clientDateTimeService.getCilentDate();
    this.clientTime=this.clientDateTimeService.getCilentTime();
    this.clientTimeZone=this.clientDateTimeService.getClientTimeZone();
   }
  ngOnInit(): void {
    this.mode = 'CheckBox';
   this.GetDataQualityList();
  }

GetDataQualityList(){
  
  var UserId=this.storageService.getItem(environment.storage.userId);
  var Workspace=this.storageService.getItem(environment.storage.workspaceId);
  var ClientId=this.storageService.getItem(environment.storage.clientId);
  var FunctionDisplayName=null;
  var functiongroup=null;
  var functiondetail=null;
  var enable =null;
  
 this.dataQualityService.getdataQualityList(UserId,Workspace,ClientId,
  FunctionDisplayName,functiongroup,functiondetail,enable).subscribe((data: any) => {
      debugger
       this.DataqualityModel=data;
       for(let i=0;i<data.length;i++){
        if(this.DataqualityModel[i].enable=="0")
        {
         this.DataqualityModel[i].enable=JSON.parse("false")
        }
        else{
         this.DataqualityModel[i].enable=JSON.parse("true")
        }
      }
       this.dataqualitylistrecord=this.DataqualityModel.length;  
                      
      
  });  
}


Makelist(args,index){
  var result= this.selectedData.findIndex(x=>x.functionName==this.DataqualityModel[index].functoinName);
  if(args.value.length==0){
    this.selectedData.splice(result,1);
  }
  else{
    if(result==-1){
      this.selectedData.push({
        functionName:this.DataqualityModel[index].functoinName,
        functionParameter:'cols',
        parameterValue: args.value.toString()
     });
    }
    else{
      this.selectedData.splice(result,1);
      this.selectedData.push({
        functionName:this.DataqualityModel[index].functoinName,
        functionParameter:'cols',
        parameterValue: args.value.toString()
     });
    }
  }


}
SaveConfiguration(){
  this.dataLabelingSaveRecord.UserId=this.userId;
  this.dataLabelingSaveRecord.Workspaceid=this.workspaceid;
  this.dataLabelingSaveRecord.ClientId=this.clientId;
  this.dataLabelingSaveRecord.ClientDate=this.clientDate;
  this.dataLabelingSaveRecord.ClientTime=this.clientTime;
  this.dataLabelingSaveRecord.ClientTimeZone=this.clientTimeZone;
  this.dataLabelingSaveRecord.DatalabelingList=this.selectedData;
  this.dataQualityService.SaveConfiguration(this.dataLabelingSaveRecord).subscribe((data:any)=>{
  console.log(data);
  });
}

}
