import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddLookupComponent } from './add-lookup/add-lookup.component';
import { DataQualityComponent } from './data-quality/data-quality.component';
import { LookuPageListComponent } from './looku-page-list/looku-page-list.component';
import { EnrichComponent } from './enrich.component';
import { RouterModule, Routes } from '@angular/router';
import { DynamicEnrichFunctionsComponent } from './dynamic-enrich-functions/dynamic-enrich-functions.component';
import { ValueTableListComponent } from './value-table-list/value-table-list.component';
import { ValueTableDataComponent } from './value-table-data/value-table-data.component';

 
const routes: Routes = [

  { path:'prepare',component: EnrichComponent ,
  children:[
    {path:'dataquality',component:DataQualityComponent},
    {path:'LookupValuesAdd',component:AddLookupComponent},
    {path:'lookupvalues',component:LookuPageListComponent},
   //{path:'LookupValues',component:TransformationComponent}
    {path:'dynamiccomponent',component:DynamicEnrichFunctionsComponent},
    {path:'valuetable',component:ValueTableListComponent},
    {path:'valuetableData',component:ValueTableDataComponent},
   ]
},

]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ],
  exports:[RouterModule]
})
export class EnrichRoutingModule { }
 