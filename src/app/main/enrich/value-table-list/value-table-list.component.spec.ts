import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValueTableListComponent } from './value-table-list.component';

describe('ValueTableListComponent', () => {
  let component: ValueTableListComponent;
  let fixture: ComponentFixture<ValueTableListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValueTableListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValueTableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
