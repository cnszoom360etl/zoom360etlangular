import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ValueTablelListService } from 'src/app/services/value-tablel-list.service';

@Component({
  selector: 'app-value-table-list',
  templateUrl: './value-table-list.component.html',
  styleUrls: ['./value-table-list.component.css']
})
export class ValueTableListComponent implements OnInit {
  displayName_arrow: boolean;
  displayName_toggle: boolean;
  objectName_arrow: boolean;
  objectName_toggle: any;
  userName_arrow: boolean;
  userName_toggle: boolean;
  createdOn_arrow: boolean;
  createdOn_toggle: boolean;
  enable_arrow: boolean;
  enable_toggle: boolean;
  valueTableData: any;
  public _valueTableList: any;
  private _recordLength: any;

  constructor(public ValuetableService:ValueTablelListService,private router: Router) { }

  ngOnInit() {
        this.ValuetableService.getValueTableList();
        this.getlist();
  }
  getlist(){
    this.ValuetableService.getValueTableList().subscribe((data:any) => { 
      debugger
      if(data.length  > 0){
         this.valueTableData = data;
        this._valueTableList = data;
        this._recordLength = this.valueTableData.length;
      }
    });
  }
  getObjectName(value:string){
    debugger
    localStorage.removeItem("objectKeyName");
    localStorage.setItem("objectKeyName",JSON.stringify(value));
    this.router.navigate(['/enrich/prepare/valuetableData']);


  }
  applySort(fieldName){
    if(fieldName=='displayName'){
     this.displayName_arrow=true;
     this.displayName_toggle=!this.displayName_toggle;
     this.ValuetableService._sortToggle=!this.ValuetableService._sortToggle;
     this.ValuetableService.sortAllValueTable(fieldName);
    }
    else if (fieldName=='objectName'){
      this.objectName_arrow=true;
     this.objectName_toggle=!this.objectName_toggle;
     this.ValuetableService._sortToggle=!this.ValuetableService._sortToggle;
     this.ValuetableService.sortAllValueTable(fieldName);
    }
    else if (fieldName=='userName'){
      this.userName_arrow=true;
     this.userName_toggle=!this.userName_toggle;
     this.ValuetableService._sortToggle=!this.ValuetableService._sortToggle;
     this.ValuetableService.sortAllValueTable(fieldName);
    }
    else if (fieldName=='createdOn'){
      this.createdOn_arrow=true;
     this.createdOn_toggle=!this.createdOn_toggle;
     this.ValuetableService._sortToggle=!this.ValuetableService._sortToggle;
     this.ValuetableService.sortAllValueTable(fieldName);
    }
    // else if (fieldName=='enable'){
    //   this.enable_arrow=true;
    //  this.enable_toggle=!this.enable_toggle;
    //  this.ValuetableService._sortToggle=!this.ValuetableService._sortToggle;
    //  this.ValuetableService.sortAllValueTable(fieldName);
    // }
  }
  reloadPage(){
    this.displayName_arrow=false;
    this.objectName_arrow=false;
    this.userName_arrow=false;
    this.createdOn_arrow=false;
    // this.enable_arrow=false;
      this.ValuetableService.getValueTableList();
  //  window.location.reload();
  }
}
