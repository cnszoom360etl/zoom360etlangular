import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EnrichRoutingModule } from './enrich-routing.module';
import { AddLookupComponent } from './add-lookup/add-lookup.component';
import { FormsModule } from '@angular/forms';
import { DataQualityComponent } from './data-quality/data-quality.component';
import { LookuPageListComponent } from './looku-page-list/looku-page-list.component';
import { EnrichComponent } from './enrich.component';
import { DropDownListModule, MultiSelectAllModule, MultiSelectModule } from '@syncfusion/ej2-angular-dropdowns';
import { EJAngular2Module } from 'ej-angular2';
import { ValueTableListComponent } from './value-table-list/value-table-list.component';
import { ValueTableDataComponent } from './value-table-data/value-table-data.component';

import { CheckBoxModule, ButtonModule } from '@syncfusion/ej2-angular-buttons';
import { NumericTextBoxModule } from '@syncfusion/ej2-angular-inputs';
import { DynamicEnrichFunctionsComponent } from './dynamic-enrich-functions/dynamic-enrich-functions.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { DropDownButtonModule } from '@syncfusion/ej2-angular-splitbuttons';
import { ToastModule } from '@syncfusion/ej2-angular-notifications';
@NgModule({
  declarations: [
    AddLookupComponent,
    DataQualityComponent,
    LookuPageListComponent,
    EnrichComponent,
    DynamicEnrichFunctionsComponent,
    ValueTableListComponent,
    ValueTableDataComponent
    
  ],
  imports: [
    CommonModule,
    EnrichRoutingModule,
    FormsModule,
    ToastModule,
    MultiSelectModule,
    EJAngular2Module.forRoot(),
    DropDownListModule,
    MultiSelectAllModule,
    ButtonModule,
    CheckBoxModule,
    DragDropModule,
    DropDownButtonModule,
  ]
})
export class EnrichModule { }
