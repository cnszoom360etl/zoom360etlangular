import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValueTableDataComponent } from './value-table-data.component';

describe('ValueTableDataComponent', () => {
  let component: ValueTableDataComponent;
  let fixture: ComponentFixture<ValueTableDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValueTableDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValueTableDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
