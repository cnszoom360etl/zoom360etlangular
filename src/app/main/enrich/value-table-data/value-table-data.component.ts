import { Component, OnInit } from '@angular/core';
import { saveValueTableData, valueTableDataModel } from 'src/app/models/enrich/valueTableDataModel';
import { ValueTableDataService } from 'src/app/services/value-table-data.service';

@Component({
  selector: 'app-value-table-data',
  templateUrl: './value-table-data.component.html',
  styleUrls: ['./value-table-data.component.css']
})
export class ValueTableDataComponent implements OnInit {
  userId:string='admin';
  workspaceId:string='1';
  clientId:string=''+1002;
  valueTable:string="LOOKUP_PROSPECT_TYPE";
  valueTableDataModel=new valueTableDataModel();
  Status_message: string;
  constructor(public valueTableDataService:ValueTableDataService) { }

  ngOnInit() {
    
    let objectName=JSON.parse(localStorage.getItem("objectKeyName"));
    this.valueTableDataService.getValueTableData(this.userId,this.workspaceId,this.clientId,objectName);
  }
  // SaveConfig(data:saveValueTableData){
  //   debugger
  //   // this.valueTableDataService._valueTableData;
  //   //  this.valueTableDataService.Status_message='';
  //   this.valueTableDataService.SaveTabledata(data);
  // }




  SaveConfig(UserData:valueTableDataModel){
    {
    debugger

    var mylist= this.valueTableDataService._valueTableData;
     this.valueTableDataService.Status_message='';
     this.valueTableDataService.SaveTabledata(mylist);
  }

}
}


