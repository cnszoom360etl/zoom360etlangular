import { Component, Injector, OnInit } from '@angular/core';
import { Router, Routes } from '@angular/router';
import { parse } from 'querystring';
import { Lookvaluemodel } from 'src/app/models/enrich/LookupvalueListModel';
import { LookupvalueListservice } from 'src/app/services/Enrich/LookValuesList';
import { environment } from 'src/environments/environment';
import { AppComponentBase } from 'src/app/services/AppComponentBase';

@Component({
  selector: 'app-looku-page-list',
  templateUrl: './looku-page-list.component.html',
  styleUrls: ['./looku-page-list.component.css']
})
export class LookuPageListComponent extends AppComponentBase implements OnInit {

  constructor( private lookupvalueListservice:LookupvalueListservice,private router: Router,injector : Injector) { super(injector)}
lookuplist:Lookvaluemodel[]=[];
lookuplistrecord:number=0;
  ngOnInit(): void {
    this.GetLookFieldList();
  }

GetLookFieldList(){
  var USER_ID =this.storageService.getItem(environment.storage.userId);
  var WORKSPACE_ID=this.storageService.getItem(environment.storage.workspaceId);
  var CLIENT_ID= this.storageService.getItem(environment.storage.clientId);
  var WORKSPACE_NAME=null ;
  var lookupid=null;
  var LOOKUP_DISPLAY_NAME =null;
  var ACCOUNT_DISPLAY_NAME=null;
  var TABLE_NAME=null;
   var ENABLED = null;
   var VISIBILTY =null; 
this.lookupvalueListservice.getLookupList(USER_ID,WORKSPACE_ID,CLIENT_ID,WORKSPACE_NAME,LOOKUP_DISPLAY_NAME,
  ACCOUNT_DISPLAY_NAME,TABLE_NAME,ENABLED,VISIBILTY,lookupid).subscribe((data:any)=>{
    debugger
  this.lookuplist=data;
   for(let i=0;i<data.length;i++){
     
     if(this.lookuplist[i].enableconnection=="0"){
       this.lookuplist[i].enableconnection=JSON.parse("false");
     }
     else{
      this.lookuplist[i].enableconnection=JSON.parse("true");
     }

     if(this.lookuplist[i].visibiltyconnection=="0"){
      this.lookuplist[i].visibiltyconnection=JSON.parse("false");
    }
    else{
     this.lookuplist[i].visibiltyconnection=JSON.parse("true");
    }
   }
  this.lookuplistrecord=this.lookuplist.length;

  })


}
GetvalueforEditmode(LookupId:string)
{
  JSON.stringify(localStorage.setItem("LookupId",LookupId));
  this.router.navigate(['/enrich/prepare/LookupValuesAdd']);
   
}
 
Addlookupvalue(){
  debugger
   localStorage.removeItem("LookupId");
  this.router.navigate(['/enrich/prepare/LookupValuesAdd']);
}
}
