import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DataGovernanceModel } from 'src/app/Models/mainmenu.model';
import { DataGovernanceService } from 'src/app/services/administration/data-governance.service';

@Component({
  selector: 'app-data-governance',
  templateUrl: './data-governance.component.html',
  styleUrls: ['./data-governance.component.css']
})
export class DataGovernanceComponent implements OnInit {
  DataGovernance : FormGroup;
  constructor(private fb: FormBuilder,public dataGovernanceService:DataGovernanceService) { }
  ActiveSourceLocation:any[]=[];
  Workspaces:any[]=[];
  ActiveDestinationLocation :any[]=[];
  PassiveDestinationLocation:any[]=[];
  ngOnInit(): void {
    this.createForm();
    this.getDropDowns();
  }
  createForm() {
    this.DataGovernance = this.fb.group({
      schemaMode: ['1'],
      childWorkspaceInheritance: ['1'],
      workspaceShareData: ['0'],
      outOffAppWebShareData: ['0'],
      outOffApiShareData: ['1'],
      rawDataStagging: ['1'],
      staggingStorageLocationType: ['1'],
      staggingRetentionDays: [0],
      activeSourceLocation: ['Active Source Location'],
      destinationWorkspace: ['Destination Workspace'],
      activeDestinationLocation:['Active Destination Location'],
      passiveDestinationLocation:['Passive Destination Location'],
      dataCollectionType:['0'],
      overrideDataSnapshot:[false],
      dataStorage:['0'],
      dataDestination:['0'],
      overrideDataStorage:[false],
      destinationRetentionDays:[0]
    });
  }
  getDropDowns(){
    this.dataGovernanceService.getActiveSourceLoactionList().subscribe((data: any) => {
      this.ActiveSourceLocation = data;       
  });
  this.dataGovernanceService.getWorkspacesList().subscribe((data: any) => {
    this.Workspaces = data;       
  });
  this.dataGovernanceService.getActiveDestinationLocationList().subscribe((data: any) => {
    this.ActiveDestinationLocation = data;       
});
this.dataGovernanceService.getPassiveDestinationLocationList().subscribe((data: any) => {
  this.PassiveDestinationLocation = data;       
});
  }
    SaveConfig(Data:DataGovernanceModel){
      console.log(Data);
      this.dataGovernanceService.saveDataGovernance(Data);
    }
}
