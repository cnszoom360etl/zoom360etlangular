import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DataExportTemplete } from 'src/app/Models/mainmenu.model';
import { ExportTemplateSetupService } from 'src/app/Services/administration/export-template-setup.service';

@Component({
  selector: 'app-export-template',
  templateUrl: './export-template.component.html',
  styleUrls: ['./export-template.component.css']
})
export class ExportTemplateComponent implements OnInit {
  CurrencySetup: FormGroup;
  constructor(private fb: FormBuilder,public exportTemplateService:ExportTemplateSetupService) { }

  ngOnInit(): void {
  }

  // createForm() {
  //   this.CurrencySetup = this.fb.group({
  //     exportXLS: ['XLS', Validators.required ],
  //     exportPPT: ['PPT', Validators.required],
  //     exportPDF: ['PDF', Validators.required ],
  //   });
  // }
  onXLSChange(event) {
    let reader = new FileReader();
    if(event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      console.log(event.target.files[0].name);
      this.exportTemplateService.exportXLS=event.target.files[0].name;
      reader.readAsDataURL(file);
      // reader.onload = () => {
      //   // this.formGroup.patchValue({
      //   //   file: reader.result
      //   // });
        
      //   // need to run CD since file load runs outside of zone
      //   //this.cd.markForCheck();
      // };
    }
  }
  onPPTChange(event) {
    let reader = new FileReader();
    if(event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      console.log(event.target.files[0].name);
      this.exportTemplateService.exportPPT=event.target.files[0].name;
      reader.readAsDataURL(file);
      // reader.onload = () => {
      //   // this.formGroup.patchValue({
      //   //   file: reader.result
      //   // });
        
      //   // need to run CD since file load runs outside of zone
      //   //this.cd.markForCheck();
      // };
    }
  }
  onPDFChange(event) {
    let reader = new FileReader();
    if(event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      console.log(event.target.files[0].name);
      this.exportTemplateService.exportPDF=event.target.files[0].name;
      reader.readAsDataURL(file);
      // reader.onload = () => {
      //   // this.formGroup.patchValue({
      //   //   file: reader.result
      //   // });
        
      //   // need to run CD since file load runs outside of zone
      //   //this.cd.markForCheck();
      // };
    }
  }
  SaveConfig(){
    this.exportTemplateService.saveTempleteSetup();

  }
}
