import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { QuotaSettingsModel } from 'src/app/Models/mainmenu.model';
import { QuotaSettingsService } from 'src/app/Services/administration/quota-settings.service';

@Component({
  selector: 'app-quota-settings',
  templateUrl: './quota-settings.component.html',
  styleUrls: ['./quota-settings.component.css']
})
export class QuotaSettingsComponent implements OnInit {
  QuotaSettings: FormGroup;
  constructor(private fb: FormBuilder,public quotaService:QuotaSettingsService) { }

  ngOnInit(): void {
    this.createForm();
  }
  createForm() {
    this.QuotaSettings = this.fb.group({
      quotaLimit: [null,Validators.required],
      quotaType: ['0'],
      quotaUsageCycle: ['0'],
      quotaStartDate: ['',Validators.required],
      quotaEndDate: ['',Validators.required],
    });
}
  SaveConfig(data:QuotaSettingsModel){
    this.quotaService.saveQuotaSetting(data);

  }

}
