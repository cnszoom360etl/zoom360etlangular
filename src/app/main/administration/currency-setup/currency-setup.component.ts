import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CurrencySetupModel } from 'src/app/Models/mainmenu.model';
import { CurrencySetupService } from 'src/app/services/administration/currency-setup.service';


@Component({
  selector: 'app-currency-setup',
  templateUrl: './currency-setup.component.html',
  styleUrls: ['./currency-setup.component.css']
})
export class CurrencySetupComponent implements OnInit {
  currentImageName:string;
  CurrencySetup: FormGroup;
  SetCurrency : boolean=false;
  
  constructor(private fb: FormBuilder,public currencyService:CurrencySetupService) { }

  ngOnInit(): void {
    this.currencyService.getCurrencyTypeList();
    this.createForm();
    this.getDafaults();
  }
  getDafaults(){
    this.currencyService.getCurrencySetupSetting().subscribe((data:CurrencySetupModel)=>{
     this.CurrencySetup.patchValue({ 
      currencyType : data.currencyType,
      currenceyTypeSign: data.currenceyTypeSign,  
      currencyImage: data.currencyImage,
      currencyCollectedData: data.currencyCollectedData,
      currencyPrepareData: data.currencyPrepareData,
      currencyPresentingData: data.currencyPresentingData,
      currencyConversion: data.currencyConversion,
      exchangeRateAndDataConversion: data.exchangeRateAndDataConversion,
      currencyReportHeaders: data.currencyReportHeaders,
      currencyVisulization: data.currencyVisulization,
      currencyValue: data.currencyValue,
      currencyApplyAndEnforce : data.currencyApplyAndEnforce
    
    });
    if(data.currencyType=='second'){
      this.SetCurrency = true;
    }
    else{
    this.SetCurrency= false;
    }
    });
  }
  onFileChange(event) {
    
    let reader = new FileReader();
    if(event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      console.log(event.target.files[0].name);
      this.currencyService.currencyImage=event.target.files[0].name;
      reader.readAsDataURL(file);
      // reader.onload = () => {
      //   // this.formGroup.patchValue({
      //   //   file: reader.result
      //   // });
        
      //   // need to run CD since file load runs outside of zone
      //   //this.cd.markForCheck();
      // };
    }
  }
  
  createForm() {
    this.CurrencySetup = this.fb.group({
      currencyType: ['first', Validators.required ],
      currenceyTypeSign: ['', Validators.required],
      currencyImage: ['', Validators.required ],
      currencyCollectedData: [false, Validators.required ],
      currencyPrepareData: [false, Validators.required ],
      currencyPresentingData: [false, Validators.required ],
      currencyConversion: [false, Validators.required ],
      exchangeRateAndDataConversion: [false, Validators.required ],
      currencyReportHeaders: ['Show sign', Validators.required ],
      currencyVisulization: ['Show sign', Validators.required ],
      currencyValue: ['Show sign', Validators.required ],
      currencyApplyAndEnforce : [null, Validators.required]
    });
  }
  showCurrencyType(value){
  
    this.SetCurrency= value;
    if(value==false){
      this.CurrencySetup.patchValue({currenceyTypeSign:''});
    }
    // if(value==false){
    //   this.CurrencySetup.patchValue({currenceyTypeSign:'₾ - Georgian lari'});
    //   this.CurrencySetup.get('currenceyTypeSign').disable();
    // }
    // else{
    //   this.CurrencySetup.get('currenceyTypeSign').enable();
    // }
  }
  SaveConfig(UserData:CurrencySetupModel){
    this.currencyService.Status_message='';
    this.currencyService.saveWorkSpaceSetup(UserData);
  }

}
