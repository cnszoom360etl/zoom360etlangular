import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ChildWorkspacesModel } from 'src/app/Models/mainmenu.model';
import { ChildWorkspacesSetupService } from 'src/app/services/administration/child-workspaces-setup.service';

@Component({
  selector: 'app-child-workspaces-setup',
  templateUrl: './child-workspaces-setup.component.html',
  styleUrls: ['./child-workspaces-setup.component.css']
})
export class ChildWorkspacesSetupComponent implements OnInit {
  ChildspaceSetup: FormGroup;
  constructor(private fb: FormBuilder,public childWorkspacesService: ChildWorkspacesSetupService) { }

  ngOnInit(): void {
    this.createForm();
  }

  createForm() {
    this.ChildspaceSetup = this.fb.group({
      childWorkspace: ['1', Validators.required ],
      childSelectedOption: [false, Validators.required],
      childChange: [false, Validators.required ],
      childOverrideSelectedOption: [false, Validators.required ],
      
    });
  }
  SaveConfig(data:ChildWorkspacesModel){
    
    console.log(data);
    this.childWorkspacesService.saveWorkSpaceSetup(data);

  }
}
