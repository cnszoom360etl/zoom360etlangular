import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { CalenderSetupModel } from 'src/app/Models/mainmenu.model';
import { CalenderSetupService } from 'src/app/services/administration/calender-setup.service';

@Component({
  selector: 'app-calender-setup',
  templateUrl: './calender-setup.component.html',
  styleUrls: ['./calender-setup.component.css']
})
export class CalenderSetupComponent implements OnInit {
  CalenderSetup: FormGroup;
  bussinessYearDateToggle:boolean=false;
  constructor(private fb: FormBuilder,public calenderService:CalenderSetupService) { }

  ngOnInit(): void {
    this.createForm();
    this.calenderService.getWeekStarDayList();
  }
  createForm() {
    this.CalenderSetup = this.fb.group({
      CalenderSetup: ['1'],
      bussinessYearDate: [''],
      finacialYearDate: [''],
      reportingYearDate: [''],
      weekStartDay: ['Monday'],
      annualHolidayCalender: [''],
      annualCampaignCalender: [''],
      notifyCampaignsCalender: [false],
      milestoneAnnualHolidayCalender: [''],
      notifyMilestoneCalender: [false],
      calenderApplyAndEnforce:['1']
    });
  }
  setbussinessYearDate(value){

  this.bussinessYearDateToggle= value;
  if(value==false){
    this.CalenderSetup.patchValue({bussinessYearDate:''});
  }
}
  SaveConfig(data:CalenderSetupModel){
    debugger
    console.log(data);
    this.calenderService.saveWorkSpaceSetup(data);
  }
}
