import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, } from '@angular/forms';
import { WorkspaceSetup, WorkspaceSettingModel } from 'src/app/Models/mainmenu.model';
import { WorkSpaceSetupService } from 'src/app/Services/administration/work-space-setup.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-workspace-setup',
  templateUrl: './workspace-setup.component.html',
  styleUrls: ['./workspace-setup.component.css']
})
export class WorkspaceSetupComponent implements OnInit {
  WorkspaceSetting: WorkspaceSettingModel=new WorkspaceSettingModel;

  constructor(private fb: FormBuilder,public WorkSpaceService:WorkSpaceSetupService,private router:Router) {
   // this.setdefaultvalues();
   }
   WorkspaceSetup: FormGroup;
  ngOnInit(): void {
    this.createForm();
    this.WorkSpaceService.getWorkSpaceParentList();
   
  this.setdefaultvalues();
  }
  createForm() {
    this.WorkspaceSetup = this.fb.group({
      WorkSpace_Name: [null, [Validators.required] ],
      WorkSpaceDisplayName: [null, [Validators.required] ],
      WorkSpaceParentName: ['Select Parent Workspace', [Validators.required] ],
      ChildWorkSpaceRule: ['All child workspaces'],
      Exclude_Child_WorkSpace: [null],
    });
  }
  setdefaultvalues(){
    this.WorkSpaceService.getWorkSpaceSettings().subscribe((data:WorkspaceSettingModel)=>{
      debugger
      this.WorkspaceSetting.workSpace_Name=data.workSpace_Name;
      this.WorkspaceSetup.patchValue({
        WorkSpace_Name: data.workSpace_Name,
        WorkSpaceDisplayName: data.workSpaceDisplayName,
        WorkSpaceParentName: data.workSpaceParentName,
        ChildWorkSpaceRule: data.childWorkSpaceRule,
        Exclude_Child_WorkSpace: data.exclude_Child_WorkSpace
       });
    });
   
   console.log('Form Value', this.WorkspaceSetup.value)
  }
  SaveConfig(UserData:WorkspaceSetup) { 
    this.WorkSpaceService.saveWorkSpaceSetup(UserData);
    this.WorkSpaceService.getGridWorkSpaceList();
    window.location.reload();
    // var listPropertyNames = Object.keys(UserData);
    // var listValues = Object.values(UserData);
    // console.log(this.selectValue);
//     var labels = document.getElementsByTagName('label');
// for (var i = 0; i < labels.length; i++) {
//     if (labels[i].htmlFor != '') {
           
                       
//     }
// } 
    // debugger;  
    //  var user=UserData;

   }  
}
