import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TimeZoneSetupModel } from 'src/app/Models/mainmenu.model';
import { TimeZoneSetupService } from 'src/app/Services/Administration/time-zone-setup.service';

@Component({
  selector: 'app-time-zone-setup',
  templateUrl: './time-zone-setup.component.html',
  styleUrls: ['./time-zone-setup.component.css']
})
export class TimeZoneSetupComponent implements OnInit {
  TimezoneSetup: FormGroup;
  constructor(private fb: FormBuilder,public timeZoneService:TimeZoneSetupService) { }
  DATE_FORMAT_TYPE_toggle:boolean=false;
  dataFormatTypeToggle:boolean=false;
  timeZoneToggle:boolean=false;
  dateFormatReportsToggle:boolean=false;
  dateFormatVisulizationToggle:boolean=false;
  timeFormatReportsToggle:boolean=false;
  timeFormatVisualizationToggle:boolean=false;
  ngOnInit(): void {
    this.timeZoneService.getDateFormatList();
    this.timeZoneService.getTimeFormatList();
    this.createForm();
  }
  createForm() {
    this.TimezoneSetup = this.fb.group({
      dataFormatType: ['1', Validators.required ],
      dateFormat: ['', Validators.required ],
      clockImage: ['', Validators.required ],
      dateCollectedData: [false, Validators.required ],
      datePreparingData: [false, Validators.required ],
      datePresentingData: [false, Validators.required ],
      dateConversion: [false, Validators.required ],
      dateConversionValue: [false, Validators.required ],
      timeZone: ['1', Validators.required ],
      timeZoneType: ['', Validators.required ],
      dateFormatReports: ['1', Validators.required ],
      reportsDate : ['', Validators.required],
      dateFormatVisulization: ['1', Validators.required ],
      visulizationDate: ['', Validators.required ],
      timeFormatReports: ['1', Validators.required ],
      reportTime : ['', Validators.required],
      timeFormatVisualization: ['1', Validators.required ],
      visualizationTime: ['', Validators.required ],
      applyAndEnforceDatetime : ['1', Validators.required]
    });
}
SaveConfig(TimeZoneData: TimeZoneSetupModel){

  console.log(TimeZoneData);
  this.timeZoneService.saveWorkSpaceSetup(TimeZoneData);
}

onFileChange(event) {
  let reader = new FileReader();
  if(event.target.files && event.target.files.length) {
    const [file] = event.target.files;
    console.log(event.target.files[0].name);
    this.timeZoneService.timeZoneImage=event.target.files[0].name;
    reader.readAsDataURL(file);
    // reader.onload = () => {
    //   // this.formGroup.patchValue({
    //   //   file: reader.result
    //   // });
      
    //   // need to run CD since file load runs outside of zone
    //   //this.cd.markForCheck();
    // };
  }
}
//1
setdateFormatVisible(value){
  this.dataFormatTypeToggle= value;
  if(value==false){
    this.TimezoneSetup.patchValue({dateFormat:''});
  }
}
//2
setTimeZoneVisible(value){
  this.timeZoneToggle= value;
  if(value==false){
    this.TimezoneSetup.patchValue({timeZoneType:''});
  }
}
//3
setdateFormatReports(value){
  this.dateFormatReportsToggle= value;
  if(value==false){
    this.TimezoneSetup.patchValue({reportsDate:''});
  }
}
//4
setdateFormatVisulization(value){
  this.dateFormatVisulizationToggle= value;
  if(value==false){
    this.TimezoneSetup.patchValue({visulizationDate:''});
  }
}
//5
setTimeFormatReports(value){
  this.timeFormatReportsToggle= value;
  if(value==false){
    this.TimezoneSetup.patchValue({reportTime:''});
  }
}
//6
setTimeFormatVisualization(value){
  this.timeFormatVisualizationToggle= value;
  if(value==false){
    this.TimezoneSetup.patchValue({visualizationTime:''});
  }
}

}
