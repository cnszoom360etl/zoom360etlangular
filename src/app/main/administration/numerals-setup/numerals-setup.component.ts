import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NumeralsSetupModel } from 'src/app/Models/mainmenu.model';
import { NumeralsSetupService } from 'src/app/Services/administration/numerals-setup.service';

@Component({
  selector: 'app-numerals-setup',
  templateUrl: './numerals-setup.component.html',
  styleUrls: ['./numerals-setup.component.css']
})
export class NumeralsSetupComponent implements OnInit {
  numeralsSetup: FormGroup;
  numberingSystemFormatToggle:boolean=false;
  numberValueConversionToggle:boolean=false;
  selectiveRoundOffPlaceToggle:boolean=false;
  signFormat: any;
  constructor(private fb: FormBuilder,public numeralsService:NumeralsSetupService) { }

  ngOnInit(): void {
    this.createForm();
  }
  createForm() {
    this.numeralsSetup = this.fb.group({
      numberingSystemFormat: ['1', Validators.required ],
      numberSignType: ['1', Validators.required],
      signFormat: ['Default sign: 100 / -100', Validators.required ],
      PositiveNumbeColorCode: ['#57C1AC', Validators.required ],
      negitiveNumberColorCode: ['#73ADD9', Validators.required ],
      numberConversion: ['1', Validators.required ],
      numberValueConversion: ['1', Validators.required ],
      numberValue: ['Use Actual number value', Validators.required ],
      selectiveDecimalPlaces: [1, Validators.required ],
      fullDecimalPlaces: [false, Validators.required ],
      roundOffNumbers: ['1', Validators.required ],
      selectiveRoundOffPlace : [1, Validators.required],
      numberApplyAndEnforce : ['1', Validators.required]
    });
  }
  showSignFormat(value){
    this.numberingSystemFormatToggle= value;
    if(value==false){
      this.numeralsSetup.patchValue({signFormat:''});
    }
    else{
      this.numeralsSetup.patchValue({signFormat:'Default sign: 100 / -100'});
    }
  }
  shownumberValueConversion(value){
    this.numberValueConversionToggle= value;
    if(value==false){
      this.numeralsSetup.patchValue({numberValueConversion:''});
    }
    else{
      this.numeralsSetup.patchValue({numberValueConversion:'1'});
    }
  }
  showselectiveRoundOffPlace(value){
    this.selectiveRoundOffPlaceToggle= value;
    if(value==false){
      this.numeralsSetup.patchValue({selectiveRoundOffPlace:''});
    }
    else{
      this.numeralsSetup.patchValue({numberValueConversion:'1'});
    }
  }
  SaveConfig(numeralsData:NumeralsSetupModel){
    console.log(numeralsData);
    this.numeralsService.saveNumeralsSetup(numeralsData);

  }
}
