import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AdministrationComponent } from './administration.component';
import { WorkspaceSetupComponent } from './workspace-setup/workspace-setup.component';
import { CurrencySetupComponent } from './currency-setup/currency-setup.component';
import { TimeZoneSetupComponent } from './time-zone-setup/time-zone-setup.component';
import { NumeralsSetupComponent } from './numerals-setup/numerals-setup.component';
import { CalenderSetupComponent } from './calender-setup/calender-setup.component';
import { DataGovernanceComponent } from './data-governance/data-governance.component';
import { QuotaSettingsComponent } from './quota-settings/quota-settings.component';
import { DisplaySettingsComponent } from './display-settings/display-settings.component';
import { ExportTemplateComponent } from './export-template/export-template.component';
import { ChildWorkspacesSetupComponent } from './child-workspaces-setup/child-workspaces-setup.component';
import { UsageHistoryComponent } from './usage-history/usage-history.component';
import { UsersLogComponent } from './users-log/users-log.component';
import { ChangesLogComponent } from './changes-log/changes-log.component';
import { ConfigurationTemplateComponent } from './configuration-template/configuration-template.component';

const routes: Routes = [
  
  //{ path: '', component:ApplicationStartupComponent},
   
    //Administration
    { path: 'settings', component: AdministrationComponent,
      children:[
        { path: 'workspacesetup', component: WorkspaceSetupComponent},
        { path: 'currencysetup', component: CurrencySetupComponent},
        { path: 'timezonesetup', component: TimeZoneSetupComponent},
        { path: 'numralssetup', component: NumeralsSetupComponent},
        { path: 'calendersetup', component: CalenderSetupComponent},
        { path:'datagovernance', component:DataGovernanceComponent},
        { path:'quotasettings', component:QuotaSettingsComponent},
        { path:'displaysettings', component:DisplaySettingsComponent},
        { path:'template', component:ExportTemplateComponent},
        { path:'childWorkspacesetup', component:ChildWorkspacesSetupComponent},
        { path:'usagehistory', component:UsageHistoryComponent},
        { path:'userlog', component:UsersLogComponent},
        { path:'changelog', component:ChangesLogComponent},
        { path:'configurationmapping',component:ConfigurationTemplateComponent},
        { path: '', redirectTo:'workspacesetup', pathMatch:'full'}
      ]  
    },

  {path: '**', redirectTo:'', pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministrationRoutingModule { }
