import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DisplaySettingsModel } from 'src/app/Models/mainmenu.model';
import { DisplaySettingsService } from 'src/app/Services/administration/display-settings.service';

@Component({
  selector: 'app-display-settings',
  templateUrl: './display-settings.component.html',
  styleUrls: ['./display-settings.component.css']
})
export class DisplaySettingsComponent implements OnInit {
  DisplaySettings: FormGroup;
  workspaceThemeColor:any[]=[];
  constructor(private fb: FormBuilder,public displaySettingService:DisplaySettingsService) { }

  ngOnInit(): void {
    this.createForm();
    this.getThemeColorList();
   
  }
  createForm() {
    this.DisplaySettings = this.fb.group({
      workspaceDisplayMode: ['1'],
      workspaceLogo: ['image'],
      logoBackgroundColor: ['#57C1AC'],
      workspaceTheme: ['Select Theme',Validators.required],
      colorPallete: ['#73ADD9'],
    });
}
getThemeColorList()
{debugger
  this.displaySettingService.getThemeColorList().subscribe((data: any)=>{
    debugger
    this.workspaceThemeColor= data;
  })
}
SaveConfig(data:DisplaySettingsModel){
  this.displaySettingService.saveDisplaySettings(data);

}
}
