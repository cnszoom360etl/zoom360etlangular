import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
 
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { HttpClientModule } from '@angular/common/http';
import { AgGridModule } from 'ag-grid-angular';
import 'ag-grid-enterprise';
 
import { CommonModule, DatePipe } from '@angular/common';
 
import {
  GoogleLoginProvider,
  FacebookLoginProvider
} from 'angularx-social-login';
 
import { AuthurizationResponseComponent } from '../authurization-response/authurization-response.component';
import { ApplicationStartupComponent } from '../application-startup/application-startup.component';
import { AppRoutingModule } from '../app-routing.module';
import { AuthGuard } from '../guards/auth.guard';
import { MainComponent } from './main.component';
import { SharedModule } from '../shared/shared.module';
import { MainRoutingModule } from './main-routing.module';
import { NavbarComponent } from './navbar/navbar.component';
import { EtlStatusbarComponent } from './etl-statusbar/etl-statusbar.component';
import { ProgressBarAllModule } from '@syncfusion/ej2-angular-progressbar';
 
@NgModule({
  declarations: [
   MainComponent,
   NavbarComponent,
   EtlStatusbarComponent
    
  ],
  imports: [
     SharedModule,
     CommonModule,
     ProgressBarAllModule,
     MainRoutingModule,
     AgGridModule.withComponents([]),
     HttpClientModule
     
   
    // SocialLoginModule
   
  ],
  exports: [TabsModule],
  providers: [AuthGuard,DatePipe,
    //,{
   // provide: AuthServiceConfig,
   // useFactory: getAuthServiceConfigs
  //}
],
   
})
export class MainModule { }
