import { Component, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { ClientLoginDetailModel } from '../models/clientLoginDetails.model';
import { dropdownModel } from '../models/common/dropdownmodel';
import { MianMenuModel } from '../Models/mainmenu.model';
import { AppComponentBase } from '../services/AppComponentBase';
import { AppMenuService } from '../Services/common/app-menu.service';
import { FiltersService } from '../Services/common/filters.service';
import { UserLogService } from '../services/common/user-log.service';
import { AddNewConnectoinWizardStyle } from '../services/extract/AddNewConnectionWizardStyle';
import { StorageService } from '../services/storage.service';
 

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent extends AppComponentBase  {
  _mainMenu : MianMenuModel[] = [];
  applicationMode:boolean=true;
  presentationMode:boolean=false;
  mode_id: number=1;
  mainmenuID:number=2;
  path:String;
  title = 'DataAnalysisRaptore';
  companyName: string;
  userName:string='';
  userProfileImage:string;
  clientName:string;
  clientLogo:string;
  clientLoginDetails:ClientLoginDetailModel={} as ClientLoginDetailModel;
  userInfo:any;
  _modelist:dropdownModel[]=[];
  dropdownname:string='Mode List';
  userId:string='';
  workSpaceId:string='';
  client_id:string='';
  constructor(private MenuService: AppMenuService,private _Filters:FiltersService,
    public _AddNewConnectoinWizardStyleservice:AddNewConnectoinWizardStyle,private userLogService: UserLogService,
    inject:Injector,
    private router: Router){
       super(inject);
      debugger
       this.userInfo = this.storageService.getItem(environment.storage.userData);
       this.userName =  this.userInfo.customUserInfo.loginUserName;
       this.companyName = this.userInfo.customUserInfo.userDeptInfo;
       this.clientName = this.userInfo.customUserInfo.clientName;
       this.clientLogo = this.userInfo.customUserInfo.clientLogo;
       this.userProfileImage=this.storageService.getItem(environment.storage.userProfileImage);
      // this.mode_id=this.storageService.getItem(environment.storage.ModeId);
       this.applicationMode= this.storageService.getItem(environment.storage.appMode);
       this.presentationMode= this.storageService.getItem(environment.storage.presentMode);
      

      
   // this.loginDataService.getLoginDetail();
   // this.getMenu(this.mode_id);
  }
  ngOnInit(){
    let path = window.location.href;
    let subMenuName = path.substring(path.lastIndexOf("/") + 1);
    this.getMenu(this.mode_id);
  }
  getMenu(mode_id){
    this.userId = this.clientDetailService.getuserID();
    this.workSpaceId=this.clientDetailService.getWorkspaceID();
    this.client_id = this.clientDetailService.getClientID();
      this.MenuService
        .getMenuItems(mode_id,this.userId,this.workSpaceId,this.client_id)
        .subscribe((data: MianMenuModel[]) => {
          debugger
           this._mainMenu = data;
            console.log('data menu',data);
            console.log('this menu',this._mainMenu);
      });
    }
  logout(){
    this.storageService.removeItem(environment.storage.userData);
    localStorage.clear();
    this.router.navigate(["/auth/login"]);
    this.companyName='';
    this.UserLoggedout();
  }
  UserLoggedout() {
  

    debugger
    this.clientLoginDetails.ClientDate= this.clientDateTimeService.getCilentDate();
    this.clientLoginDetails.ClientTime= this.clientDateTimeService.getCilentTime();
    this.clientLoginDetails.ClientTimeZone= this.clientDateTimeService.getClientTimeZone();
    this.clientLoginDetails.location=this.clientDeviceInfoService.getLocation();
    this.clientLoginDetails.ipAddress=this.clientDeviceInfoService.getIPAddress();
    this.clientLoginDetails.macAddress=this.clientDeviceInfoService.getMacAddress();
    this.clientLoginDetails.deviceType=this.clientDeviceInfoService.getDeviceType();
    this.clientLoginDetails.loginStatus='Successfully Logout';
    this.clientLoginDetails.connectionType=this.clientDeviceInfoService.getConnectionType();
    this.clientLoginDetails.Email=this.clientDetailService.getuserID();
    this.clientLoginDetails.workspaceId=this.clientDetailService.getWorkspaceID();
    this.clientLoginDetails.clientID=this.clientDetailService.getClientID();
    this.clientLoginDetails.userActivity='0';
    this.userLogService.UserLoggedin(this.clientLoginDetails).subscribe(
      (data:any) => {
   
        console.log(data);
      },
      (err:any) => {
  
        //this.testToast.toast[2].content='err.statusText';
        console.log(err.statusText);
      });
  }
  changeMode(value){
     if(value==1){
   this.applicationMode=true;
   this.presentationMode=false;
   this.mode_id=1;
   this.getMenu(this.mode_id)

  }
  else if(value==2){
    this.applicationMode=false;
    this.presentationMode=true;
    this.mode_id=2;
    this.getMenu(this.mode_id)
  }
//test usman 
}
// setMainMenuId(id){
//   this.MenuService.setMainMenuId(id);
//   this.storageService.setItem(environment.storage.mainMenu,id);
//   }
}
