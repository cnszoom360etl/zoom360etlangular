import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
 
import { AgGridModule } from 'ag-grid-angular';
import 'ag-grid-enterprise';

// Import FusionCharts library and chart modules
import * as FusionCharts from "fusioncharts";
import * as charts from "fusioncharts/fusioncharts.charts";
import * as FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";
import { FusionChartsModule } from 'angular-fusioncharts';
import { KPIAchivementsComponent } from './KPIAchivements.component';
import { KPIAchivementsRoutingModule } from './KPIAchivements-routing.module';
import { KpiLibraryComponent } from '../publish/publish-dash-board/kpi-library/kpi-library.component';
import { SharedModule } from 'src/app/shared/shared.module';

// Pass the fusioncharts library and chart modules
FusionChartsModule.fcRoot(FusionCharts, charts, FusionTheme);
@NgModule({
  declarations: [
    KPIAchivementsComponent,
    KpiLibraryComponent
  ],
  imports: [
    SharedModule,
    CommonModule,
    KPIAchivementsRoutingModule,
    FusionChartsModule,
    AgGridModule.withComponents([]),
  ]
})
export class KPIAchivementsModule { }
