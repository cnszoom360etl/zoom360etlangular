import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { KPIAchivementsComponent } from './KPIAchivements.component';
import { KpiLibraryComponent } from '../publish/publish-dash-board/kpi-library/kpi-library.component';

const routes: Routes = [
  { path: 'achivements', component:KPIAchivementsComponent,
    children:[
    { path: 'graph', component:KpiLibraryComponent}
    ]
  }
]
// const routes: Routes = [];
@NgModule({
  declarations: [],
  imports: [
     CommonModule,
    RouterModule.forChild(routes),
  ],
  exports:[RouterModule]
})
export class KPIAchivementsRoutingModule { }
